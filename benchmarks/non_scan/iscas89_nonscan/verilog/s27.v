// Benchmark "s27.blif" written by ABC on Mon Apr  8 18:03:25 2019

module \s27.blif  ( clock, 
    G0, G1, G2, G3,
    G17  );
  input  clock;
  input  G0, G1, G2, G3;
  output G17;
  reg G5, G6, G7;
  wire G14, G8, G12, G15, G16, G9, n12, n17, n22;
  assign G17 = ~n17;
  assign n12 = ~G14 & ~n17;
  assign n17 = ~G5 & ~G9;
  assign n22 = ~G2 & ~G12;
  assign G14 = ~G0;
  assign G8 = G14 & G6;
  assign G12 = ~G1 & ~G7;
  assign G15 = G12 | G8;
  assign G16 = G3 | G8;
  assign G9 = ~G16 | ~G15;
  always @ (posedge clock) begin
    G5 <= n12;
    G6 <= n17;
    G7 <= n22;
  end
endmodule


