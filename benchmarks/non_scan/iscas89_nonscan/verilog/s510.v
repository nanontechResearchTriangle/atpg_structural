// Benchmark "s510.blif" written by ABC on Mon Apr  8 18:07:11 2019

module \s510.blif  ( clock, 
    john, cnt13, cnt21, cnt284, pcnt6, cnt261, cnt44, pcnt12, pcnt17,
    cnt591, cnt45, cnt567, pcnt27, cnt283, cnt272, cnt10, cnt511, pcnt241,
    cnt509,
    csm, pclr, pc, cclr, vsync, cblank, csync  );
  input  clock;
  input  john, cnt13, cnt21, cnt284, pcnt6, cnt261, cnt44, pcnt12,
    pcnt17, cnt591, cnt45, cnt567, pcnt27, cnt283, cnt272, cnt10, cnt511,
    pcnt241, cnt509;
  output csm, pclr, pc, cclr, vsync, cblank, csync;
  reg st_5, st_4, st_3, st_2, st_1, st_0;
  wire I68, I67, I78, I73, I61, I60, I59, I58, I57, I56, I69, I70, I555,
    I554, I590, I591, I595, I594, I547, I546, I666, I667, I474, I475, I799,
    I798, I494, I495, I466, I467, I463, I462, I131, I130, I566, I567, I482,
    I483, I531, I530, I487, I486, I606, I607, I779, I778, I346, I347, I205,
    I204, I217, I216, I936_2, I946_1, I946_2, I936_1, I1089_1, I1044_1,
    I578, I943_1, I675, I1102_2, I671, I1059_1, I551, I1071_1, I1106_1,
    I663, I1123_1, I498, I967_1, I570, I1055_1, I535, I598, I1062_1, I795,
    I618, I1120_1, I95, I603, I1116_1, I950_2, I455, I950_1, I1102_1, I587,
    I104, I954_2, I543, I490, I1081_1, I1106_2, I940_1, I539, I1077_1,
    I694, I698, I988_1, I787, I1085_1, I954_1, I1081_2, I230, I1116_2,
    I232, I1065_1, I234, I1113_1, I1055_2, I1085_2, I1038_1, I985_1,
    I914_1, I1068_1, I933_1, I958_1, I642, I958_2, I924_2, I478, I903_2,
    I1092_1, I458, I917_1, I921_1, I627, I909_1, I962_2, I506, I1095_1,
    I209, I1099_1, I917_2, I559, I982_1, I1074_1, I1095_2, I970_1, I207,
    I900_1, I742, I903_1, I962_1, I975_1, I747, I978_1, I928_1, I1110_1,
    I731, I924_1, I658, I814, I574, I511, I638, I739, I774, I583, I390,
    I834, I563, I274, I810, I782, I870, I298, I710, I714, I326, I837, I615,
    I270, I838, I872, I266, I877, I213, I278, I282, I823, I855, I867, I841,
    I861, I884, I889, I827, I881, I899, I895, I821, I863, I831, I874, I887,
    I259, I371, n54, n59, n64, n69, n74, n79;
  assign csm = ~I555 & ~I798;
  assign pclr = ~I917_1 | ~I917_2;
  assign pc = ~I921_1 | ~I837;
  assign cclr = ~I486 | ~I877 | ~I546 | ~I390;
  assign vsync = ~I867 | ~I914_1 | ~I855;
  assign cblank = ~I928_1 | ~I841;
  assign csync = ~I881 | ~I924_1 | ~I924_2;
  assign n54 = ~I900_1 | ~I821;
  assign n59 = ~I823 | ~I903_1 | ~I903_2;
  assign n64 = ~I278 | ~I274 | ~I270 | ~I266;
  assign n69 = ~I282 | ~I889 | ~I827 | ~I298;
  assign n74 = ~I895 | ~I909_1 | ~I899;
  assign n79 = ~I887 | ~I778 | ~I782;
  assign I68 = ~cnt44;
  assign I67 = ~cnt261;
  assign I78 = ~cnt511;
  assign I73 = ~cnt567;
  assign I61 = ~st_0;
  assign I60 = ~st_1;
  assign I59 = ~st_2;
  assign I58 = ~st_3;
  assign I57 = ~st_4;
  assign I56 = ~st_5;
  assign I69 = ~pcnt12;
  assign I70 = ~pcnt17;
  assign I555 = ~st_0 & ~st_1;
  assign I554 = ~I555;
  assign I590 = ~st_1 | ~st_2;
  assign I591 = ~I590;
  assign I595 = ~st_0 & ~st_2;
  assign I594 = ~I595;
  assign I547 = ~I61 & ~st_2;
  assign I546 = ~I547;
  assign I666 = ~I61 | ~st_3;
  assign I667 = ~I666;
  assign I474 = ~I56 | ~I57;
  assign I475 = ~I474;
  assign I799 = ~I59 & ~I56 & ~I58;
  assign I798 = ~I799;
  assign I494 = ~I57 | ~I547;
  assign I495 = ~I494;
  assign I466 = ~st_3 | ~I535;
  assign I467 = ~I466;
  assign I463 = ~I458 & ~I594;
  assign I462 = ~I463;
  assign I131 = ~I936_1 & ~I936_2;
  assign I130 = ~I131;
  assign I566 = ~I663 | ~st_2;
  assign I567 = ~I566;
  assign I482 = ~I58 | ~I551;
  assign I483 = ~I482;
  assign I531 = ~I574 & ~I59;
  assign I530 = ~I531;
  assign I487 = ~st_4 & ~I498;
  assign I486 = ~I487;
  assign I606 = ~I95 | ~I57;
  assign I607 = ~I606;
  assign I779 = ~I638 & ~I95 & ~st_4;
  assign I778 = ~I779;
  assign I346 = ~I985_1 | ~I463;
  assign I347 = ~I346;
  assign I205 = ~I563 & ~I940_1;
  assign I204 = ~I205;
  assign I217 = ~I954_1 & ~I954_2;
  assign I216 = ~I217;
  assign I936_2 = cnt272 & st_2;
  assign I946_1 = cnt10 & st_5;
  assign I946_2 = john & st_4;
  assign I936_1 = cnt591 & I59;
  assign I1089_1 = I59 & I555;
  assign I1044_1 = I70 & cnt284;
  assign I578 = ~I61 | ~st_1;
  assign I943_1 = I578 & st_3;
  assign I675 = ~I61 & ~st_1;
  assign I1102_2 = I56 & I675;
  assign I671 = ~I458 & ~I59;
  assign I1059_1 = st_5 & I671;
  assign I551 = ~I61 & ~I57;
  assign I1071_1 = I551 & I671;
  assign I1106_1 = I60 & I551;
  assign I663 = ~st_1 & ~I58;
  assign I1123_1 = I551 & I663;
  assign I498 = ~I511 | ~I587;
  assign I967_1 = I498 & I57;
  assign I570 = ~I458 | ~I56;
  assign I1055_1 = st_2 & I570 & st_0;
  assign I535 = ~I590 & ~st_0;
  assign I598 = ~cnt13 | ~I56;
  assign I1062_1 = I535 & I598;
  assign I795 = ~I578 & ~st_3 & ~st_2;
  assign I618 = ~I69 | ~cnt44;
  assign I1120_1 = I795 & I618;
  assign I95 = ~I587 & ~I591;
  assign I603 = ~I61 & ~I56;
  assign I1116_1 = I95 & I603;
  assign I950_2 = I463 & cnt283;
  assign I455 = ~I554 & ~I658;
  assign I950_1 = I455 & cnt45;
  assign I1102_1 = st_5 & I455;
  assign I587 = ~st_1 & ~st_2;
  assign I104 = ~I933_1 | ~I56;
  assign I954_2 = I104 & cnt45 & I587;
  assign I543 = ~I742 & ~I590;
  assign I490 = ~cnt284 | ~pcnt17;
  assign I1081_1 = I58 & I543 & I490;
  assign I1106_2 = I57 & I543;
  assign I940_1 = I495 & I60;
  assign I539 = ~I546 & ~I60;
  assign I1077_1 = I104 & I539;
  assign I694 = ~I795 | ~I57;
  assign I698 = ~I563 | ~I59;
  assign I988_1 = I694 & I698;
  assign I787 = ~I574 & ~I554 & ~st_5;
  assign I1085_1 = I787 & I130;
  assign I954_1 = I567 & st_5 & cnt509;
  assign I1081_2 = st_2 & I483;
  assign I230 = ~I958_1 | ~I958_2;
  assign I1116_2 = I61 & I230;
  assign I232 = ~I810 | ~I962_1 | ~I962_2;
  assign I1065_1 = I475 & I232;
  assign I234 = ~I213 | ~I814 | ~I710 | ~I714;
  assign I1113_1 = st_4 & I234;
  assign I1055_2 = I58 & I204;
  assign I1085_2 = I61 & I216;
  assign I1038_1 = cnt21 | st_0;
  assign I985_1 = pcnt27 | I73;
  assign I914_1 = I60 | I61;
  assign I1068_1 = st_4 | I590;
  assign I933_1 = I57 | I58;
  assign I958_1 = I57 | I59;
  assign I642 = ~I739 | ~st_2;
  assign I958_2 = cnt284 | I642;
  assign I924_2 = I474 | I666;
  assign I478 = ~I547 | ~I739;
  assign I903_2 = I58 | I478;
  assign I1092_1 = st_4 | I478;
  assign I458 = ~st_3 | ~st_1;
  assign I917_1 = st_5 | I458 | I494;
  assign I921_1 = I494 | I570;
  assign I627 = ~pcnt241 & ~I78;
  assign I909_1 = I466 | I627;
  assign I962_2 = I466 | I78;
  assign I506 = ~I535 | ~I58;
  assign I1095_1 = cnt13 | I506;
  assign I209 = ~I946_1 & ~I946_2;
  assign I1099_1 = I506 | I209;
  assign I917_2 = I482 | I590;
  assign I559 = ~I658 & ~I56;
  assign I982_1 = I559 | I487;
  assign I1074_1 = I475 | I546;
  assign I1095_2 = I475 | I578;
  assign I970_1 = I495 | I603;
  assign I207 = ~I595 & ~I943_1;
  assign I900_1 = I56 | I207;
  assign I742 = ~I56 | ~st_0;
  assign I903_1 = I606 | I742;
  assign I962_1 = I462 | I73;
  assign I975_1 = I531 | I483;
  assign I747 = ~I638 & ~I1044_1;
  assign I978_1 = I483 | I747;
  assign I928_1 = st_0 | I530;
  assign I1110_1 = I61 | I530;
  assign I731 = ~I583 & ~I607;
  assign I924_1 = st_0 | I731;
  assign I658 = ~st_2 | ~I58;
  assign I814 = ~I595 | ~I58 | ~cnt21;
  assign I574 = ~st_3 | ~I57;
  assign I511 = ~st_3 & ~st_5;
  assign I638 = ~I511 | ~st_0;
  assign I739 = ~st_5 & ~st_1;
  assign I774 = ~I458 | ~st_5 | ~I547;
  assign I583 = ~I511 & ~I60;
  assign I390 = ~st_0 | ~I583;
  assign I834 = ~I1068_1 | ~I642;
  assign I563 = ~I578 & ~I56;
  assign I274 = ~I56 | ~I667;
  assign I810 = ~I455 | ~pcnt6 | ~cnt284;
  assign I782 = ~I675 | ~I67 | ~I559;
  assign I870 = ~I1092_1 | ~I566;
  assign I298 = ~I539 | ~I574;
  assign I710 = ~I467 | ~cnt10;
  assign I714 = ~I1038_1 | ~I567;
  assign I326 = ~I982_1 | ~I61;
  assign I837 = ~I487 & ~I1071_1;
  assign I615 = ~I475 & ~st_2;
  assign I270 = ~st_3 | ~I615;
  assign I838 = ~I1074_1 | ~I530;
  assign I872 = ~I774 | ~I1095_1 | ~I1095_2;
  assign I266 = ~I970_1 | ~st_1;
  assign I877 = ~I551 & ~I1102_1 & ~I1102_2;
  assign I213 = ~I950_1 & ~I950_2;
  assign I278 = ~I975_1 | ~I60;
  assign I282 = ~I978_1 | ~st_1;
  assign I823 = ~I259 & ~I1059_1;
  assign I855 = ~I615 & ~st_3;
  assign I867 = ~I834 & ~I1089_1;
  assign I841 = ~I799 & ~I1077_1;
  assign I861 = ~I1081_1 & ~I1081_2;
  assign I884 = ~I326 | ~I1110_1 | ~I861;
  assign I889 = ~I870 & ~I1116_1 & ~I1116_2;
  assign I827 = ~I531 & ~I1062_1;
  assign I881 = ~I838 & ~I1106_1 & ~I1106_2;
  assign I899 = ~I1123_1 & ~I872 & ~I347;
  assign I895 = ~I884 & ~I1120_1;
  assign I821 = ~I1055_1 & ~I1055_2;
  assign I863 = ~I1085_1 & ~I1085_2;
  assign I831 = ~I371 & ~I1065_1;
  assign I874 = ~I831 | ~I1099_1 | ~I863;
  assign I887 = ~I874 & ~I1113_1;
  assign I259 = ~st_0 & ~I967_1;
  assign I371 = ~I68 & ~I988_1;
  always @ (posedge clock) begin
    st_5 <= n54;
    st_4 <= n59;
    st_3 <= n64;
    st_2 <= n69;
    st_1 <= n74;
    st_0 <= n79;
  end
endmodule


