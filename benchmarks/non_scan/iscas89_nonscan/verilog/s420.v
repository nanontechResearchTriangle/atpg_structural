// Benchmark "s420.1.blif" written by ABC on Mon Apr  8 18:06:27 2019

module \s420.1.blif  ( clock, 
    \P.0 , \C.16 , \C.15 , \C.14 , \C.13 , \C.12 , \C.11 , \C.10 , \C.9 ,
    \C.8 , \C.7 , \C.6 , \C.5 , \C.4 , \C.3 , \C.2 , \C.1 , \C.0 ,
    Z  );
  input  clock;
  input  \P.0 , \C.16 , \C.15 , \C.14 , \C.13 , \C.12 , \C.11 , \C.10 ,
    \C.9 , \C.8 , \C.7 , \C.6 , \C.5 , \C.4 , \C.3 , \C.2 , \C.1 , \C.0 ;
  output Z;
  reg \X.4 , \X.3 , \X.2 , \X.1 , \X.8 , \X.7 , \X.6 , \X.5 , \X.12 ,
    \X.11 , \X.10 , \X.9 , \X.16 , \X.15 , \X.14 , \X.13 ;
  wire I69, \I73.1 , \I73.2 , I66, \I7.1 , \I7.2 , \I88.1 , \I88.2 , I48,
    I49, I50, I68, I167, \I171.1 , \I171.2 , I164, \I105.1 , \I105.2 ,
    \I186.1 , \I1.2 , \I186.2 , I146, I147, I148, I166, I265, \I269.1 ,
    \I269.2 , I262, \I203.1 , \I203.2 , \I284.1 , \I1.3 , \I284.2 , I244,
    I245, I246, I264, I359, \I301.1 , \I301.2 , \I378.1 , \I1.4 , \I378.2 ,
    I344, I345, I358, I357, I360, I410, I411, I412, I413, I414, I422, I423,
    I438, I439, I440, I441, I442, I450, I451, I466, I467, I468, I469, I470,
    I478, I479, I494, I495, I496, I497, I498, I505, I506, \P.2 , I546,
    \P.3 , I547, I550, I551, \P.6 , I570, \P.7 , I571, I574, I575, \P.10 ,
    I594, \P.11 , I595, I598, I599, \P.14 , I618, \P.15 , I619, I622, I623,
    \I73.3 , \I73.4 , \I7.3 , \I7.4 , \I88.3 , \I88.4 , \I171.3 , \I171.4 ,
    \I105.3 , \I105.4 , \I186.3 , \I186.4 , \I269.3 , \I269.4 , \I203.3 ,
    \I203.4 , \I284.3 , \I284.4 , \I301.3 , \I301.4 , \I378.3 , \I378.4 ,
    \I387.1 , \I2.1 , \I2.2 , \I2.3 , \I407.1 , \I407.2 , \I408.2 ,
    \I407.3 , \I408.3 , \I403.2 , \P.5 , \I404.2 , \I405.2 , \I406.2 ,
    \P.8 , \I403.3 , \P.9 , \I404.3 , \I405.3 , \I406.3 , \P.12 , \I403.4 ,
    \P.13 , \I404.4 , \I405.4 , \I406.4 , \P.16 , \P.1 , \I559.1 ,
    \I559.2 , \I583.1 , \P.4 , \I583.2 , \I607.1 , \I607.2 , \I631.1 ,
    \I631.2 , \I534.5 , \I70.1 , I64, \I95.1 , \I168.1 , I162, \I193.1 ,
    \I266.1 , I260, \I291.1 , I361, \I363.1 , \I366.1 , \I384.1 , \I555.1 ,
    \I555.2 , \I579.1 , \I579.2 , \I603.1 , \I603.2 , \I627.1 , \I627.2 ,
    \I533.1 , \I533.2 , \I534.2 , \I533.3 , \I534.3 , \I533.4 , \I534.4 ,
    I62, I160, I258, I355, I420, I448, I476, I503, I554, I578, I602, I626,
    n40, n45, n50, n55, n60, n65, n70, n75, n80, n85, n90, n95, n100, n105,
    n110, n115;
  assign Z = \I534.4  | \I534.5 ;
  assign n40 = ~\I70.1  | ~I62;
  assign n45 = \I73.3  | \I73.4 ;
  assign n50 = ~\I7.3  & ~\I7.4 ;
  assign n55 = \I88.3  | \I88.4 ;
  assign n60 = ~\I168.1  | ~I160;
  assign n65 = \I171.3  | \I171.4 ;
  assign n70 = ~\I105.3  & ~\I105.4 ;
  assign n75 = \I186.3  | \I186.4 ;
  assign n80 = ~\I266.1  | ~I258;
  assign n85 = \I269.3  | \I269.4 ;
  assign n90 = ~\I203.3  & ~\I203.4 ;
  assign n95 = \I284.3  | \I284.4 ;
  assign n100 = ~\I363.1  | ~I355;
  assign n105 = ~\I366.1  | ~I357;
  assign n110 = ~\I301.3  & ~\I301.4 ;
  assign n115 = \I378.3  | \I378.4 ;
  assign I69 = ~I64 & ~I48;
  assign \I73.1  = ~I69;
  assign \I73.2  = ~\X.3 ;
  assign I66 = ~\X.1  | ~\P.0 ;
  assign \I7.1  = ~I66;
  assign \I7.2  = ~\X.2 ;
  assign \I88.1  = ~\X.1 ;
  assign \I88.2  = ~\P.0 ;
  assign I48 = ~\P.0 ;
  assign I49 = ~\X.4 ;
  assign I50 = ~\X.3 ;
  assign I68 = ~I69;
  assign I167 = ~I162 & ~I146;
  assign \I171.1  = ~I167;
  assign \I171.2  = ~\X.7 ;
  assign I164 = ~\X.5  | ~\I1.2 ;
  assign \I105.1  = ~I164;
  assign \I105.2  = ~\X.6 ;
  assign \I186.1  = ~\X.5 ;
  assign \I1.2  = \I2.1  & \P.0 ;
  assign \I186.2  = ~\I1.2 ;
  assign I146 = ~\I1.2 ;
  assign I147 = ~\X.8 ;
  assign I148 = ~\X.7 ;
  assign I166 = ~I167;
  assign I265 = ~I260 & ~I244;
  assign \I269.1  = ~I265;
  assign \I269.2  = ~\X.11 ;
  assign I262 = ~\X.9  | ~\I1.3 ;
  assign \I203.1  = ~I262;
  assign \I203.2  = ~\X.10 ;
  assign \I284.1  = ~\X.9 ;
  assign \I1.3  = \I2.2  & \I1.2 ;
  assign \I284.2  = ~\I1.3 ;
  assign I244 = ~\I1.3 ;
  assign I245 = ~\X.12 ;
  assign I246 = ~\X.11 ;
  assign I264 = ~I265;
  assign I359 = ~\X.13  | ~\I1.4 ;
  assign \I301.1  = ~I359;
  assign \I301.2  = ~\X.14 ;
  assign \I378.1  = ~\X.13 ;
  assign \I1.4  = \I2.3  & \I1.3 ;
  assign \I378.2  = ~\I1.4 ;
  assign I344 = ~\X.15 ;
  assign I345 = ~\X.14 ;
  assign I358 = ~I344 & ~\I387.1 ;
  assign I357 = ~I358;
  assign I360 = ~I359;
  assign I410 = ~\P.0 ;
  assign I411 = ~\X.1 ;
  assign I412 = ~\X.2 ;
  assign I413 = ~\X.3 ;
  assign I414 = ~\X.4 ;
  assign I422 = ~I411 | ~\P.0 ;
  assign I423 = ~I422;
  assign I438 = ~\P.0 ;
  assign I439 = ~\X.5 ;
  assign I440 = ~\X.6 ;
  assign I441 = ~\X.7 ;
  assign I442 = ~\X.8 ;
  assign I450 = ~I439 | ~\P.0 ;
  assign I451 = ~I450;
  assign I466 = ~\P.0 ;
  assign I467 = ~\X.9 ;
  assign I468 = ~\X.10 ;
  assign I469 = ~\X.11 ;
  assign I470 = ~\X.12 ;
  assign I478 = ~I467 | ~\P.0 ;
  assign I479 = ~I478;
  assign I494 = ~\P.0 ;
  assign I495 = ~\X.13 ;
  assign I496 = ~\X.14 ;
  assign I497 = ~\X.15 ;
  assign I498 = ~\X.16 ;
  assign I505 = ~I495 | ~\P.0 ;
  assign I506 = ~I505;
  assign \P.2  = ~I412 & ~I422;
  assign I546 = ~\P.2 ;
  assign \P.3  = ~I413 & ~I420;
  assign I547 = ~\P.3 ;
  assign I550 = ~\C.2 ;
  assign I551 = ~\C.3 ;
  assign \P.6  = \I407.1  & \I404.2 ;
  assign I570 = ~\P.6 ;
  assign \P.7  = \I407.1  & \I405.2 ;
  assign I571 = ~\P.7 ;
  assign I574 = ~\C.6 ;
  assign I575 = ~\C.7 ;
  assign \P.10  = \I408.2  & \I404.3 ;
  assign I594 = ~\P.10 ;
  assign \P.11  = \I408.2  & \I405.3 ;
  assign I595 = ~\P.11 ;
  assign I598 = ~\C.10 ;
  assign I599 = ~\C.11 ;
  assign \P.14  = \I408.3  & \I404.4 ;
  assign I618 = ~\P.14 ;
  assign \P.15  = \I408.3  & \I405.4 ;
  assign I619 = ~\P.15 ;
  assign I622 = ~\C.14 ;
  assign I623 = ~\C.15 ;
  assign \I73.3  = I69 & \I73.2 ;
  assign \I73.4  = \X.3  & \I73.1 ;
  assign \I7.3  = I66 & \I7.2 ;
  assign \I7.4  = \X.2  & \I7.1 ;
  assign \I88.3  = \X.1  & \I88.2 ;
  assign \I88.4  = \P.0  & \I88.1 ;
  assign \I171.3  = I167 & \I171.2 ;
  assign \I171.4  = \X.7  & \I171.1 ;
  assign \I105.3  = I164 & \I105.2 ;
  assign \I105.4  = \X.6  & \I105.1 ;
  assign \I186.3  = \X.5  & \I186.2 ;
  assign \I186.4  = \I1.2  & \I186.1 ;
  assign \I269.3  = I265 & \I269.2 ;
  assign \I269.4  = \X.11  & \I269.1 ;
  assign \I203.3  = I262 & \I203.2 ;
  assign \I203.4  = \X.10  & \I203.1 ;
  assign \I284.3  = \X.9  & \I284.2 ;
  assign \I284.4  = \I1.3  & \I284.1 ;
  assign \I301.3  = I359 & \I301.2 ;
  assign \I301.4  = \X.14  & \I301.1 ;
  assign \I378.3  = \X.13  & \I378.2 ;
  assign \I378.4  = \I1.4  & \I378.1 ;
  assign \I387.1  = I360 & \X.14 ;
  assign \I2.1  = ~I50 & ~I64 & ~I49;
  assign \I2.2  = ~I148 & ~I162 & ~I147;
  assign \I2.3  = ~I246 & ~I260 & ~I245;
  assign \I407.1  = ~\X.1  & ~\X.3  & ~\X.4  & ~\X.2 ;
  assign \I407.2  = ~\X.5  & ~\X.7  & ~\X.8  & ~\X.6 ;
  assign \I408.2  = \I407.1  & \I407.2 ;
  assign \I407.3  = ~\X.9  & ~\X.11  & ~\X.12  & ~\X.10 ;
  assign \I408.3  = \I408.2  & \I407.3 ;
  assign \I403.2  = ~I438 & ~I439;
  assign \P.5  = \I407.1  & \I403.2 ;
  assign \I404.2  = ~I440 & ~I450;
  assign \I405.2  = ~I441 & ~I448;
  assign \I406.2  = ~I442 & ~\X.7  & ~I448;
  assign \P.8  = \I407.1  & \I406.2 ;
  assign \I403.3  = ~I466 & ~I467;
  assign \P.9  = \I408.2  & \I403.3 ;
  assign \I404.3  = ~I468 & ~I478;
  assign \I405.3  = ~I469 & ~I476;
  assign \I406.3  = ~I470 & ~\X.11  & ~I476;
  assign \P.12  = \I408.2  & \I406.3 ;
  assign \I403.4  = ~I494 & ~I495;
  assign \P.13  = \I408.3  & \I403.4 ;
  assign \I404.4  = ~I496 & ~I505;
  assign \I405.4  = ~I497 & ~I503;
  assign \I406.4  = ~I498 & ~\X.15  & ~I503;
  assign \P.16  = \I408.3  & \I406.4 ;
  assign \P.1  = ~I410 & ~I411;
  assign \I559.1  = \P.1  & \C.1 ;
  assign \I559.2  = \P.0  & \C.0 ;
  assign \I583.1  = \P.5  & \C.5 ;
  assign \P.4  = ~I414 & ~\X.3  & ~I420;
  assign \I583.2  = \P.4  & \C.4 ;
  assign \I607.1  = \P.9  & \C.9 ;
  assign \I607.2  = \P.8  & \C.8 ;
  assign \I631.1  = \P.13  & \C.13 ;
  assign \I631.2  = \P.12  & \C.12 ;
  assign \I534.5  = \P.16  & \C.16 ;
  assign \I70.1  = I50 | I68 | \X.4 ;
  assign I64 = ~\X.1  | ~\X.2 ;
  assign \I95.1  = I48 | I64 | I50;
  assign \I168.1  = I148 | I166 | \X.8 ;
  assign I162 = ~\X.5  | ~\X.6 ;
  assign \I193.1  = I146 | I162 | I148;
  assign \I266.1  = I246 | I264 | \X.12 ;
  assign I260 = ~\X.9  | ~\X.10 ;
  assign \I291.1  = I244 | I260 | I246;
  assign I361 = ~I360 | ~\X.14 ;
  assign \I363.1  = I344 | I361 | \X.16 ;
  assign \I366.1  = I361 | \X.15 ;
  assign \I384.1  = I344 | I359 | I345;
  assign \I555.1  = I547 | I551;
  assign \I555.2  = I546 | I550;
  assign \I579.1  = I571 | I575;
  assign \I579.2  = I570 | I574;
  assign \I603.1  = I595 | I599;
  assign \I603.2  = I594 | I598;
  assign \I627.1  = I619 | I623;
  assign \I627.2  = I618 | I622;
  assign \I533.1  = ~I554 | ~\I555.1  | ~\I555.2 ;
  assign \I533.2  = ~I578 | ~\I579.1  | ~\I579.2 ;
  assign \I534.2  = \I533.1  | \I533.2 ;
  assign \I533.3  = ~I602 | ~\I603.1  | ~\I603.2 ;
  assign \I534.3  = \I534.2  | \I533.3 ;
  assign \I533.4  = ~I626 | ~\I627.1  | ~\I627.2 ;
  assign \I534.4  = \I534.3  | \I533.4 ;
  assign I62 = ~\I95.1  | ~\X.4 ;
  assign I160 = ~\I193.1  | ~\X.8 ;
  assign I258 = ~\I291.1  | ~\X.12 ;
  assign I355 = ~\I384.1  | ~\X.16 ;
  assign I420 = ~I423 | ~I412;
  assign I448 = ~I451 | ~I440;
  assign I476 = ~I479 | ~I468;
  assign I503 = ~I506 | ~I496;
  assign I554 = ~\I559.1  & ~\I559.2 ;
  assign I578 = ~\I583.1  & ~\I583.2 ;
  assign I602 = ~\I607.1  & ~\I607.2 ;
  assign I626 = ~\I631.1  & ~\I631.2 ;
  always @ (posedge clock) begin
    \X.4  <= n40;
    \X.3  <= n45;
    \X.2  <= n50;
    \X.1  <= n55;
    \X.8  <= n60;
    \X.7  <= n65;
    \X.6  <= n70;
    \X.5  <= n75;
    \X.12  <= n80;
    \X.11  <= n85;
    \X.10  <= n90;
    \X.9  <= n95;
    \X.16  <= n100;
    \X.15  <= n105;
    \X.14  <= n110;
    \X.13  <= n115;
  end
endmodule


