// Benchmark "s444.blif" written by ABC on Mon Apr  8 18:06:40 2019

module \s444.blif  ( clock, 
    G0, G1, G2,
    G118, G167, G107, G119, G168, G108  );
  input  clock;
  input  G0, G1, G2;
  output G118, G167, G107, G119, G168, G108;
  reg G11, G12, G13, G14, G15, G16, G17, G18, G19, G20, G21, G22, G23, G24,
    G25, G26, G27, G28, G29, G30, G31;
  wire I372, I382, I318, G34, I180, G35, G77, G135, G36, G78, G144, G32, G74,
    G142, I392, G55, G102, G136, G156, G56, G143, G161, I321, G53, I324,
    G76, G150, I336, G152, G160, G106, G43, I182, G99, G139, G153, G157,
    G103, G40, G38, G57, G60, G97, G79, G44, G42, G48, G46, G162, I105,
    G166, G52, G50, G82, G61, G59, G65, G63, G69, G67, G73, G71, G83, G81,
    G87, G85, G91, G89, G96, G94, G121, G122, G124, G125, G126, G127, G158,
    G159, G154, G104, G105, G100, G117, G115, G165, G163, G116, G164, G141,
    G137, G138, G140, G133, G134, G145, G146, G147, G129, G131, I181, I190,
    G47, I200, G51, I210, G128, G120, G132, G123, G151, I191, I192, I201,
    I202, G130, G149, I211, I212, G148, I225, G64, I235, G68, I245, G72,
    I255, I226, I227, I236, I237, I246, I247, I256, I257, I271, G86, I281,
    G90, I291, G95, I302, I272, I273, I282, I283, I292, I293, I303, I304,
    G33, G54, G75, G98, G93, n20, n25, n30, n35, n40, n45, n50, n55, n60,
    n65, n70, n75, n80, n85, n90, n95, n100, n105, n110, n115, n120;
  assign G118 = ~I336;
  assign G167 = ~G29;
  assign G107 = ~I321;
  assign G119 = ~G28;
  assign G168 = ~I392;
  assign G108 = ~I324;
  assign n20 = ~G152 & ~G98 & ~G38;
  assign n25 = ~G152 & ~G98 & ~G42;
  assign n30 = ~G152 & ~G98 & ~G46;
  assign n35 = ~G152 & ~G98 & ~G50;
  assign n40 = ~G152 & ~G97 & ~G59;
  assign n45 = ~G152 & ~G97 & ~G63;
  assign n50 = ~G152 & ~G97 & ~G67;
  assign n55 = ~G152 & ~G97 & ~G71;
  assign n60 = ~G152 & ~G93 & ~G81;
  assign n65 = ~G152 & ~G93 & ~G85;
  assign n70 = ~G152 & ~G93 & ~G89;
  assign n75 = ~G152 & ~G93 & ~G94;
  assign n80 = G100 & G99;
  assign n85 = ~I105;
  assign n90 = ~G122 & ~G123;
  assign n95 = ~G127 & ~G126 & ~G124 & ~G125;
  assign n100 = ~G139 | ~G140 | ~G141;
  assign n105 = ~G115 & ~G116;
  assign n110 = ~G163 & ~G164;
  assign n115 = ~G150 & ~G151;
  assign n120 = G154 & G153;
  assign I372 = ~G0;
  assign I382 = ~G1;
  assign I318 = ~G2;
  assign G34 = ~G11;
  assign I180 = ~G11;
  assign G35 = ~G12;
  assign G77 = ~G20;
  assign G135 = ~G20;
  assign G36 = ~G13;
  assign G78 = ~G21;
  assign G144 = ~G21;
  assign G32 = ~G14;
  assign G74 = ~G22;
  assign G142 = ~G22;
  assign I392 = ~G30;
  assign G55 = ~G15;
  assign G102 = ~G23;
  assign G136 = ~G23;
  assign G156 = ~G31;
  assign G56 = ~G16;
  assign G143 = ~G24;
  assign G161 = ~G17;
  assign I321 = ~G25;
  assign G53 = ~G18;
  assign I324 = ~G26;
  assign G76 = ~G19;
  assign G150 = ~G19;
  assign I336 = ~G27;
  assign G152 = ~I372;
  assign G160 = ~I382;
  assign G106 = ~I318;
  assign G43 = ~G34;
  assign I182 = ~I180;
  assign G99 = ~G152;
  assign G139 = ~G152;
  assign G153 = ~G152;
  assign G157 = ~G160;
  assign G103 = ~G106;
  assign G40 = ~I181 | ~I182;
  assign G38 = ~G40;
  assign G57 = ~G31 & ~G98;
  assign G60 = ~G57;
  assign G97 = ~G54 & ~G53 & ~G57;
  assign G79 = ~G97;
  assign G44 = ~I191 | ~I192;
  assign G42 = ~G44;
  assign G48 = ~I201 | ~I202;
  assign G46 = ~G48;
  assign G162 = ~G120 | ~G149;
  assign I105 = ~G162;
  assign G166 = ~G162;
  assign G52 = ~I211 | ~I212;
  assign G50 = ~G52;
  assign G82 = ~G79;
  assign G61 = ~I226 | ~I227;
  assign G59 = ~G61;
  assign G65 = ~I236 | ~I237;
  assign G63 = ~G65;
  assign G69 = ~I246 | ~I247;
  assign G67 = ~G69;
  assign G73 = ~I256 | ~I257;
  assign G71 = ~G73;
  assign G83 = ~I272 | ~I273;
  assign G81 = ~G83;
  assign G87 = ~I282 | ~I283;
  assign G85 = ~G87;
  assign G91 = ~I292 | ~I293;
  assign G89 = ~G91;
  assign G96 = ~I303 | ~I304;
  assign G94 = ~G96;
  assign G121 = ~G19 | ~G135 | ~G142 | ~G136;
  assign G122 = G24 & G121;
  assign G124 = G150 & G139 & G22;
  assign G125 = G19 & G139 & G20;
  assign G126 = G139 & G21;
  assign G127 = G139 & G24;
  assign G158 = G31 | G160;
  assign G159 = G156 | G157;
  assign G154 = G158 & G159;
  assign G104 = G23 | G106;
  assign G105 = G102 | G103;
  assign G100 = G104 & G105;
  assign G117 = ~G147 | ~G145 | ~G146;
  assign G115 = G162 & G161 & G117;
  assign G165 = ~G148 | ~G149;
  assign G163 = G162 & G161 & G165;
  assign G116 = G117 & G166;
  assign G164 = G165 & G166;
  assign G141 = G21 | G24 | G22;
  assign G137 = G19 | G136 | G20;
  assign G138 = G136 | G142;
  assign G140 = G24 | G21 | G20 | G150;
  assign G133 = G152 | G136 | G22 | G144;
  assign G134 = G21 | G152 | G142;
  assign G145 = G152 | G142 | G20 | G19;
  assign G146 = G152 | G143;
  assign G147 = G152 | G144;
  assign G129 = ~G19 | ~G135;
  assign G131 = G144 | G22 | G23 | G129;
  assign I181 = ~G11 | ~I180;
  assign I190 = ~G12 | ~G43;
  assign G47 = ~G34 & ~G35;
  assign I200 = ~G13 | ~G47;
  assign G51 = ~G36 & ~G34 & ~G35;
  assign I210 = ~G14 | ~G51;
  assign G128 = ~G152 & ~G136 & ~G20 & ~G144;
  assign G120 = ~G150 | ~G128;
  assign G132 = ~G133 | ~G134;
  assign G123 = ~G137 | ~G138 | ~G21 | ~G139;
  assign G151 = ~G20 | ~G144 | ~G143 | ~G139;
  assign I191 = ~G12 | ~I190;
  assign I192 = ~G43 | ~I190;
  assign I201 = ~G13 | ~I200;
  assign I202 = ~G47 | ~I200;
  assign G130 = ~G143 & ~G152;
  assign G149 = ~G131 | ~G130;
  assign I211 = ~G14 | ~I210;
  assign I212 = ~G51 | ~I210;
  assign G148 = ~G132 | ~G150 | ~G135;
  assign I225 = ~G15 | ~G60;
  assign G64 = ~G55 & ~G57;
  assign I235 = ~G16 | ~G64;
  assign G68 = ~G57 & ~G55 & ~G56;
  assign I245 = ~G17 | ~G68;
  assign G72 = ~G57 & ~G161 & ~G55 & ~G56;
  assign I255 = ~G18 | ~G72;
  assign I226 = ~G15 | ~I225;
  assign I227 = ~G60 | ~I225;
  assign I236 = ~G16 | ~I235;
  assign I237 = ~G64 | ~I235;
  assign I246 = ~G17 | ~I245;
  assign I247 = ~G68 | ~I245;
  assign I256 = ~G18 | ~I255;
  assign I257 = ~G72 | ~I255;
  assign I271 = ~G19 | ~G82;
  assign G86 = ~G76 & ~G79;
  assign I281 = ~G20 | ~G86;
  assign G90 = ~G79 & ~G76 & ~G77;
  assign I291 = ~G21 | ~G90;
  assign G95 = ~G79 & ~G78 & ~G76 & ~G77;
  assign I302 = ~G22 | ~G95;
  assign I272 = ~G19 | ~I271;
  assign I273 = ~G82 | ~I271;
  assign I282 = ~G20 | ~I281;
  assign I283 = ~G86 | ~I281;
  assign I292 = ~G21 | ~I291;
  assign I293 = ~G90 | ~I291;
  assign I303 = ~G22 | ~I302;
  assign I304 = ~G95 | ~I302;
  assign G33 = ~G13 & ~G11 & ~G12;
  assign G54 = ~G17 & ~G15 & ~G16;
  assign G75 = ~G21 & ~G19 & ~G20;
  assign G98 = ~G32 & ~G33;
  assign G93 = ~G75 & ~G74 & ~G79;
  always @ (posedge clock) begin
    G11 <= n20;
    G12 <= n25;
    G13 <= n30;
    G14 <= n35;
    G15 <= n40;
    G16 <= n45;
    G17 <= n50;
    G18 <= n55;
    G19 <= n60;
    G20 <= n65;
    G21 <= n70;
    G22 <= n75;
    G23 <= n80;
    G24 <= n85;
    G25 <= n90;
    G26 <= n95;
    G27 <= n100;
    G28 <= n105;
    G29 <= n110;
    G30 <= n115;
    G31 <= n120;
  end
endmodule


