// Benchmark "s967.blif" written by ABC on Mon Apr  8 18:09:40 2019

module \s967.blif  ( clock, 
    Rdy1RtHS1, Rdy2RtHS1, Rdy1BmHS1, Rdy2BmHS1, InDoneHS1, RtTSHS1,
    TpArrayHS1, OutputHS1, WantBmHS1, WantRtHS1, OutAvHS1, FullOHS1,
    FullIHS1, Prog_2, Prog_1, Prog_0,
    ActRtHS1, ActBmHS1, GoRtHS1, GoBmHS1, NewTrHS1, ReRtTSHS1, Mode0HS1,
    Mode1HS1, Mode2HS1, NewLineHS1, ShftORHS1, ShftIRHS1, LxHInHS1,
    TxHInHS1, LoadOHHS1, LoadIHHS1, SeOutAvHS1, SeFullOHS1, SeFullIHS1,
    TgWhBufHS1, ReWhBufHS1, LdProgHS1, DumpIHS1  );
  input  clock;
  input  Rdy1RtHS1, Rdy2RtHS1, Rdy1BmHS1, Rdy2BmHS1, InDoneHS1, RtTSHS1,
    TpArrayHS1, OutputHS1, WantBmHS1, WantRtHS1, OutAvHS1, FullOHS1,
    FullIHS1, Prog_2, Prog_1, Prog_0;
  output ActRtHS1, ActBmHS1, GoRtHS1, GoBmHS1, NewTrHS1, ReRtTSHS1, Mode0HS1,
    Mode1HS1, Mode2HS1, NewLineHS1, ShftORHS1, ShftIRHS1, LxHInHS1,
    TxHInHS1, LoadOHHS1, LoadIHHS1, SeOutAvHS1, SeFullOHS1, SeFullIHS1,
    TgWhBufHS1, ReWhBufHS1, LdProgHS1, DumpIHS1;
  reg I2, I3, I4, I5, I6, I7, ActRtHS1, ActBmHS1, GoRtHS1, GoBmHS1,
    NewTrHS1, ReRtTSHS1, Mode0HS1, Mode1HS1, Mode2HS1, NewLineHS1,
    ShftORHS1, ShftIRHS1, LxHInHS1, TxHInHS1, LoadOHHS1, LoadIHHS1,
    SeOutAvHS1, SeFullOHS1, SeFullIHS1, TgWhBufHS1, ReWhBufHS1, LdProgHS1,
    DumpIHS1;
  wire I137, I131, I606_2, I138, I124, I606_1, I139, I141, I142, I143, I128,
    I135, I144, I126, I136, I145, I132, I127, I133, I130, I400, I399, I250,
    I249, I235, I236, I256, I255, I436, I435, I245, I246, I186, I185, I433,
    I434, I427, I428, I393, I394, I248, I247, I347, I348, I227, I228, I290,
    I289, I307, I308, I239, I240, I404, I403, I283, I284, I204, I203, I270,
    I269, I497, I498, I268, I267, I466, I465, I190, I189, I475, I341, I342,
    I173, I487, I488, I244, I243, I495, I496, I461, I462, I280, I279, I340,
    I339, I181, I182, I206, I205, I353, I354, I285, I286, I338, I337, I187,
    I188, I193, I194, I272, I271, I200, I199, I456, I455, I184, I183, I209,
    I210, I463, I464, I225, I469, I470, I294, I293, I457, I458, I471, I472,
    I511, I512, I494, I493, I503, I504, I264, I263, I208, I207, I262, I261,
    I528, I527, I216, I215, I152, I473, I474, I419, I420, I449, I450, I446,
    I445, I192, I191, I153, I508, I507, I526, I525, I155, I162, I442, I441,
    I167, I170, I518, I517, I530, I529, I531, I532, I161, I515, I516, I154,
    I606_3, I606_4, I694_1, I824_1, I223, I623_1, I310, I626_1, I229,
    I638_1, I345, I657_1, I251, I706_1, I417, I291, I754_1, I645_1, I320,
    I906_2, I327, I654_1, I459, I499, I700_1, I578_1, I363, I751_1, I574_2,
    I313, I666_1, I375, I747_1, I325, I717_1, I322, I557_1, I564_1, I482,
    I380, I557_2, I896_1, I416, I631_1, I274, I862_1, I362, I906_1, I886_1,
    I258, I899_2, I903_1, I288, I876_1, I303, I305, I648_1, I277, I660_1,
    I631_2, I318, I567_1, I316, I890_1, I786_1, I740_1, I424, I626_2,
    I561_1, I596_2, I862_2, I179, I859_1, I260, I869_1, I882_1, I265, I502,
    I899_1, I596_1, I491, I813_1, I398, I574_1, I619_1, I388, I847_1, I408,
    I732_1, I233, I819_1, I371, I614_1, I302, I722_1, I430, I726_1, I763_1,
    I772_1, I478, I711_1, I703_1, I619_2, I651_1, I344, I663_1, I329,
    I585_2, I365, I866_1, I541_1, I331, I297, I852_1, I855_2, I547_1,
    I571_1, I872_2, I217, I483, I893_1, I409, I603_1, I855_1, I485, I879_1,
    I589_1, I349, I872_1, I550_1, I238, I589_2, I554_1, I196, I582_1, I392,
    I635_1, I401, I585_1, I324, I396, I669_1, I422, I641_1, I544_1, I241,
    I355, I369, I386, I437, I253, I357, I431, I232, I377, I360, I413, I412,
    I335, I373, I382, I490, I178, I351, I299, I367, I296, I443, I440, I282,
    I390, I426, I405, I311, I175, I384, I505, I519, I510, I197, I221, I522,
    I201, I523, I514, I540, I536, I538, I534, I480, I212, I214, I276, I220,
    I452, I454, I448, I468, n80, n85, n90, n95, n100, n105, n110, n114,
    n118, n122, n126, n130, n134, n138, n142, n146, n150, n154, n158, n162,
    n166, n170, n174, n178, n182, n186, n190, n194, n198;
  assign n80 = ~I541_1 | ~I534;
  assign n85 = ~I443 | ~I544_1 | ~I516;
  assign n90 = ~I285 | ~I547_1 | ~I243;
  assign n95 = ~I504 | ~I550_1 | ~I207;
  assign n100 = ~I536 | ~I445 | ~I540;
  assign n105 = ~I554_1 | ~I538;
  assign n110 = ~I152;
  assign n114 = ~I153;
  assign n118 = ~I154;
  assign n122 = ~I155;
  assign n126 = ~I369 & ~I747_1;
  assign n130 = ~I399 & ~I253 & ~I325;
  assign n134 = ~I469 | ~I510;
  assign n138 = ~I571_1 | ~I455;
  assign n142 = ~I225;
  assign n146 = ~I161;
  assign n150 = ~I162;
  assign n154 = ~I463 | ~I225;
  assign n158 = ~I582_1 | ~I512;
  assign n162 = ~I299 | ~I585_1 | ~I585_2;
  assign n166 = ~I532 | ~I589_1 | ~I589_2;
  assign n170 = ~I167;
  assign n174 = ~I228 & ~OutAvHS1;
  assign n178 = ~I293 | ~I221 | ~I514 | ~I457;
  assign n182 = ~I170;
  assign n186 = ~I136 & ~OutAvHS1;
  assign n190 = ~FullIHS1 & ~OutAvHS1;
  assign n194 = ~I173;
  assign n198 = ~I603_1 | ~I473;
  assign I137 = ~Prog_2;
  assign I131 = ~OutputHS1;
  assign I606_2 = ~Prog_1;
  assign I138 = ~Prog_1;
  assign I124 = ~Rdy1RtHS1;
  assign I606_1 = ~Prog_0;
  assign I139 = ~Prog_0;
  assign I141 = ~I3;
  assign I142 = ~I4;
  assign I143 = ~I5;
  assign I128 = ~InDoneHS1;
  assign I135 = ~FullOHS1;
  assign I144 = ~I6;
  assign I126 = ~Rdy1BmHS1;
  assign I136 = ~FullIHS1;
  assign I145 = ~I7;
  assign I132 = ~WantBmHS1;
  assign I127 = ~Rdy2BmHS1;
  assign I133 = ~WantRtHS1;
  assign I130 = ~TpArrayHS1;
  assign I400 = ~I3 & ~I2;
  assign I399 = ~I400;
  assign I250 = ~FullOHS1 & ~Rdy1RtHS1;
  assign I249 = ~I250;
  assign I235 = ~Rdy2RtHS1 | ~Rdy1RtHS1;
  assign I236 = ~I235;
  assign I256 = ~I5 & ~TpArrayHS1;
  assign I255 = ~I256;
  assign I436 = ~I137 & ~FullOHS1;
  assign I435 = ~I436;
  assign I245 = ~I138 | ~I137;
  assign I246 = ~I245;
  assign I186 = ~I5 & ~I386;
  assign I185 = ~I186;
  assign I433 = ~I142 | ~I2;
  assign I434 = ~I433;
  assign I427 = ~I143 | ~I142;
  assign I428 = ~I427;
  assign I393 = ~I135 | ~Rdy2BmHS1;
  assign I394 = ~I393;
  assign I248 = ~I7 & ~I144;
  assign I247 = ~I248;
  assign I347 = ~I388 | ~I223;
  assign I348 = ~I347;
  assign I227 = ~I136 | ~I135;
  assign I228 = ~I227;
  assign I290 = ~I136 & ~I144;
  assign I289 = ~I290;
  assign I307 = ~I136 | ~I126;
  assign I308 = ~I307;
  assign I239 = ~I145 | ~WantRtHS1;
  assign I240 = ~I239;
  assign I404 = ~I145 & ~I6;
  assign I403 = ~I404;
  assign I283 = ~I141 | ~I235;
  assign I284 = ~I283;
  assign I204 = ~I127 & ~I6;
  assign I203 = ~I204;
  assign I270 = ~I6 & ~I229 & ~I145;
  assign I269 = ~I270;
  assign I497 = ~I241 | ~I847_1 | ~I144;
  assign I498 = ~I497;
  assign I268 = ~I7 & ~I694_1;
  assign I267 = ~I268;
  assign I466 = ~I144 & ~I130 & ~I375;
  assign I465 = ~I466;
  assign I190 = ~I623_1 & ~I6;
  assign I189 = ~I190;
  assign I475 = ~n130;
  assign I341 = ~I241 | ~I233 | ~I139;
  assign I342 = ~I341;
  assign I173 = ~I320 | ~I310;
  assign I487 = ~I235 | ~I145 | ~I412;
  assign I488 = ~I487;
  assign I244 = ~I413 & ~I369;
  assign I243 = ~I244;
  assign I495 = ~I398 | ~I424 | ~I144;
  assign I496 = ~I495;
  assign I461 = ~I478 | ~I490 | ~I142;
  assign I462 = ~I461;
  assign I280 = ~I139 & ~I706_1;
  assign I279 = ~I280;
  assign I340 = ~I7 & ~I754_1;
  assign I339 = ~I340;
  assign I181 = ~I614_1 | ~I313;
  assign I182 = ~I181;
  assign I206 = ~I369 & ~I645_1;
  assign I205 = ~I206;
  assign I353 = ~I763_1 | ~I144;
  assign I354 = ~I353;
  assign I285 = ~I711_1 | ~I490;
  assign I286 = ~I285;
  assign I338 = ~I3 & ~I751_1;
  assign I337 = ~I338;
  assign I187 = ~I619_1 | ~I619_2;
  assign I188 = ~I187;
  assign I193 = ~I331 | ~I377;
  assign I194 = ~I193;
  assign I272 = ~I331 & ~I3;
  assign I271 = ~I272;
  assign I200 = ~I351 & ~I638_1;
  assign I199 = ~I200;
  assign I456 = ~I824_1 & ~I3 & ~I351;
  assign I455 = ~I456;
  assign I184 = ~I316 & ~I318;
  assign I183 = ~I184;
  assign I209 = ~I651_1 | ~I322;
  assign I210 = ~I209;
  assign I463 = ~I274 | ~I143 | ~I137;
  assign I464 = ~I463;
  assign I225 = ~I382 | ~I296;
  assign I469 = ~I288 | ~I380 | ~I143;
  assign I470 = ~I469;
  assign I294 = ~I369 & ~I717_1;
  assign I293 = ~I294;
  assign I457 = ~I390 | ~I228 | ~I282;
  assign I458 = ~I457;
  assign I471 = ~I360 | ~I136 | ~I426;
  assign I472 = ~I471;
  assign I511 = ~I866_1 | ~I299;
  assign I512 = ~I511;
  assign I494 = ~I367 & ~I427;
  assign I493 = ~I494;
  assign I503 = ~I852_1 | ~I461;
  assign I504 = ~I503;
  assign I264 = ~I431 & ~I483;
  assign I263 = ~I264;
  assign I208 = ~I399 & ~I648_1;
  assign I207 = ~I208;
  assign I262 = ~I405 & ~Prog_2;
  assign I261 = ~I262;
  assign I528 = ~I480 & ~I286;
  assign I527 = ~I528;
  assign I216 = ~Prog_2 & ~I660_1;
  assign I215 = ~I216;
  assign I152 = ~I470 & ~n142 & ~I557_1 & ~I557_2;
  assign I473 = ~I133 | ~I502 | ~I175;
  assign I474 = ~I473;
  assign I419 = ~I384 | ~Prog_0;
  assign I420 = ~I419;
  assign I449 = ~WantRtHS1 | ~I819_1 | ~I384;
  assign I450 = ~I449;
  assign I446 = ~I403 & ~I493 & ~I357;
  assign I445 = ~I446;
  assign I192 = ~I626_1 & ~I626_2;
  assign I191 = ~I192;
  assign I153 = ~I214 & ~n126 & ~I561_1 & ~n142;
  assign I508 = ~I859_1 & ~I458;
  assign I507 = ~I508;
  assign I526 = ~I886_1 & ~I480 & ~I505;
  assign I525 = ~I526;
  assign I155 = ~I519 & ~I454 & ~I567_1 & ~I458;
  assign I162 = ~n142 & ~I244 & ~I578_1 & ~I324;
  assign I442 = ~I175 & ~I813_1;
  assign I441 = ~I442;
  assign I167 = ~I220 & ~I276 & ~I474;
  assign I170 = ~I276 & ~I474 & ~I596_1 & ~I596_2;
  assign I518 = ~I876_1 & ~I212 & ~I450;
  assign I517 = ~I518;
  assign I530 = ~I890_1 & ~I452 & ~I507;
  assign I529 = ~I530;
  assign I531 = ~I522 | ~I893_1 | ~I293;
  assign I532 = ~I531;
  assign I161 = ~I462 & ~I458 & ~I574_1 & ~I574_2;
  assign I515 = ~I201 | ~I872_1 | ~I872_2;
  assign I516 = ~I515;
  assign I154 = ~I564_1 & ~I529;
  assign I606_3 = Prog_0 & I606_2;
  assign I606_4 = I606_1 & Prog_1;
  assign I694_1 = I6 & I143;
  assign I824_1 = Prog_2 & I143;
  assign I223 = ~FullIHS1 | ~FullOHS1;
  assign I623_1 = I143 & I223 & Prog_0;
  assign I310 = ~I7 & ~I6;
  assign I626_1 = I137 & I310;
  assign I229 = ~I137 | ~I141;
  assign I638_1 = I143 & I229;
  assign I345 = ~I386 | ~I141;
  assign I657_1 = I143 & I345;
  assign I251 = ~I136 | ~Rdy2RtHS1;
  assign I706_1 = I249 & I251;
  assign I417 = ~I144 | ~Prog_2;
  assign I291 = ~Prog_0 | ~I235;
  assign I754_1 = I417 & I291;
  assign I645_1 = I6 & I255;
  assign I320 = ~I369 & ~I5;
  assign I906_2 = I7 & I320;
  assign I327 = ~I241 | ~I229;
  assign I654_1 = I143 & I327;
  assign I459 = ~Prog_0 | ~I124 | ~I360;
  assign I499 = ~I139 | ~I132 | ~I436 | ~WantRtHS1;
  assign I700_1 = I459 & I499;
  assign I578_1 = I320 & I247;
  assign I363 = ~I227 | ~I7;
  assign I751_1 = I289 & I363;
  assign I574_2 = n130 & Prog_2;
  assign I313 = ~I408 | ~Prog_0;
  assign I666_1 = I313 & I341;
  assign I375 = ~I143 | ~I7;
  assign I747_1 = I375 & I267;
  assign I325 = ~I310 | ~I5;
  assign I717_1 = I325 & I465;
  assign I322 = ~I413 & ~I4;
  assign I557_1 = I2 & I322;
  assign I564_1 = I322 & I236 & I400;
  assign I482 = ~I433 & ~I3 & ~I247;
  assign I380 = ~Prog_2 & ~I139;
  assign I557_2 = I482 & I380;
  assign I896_1 = FullOHS1 & I482;
  assign I416 = ~I7 & ~Rdy2RtHS1;
  assign I631_1 = I416 & I189;
  assign I274 = ~I7 & ~I335;
  assign I862_1 = I274 & I185;
  assign I362 = ~I228 & ~I137;
  assign I906_1 = I274 & I362;
  assign I886_1 = I229 & I488;
  assign I258 = ~I297 & ~I229;
  assign I899_2 = I248 & I258;
  assign I903_1 = I258 & I404;
  assign I288 = ~I3 & ~I373;
  assign I876_1 = I288 & I362;
  assign I303 = ~I726_1 | ~I6;
  assign I305 = ~I430 | ~I253;
  assign I648_1 = I303 & I305;
  assign I277 = ~I703_1 | ~I139;
  assign I660_1 = I277 & I279;
  assign I631_2 = I354 & I127;
  assign I318 = ~I403 & ~I371;
  assign I567_1 = I494 & I318;
  assign I316 = ~I377 & ~I124;
  assign I890_1 = I316 & I494;
  assign I786_1 = I337 & I339;
  assign I740_1 = I269 & I271;
  assign I424 = ~I227 & ~I137;
  assign I626_2 = I183 & I424;
  assign I561_1 = I139 & I464;
  assign I596_2 = I464 & InDoneHS1;
  assign I862_2 = n142 & Prog_0;
  assign I179 = ~WantBmHS1 | ~I371;
  assign I859_1 = WantRtHS1 & I264 & I179;
  assign I260 = ~I357 & ~Prog_0;
  assign I869_1 = I262 & I260;
  assign I882_1 = I342 & I262;
  assign I265 = ~I232 | ~WantRtHS1;
  assign I502 = ~I127 & ~I431 & ~I405 & ~I132;
  assign I899_1 = I265 & I502;
  assign I596_1 = I472 & I260;
  assign I491 = ~I424 | ~I133 | ~I440;
  assign I813_1 = I263 & I491;
  assign I398 = ~I437 & ~I427;
  assign I574_1 = I398 & I191;
  assign I619_1 = WantBmHS1 | WantRtHS1 | I7;
  assign I388 = ~Prog_0 & ~I5;
  assign I847_1 = I7 | I388;
  assign I408 = ~Rdy2RtHS1 & ~I124;
  assign I732_1 = I133 | I408;
  assign I233 = ~I127 | ~I126;
  assign I819_1 = I233 | I132;
  assign I371 = ~Rdy1BmHS1 | ~I127;
  assign I614_1 = I371 | Prog_0;
  assign I302 = ~I375 & ~I142;
  assign I722_1 = I310 | I302;
  assign I430 = ~I7 & ~I143;
  assign I726_1 = I430 | I302;
  assign I763_1 = I7 | I348;
  assign I772_1 = I3 | I424;
  assign I478 = ~I241 & ~I7 & ~I227;
  assign I711_1 = I4 | I478;
  assign I703_1 = I308 | I394;
  assign I619_2 = I240 | I371;
  assign I651_1 = I2 | I284;
  assign I344 = ~I371 & ~FullIHS1;
  assign I663_1 = I380 | I344;
  assign I329 = ~I398 | ~I126;
  assign I585_2 = I145 | I329;
  assign I365 = ~I428 | ~I310;
  assign I866_1 = I365 | I137 | I3;
  assign I541_1 = I246 | I475;
  assign I331 = ~I178 | ~I5;
  assign I297 = ~I434 | ~I128;
  assign I852_1 = I331 | I297;
  assign I855_2 = InDoneHS1 | I243;
  assign I547_1 = I205 | I145;
  assign I571_1 = I225 | I138;
  assign I872_2 = I225 | InDoneHS1;
  assign I217 = ~I663_1 | ~I144;
  assign I483 = ~I408 | ~I440 | ~I135;
  assign I893_1 = I217 | I483;
  assign I409 = ~Rdy2RtHS1 | ~I426 | ~I136;
  assign I603_1 = I409 | I499;
  assign I855_1 = I182 | I471;
  assign I485 = ~I426 | ~I137 | ~I223;
  assign I879_1 = I341 | I485;
  assign I589_1 = I495 | I188;
  assign I349 = ~I434 | ~I3;
  assign I872_1 = I349 | I194;
  assign I550_1 = I493 | I184;
  assign I238 = ~I6 & ~I132;
  assign I589_2 = I263 | I238;
  assign I554_1 = I182 | I261;
  assign I196 = ~I631_1 & ~I631_2;
  assign I582_1 = I433 | I196;
  assign I392 = ~I5 & ~I786_1;
  assign I635_1 = I392 | I498;
  assign I401 = ~I434 | ~I382;
  assign I585_1 = I215 | I401;
  assign I324 = ~I433 & ~I740_1;
  assign I396 = ~I225 & ~I131;
  assign I669_1 = I324 | I396;
  assign I422 = ~I311 & ~I7;
  assign I641_1 = I420 | I422;
  assign I544_1 = I441 | I203;
  assign I241 = ~Rdy2BmHS1 | ~Rdy1BmHS1;
  assign I355 = ~Rdy2RtHS1 | ~I124;
  assign I369 = ~I4 | ~I400;
  assign I386 = ~Prog_2 & ~Prog_0;
  assign I437 = ~I141 | ~I2;
  assign I253 = ~I142 | ~RtTSHS1;
  assign I357 = ~Rdy2BmHS1 | ~I126;
  assign I431 = ~Prog_2 | ~I136;
  assign I232 = ~Rdy2RtHS1 & ~Rdy1RtHS1;
  assign I377 = ~I6 | ~I416;
  assign I360 = ~Prog_2 & ~I135;
  assign I413 = ~I248 | ~I5;
  assign I412 = ~I433 & ~I144;
  assign I335 = ~I412 | ~I141;
  assign I373 = ~I404 | ~I434;
  assign I382 = ~I5 & ~I7;
  assign I490 = ~I5 & ~I6 & ~I399;
  assign I178 = ~I310 & ~I390;
  assign I351 = ~I434 | ~I178;
  assign I299 = ~I722_1 | ~I400;
  assign I367 = ~I772_1 | ~I2;
  assign I296 = ~I349 & ~I6;
  assign I443 = ~I127 | ~I7 | ~I296;
  assign I440 = ~I401 & ~I3;
  assign I282 = ~Rdy1RtHS1 & ~I329;
  assign I390 = ~I144 & ~I145;
  assign I426 = ~I437 & ~I365;
  assign I405 = ~I426 | ~I135;
  assign I311 = ~I732_1 | ~I496;
  assign I175 = ~I606_3 & ~I606_4;
  assign I384 = ~I409 & ~I435;
  assign I505 = ~I855_1 | ~I855_2;
  assign I519 = ~I879_1 | ~I461;
  assign I510 = ~I862_1 & ~I862_2;
  assign I197 = ~I635_1 | ~I434;
  assign I221 = ~I669_1 | ~InDoneHS1;
  assign I522 = ~I882_1 & ~I468 & ~I458;
  assign I201 = ~I641_1 | ~I179;
  assign I523 = ~I487 | ~I197 | ~I199;
  assign I514 = ~I468 & ~I869_1;
  assign I540 = ~I210 & ~I906_1 & ~I906_2;
  assign I536 = ~I525 & ~I899_1 & ~I899_2;
  assign I538 = ~I527 & ~I517 & ~I903_1 & ~I448;
  assign I534 = ~n142 & ~I282 & ~I896_1 & ~I523;
  assign I480 = ~I375 & ~I228 & ~I335;
  assign I212 = ~I373 & ~I654_1;
  assign I214 = ~I373 & ~I657_1;
  assign I276 = ~I409 & ~I700_1;
  assign I220 = ~I471 & ~I666_1;
  assign I452 = ~I236 & ~I485 & ~I139 & ~I232;
  assign I454 = ~I311 & ~I371 & ~I132;
  assign I448 = ~I247 & ~I493 & ~I355;
  assign I468 = ~I261 & ~I139 & ~I355;
  always @ (posedge clock) begin
    I2 <= n80;
    I3 <= n85;
    I4 <= n90;
    I5 <= n95;
    I6 <= n100;
    I7 <= n105;
    ActRtHS1 <= n110;
    ActBmHS1 <= n114;
    GoRtHS1 <= n118;
    GoBmHS1 <= n122;
    NewTrHS1 <= n126;
    ReRtTSHS1 <= n130;
    Mode0HS1 <= n134;
    Mode1HS1 <= n138;
    Mode2HS1 <= n142;
    NewLineHS1 <= n146;
    ShftORHS1 <= n150;
    ShftIRHS1 <= n154;
    LxHInHS1 <= n158;
    TxHInHS1 <= n162;
    LoadOHHS1 <= n166;
    LoadIHHS1 <= n170;
    SeOutAvHS1 <= n174;
    SeFullOHS1 <= n178;
    SeFullIHS1 <= n182;
    TgWhBufHS1 <= n186;
    ReWhBufHS1 <= n190;
    LdProgHS1 <= n194;
    DumpIHS1 <= n198;
  end
endmodule


