// Benchmark "s953.blif" written by ABC on Mon Apr  8 18:09:29 2019

module \s953.blif  ( clock, 
    Rdy1RtHS1, Rdy2RtHS1, Rdy1BmHS1, Rdy2BmHS1, InDoneHS1, RtTSHS1,
    TpArrayHS1, OutputHS1, WantBmHS1, WantRtHS1, OutAvHS1, FullOHS1,
    FullIHS1, Prog_2, Prog_1, Prog_0,
    ReWhBufHS1, TgWhBufHS1, SeOutAvHS1, LdProgHS1, Mode2HS1, ReRtTSHS1,
    ShftIRHS1, NewTrHS1, Mode1HS1, ShftORHS1, ActRtHS1, Mode0HS1, TxHInHS1,
    LxHInHS1, NewLineHS1, ActBmHS1, GoBmHS1, LoadOHHS1, DumpIHS1,
    SeFullOHS1, GoRtHS1, LoadIHHS1, SeFullIHS1  );
  input  clock;
  input  Rdy1RtHS1, Rdy2RtHS1, Rdy1BmHS1, Rdy2BmHS1, InDoneHS1, RtTSHS1,
    TpArrayHS1, OutputHS1, WantBmHS1, WantRtHS1, OutAvHS1, FullOHS1,
    FullIHS1, Prog_2, Prog_1, Prog_0;
  output ReWhBufHS1, TgWhBufHS1, SeOutAvHS1, LdProgHS1, Mode2HS1, ReRtTSHS1,
    ShftIRHS1, NewTrHS1, Mode1HS1, ShftORHS1, ActRtHS1, Mode0HS1, TxHInHS1,
    LxHInHS1, NewLineHS1, ActBmHS1, GoBmHS1, LoadOHHS1, DumpIHS1,
    SeFullOHS1, GoRtHS1, LoadIHHS1, SeFullIHS1;
  reg State_5, State_4, State_3, State_2, State_1, State_0, ActRtHS1,
    ActBmHS1, GoRtHS1, GoBmHS1, NewTrHS1, ReRtTSHS1, Mode0HS1, Mode1HS1,
    Mode2HS1, NewLineHS1, ShftORHS1, ShftIRHS1, LxHInHS1, TxHInHS1,
    LoadOHHS1, LoadIHHS1, SeOutAvHS1, SeFullOHS1, SeFullIHS1, TgWhBufHS1,
    ReWhBufHS1, LdProgHS1, DumpIHS1;
  wire I265, I266, I263, I264, I271, I272, I284, I283, I282, I275, I274,
    I281, I280, I279, I278, I277, I276, I269, I267, I345, I344, I326, I327,
    I624, I625, I495, I494, I512, I513, I509, I508, I570, I571, I330, I331,
    I440, I441, I505, I504, I338, I339, I343, I342, I425, I424, I487, I486,
    I436, I437, I450, I451, I458, I459, I534, I535, I555, I554, I391, I390,
    I340, I341, I396, I397, I414, I415, I468, I469, I323, I322, I399, I398,
    I429, I428, I453, I452, I445, I444, I381, I380, I366, I367, I474, I475,
    I430, I431, I434, I435, I466, I467, I371, I370, I376, I377, I359, I358,
    I553, I552, I567, I566, I411, I410, I354, I355, I362, I363, I378, I379,
    I422, I423, I328, I329, I295, I447, I446, I770, I771, I690, I691, I768,
    I769, I476, I477, I404, I405, I660, I661, I297, I662, I663, I294, I350,
    I351, I778, I779, I311, I287, I300, I303, I840_2, I873_1, I840_1, I610,
    I612, I850_1, I335, I963_1, I357, I966_1, I325, I1025_1, I360, I910_1,
    I614, I850_2, I497, I1044_1, I1077_1, I506, I1083_1, I393, I1170_1,
    I521, I1193_1, I1184_1, I382, I1080_1, I1107_1, I418, I1103_1, I1196_1,
    I1040_1, I1103_2, I1180_1, I317, I1031_1, I529, I1166_1, I412, I1160_1,
    I1034_1, I531, I1163_1, I590, I1136_1, I1166_2, I1173_1, I388, I1110_1,
    I1188_2, I1199_2, I580, I789_1, I1184_2, I1188_1, I596, I1143_2, I384,
    I1100_1, I568, I1128_1, I1056_1, I1176_1, I556, I1097_1, I348, I1180_2,
    I600, I1176_2, I364, I562, I810_1, I1199_1, I353, I1143_1, I573,
    I1140_1, I582, I1094_1, I1047_2, I881_1, I1047_1, I881_2, I493, I857_1,
    I523, I834_1, I892_1, I336, I1037_1, I457, I861_2, I892_2, I320,
    I896_1, I455, I861_1, I589, I1121_1, I796_1, I543, I577, I1203_2, I449,
    I537, I1216_1, I1113_1, I479, I1118_1, I463, I491, I1203_1, I465, I489,
    I1216_2, I1154_1, I1028_1, I593, I595, I1132_1, I1132_2, I565, I1148_1,
    I559, I1121_2, I561, I1125_1, I526, I1087_1, I814_1, I599, I1157_1,
    I421, I1210_1, I585, I587, I1091_1, I547, I575, I829_1, I498, I1213_1,
    I519, I579, I1207_1, I1151_1, I511, I473, I539, I525, I514, I439, I461,
    I318, I394, I482, I372, I374, I548, I485, I503, I442, I551, I481, I680,
    I532, I407, I432, I500, I634, I403, I609, I416, I676, I682, I738, I746,
    I706, I545, I517, I715, I713, I719, I717, I675, I725, I733, I729, I731,
    I702, I684, I686, I678, I655, I657, I671, I673, I742, I689, I693, I711,
    I659, I669, I667, I744, I740, I721, I723, I737, I735, I704, I699, I695,
    I697, I665, I750, I700, I708, I777, I767, I386, I315, I347, I470, I333,
    I540, I408, n80, n85, n90, n95, n100, n105, n110, n114, n118, n122,
    n126, n130, n134, n138, n142, n146, n150, n154, n158, n162, n166, n170,
    n174, n178, n182, n186, n190, n194, n198;
  assign n80 = ~I769 | ~I711 | ~I771;
  assign n85 = ~I721 | ~I723;
  assign n90 = ~I551 | ~I725 | ~I381;
  assign n95 = ~I733 | ~I729 | ~I731 | ~I397;
  assign n100 = ~I777 | ~I377 | ~I779;
  assign n105 = ~I311;
  assign n110 = ~I655 | ~I657;
  assign n114 = ~I659 | ~I377 | ~I661;
  assign n118 = ~I287;
  assign n122 = ~I667 | ~I475 | ~I669;
  assign n126 = ~I377 | ~I469;
  assign n130 = ~I415;
  assign n134 = ~I671 | ~I673;
  assign n138 = ~I796_1 | ~I675;
  assign n142 = ~I323;
  assign n146 = ~I294;
  assign n150 = ~I295;
  assign n154 = ~I371 | ~I323;
  assign n158 = ~I297;
  assign n162 = ~I691 | ~I689 | ~I693;
  assign n166 = ~I699 | ~I695 | ~I697 | ~I481;
  assign n170 = ~I300;
  assign n174 = ~OutAvHS1 & ~I326;
  assign n178 = ~I814_1 | ~I767;
  assign n182 = ~I303;
  assign n186 = ~OutAvHS1 & ~I275;
  assign n190 = ~OutAvHS1 & ~FullIHS1;
  assign n194 = ~I441 & ~I278 & ~State_2;
  assign n198 = ~I829_1 | ~I351;
  assign I265 = ~Rdy1BmHS1;
  assign I266 = ~Rdy2BmHS1;
  assign I263 = ~Rdy1RtHS1;
  assign I264 = ~Rdy2RtHS1;
  assign I271 = ~WantBmHS1;
  assign I272 = ~WantRtHS1;
  assign I284 = ~Prog_0;
  assign I283 = ~Prog_1;
  assign I282 = ~Prog_2;
  assign I275 = ~FullIHS1;
  assign I274 = ~FullOHS1;
  assign I281 = ~State_0;
  assign I280 = ~State_1;
  assign I279 = ~State_2;
  assign I278 = ~State_3;
  assign I277 = ~State_4;
  assign I276 = ~State_5;
  assign I269 = ~TpArrayHS1;
  assign I267 = ~InDoneHS1;
  assign I345 = ~Rdy1RtHS1 | ~Rdy2RtHS1;
  assign I344 = ~I345;
  assign I326 = ~FullOHS1 & ~FullIHS1;
  assign I327 = ~I326;
  assign I624 = ~State_2 & ~I511;
  assign I625 = ~I624;
  assign I495 = ~I280 | ~I281;
  assign I494 = ~I495;
  assign I512 = ~I280 & ~State_0;
  assign I513 = ~I512;
  assign I509 = ~I276 | ~I277;
  assign I508 = ~I509;
  assign I570 = ~I276 & ~State_3;
  assign I571 = ~I570;
  assign I330 = ~WantBmHS1 | ~I493;
  assign I331 = ~I330;
  assign I440 = ~I495 & ~I509;
  assign I441 = ~I440;
  assign I505 = ~I279 | ~I570;
  assign I504 = ~I505;
  assign I338 = ~I857_1 | ~I439;
  assign I339 = ~I338;
  assign I343 = ~I482 | ~I276 | ~I394;
  assign I342 = ~I343;
  assign I425 = ~I508 | ~State_2 | ~I281;
  assign I424 = ~I425;
  assign I487 = ~State_3 | ~I508;
  assign I486 = ~I487;
  assign I436 = ~State_1 & ~I505;
  assign I437 = ~I436;
  assign I450 = ~State_0 & ~I505;
  assign I451 = ~I450;
  assign I458 = ~I279 & ~I571;
  assign I459 = ~I458;
  assign I534 = ~State_4 & ~I571;
  assign I535 = ~I534;
  assign I555 = ~I330 & ~I1025_1;
  assign I554 = ~I555;
  assign I391 = ~State_2 & ~I910_1;
  assign I390 = ~I391;
  assign I340 = ~I861_1 | ~I861_2;
  assign I341 = ~I340;
  assign I396 = ~I280 & ~I425;
  assign I397 = ~I396;
  assign I414 = ~I521 & ~State_1 & ~I425;
  assign I415 = ~I414;
  assign I468 = ~I487 & ~State_0 & ~I386;
  assign I469 = ~I468;
  assign I323 = ~I436 | ~State_4 | ~I281;
  assign I322 = ~I323;
  assign I399 = ~I284 | ~I436;
  assign I398 = ~I399;
  assign I429 = ~Prog_0 | ~I450;
  assign I428 = ~I429;
  assign I453 = ~I504 | ~I277 | ~I327;
  assign I452 = ~I453;
  assign I445 = ~I374 | ~I534;
  assign I444 = ~I445;
  assign I381 = ~State_3 | ~I396;
  assign I380 = ~I381;
  assign I366 = ~I399 & ~State_0 & ~I335;
  assign I367 = ~I366;
  assign I474 = ~I493 & ~I577;
  assign I475 = ~I474;
  assign I430 = ~Prog_2 & ~I451;
  assign I431 = ~I430;
  assign I434 = ~FullIHS1 & ~I503;
  assign I435 = ~I434;
  assign I466 = ~Rdy1BmHS1 & ~I503;
  assign I467 = ~I466;
  assign I371 = ~I279 | ~I382;
  assign I370 = ~I371;
  assign I376 = ~I281 & ~I479;
  assign I377 = ~I376;
  assign I359 = ~I532 | ~Rdy1RtHS1 | ~I432;
  assign I358 = ~I359;
  assign I553 = ~State_1 | ~I500;
  assign I552 = ~I553;
  assign I567 = ~I388 & ~I1044_1;
  assign I566 = ~I567;
  assign I411 = ~I416 | ~I279 | ~Prog_0;
  assign I410 = ~I411;
  assign I354 = ~I367 & ~I543;
  assign I355 = ~I354;
  assign I362 = ~State_0 & ~I407;
  assign I363 = ~I362;
  assign I378 = ~FullIHS1 & ~I431;
  assign I379 = ~I378;
  assign I422 = ~I431 & ~I525;
  assign I423 = ~I422;
  assign I328 = ~I539 & ~I609 & ~I511;
  assign I329 = ~I328;
  assign I295 = ~I680 & ~I376 & ~I682;
  assign I447 = ~Rdy2RtHS1 | ~I362;
  assign I446 = ~I447;
  assign I770 = ~I715 | ~I713;
  assign I771 = ~I770;
  assign I690 = ~I1132_1 | ~I1132_2;
  assign I691 = ~I690;
  assign I768 = ~I719 | ~I717;
  assign I769 = ~I768;
  assign I476 = ~I519 & ~I545;
  assign I477 = ~I476;
  assign I404 = ~I284 & ~I421;
  assign I405 = ~I404;
  assign I660 = ~I1087_1 | ~I469;
  assign I661 = ~I660;
  assign I297 = ~I684 & ~I376 & ~I686;
  assign I662 = ~I1091_1 | ~I329;
  assign I663 = ~I662;
  assign I294 = ~I676 & ~I408 & ~I678;
  assign I350 = ~I325 & ~I477;
  assign I351 = ~I350;
  assign I778 = ~I737 | ~I735;
  assign I779 = ~I778;
  assign I311 = ~I744 & ~I742 & ~I746;
  assign I287 = ~I750 & ~I789_1;
  assign I300 = ~I700 & ~I810_1;
  assign I303 = ~I706 & ~I708;
  assign I840_2 = Prog_1 & Prog_0;
  assign I873_1 = I263 & I264;
  assign I840_1 = I283 & I284;
  assign I610 = ~Prog_2 & ~I284;
  assign I612 = ~Rdy1RtHS1 & ~I274;
  assign I850_1 = I610 & I612;
  assign I335 = ~I277 | ~I282;
  assign I963_1 = I335 & I345;
  assign I357 = ~Rdy1BmHS1 | ~Rdy2BmHS1;
  assign I966_1 = I335 & I357;
  assign I325 = ~I840_1 & ~I840_2;
  assign I1025_1 = Rdy2BmHS1 & I325;
  assign I360 = ~I881_1 | ~I881_2;
  assign I910_1 = I277 & I360;
  assign I614 = ~I523 & ~I575;
  assign I850_2 = WantRtHS1 & I614;
  assign I497 = ~I455 | ~I457;
  assign I1044_1 = I497 & I570;
  assign I1077_1 = I458 & I512;
  assign I506 = ~State_1 & ~I281;
  assign I1083_1 = I458 & I506;
  assign I393 = ~I282 | ~I283;
  assign I1170_1 = I393 & I414;
  assign I521 = ~RtTSHS1 | ~I278;
  assign I1193_1 = I424 & I521;
  assign I1184_1 = I486 & I506;
  assign I382 = ~I485 & ~I276 & ~Prog_2;
  assign I1080_1 = Prog_0 & I382;
  assign I1107_1 = I284 & I382;
  assign I418 = ~I279 & ~I485;
  assign I1103_1 = State_5 & I418;
  assign I1196_1 = I345 & I418;
  assign I1040_1 = OutputHS1 & I322;
  assign I1103_2 = Prog_0 & I322;
  assign I1180_1 = I267 & I322;
  assign I317 = ~FullOHS1 | ~FullIHS1;
  assign I1031_1 = I317 & I398;
  assign I529 = ~I399 | ~I489;
  assign I1166_1 = I357 & I529;
  assign I412 = ~I282 & ~I437;
  assign I1160_1 = I281 & I412;
  assign I1034_1 = I317 & I428;
  assign I531 = ~I429 | ~I491;
  assign I1163_1 = I345 & I531;
  assign I590 = ~I429 & ~I539;
  assign I1136_1 = I282 & I590;
  assign I1166_2 = Prog_2 & I452;
  assign I1173_1 = I263 & I466;
  assign I388 = ~I320 & ~I459;
  assign I1110_1 = I277 & I388;
  assign I1188_2 = I267 & I388;
  assign I1199_2 = I267 & I380;
  assign I580 = ~I345 & ~I397;
  assign I789_1 = I278 & I580;
  assign I1184_2 = I269 & I376;
  assign I1188_1 = State_1 & I376;
  assign I596 = ~I336 & ~I367;
  assign I1143_2 = I274 & I596;
  assign I384 = ~I493 & ~I315 & ~I407;
  assign I1100_1 = WantBmHS1 & I384;
  assign I568 = ~I1047_1 | ~I1047_2;
  assign I1128_1 = I378 & I568;
  assign I1056_1 = I280 & I358;
  assign I1176_1 = State_4 & I566;
  assign I556 = ~I1028_1 | ~I355;
  assign I1097_1 = I317 & I556;
  assign I348 = ~I315 & ~I363;
  assign I1180_2 = I348 & I554;
  assign I600 = ~I331 & ~I447;
  assign I1176_2 = Prog_0 & I600;
  assign I364 = ~I525 & ~I274 & ~I379;
  assign I562 = ~I1037_1 | ~I439;
  assign I810_1 = I364 & I562;
  assign I1199_1 = I338 & I364;
  assign I353 = ~I344 & ~I873_1;
  assign I1143_1 = I353 & I404;
  assign I573 = ~I517 | ~I545;
  assign I1140_1 = I271 & I573;
  assign I582 = ~I331 & ~I517;
  assign I1094_1 = WantRtHS1 & I582;
  assign I1047_2 = Rdy1BmHS1 | Prog_0;
  assign I881_1 = InDoneHS1 | Prog_2;
  assign I1047_1 = I264 | I284;
  assign I881_2 = I282 | I326;
  assign I493 = ~Rdy1BmHS1 | ~I266;
  assign I857_1 = Prog_0 | I493;
  assign I523 = ~I274 | ~Prog_2;
  assign I834_1 = FullIHS1 | I523;
  assign I892_1 = I279 | I495;
  assign I336 = ~I473 | ~I357;
  assign I1037_1 = Prog_0 | I336;
  assign I457 = ~I266 | ~I506;
  assign I861_2 = I265 | I457;
  assign I892_2 = I269 | I625;
  assign I320 = ~I495 | ~I511;
  assign I896_1 = I279 | I320;
  assign I455 = ~I264 | ~I512;
  assign I861_1 = I263 | I455;
  assign I589 = ~Prog_2 | ~I482;
  assign I1121_1 = State_0 | I589;
  assign I796_1 = I283 | I323;
  assign I543 = ~I265 | ~Rdy2BmHS1;
  assign I577 = ~I436 | ~State_0 | ~I318;
  assign I1203_2 = I543 | I577;
  assign I449 = ~I450 | ~State_1 | ~I318;
  assign I537 = ~I263 | ~Rdy2RtHS1;
  assign I1216_1 = I449 | I537;
  assign I1113_1 = I282 | I415;
  assign I479 = ~I279 | ~I486;
  assign I1118_1 = State_1 | I479;
  assign I463 = ~I390 & ~I963_1;
  assign I491 = ~State_5 | ~I548;
  assign I1203_1 = I463 | I491;
  assign I465 = ~I390 & ~I966_1;
  assign I489 = ~I506 | ~I570;
  assign I1216_2 = I465 | I489;
  assign I1154_1 = I267 | I371;
  assign I1028_1 = I367 | I493;
  assign I593 = ~I284 | ~I430;
  assign I595 = ~Rdy2BmHS1 | ~I274;
  assign I1132_1 = I593 | I595;
  assign I1132_2 = I281 | I467;
  assign I565 = ~I444 & ~I1040_1;
  assign I1148_1 = I267 | I565;
  assign I559 = ~I412 & ~I1031_1;
  assign I1121_2 = Rdy2BmHS1 | I559;
  assign I561 = ~I432 & ~I1034_1;
  assign I1125_1 = Rdy2RtHS1 | I561;
  assign I526 = ~I370 & ~I416;
  assign I1087_1 = Prog_0 | I526;
  assign I814_1 = FullOHS1 | I355;
  assign I599 = ~I275 | ~I354;
  assign I1157_1 = I274 | I599;
  assign I421 = ~I274 | ~I422;
  assign I1210_1 = I339 | I421;
  assign I585 = ~I353 | ~I422;
  assign I587 = ~Prog_0 | ~I317;
  assign I1091_1 = I585 | I587;
  assign I547 = ~WantRtHS1 | ~I446;
  assign I575 = ~I271 | ~I284;
  assign I829_1 = I547 | I575;
  assign I498 = ~I271 & ~I473;
  assign I1213_1 = I498 | I547;
  assign I519 = ~Rdy2BmHS1 | ~WantBmHS1;
  assign I579 = ~I446 & ~I1056_1;
  assign I1207_1 = I519 | I579;
  assign I1151_1 = I405 | I537;
  assign I511 = ~State_1 | ~State_0;
  assign I473 = ~I265 | ~I266;
  assign I539 = ~I263 | ~I274;
  assign I525 = ~I277 | ~I280;
  assign I514 = ~I263 & ~Rdy2RtHS1;
  assign I439 = ~Prog_0 | ~I514;
  assign I461 = ~I282 | ~I506;
  assign I318 = ~I834_1 | ~I277;
  assign I394 = ~I357 & ~State_0 & ~I327;
  assign I482 = ~I525 & ~State_3 & ~State_2;
  assign I372 = ~I892_1 | ~I892_2;
  assign I374 = ~I896_1 | ~I461;
  assign I548 = ~State_3 & ~I513;
  assign I485 = ~I277 | ~I548;
  assign I503 = ~I277 | ~I504;
  assign I442 = ~I509 & ~State_1 & ~I347;
  assign I551 = ~I279 | ~I442;
  assign I481 = ~I372 | ~I486;
  assign I680 = ~I445 | ~I381;
  assign I532 = ~State_4 & ~I327;
  assign I407 = ~I412 | ~I532;
  assign I432 = ~I282 & ~I451;
  assign I500 = ~I281 & ~I453;
  assign I634 = ~I264 & ~I333;
  assign I403 = ~I494 | ~I634 | ~I434;
  assign I609 = ~I265 | ~I434;
  assign I416 = ~I461 & ~I535;
  assign I676 = ~I1113_1 | ~I343;
  assign I682 = ~I1118_1 | ~I323;
  assign I738 = ~I1203_1 | ~I1203_2;
  assign I746 = ~I1216_1 | ~I1216_2;
  assign I706 = ~I1154_1 | ~I403;
  assign I545 = ~I272 | ~I362;
  assign I517 = ~I264 | ~I358;
  assign I715 = ~I1166_1 & ~I1166_2;
  assign I713 = ~I470 & ~I1163_1;
  assign I719 = ~I500 & ~I1173_1;
  assign I717 = ~I322 & ~I1170_1;
  assign I675 = ~I470 & ~I1110_1;
  assign I725 = ~I1184_1 & ~I1184_2;
  assign I733 = ~I342 & ~I1193_1;
  assign I729 = ~I1188_1 & ~I1188_2;
  assign I731 = ~I540 & ~I474;
  assign I702 = ~I1148_1 | ~I481;
  assign I684 = ~I1121_1 | ~I1121_2;
  assign I686 = ~I1125_1 | ~I441;
  assign I678 = ~I329 | ~I423;
  assign I655 = ~I322 & ~I1077_1;
  assign I657 = ~I410 & ~I1080_1;
  assign I671 = ~I1103_1 & ~I1103_2;
  assign I673 = ~I410 & ~I1107_1;
  assign I742 = ~I1210_1 | ~I551;
  assign I689 = ~I440 & ~I1128_1;
  assign I693 = ~I376 & ~I1136_1;
  assign I711 = ~I388 & ~I1160_1;
  assign I659 = ~I322 & ~I1083_1;
  assign I669 = ~I342 & ~I1100_1;
  assign I667 = ~I328 & ~I1097_1;
  assign I744 = ~I1213_1 | ~I553;
  assign I740 = ~I1207_1 | ~I477;
  assign I721 = ~I1176_1 & ~I1176_2;
  assign I723 = ~I1180_1 & ~I1180_2;
  assign I737 = ~I1199_1 & ~I1199_2;
  assign I735 = ~I552 & ~I1196_1;
  assign I704 = ~I1151_1 | ~I329;
  assign I699 = ~I1143_1 & ~I1143_2;
  assign I695 = ~I408 & ~I328;
  assign I697 = ~I384 & ~I1140_1;
  assign I665 = ~I540 & ~I1094_1;
  assign I750 = ~I665 | ~I663;
  assign I700 = ~I403 | ~I351;
  assign I708 = ~I1157_1 | ~I351;
  assign I777 = ~I740 & ~I738;
  assign I767 = ~I704 & ~I702;
  assign I386 = ~State_2 & ~I280;
  assign I315 = ~I272 & ~I514;
  assign I347 = ~State_3 & ~I394;
  assign I470 = ~I571 & ~I320 & ~I335;
  assign I333 = ~I850_1 & ~I850_2;
  assign I540 = ~I449 & ~Rdy2RtHS1 & ~I263;
  assign I408 = ~I523 & ~I341 & ~I435;
  always @ (posedge clock) begin
    State_5 <= n80;
    State_4 <= n85;
    State_3 <= n90;
    State_2 <= n95;
    State_1 <= n100;
    State_0 <= n105;
    ActRtHS1 <= n110;
    ActBmHS1 <= n114;
    GoRtHS1 <= n118;
    GoBmHS1 <= n122;
    NewTrHS1 <= n126;
    ReRtTSHS1 <= n130;
    Mode0HS1 <= n134;
    Mode1HS1 <= n138;
    Mode2HS1 <= n142;
    NewLineHS1 <= n146;
    ShftORHS1 <= n150;
    ShftIRHS1 <= n154;
    LxHInHS1 <= n158;
    TxHInHS1 <= n162;
    LoadOHHS1 <= n166;
    LoadIHHS1 <= n170;
    SeOutAvHS1 <= n174;
    SeFullOHS1 <= n178;
    SeFullIHS1 <= n182;
    TgWhBufHS1 <= n186;
    ReWhBufHS1 <= n190;
    LdProgHS1 <= n194;
    DumpIHS1 <= n198;
  end
endmodule


