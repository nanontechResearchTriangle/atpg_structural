// Benchmark "s635.blif" written by ABC on Mon Apr  8 18:07:39 2019

module \s635.blif  ( clock, 
    Enable, LocalReset,
    Out  );
  input  clock;
  input  Enable, LocalReset;
  output Out;
  reg \number.0 , \number.1 , \number.2 , \number.3 , \number.4 ,
    \number.5 , \number.6 , \number.7 , \number.8 , \number.9 ,
    \number.10 , \number.11 , \number.12 , \number.13 , \number.14 ,
    \number.15 , \number.16 , \number.17 , \number.18 , \number.19 ,
    \number.20 , \number.21 , \number.22 , \number.23 , \number.24 ,
    \number.25 , \number.26 , \number.27 , \number.28 , \number.29 ,
    \number.30 , \number.31 ;
  wire I258, I286, I174, I706, I90, I398, I34, I482, I594, I426, I62, I538,
    I734, I370, I9, I32, I872, I622, I846, I650, I314, I202, I566, I678,
    I874, I230, I454, I818, I510, I146, I790, I118, I762, I342, I16,
    \I1.0 , I33, I14, I39, \I1.1 , I61, I67, I68, \I1.2 , I89, I95, I96,
    \I1.3 , I117, I123, I124, \I1.4 , I145, I151, I152, \I1.5 , I173, I179,
    I180, \I1.6 , I201, I207, I208, \I1.7 , I229, I235, I236, \I1.8 , I257,
    I263, I264, \I1.9 , I285, I291, I292, \I1.10 , I313, I319, I320,
    \I1.11 , I341, I347, I348, \I1.12 , I369, I375, I376, \I1.13 , I397,
    I403, I404, \I1.14 , I425, I431, I432, \I1.15 , I453, I459, I460,
    \I1.16 , I481, I487, I488, \I1.17 , I509, I515, I516, \I1.18 , I537,
    I543, I544, \I1.19 , I565, I571, I572, \I1.20 , I593, I599, I600,
    \I1.21 , I621, I627, I628, \I1.22 , I649, I655, I656, \I1.23 , I677,
    I683, I684, \I1.24 , I705, I711, I712, \I1.25 , I733, I739, I740,
    \I1.26 , I761, I767, I768, \I1.27 , I789, I795, I796, \I1.28 , I817,
    I823, I824, \I1.29 , I845, I851, I852, \I1.30 , I879, I880, I877,
    \I890.0.7 , \I890.0.1 , \I890.0.11 , \I890.0.5 , \I890.0.3 ,
    \I890.0.10 , \I890.0.12 , \I890.0.15 , \I890.0.4 , \I890.0.8 ,
    \I890.0.9 , \I890.0.14 , \I890.0.0 , \I890.0.2 , \I890.0.13 ,
    \I890.0.6 , \I890.1.5 , \I890.1.2 , \I890.1.4 , \I890.1.7 , \I890.1.0 ,
    \I890.1.1 , \I890.1.6 , \I890.1.3 , \I890.2.2 , \I890.2.0 , \I890.2.3 ,
    \I890.2.1 , \I890.3.1 , \I890.3.0 , \I19.1 , \I42.1 , I40, \I42.2 ,
    \I70.1 , \I70.2 , \I98.1 , \I98.2 , \I126.1 , \I126.2 , \I154.1 ,
    \I154.2 , \I182.1 , \I182.2 , \I210.1 , \I210.2 , \I238.1 , \I238.2 ,
    \I266.1 , \I266.2 , \I294.1 , \I294.2 , \I322.1 , \I322.2 , \I350.1 ,
    \I350.2 , \I378.1 , \I378.2 , \I406.1 , \I406.2 , \I434.1 , \I434.2 ,
    \I462.1 , \I462.2 , \I490.1 , \I490.2 , \I518.1 , \I518.2 , \I546.1 ,
    \I546.2 , \I574.1 , \I574.2 , \I602.1 , \I602.2 , \I630.1 , \I630.2 ,
    \I658.1 , \I658.2 , \I686.1 , \I686.2 , \I714.1 , \I714.2 , \I742.1 ,
    \I742.2 , \I770.1 , \I770.2 , \I798.1 , \I798.2 , \I826.1 , \I826.2 ,
    \I854.1 , \I854.2 , \I881.2 , \I881.1 , n8, n13, n18, n23, n28, n33,
    n38, n43, n48, n53, n58, n63, n68, n73, n78, n83, n88, n93, n98, n103,
    n108, n113, n118, n123, n128, n133, n138, n143, n148, n153, n158, n163;
  assign Out = \I890.3.0  & \I890.3.1 ;
  assign n8 = ~I14;
  assign n13 = ~\I42.1  | ~\I42.2 ;
  assign n18 = ~\I70.1  | ~\I70.2 ;
  assign n23 = ~\I98.1  | ~\I98.2 ;
  assign n28 = ~\I126.1  | ~\I126.2 ;
  assign n33 = ~\I154.1  | ~\I154.2 ;
  assign n38 = ~\I182.1  | ~\I182.2 ;
  assign n43 = ~\I210.1  | ~\I210.2 ;
  assign n48 = ~\I238.1  | ~\I238.2 ;
  assign n53 = ~\I266.1  | ~\I266.2 ;
  assign n58 = ~\I294.1  | ~\I294.2 ;
  assign n63 = ~\I322.1  | ~\I322.2 ;
  assign n68 = ~\I350.1  | ~\I350.2 ;
  assign n73 = ~\I378.1  | ~\I378.2 ;
  assign n78 = ~\I406.1  | ~\I406.2 ;
  assign n83 = ~\I434.1  | ~\I434.2 ;
  assign n88 = ~\I462.1  | ~\I462.2 ;
  assign n93 = ~\I490.1  | ~\I490.2 ;
  assign n98 = ~\I518.1  | ~\I518.2 ;
  assign n103 = ~\I546.1  | ~\I546.2 ;
  assign n108 = ~\I574.1  | ~\I574.2 ;
  assign n113 = ~\I602.1  | ~\I602.2 ;
  assign n118 = ~\I630.1  | ~\I630.2 ;
  assign n123 = ~\I658.1  | ~\I658.2 ;
  assign n128 = ~\I686.1  | ~\I686.2 ;
  assign n133 = ~\I714.1  | ~\I714.2 ;
  assign n138 = ~\I742.1  | ~\I742.2 ;
  assign n143 = ~\I770.1  | ~\I770.2 ;
  assign n148 = ~\I798.1  | ~\I798.2 ;
  assign n153 = ~\I826.1  | ~\I826.2 ;
  assign n158 = ~\I854.1  | ~\I854.2 ;
  assign n163 = ~I877;
  assign I258 = ~\number.9 ;
  assign I286 = ~\number.10 ;
  assign I174 = ~\number.6 ;
  assign I706 = ~\number.25 ;
  assign I90 = ~\number.3 ;
  assign I398 = ~\number.14 ;
  assign I34 = ~\number.1 ;
  assign I482 = ~\number.17 ;
  assign I594 = ~\number.21 ;
  assign I426 = ~\number.15 ;
  assign I62 = ~\number.2 ;
  assign I538 = ~\number.19 ;
  assign I734 = ~\number.26 ;
  assign I370 = ~\number.13 ;
  assign I9 = ~LocalReset;
  assign I32 = ~LocalReset;
  assign I872 = ~LocalReset;
  assign I622 = ~\number.22 ;
  assign I846 = ~\number.30 ;
  assign I650 = ~\number.23 ;
  assign I314 = ~\number.11 ;
  assign I202 = ~\number.7 ;
  assign I566 = ~\number.20 ;
  assign I678 = ~\number.24 ;
  assign I874 = ~\number.31 ;
  assign I230 = ~\number.8 ;
  assign I454 = ~\number.16 ;
  assign I818 = ~\number.29 ;
  assign I510 = ~\number.18 ;
  assign I146 = ~\number.5 ;
  assign I790 = ~\number.28 ;
  assign I118 = ~\number.4 ;
  assign I762 = ~\number.27 ;
  assign I342 = ~\number.12 ;
  assign I16 = ~Enable | ~\number.0 ;
  assign \I1.0  = ~I16;
  assign I33 = ~\I1.0 ;
  assign I14 = ~I16 | ~\I19.1  | ~I9;
  assign I39 = ~I33;
  assign \I1.1  = ~I34 & ~I40;
  assign I61 = ~\I1.1 ;
  assign I67 = ~I61;
  assign I68 = ~I67;
  assign \I1.2  = ~I62 & ~I68;
  assign I89 = ~\I1.2 ;
  assign I95 = ~I89;
  assign I96 = ~I95;
  assign \I1.3  = ~I90 & ~I96;
  assign I117 = ~\I1.3 ;
  assign I123 = ~I117;
  assign I124 = ~I123;
  assign \I1.4  = ~I118 & ~I124;
  assign I145 = ~\I1.4 ;
  assign I151 = ~I145;
  assign I152 = ~I151;
  assign \I1.5  = ~I146 & ~I152;
  assign I173 = ~\I1.5 ;
  assign I179 = ~I173;
  assign I180 = ~I179;
  assign \I1.6  = ~I174 & ~I180;
  assign I201 = ~\I1.6 ;
  assign I207 = ~I201;
  assign I208 = ~I207;
  assign \I1.7  = ~I202 & ~I208;
  assign I229 = ~\I1.7 ;
  assign I235 = ~I229;
  assign I236 = ~I235;
  assign \I1.8  = ~I230 & ~I236;
  assign I257 = ~\I1.8 ;
  assign I263 = ~I257;
  assign I264 = ~I263;
  assign \I1.9  = ~I258 & ~I264;
  assign I285 = ~\I1.9 ;
  assign I291 = ~I285;
  assign I292 = ~I291;
  assign \I1.10  = ~I286 & ~I292;
  assign I313 = ~\I1.10 ;
  assign I319 = ~I313;
  assign I320 = ~I319;
  assign \I1.11  = ~I314 & ~I320;
  assign I341 = ~\I1.11 ;
  assign I347 = ~I341;
  assign I348 = ~I347;
  assign \I1.12  = ~I342 & ~I348;
  assign I369 = ~\I1.12 ;
  assign I375 = ~I369;
  assign I376 = ~I375;
  assign \I1.13  = ~I370 & ~I376;
  assign I397 = ~\I1.13 ;
  assign I403 = ~I397;
  assign I404 = ~I403;
  assign \I1.14  = ~I398 & ~I404;
  assign I425 = ~\I1.14 ;
  assign I431 = ~I425;
  assign I432 = ~I431;
  assign \I1.15  = ~I426 & ~I432;
  assign I453 = ~\I1.15 ;
  assign I459 = ~I453;
  assign I460 = ~I459;
  assign \I1.16  = ~I454 & ~I460;
  assign I481 = ~\I1.16 ;
  assign I487 = ~I481;
  assign I488 = ~I487;
  assign \I1.17  = ~I482 & ~I488;
  assign I509 = ~\I1.17 ;
  assign I515 = ~I509;
  assign I516 = ~I515;
  assign \I1.18  = ~I510 & ~I516;
  assign I537 = ~\I1.18 ;
  assign I543 = ~I537;
  assign I544 = ~I543;
  assign \I1.19  = ~I538 & ~I544;
  assign I565 = ~\I1.19 ;
  assign I571 = ~I565;
  assign I572 = ~I571;
  assign \I1.20  = ~I566 & ~I572;
  assign I593 = ~\I1.20 ;
  assign I599 = ~I593;
  assign I600 = ~I599;
  assign \I1.21  = ~I594 & ~I600;
  assign I621 = ~\I1.21 ;
  assign I627 = ~I621;
  assign I628 = ~I627;
  assign \I1.22  = ~I622 & ~I628;
  assign I649 = ~\I1.22 ;
  assign I655 = ~I649;
  assign I656 = ~I655;
  assign \I1.23  = ~I650 & ~I656;
  assign I677 = ~\I1.23 ;
  assign I683 = ~I677;
  assign I684 = ~I683;
  assign \I1.24  = ~I678 & ~I684;
  assign I705 = ~\I1.24 ;
  assign I711 = ~I705;
  assign I712 = ~I711;
  assign \I1.25  = ~I706 & ~I712;
  assign I733 = ~\I1.25 ;
  assign I739 = ~I733;
  assign I740 = ~I739;
  assign \I1.26  = ~I734 & ~I740;
  assign I761 = ~\I1.26 ;
  assign I767 = ~I761;
  assign I768 = ~I767;
  assign \I1.27  = ~I762 & ~I768;
  assign I789 = ~\I1.27 ;
  assign I795 = ~I789;
  assign I796 = ~I795;
  assign \I1.28  = ~I790 & ~I796;
  assign I817 = ~\I1.28 ;
  assign I823 = ~I817;
  assign I824 = ~I823;
  assign \I1.29  = ~I818 & ~I824;
  assign I845 = ~\I1.29 ;
  assign I851 = ~I845;
  assign I852 = ~I851;
  assign \I1.30  = ~I846 & ~I852;
  assign I879 = ~\I1.30 ;
  assign I880 = ~I879;
  assign I877 = ~I872 | ~\I881.1  | ~\I881.2 ;
  assign \I890.0.7  = \number.14  & \number.15 ;
  assign \I890.0.1  = \number.2  & \number.3 ;
  assign \I890.0.11  = \number.22  & \number.23 ;
  assign \I890.0.5  = \number.10  & \number.11 ;
  assign \I890.0.3  = \number.6  & \number.7 ;
  assign \I890.0.10  = \number.20  & \number.21 ;
  assign \I890.0.12  = \number.24  & \number.25 ;
  assign \I890.0.15  = \number.30  & \number.31 ;
  assign \I890.0.4  = \number.8  & \number.9 ;
  assign \I890.0.8  = \number.16  & \number.17 ;
  assign \I890.0.9  = \number.18  & \number.19 ;
  assign \I890.0.14  = \number.28  & \number.29 ;
  assign \I890.0.0  = \number.0  & \number.1 ;
  assign \I890.0.2  = \number.4  & \number.5 ;
  assign \I890.0.13  = \number.26  & \number.27 ;
  assign \I890.0.6  = \number.12  & \number.13 ;
  assign \I890.1.5  = \I890.0.10  & \I890.0.11 ;
  assign \I890.1.2  = \I890.0.4  & \I890.0.5 ;
  assign \I890.1.4  = \I890.0.8  & \I890.0.9 ;
  assign \I890.1.7  = \I890.0.14  & \I890.0.15 ;
  assign \I890.1.0  = \I890.0.0  & \I890.0.1 ;
  assign \I890.1.1  = \I890.0.2  & \I890.0.3 ;
  assign \I890.1.6  = \I890.0.12  & \I890.0.13 ;
  assign \I890.1.3  = \I890.0.6  & \I890.0.7 ;
  assign \I890.2.2  = \I890.1.4  & \I890.1.5 ;
  assign \I890.2.0  = \I890.1.0  & \I890.1.1 ;
  assign \I890.2.3  = \I890.1.6  & \I890.1.7 ;
  assign \I890.2.1  = \I890.1.2  & \I890.1.3 ;
  assign \I890.3.1  = \I890.2.2  & \I890.2.3 ;
  assign \I890.3.0  = \I890.2.0  & \I890.2.1 ;
  assign \I19.1  = Enable | \number.0 ;
  assign \I42.1  = I39 | LocalReset | I34;
  assign I40 = ~I32 | ~I39;
  assign \I42.2  = \number.1  | I40;
  assign \I70.1  = I67 | LocalReset | I62;
  assign \I70.2  = \number.2  | I68;
  assign \I98.1  = I95 | LocalReset | I90;
  assign \I98.2  = \number.3  | I96;
  assign \I126.1  = I123 | LocalReset | I118;
  assign \I126.2  = \number.4  | I124;
  assign \I154.1  = I151 | LocalReset | I146;
  assign \I154.2  = \number.5  | I152;
  assign \I182.1  = I179 | LocalReset | I174;
  assign \I182.2  = \number.6  | I180;
  assign \I210.1  = I207 | LocalReset | I202;
  assign \I210.2  = \number.7  | I208;
  assign \I238.1  = I235 | LocalReset | I230;
  assign \I238.2  = \number.8  | I236;
  assign \I266.1  = I263 | LocalReset | I258;
  assign \I266.2  = \number.9  | I264;
  assign \I294.1  = I291 | LocalReset | I286;
  assign \I294.2  = \number.10  | I292;
  assign \I322.1  = I319 | LocalReset | I314;
  assign \I322.2  = \number.11  | I320;
  assign \I350.1  = I347 | LocalReset | I342;
  assign \I350.2  = \number.12  | I348;
  assign \I378.1  = I375 | LocalReset | I370;
  assign \I378.2  = \number.13  | I376;
  assign \I406.1  = I403 | LocalReset | I398;
  assign \I406.2  = \number.14  | I404;
  assign \I434.1  = I431 | LocalReset | I426;
  assign \I434.2  = \number.15  | I432;
  assign \I462.1  = I459 | LocalReset | I454;
  assign \I462.2  = \number.16  | I460;
  assign \I490.1  = I487 | LocalReset | I482;
  assign \I490.2  = \number.17  | I488;
  assign \I518.1  = I515 | LocalReset | I510;
  assign \I518.2  = \number.18  | I516;
  assign \I546.1  = I543 | LocalReset | I538;
  assign \I546.2  = \number.19  | I544;
  assign \I574.1  = I571 | LocalReset | I566;
  assign \I574.2  = \number.20  | I572;
  assign \I602.1  = I599 | LocalReset | I594;
  assign \I602.2  = \number.21  | I600;
  assign \I630.1  = I627 | LocalReset | I622;
  assign \I630.2  = \number.22  | I628;
  assign \I658.1  = I655 | LocalReset | I650;
  assign \I658.2  = \number.23  | I656;
  assign \I686.1  = I683 | LocalReset | I678;
  assign \I686.2  = \number.24  | I684;
  assign \I714.1  = I711 | LocalReset | I706;
  assign \I714.2  = \number.25  | I712;
  assign \I742.1  = I739 | LocalReset | I734;
  assign \I742.2  = \number.26  | I740;
  assign \I770.1  = I767 | LocalReset | I762;
  assign \I770.2  = \number.27  | I768;
  assign \I798.1  = I795 | LocalReset | I790;
  assign \I798.2  = \number.28  | I796;
  assign \I826.1  = I823 | LocalReset | I818;
  assign \I826.2  = \number.29  | I824;
  assign \I854.1  = I851 | LocalReset | I846;
  assign \I854.2  = \number.30  | I852;
  assign \I881.2  = I874 | I879;
  assign \I881.1  = \number.31  | I880;
  always @ (posedge clock) begin
    \number.0  <= n8;
    \number.1  <= n13;
    \number.2  <= n18;
    \number.3  <= n23;
    \number.4  <= n28;
    \number.5  <= n33;
    \number.6  <= n38;
    \number.7  <= n43;
    \number.8  <= n48;
    \number.9  <= n53;
    \number.10  <= n58;
    \number.11  <= n63;
    \number.12  <= n68;
    \number.13  <= n73;
    \number.14  <= n78;
    \number.15  <= n83;
    \number.16  <= n88;
    \number.17  <= n93;
    \number.18  <= n98;
    \number.19  <= n103;
    \number.20  <= n108;
    \number.21  <= n113;
    \number.22  <= n118;
    \number.23  <= n123;
    \number.24  <= n128;
    \number.25  <= n133;
    \number.26  <= n138;
    \number.27  <= n143;
    \number.28  <= n148;
    \number.29  <= n153;
    \number.30  <= n158;
    \number.31  <= n163;
  end
endmodule


