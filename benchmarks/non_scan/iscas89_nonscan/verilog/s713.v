// Benchmark "s713.blif" written by ABC on Mon Apr  8 18:08:07 2019

module \s713.blif  ( clock, 
    G1, G2, G3, G4, G5, G6, G8, G9, G10, G11, G12, G13, G14, G15, G16, G17,
    G18, G19, G20, G21, G22, G23, G24, G25, G26, G27, G28, G29, G30, G31,
    G32, G33, G34, G35, G36,
    G103BF, G104BF, G105BF, G106BF, G107, G83, G84, G85, G86BF, G87BF,
    G88BF, G89BF, G90, G91, G92, G94, G95BF, G96BF, G97BF, G98BF, G99BF,
    G100BF, G101BF  );
  input  clock;
  input  G1, G2, G3, G4, G5, G6, G8, G9, G10, G11, G12, G13, G14, G15,
    G16, G17, G18, G19, G20, G21, G22, G23, G24, G25, G26, G27, G28, G29,
    G30, G31, G32, G33, G34, G35, G36;
  output G103BF, G104BF, G105BF, G106BF, G107, G83, G84, G85, G86BF, G87BF,
    G88BF, G89BF, G90, G91, G92, G94, G95BF, G96BF, G97BF, G98BF, G99BF,
    G100BF, G101BF;
  reg G64, G65, G66, G67, G68, G69, G70, G71, G72, G73, G74, G75, G76, G77,
    G78, G79, G80, G81, G82;
  wire I633, G366, G379, I643, I646, I649, I652, I655, I660, I680, I684,
    I687, I165, II178, I169, G113, I172, G115, I175, G117, I178, G219,
    I181, G119, I184, G221, I187, G121, I190, G223, I193, G209, I196, G109,
    I199, G211, I202, G111, I205, G213, I208, G215, I211, G217, G352, G360,
    G361, G362, G363, G364, G367, G386, G388, G389, G110, G114, G118, G216,
    G218, G220, G222, G365, G368, G387, G225, G390, G289, I356, G324, I254,
    G166, I257, G325, G338, I260, G194, I263, G339, G344, I266, G202, I269,
    G345, G312, I272, G313, G315, I275, G316, G318, I278, G319, G321, I281,
    G322, G143, I287, G381, I291, G375, I295, G371, I303, G350, G281, I299,
    G283, I313, G382, G100, G376, G98, G372, G96, I301, I315, G135, I321,
    G329, G137, I324, G333, G87, I406, G89, I422, G173, G183, I335, G174,
    I338, G184, I341, G355, G359, G356, G108, G116, G293, I354, G146, I357,
    G294, G309, I360, G162, I363, G310, G341, I366, G198, I369, G342, G303,
    I372, G154, I375, G304, I378, G383, I382, G396, I386, G373, I390, G392,
    G384, G101, G397, G106, G374, G97, G393, G104, II476, G278, I279, G224,
    G282, I306, G286, I334, G285, I327, G268, II208, I308, I336, I329,
    I210, G136, I442, G331, G88, I414, G178, I449, G179, I452, G357, G358,
    G112, G335, I460, G190, I463, G336, G306, I466, G158, I469, G307, I472,
    G377, I476, G378, G99, G395, G105, G277, II272, G276, I265, G284, I320,
    G279, I285, G280, I292, I322, II287, I294, G134, I517, G327, G86, I398,
    G168, I524, G169, I527, G353, G354, G120, G347, I535, G206, I538, G348,
    G300, I541, G150, I544, G301, I547, G369, I551, G370, G95, G391, G103,
    G271, I230, G275, I258, G288, I348, G287, II341, G270, I222, I350,
    I343, G272, I237, G273, I244, G274, I251, I224, G124, I608, G298, G231,
    G232, G233, G234, G247, G248, G263, G264, G214, G210, G240, G266, G229,
    G245, G253, I533, G227, G243, G249, G265, G236, G237, G252, II527,
    G212, G251, I512, II538, G228, G244, G256, G230, G235, G246, I515,
    G261, G208, I495, G255, G257, I537, G226, G242, I553, G241, G267, G238,
    G239, G254, I518, I521, II524, G258, G259, G260, I546, I300, I314,
    I307, II335, I328, I209, II321, I286, I293, I349, I342, I223, n118,
    n123, n128, n133, n138, n143, n148, n153, n158, n163, n168, n173, n178,
    n183, n188, n193, n198, n203, n208;
  assign G103BF = ~G103;
  assign G104BF = ~G104;
  assign G105BF = ~G105;
  assign G106BF = ~G106;
  assign G107 = G313 & G18;
  assign G83 = G316 & G19;
  assign G84 = G319 & G20;
  assign G85 = G322 & G21;
  assign G86BF = ~G86;
  assign G87BF = ~G87;
  assign G88BF = ~G88;
  assign G89BF = ~G89;
  assign G90 = G298 & G26;
  assign G91 = ~I165;
  assign G92 = G350 & G28;
  assign G94 = ~II178;
  assign G95BF = ~G95;
  assign G96BF = ~G96;
  assign G97BF = ~G97;
  assign G98BF = ~G98;
  assign G99BF = ~G99;
  assign G100BF = ~G100;
  assign G101BF = ~G101;
  assign n118 = ~I551;
  assign n123 = G397 & G395 & G366 & G392;
  assign n128 = ~I476;
  assign n133 = G366 & G396;
  assign n138 = ~I209 | ~I210;
  assign n143 = ~I286 | ~II287;
  assign n148 = ~II321 | ~I322;
  assign n153 = ~I328 | ~I329;
  assign n158 = ~II335 | ~I336;
  assign n163 = ~I342 | ~I343;
  assign n168 = ~I349 | ~I350;
  assign n173 = ~I230;
  assign n178 = ~I237;
  assign n183 = ~I244;
  assign n188 = ~I251;
  assign n193 = ~I258;
  assign n198 = ~I265;
  assign n203 = ~II272;
  assign n208 = ~I279;
  assign I633 = ~G1;
  assign G366 = ~G2;
  assign G379 = ~G3;
  assign I643 = ~G4;
  assign I646 = ~G5;
  assign I649 = ~G6;
  assign I652 = ~G8;
  assign I655 = ~G9;
  assign I660 = ~G10;
  assign I680 = ~G11;
  assign I684 = ~G12;
  assign I687 = ~G13;
  assign I165 = ~G27;
  assign II178 = ~G29;
  assign I169 = ~G70;
  assign G113 = ~I169;
  assign I172 = ~G71;
  assign G115 = ~I172;
  assign I175 = ~G72;
  assign G117 = ~I175;
  assign I178 = ~G80;
  assign G219 = ~I178;
  assign I181 = ~G73;
  assign G119 = ~I181;
  assign I184 = ~G81;
  assign G221 = ~I184;
  assign I187 = ~G74;
  assign G121 = ~I187;
  assign I190 = ~G82;
  assign G223 = ~I190;
  assign I193 = ~G75;
  assign G209 = ~I193;
  assign I196 = ~G68;
  assign G109 = ~I196;
  assign I199 = ~G76;
  assign G211 = ~I199;
  assign I202 = ~G69;
  assign G111 = ~I202;
  assign I205 = ~G77;
  assign G213 = ~I205;
  assign I208 = ~G78;
  assign G215 = ~I208;
  assign I211 = ~G79;
  assign G217 = ~I211;
  assign G352 = ~I633;
  assign G360 = ~I643;
  assign G361 = ~I646;
  assign G362 = ~I649;
  assign G363 = ~I652;
  assign G364 = ~I655;
  assign G367 = ~I660;
  assign G386 = ~I680;
  assign G388 = ~I684;
  assign G389 = ~I687;
  assign G110 = ~G360;
  assign G114 = ~G360;
  assign G118 = ~G360;
  assign G216 = ~G360;
  assign G218 = ~G360;
  assign G220 = ~G360;
  assign G222 = ~G360;
  assign G365 = ~G364;
  assign G368 = ~G367;
  assign G387 = ~G386;
  assign G225 = ~G388;
  assign G390 = ~G389;
  assign G289 = G389 & G386 & G388;
  assign I356 = ~G289;
  assign G324 = G110 & G111;
  assign I254 = ~G324;
  assign G166 = ~I254;
  assign I257 = ~G324;
  assign G325 = ~I257;
  assign G338 = G114 & G115;
  assign I260 = ~G338;
  assign G194 = ~I260;
  assign I263 = ~G338;
  assign G339 = ~I263;
  assign G344 = G118 & G119;
  assign I266 = ~G344;
  assign G202 = ~I266;
  assign I269 = ~G344;
  assign G345 = ~I269;
  assign G312 = G216 & G217;
  assign I272 = ~G312;
  assign G313 = ~I272;
  assign G315 = G218 & G219;
  assign I275 = ~G315;
  assign G316 = ~I275;
  assign G318 = G220 & G221;
  assign I278 = ~G318;
  assign G319 = ~I278;
  assign G321 = G222 & G223;
  assign I281 = ~G321;
  assign G322 = ~I281;
  assign G143 = ~I356;
  assign I287 = ~G166;
  assign G381 = ~I287;
  assign I291 = ~G194;
  assign G375 = ~I291;
  assign I295 = ~G202;
  assign G371 = ~I295;
  assign I303 = ~G143;
  assign G350 = ~I303;
  assign G281 = G65 | G232 | G248;
  assign I299 = ~G281;
  assign G283 = G264 | G234 | G67;
  assign I313 = ~G283;
  assign G382 = ~G381;
  assign G100 = G325 & G35;
  assign G376 = ~G375;
  assign G98 = G339 & G33;
  assign G372 = ~G371;
  assign G96 = G345 & G31;
  assign I301 = ~I299;
  assign I315 = ~I313;
  assign G135 = ~I300 | ~I301;
  assign I321 = ~G135;
  assign G329 = ~I321;
  assign G137 = ~I314 | ~I315;
  assign I324 = ~G137;
  assign G333 = ~I324;
  assign G87 = G329 & G23;
  assign I406 = ~G87;
  assign G89 = G333 & G25;
  assign I422 = ~G89;
  assign G173 = ~I406;
  assign G183 = ~I422;
  assign I335 = ~G173;
  assign G174 = ~I335;
  assign I338 = ~G183;
  assign G184 = ~I338;
  assign I341 = ~G174;
  assign G355 = ~I341;
  assign G359 = ~G184;
  assign G356 = ~G355;
  assign G108 = ~G359;
  assign G116 = ~G356;
  assign G293 = G108 & G109;
  assign I354 = ~G293;
  assign G146 = ~I354;
  assign I357 = ~G293;
  assign G294 = ~I357;
  assign G309 = G214 & G215;
  assign I360 = ~G309;
  assign G162 = ~I360;
  assign I363 = ~G309;
  assign G310 = ~I363;
  assign G341 = G116 & G117;
  assign I366 = ~G341;
  assign G198 = ~I366;
  assign I369 = ~G341;
  assign G342 = ~I369;
  assign G303 = G210 & G211;
  assign I372 = ~G303;
  assign G154 = ~I372;
  assign I375 = ~G303;
  assign G304 = ~I375;
  assign I378 = ~G146;
  assign G383 = ~I378;
  assign I382 = ~G162;
  assign G396 = ~I382;
  assign I386 = ~G198;
  assign G373 = ~I386;
  assign I390 = ~G154;
  assign G392 = ~I390;
  assign G384 = ~G383;
  assign G101 = G294 & G36;
  assign G397 = ~G396;
  assign G106 = G310 & G17;
  assign G374 = ~G373;
  assign G97 = G342 & G32;
  assign G393 = ~G392;
  assign G104 = G304 & G15;
  assign II476 = ~G384;
  assign G278 = G366 & G396;
  assign I279 = ~G278;
  assign G224 = ~II476;
  assign G282 = G263 | G233 | G249;
  assign I306 = ~G282;
  assign G286 = G237 | G253;
  assign I334 = ~G286;
  assign G285 = G236 | G252;
  assign I327 = ~G285;
  assign G268 = G224 | G240;
  assign II208 = ~G268;
  assign I308 = ~I306;
  assign I336 = ~I334;
  assign I329 = ~I327;
  assign I210 = ~II208;
  assign G136 = ~I307 | ~I308;
  assign I442 = ~G136;
  assign G331 = ~I442;
  assign G88 = G331 & G24;
  assign I414 = ~G88;
  assign G178 = ~I414;
  assign I449 = ~G178;
  assign G179 = ~I449;
  assign I452 = ~G179;
  assign G357 = ~I452;
  assign G358 = ~G357;
  assign G112 = ~G358;
  assign G335 = G112 & G113;
  assign I460 = ~G335;
  assign G190 = ~I460;
  assign I463 = ~G335;
  assign G336 = ~I463;
  assign G306 = G212 & G213;
  assign I466 = ~G306;
  assign G158 = ~I466;
  assign I469 = ~G306;
  assign G307 = ~I469;
  assign I472 = ~G190;
  assign G377 = ~I472;
  assign I476 = ~G158;
  assign G378 = ~G377;
  assign G99 = G336 & G34;
  assign G395 = ~G158;
  assign G105 = G307 & G16;
  assign G277 = G397 & G366 & G158;
  assign II272 = ~G277;
  assign G276 = G397 & G395 & G366 & G392;
  assign I265 = ~G276;
  assign G284 = G235 | G251;
  assign I320 = ~G284;
  assign G279 = G230 | G246;
  assign I285 = ~G279;
  assign G280 = G261 | G231 | G247;
  assign I292 = ~G280;
  assign I322 = ~I320;
  assign II287 = ~I285;
  assign I294 = ~I292;
  assign G134 = ~I293 | ~I294;
  assign I517 = ~G134;
  assign G327 = ~I517;
  assign G86 = G327 & G22;
  assign I398 = ~G86;
  assign G168 = ~I398;
  assign I524 = ~G168;
  assign G169 = ~I524;
  assign I527 = ~G169;
  assign G353 = ~I527;
  assign G354 = ~G353;
  assign G120 = ~G354;
  assign G347 = G120 & G121;
  assign I535 = ~G347;
  assign G206 = ~I535;
  assign I538 = ~G347;
  assign G348 = ~I538;
  assign G300 = G208 & G209;
  assign I541 = ~G300;
  assign G150 = ~I541;
  assign I544 = ~G300;
  assign G301 = ~I544;
  assign I547 = ~G206;
  assign G369 = ~I547;
  assign I551 = ~G150;
  assign G370 = ~G369;
  assign G95 = G348 & G30;
  assign G391 = ~G150;
  assign G103 = G301 & G14;
  assign G271 = G257 | G226 | G242;
  assign I230 = ~G271;
  assign G275 = I553 & G395 & G397;
  assign I258 = ~G275;
  assign G288 = G239 | G255;
  assign I348 = ~G288;
  assign G287 = G238 | G254;
  assign II341 = ~G287;
  assign G270 = G265 | G266 | G267 | I546;
  assign I222 = ~G270;
  assign I350 = ~I348;
  assign I343 = ~II341;
  assign G272 = G258 | G227 | G243;
  assign I237 = ~G272;
  assign G273 = G259 | G228 | G244;
  assign I244 = ~G273;
  assign G274 = G260 | G229 | G245;
  assign I251 = ~G274;
  assign I224 = ~I222;
  assign G124 = ~I223 | ~I224;
  assign I608 = ~G124;
  assign G298 = ~I608;
  assign G231 = G379 & G387;
  assign G232 = G379 & G387;
  assign G233 = G379 & G387;
  assign G234 = G379 & G387;
  assign G247 = G390 & G368 & G379 & G365;
  assign G248 = G390 & G367 & G379 & G365;
  assign G263 = G390 & G368 & G379 & G364;
  assign G264 = G390 & G367 & G379 & G364;
  assign G214 = ~G379 | ~G359;
  assign G210 = ~G379 | ~G356;
  assign G240 = G359 & G383;
  assign G266 = G390 & G383 & G364 & G367;
  assign G229 = G366 & G396;
  assign G245 = G352 & G396;
  assign G253 = G375 & G356 & G373;
  assign I533 = G373 & G365 & G367;
  assign G227 = G366 & G392;
  assign G243 = G392 & G361;
  assign G249 = G397 & G366 & G66;
  assign G265 = I533 & G375 & G390;
  assign G236 = G374 & G376;
  assign G237 = G374 & G375;
  assign G252 = G375 & G355 & G374;
  assign II527 = G393 & G366 & G64;
  assign G212 = ~G379 | ~G358;
  assign G251 = G381 & G358 & G377;
  assign I512 = G377 & G364 & G368;
  assign II538 = G387 & G383 & G377 & G381;
  assign G228 = G366 & G158;
  assign G244 = G158 & G362;
  assign G256 = I512 & G381 & G390;
  assign G230 = G378 & G382;
  assign G235 = G378 & G381;
  assign G246 = G381 & G357 & G378;
  assign I515 = G397 & G393 & G395;
  assign G261 = II527 & G395 & G397;
  assign G208 = ~G379 | ~G354;
  assign I495 = G369 & G365 & G368;
  assign G255 = G371 & G354 & G369;
  assign G257 = I515 & G371 & G363 & G369;
  assign I537 = G375 & G373 & G369 & G371;
  assign G226 = G366 & G150;
  assign G242 = G150 & G363;
  assign I553 = G393 & G366 & G150;
  assign G241 = I495 & G371 & G390;
  assign G267 = I537 & II538;
  assign G238 = G370 & G372;
  assign G239 = G370 & G371;
  assign G254 = G371 & G353 & G370;
  assign I518 = G397 & G391 & G395;
  assign I521 = G397 & G391 & G393;
  assign II524 = G393 & G352 & G391;
  assign G258 = I518 & G375 & G361 & G373;
  assign G259 = I521 & G381 & G362 & G377;
  assign G260 = II524 & G395 & G383;
  assign I546 = G256 | G225 | G241;
  assign I300 = ~G281 | ~I299;
  assign I314 = ~G283 | ~I313;
  assign I307 = ~G282 | ~I306;
  assign II335 = ~G286 | ~I334;
  assign I328 = ~G285 | ~I327;
  assign I209 = ~G268 | ~II208;
  assign II321 = ~G284 | ~I320;
  assign I286 = ~G279 | ~I285;
  assign I293 = ~G280 | ~I292;
  assign I349 = ~G288 | ~I348;
  assign I342 = ~G287 | ~II341;
  assign I223 = ~G270 | ~I222;
  always @ (posedge clock) begin
    G64 <= n118;
    G65 <= n123;
    G66 <= n128;
    G67 <= n133;
    G68 <= n138;
    G69 <= n143;
    G70 <= n148;
    G71 <= n153;
    G72 <= n158;
    G73 <= n163;
    G74 <= n168;
    G75 <= n173;
    G76 <= n178;
    G77 <= n183;
    G78 <= n188;
    G79 <= n193;
    G80 <= n198;
    G81 <= n203;
    G82 <= n208;
  end
endmodule


