// Benchmark "s838.1.blif" written by ABC on Mon Apr  8 18:08:51 2019

module \s838.1.blif  ( clock, 
    \P.0 , \C.32 , \C.31 , \C.30 , \C.29 , \C.28 , \C.27 , \C.26 , \C.25 ,
    \C.24 , \C.23 , \C.22 , \C.21 , \C.20 , \C.19 , \C.18 , \C.17 , \C.16 ,
    \C.15 , \C.14 , \C.13 , \C.12 , \C.11 , \C.10 , \C.9 , \C.8 , \C.7 ,
    \C.6 , \C.5 , \C.4 , \C.3 , \C.2 , \C.1 , \C.0 ,
    Z  );
  input  clock;
  input  \P.0 , \C.32 , \C.31 , \C.30 , \C.29 , \C.28 , \C.27 , \C.26 ,
    \C.25 , \C.24 , \C.23 , \C.22 , \C.21 , \C.20 , \C.19 , \C.18 , \C.17 ,
    \C.16 , \C.15 , \C.14 , \C.13 , \C.12 , \C.11 , \C.10 , \C.9 , \C.8 ,
    \C.7 , \C.6 , \C.5 , \C.4 , \C.3 , \C.2 , \C.1 , \C.0 ;
  output Z;
  reg \X.4 , \X.3 , \X.2 , \X.1 , \X.8 , \X.7 , \X.6 , \X.5 , \X.12 ,
    \X.11 , \X.10 , \X.9 , \X.16 , \X.15 , \X.14 , \X.13 , \X.20 , \X.19 ,
    \X.18 , \X.17 , \X.24 , \X.23 , \X.22 , \X.21 , \X.28 , \X.27 , \X.26 ,
    \X.25 , \X.32 , \X.31 , \X.30 , \X.29 ;
  wire I69, \I73.1 , \I73.2 , I66, \I7.1 , \I7.2 , \I88.1 , \I88.2 , I48,
    I49, I50, I68, I167, \I171.1 , \I171.2 , I164, \I105.1 , \I105.2 ,
    \I186.1 , \I1.2 , \I186.2 , I146, I147, I148, I166, I265, \I269.1 ,
    \I269.2 , I262, \I203.1 , \I203.2 , \I284.1 , \I1.3 , \I284.2 , I244,
    I245, I246, I264, I363, \I367.1 , \I367.2 , I360, \I301.1 , \I301.2 ,
    \I382.1 , \I1.4 , \I382.2 , I342, I343, I344, I362, I461, \I465.1 ,
    \I465.2 , I458, \I399.1 , \I399.2 , \I480.1 , \I1.5 , \I480.2 , I440,
    I441, I442, I460, I559, \I563.1 , \I563.2 , I556, \I497.1 , \I497.2 ,
    \I578.1 , \I1.6 , \I578.2 , I538, I539, I540, I558, I657, \I661.1 ,
    \I661.2 , I654, \I595.1 , \I595.2 , \I676.1 , \I1.7 , \I676.2 , I636,
    I637, I638, I656, I751, \I693.1 , \I693.2 , \I770.1 , \I1.8 , \I770.2 ,
    I736, I737, I750, I749, I752, I806, I807, I808, I809, I810, I818, I819,
    I834, I835, I836, I837, I838, I846, I847, I862, I863, I864, I865, I866,
    I874, I875, I890, I891, I892, I893, I894, I902, I903, I918, I919, I920,
    I921, I922, I930, I931, I946, I947, I948, I949, I950, I958, I959, I974,
    I975, I976, I977, I978, I986, I987, I1002, I1003, I1004, I1005, I1006,
    I1013, I1014, \P.2 , I1074, \P.3 , I1075, I1078, I1079, \P.6 , I1098,
    \P.7 , I1099, I1102, I1103, \P.10 , I1122, \P.11 , I1123, I1126, I1127,
    \P.14 , I1146, \P.15 , I1147, I1150, I1151, \P.18 , I1170, \P.19 ,
    I1171, I1174, I1175, \P.22 , I1194, \P.23 , I1195, I1198, I1199,
    \P.26 , I1218, \P.27 , I1219, I1222, I1223, \P.30 , I1242, \P.31 ,
    I1243, I1246, I1247, \I73.3 , \I73.4 , \I7.3 , \I7.4 , \I88.3 ,
    \I88.4 , \I171.3 , \I171.4 , \I105.3 , \I105.4 , \I186.3 , \I186.4 ,
    \I269.3 , \I269.4 , \I203.3 , \I203.4 , \I284.3 , \I284.4 , \I367.3 ,
    \I367.4 , \I301.3 , \I301.4 , \I382.3 , \I382.4 , \I465.3 , \I465.4 ,
    \I399.3 , \I399.4 , \I480.3 , \I480.4 , \I563.3 , \I563.4 , \I497.3 ,
    \I497.4 , \I578.3 , \I578.4 , \I661.3 , \I661.4 , \I595.3 , \I595.4 ,
    \I676.3 , \I676.4 , \I693.3 , \I693.4 , \I770.3 , \I770.4 , \I779.1 ,
    \I2.1 , \I2.2 , \I2.3 , \I2.4 , \I2.5 , \I2.6 , \I2.7 , \I803.1 ,
    \I803.2 , \I804.2 , \I803.3 , \I804.3 , \I803.4 , \I804.4 , \I803.5 ,
    \I804.5 , \I803.6 , \I804.6 , \I803.7 , \I804.7 , \I799.2 , \P.5 ,
    \I800.2 , \I801.2 , \I802.2 , \P.8 , \I799.3 , \P.9 , \I800.3 ,
    \I801.3 , \I802.3 , \P.12 , \I799.4 , \P.13 , \I800.4 , \I801.4 ,
    \I802.4 , \P.16 , \I799.5 , \P.17 , \I800.5 , \I801.5 , \I802.5 ,
    \P.20 , \I799.6 , \P.21 , \I800.6 , \I801.6 , \I802.6 , \P.24 ,
    \I799.7 , \P.25 , \I800.7 , \I801.7 , \I802.7 , \P.28 , \I799.8 ,
    \P.29 , \I800.8 , \I801.8 , \I802.8 , \P.32 , \P.1 , \I1087.1 ,
    \I1087.2 , \I1111.1 , \P.4 , \I1111.2 , \I1135.1 , \I1135.2 ,
    \I1159.1 , \I1159.2 , \I1183.1 , \I1183.2 , \I1207.1 , \I1207.2 ,
    \I1231.1 , \I1231.2 , \I1255.1 , \I1255.2 , \I1062.9 , \I70.1 , I64,
    \I95.1 , \I168.1 , I162, \I193.1 , \I266.1 , I260, \I291.1 , \I364.1 ,
    I358, \I389.1 , \I462.1 , I456, \I487.1 , \I560.1 , I554, \I585.1 ,
    \I658.1 , I652, \I683.1 , I753, \I755.1 , \I758.1 , \I776.1 ,
    \I1083.1 , \I1083.2 , \I1107.1 , \I1107.2 , \I1131.1 , \I1131.2 ,
    \I1155.1 , \I1155.2 , \I1179.1 , \I1179.2 , \I1203.1 , \I1203.2 ,
    \I1227.1 , \I1227.2 , \I1251.1 , \I1251.2 , \I1061.1 , \I1061.2 ,
    \I1062.2 , \I1061.3 , \I1062.3 , \I1061.4 , \I1062.4 , \I1061.5 ,
    \I1062.5 , \I1061.6 , \I1062.6 , \I1061.7 , \I1062.7 , \I1061.8 ,
    \I1062.8 , I62, I160, I258, I356, I454, I552, I650, I747, I816, I844,
    I872, I900, I928, I956, I984, I1011, I1082, I1106, I1130, I1154, I1178,
    I1202, I1226, I1250, n72, n77, n82, n87, n92, n97, n102, n107, n112,
    n117, n122, n127, n132, n137, n142, n147, n152, n157, n162, n167, n172,
    n177, n182, n187, n192, n197, n202, n207, n212, n217, n222, n227;
  assign Z = \I1062.8  | \I1062.9 ;
  assign n72 = ~\I70.1  | ~I62;
  assign n77 = \I73.3  | \I73.4 ;
  assign n82 = ~\I7.3  & ~\I7.4 ;
  assign n87 = \I88.3  | \I88.4 ;
  assign n92 = ~\I168.1  | ~I160;
  assign n97 = \I171.3  | \I171.4 ;
  assign n102 = ~\I105.3  & ~\I105.4 ;
  assign n107 = \I186.3  | \I186.4 ;
  assign n112 = ~\I266.1  | ~I258;
  assign n117 = \I269.3  | \I269.4 ;
  assign n122 = ~\I203.3  & ~\I203.4 ;
  assign n127 = \I284.3  | \I284.4 ;
  assign n132 = ~\I364.1  | ~I356;
  assign n137 = \I367.3  | \I367.4 ;
  assign n142 = ~\I301.3  & ~\I301.4 ;
  assign n147 = \I382.3  | \I382.4 ;
  assign n152 = ~\I462.1  | ~I454;
  assign n157 = \I465.3  | \I465.4 ;
  assign n162 = ~\I399.3  & ~\I399.4 ;
  assign n167 = \I480.3  | \I480.4 ;
  assign n172 = ~\I560.1  | ~I552;
  assign n177 = \I563.3  | \I563.4 ;
  assign n182 = ~\I497.3  & ~\I497.4 ;
  assign n187 = \I578.3  | \I578.4 ;
  assign n192 = ~\I658.1  | ~I650;
  assign n197 = \I661.3  | \I661.4 ;
  assign n202 = ~\I595.3  & ~\I595.4 ;
  assign n207 = \I676.3  | \I676.4 ;
  assign n212 = ~\I755.1  | ~I747;
  assign n217 = ~\I758.1  | ~I749;
  assign n222 = ~\I693.3  & ~\I693.4 ;
  assign n227 = \I770.3  | \I770.4 ;
  assign I69 = ~I64 & ~I48;
  assign \I73.1  = ~I69;
  assign \I73.2  = ~\X.3 ;
  assign I66 = ~\X.1  | ~\P.0 ;
  assign \I7.1  = ~I66;
  assign \I7.2  = ~\X.2 ;
  assign \I88.1  = ~\X.1 ;
  assign \I88.2  = ~\P.0 ;
  assign I48 = ~\P.0 ;
  assign I49 = ~\X.4 ;
  assign I50 = ~\X.3 ;
  assign I68 = ~I69;
  assign I167 = ~I162 & ~I146;
  assign \I171.1  = ~I167;
  assign \I171.2  = ~\X.7 ;
  assign I164 = ~\X.5  | ~\I1.2 ;
  assign \I105.1  = ~I164;
  assign \I105.2  = ~\X.6 ;
  assign \I186.1  = ~\X.5 ;
  assign \I1.2  = \I2.1  & \P.0 ;
  assign \I186.2  = ~\I1.2 ;
  assign I146 = ~\I1.2 ;
  assign I147 = ~\X.8 ;
  assign I148 = ~\X.7 ;
  assign I166 = ~I167;
  assign I265 = ~I260 & ~I244;
  assign \I269.1  = ~I265;
  assign \I269.2  = ~\X.11 ;
  assign I262 = ~\X.9  | ~\I1.3 ;
  assign \I203.1  = ~I262;
  assign \I203.2  = ~\X.10 ;
  assign \I284.1  = ~\X.9 ;
  assign \I1.3  = \I2.2  & \I1.2 ;
  assign \I284.2  = ~\I1.3 ;
  assign I244 = ~\I1.3 ;
  assign I245 = ~\X.12 ;
  assign I246 = ~\X.11 ;
  assign I264 = ~I265;
  assign I363 = ~I358 & ~I342;
  assign \I367.1  = ~I363;
  assign \I367.2  = ~\X.15 ;
  assign I360 = ~\X.13  | ~\I1.4 ;
  assign \I301.1  = ~I360;
  assign \I301.2  = ~\X.14 ;
  assign \I382.1  = ~\X.13 ;
  assign \I1.4  = \I2.3  & \I1.3 ;
  assign \I382.2  = ~\I1.4 ;
  assign I342 = ~\I1.4 ;
  assign I343 = ~\X.16 ;
  assign I344 = ~\X.15 ;
  assign I362 = ~I363;
  assign I461 = ~I456 & ~I440;
  assign \I465.1  = ~I461;
  assign \I465.2  = ~\X.19 ;
  assign I458 = ~\X.17  | ~\I1.5 ;
  assign \I399.1  = ~I458;
  assign \I399.2  = ~\X.18 ;
  assign \I480.1  = ~\X.17 ;
  assign \I1.5  = \I2.4  & \I1.4 ;
  assign \I480.2  = ~\I1.5 ;
  assign I440 = ~\I1.5 ;
  assign I441 = ~\X.20 ;
  assign I442 = ~\X.19 ;
  assign I460 = ~I461;
  assign I559 = ~I554 & ~I538;
  assign \I563.1  = ~I559;
  assign \I563.2  = ~\X.23 ;
  assign I556 = ~\X.21  | ~\I1.6 ;
  assign \I497.1  = ~I556;
  assign \I497.2  = ~\X.22 ;
  assign \I578.1  = ~\X.21 ;
  assign \I1.6  = \I2.5  & \I1.5 ;
  assign \I578.2  = ~\I1.6 ;
  assign I538 = ~\I1.6 ;
  assign I539 = ~\X.24 ;
  assign I540 = ~\X.23 ;
  assign I558 = ~I559;
  assign I657 = ~I652 & ~I636;
  assign \I661.1  = ~I657;
  assign \I661.2  = ~\X.27 ;
  assign I654 = ~\X.25  | ~\I1.7 ;
  assign \I595.1  = ~I654;
  assign \I595.2  = ~\X.26 ;
  assign \I676.1  = ~\X.25 ;
  assign \I1.7  = \I2.6  & \I1.6 ;
  assign \I676.2  = ~\I1.7 ;
  assign I636 = ~\I1.7 ;
  assign I637 = ~\X.28 ;
  assign I638 = ~\X.27 ;
  assign I656 = ~I657;
  assign I751 = ~\X.29  | ~\I1.8 ;
  assign \I693.1  = ~I751;
  assign \I693.2  = ~\X.30 ;
  assign \I770.1  = ~\X.29 ;
  assign \I1.8  = \I2.7  & \I1.7 ;
  assign \I770.2  = ~\I1.8 ;
  assign I736 = ~\X.31 ;
  assign I737 = ~\X.30 ;
  assign I750 = ~I736 & ~\I779.1 ;
  assign I749 = ~I750;
  assign I752 = ~I751;
  assign I806 = ~\P.0 ;
  assign I807 = ~\X.1 ;
  assign I808 = ~\X.2 ;
  assign I809 = ~\X.3 ;
  assign I810 = ~\X.4 ;
  assign I818 = ~I807 | ~\P.0 ;
  assign I819 = ~I818;
  assign I834 = ~\P.0 ;
  assign I835 = ~\X.5 ;
  assign I836 = ~\X.6 ;
  assign I837 = ~\X.7 ;
  assign I838 = ~\X.8 ;
  assign I846 = ~I835 | ~\P.0 ;
  assign I847 = ~I846;
  assign I862 = ~\P.0 ;
  assign I863 = ~\X.9 ;
  assign I864 = ~\X.10 ;
  assign I865 = ~\X.11 ;
  assign I866 = ~\X.12 ;
  assign I874 = ~I863 | ~\P.0 ;
  assign I875 = ~I874;
  assign I890 = ~\P.0 ;
  assign I891 = ~\X.13 ;
  assign I892 = ~\X.14 ;
  assign I893 = ~\X.15 ;
  assign I894 = ~\X.16 ;
  assign I902 = ~I891 | ~\P.0 ;
  assign I903 = ~I902;
  assign I918 = ~\P.0 ;
  assign I919 = ~\X.17 ;
  assign I920 = ~\X.18 ;
  assign I921 = ~\X.19 ;
  assign I922 = ~\X.20 ;
  assign I930 = ~I919 | ~\P.0 ;
  assign I931 = ~I930;
  assign I946 = ~\P.0 ;
  assign I947 = ~\X.21 ;
  assign I948 = ~\X.22 ;
  assign I949 = ~\X.23 ;
  assign I950 = ~\X.24 ;
  assign I958 = ~I947 | ~\P.0 ;
  assign I959 = ~I958;
  assign I974 = ~\P.0 ;
  assign I975 = ~\X.25 ;
  assign I976 = ~\X.26 ;
  assign I977 = ~\X.27 ;
  assign I978 = ~\X.28 ;
  assign I986 = ~I975 | ~\P.0 ;
  assign I987 = ~I986;
  assign I1002 = ~\P.0 ;
  assign I1003 = ~\X.29 ;
  assign I1004 = ~\X.30 ;
  assign I1005 = ~\X.31 ;
  assign I1006 = ~\X.32 ;
  assign I1013 = ~I1003 | ~\P.0 ;
  assign I1014 = ~I1013;
  assign \P.2  = ~I808 & ~I818;
  assign I1074 = ~\P.2 ;
  assign \P.3  = ~I809 & ~I816;
  assign I1075 = ~\P.3 ;
  assign I1078 = ~\C.2 ;
  assign I1079 = ~\C.3 ;
  assign \P.6  = \I803.1  & \I800.2 ;
  assign I1098 = ~\P.6 ;
  assign \P.7  = \I803.1  & \I801.2 ;
  assign I1099 = ~\P.7 ;
  assign I1102 = ~\C.6 ;
  assign I1103 = ~\C.7 ;
  assign \P.10  = \I804.2  & \I800.3 ;
  assign I1122 = ~\P.10 ;
  assign \P.11  = \I804.2  & \I801.3 ;
  assign I1123 = ~\P.11 ;
  assign I1126 = ~\C.10 ;
  assign I1127 = ~\C.11 ;
  assign \P.14  = \I804.3  & \I800.4 ;
  assign I1146 = ~\P.14 ;
  assign \P.15  = \I804.3  & \I801.4 ;
  assign I1147 = ~\P.15 ;
  assign I1150 = ~\C.14 ;
  assign I1151 = ~\C.15 ;
  assign \P.18  = \I804.4  & \I800.5 ;
  assign I1170 = ~\P.18 ;
  assign \P.19  = \I804.4  & \I801.5 ;
  assign I1171 = ~\P.19 ;
  assign I1174 = ~\C.18 ;
  assign I1175 = ~\C.19 ;
  assign \P.22  = \I804.5  & \I800.6 ;
  assign I1194 = ~\P.22 ;
  assign \P.23  = \I804.5  & \I801.6 ;
  assign I1195 = ~\P.23 ;
  assign I1198 = ~\C.22 ;
  assign I1199 = ~\C.23 ;
  assign \P.26  = \I804.6  & \I800.7 ;
  assign I1218 = ~\P.26 ;
  assign \P.27  = \I804.6  & \I801.7 ;
  assign I1219 = ~\P.27 ;
  assign I1222 = ~\C.26 ;
  assign I1223 = ~\C.27 ;
  assign \P.30  = \I804.7  & \I800.8 ;
  assign I1242 = ~\P.30 ;
  assign \P.31  = \I804.7  & \I801.8 ;
  assign I1243 = ~\P.31 ;
  assign I1246 = ~\C.30 ;
  assign I1247 = ~\C.31 ;
  assign \I73.3  = I69 & \I73.2 ;
  assign \I73.4  = \X.3  & \I73.1 ;
  assign \I7.3  = I66 & \I7.2 ;
  assign \I7.4  = \X.2  & \I7.1 ;
  assign \I88.3  = \X.1  & \I88.2 ;
  assign \I88.4  = \P.0  & \I88.1 ;
  assign \I171.3  = I167 & \I171.2 ;
  assign \I171.4  = \X.7  & \I171.1 ;
  assign \I105.3  = I164 & \I105.2 ;
  assign \I105.4  = \X.6  & \I105.1 ;
  assign \I186.3  = \X.5  & \I186.2 ;
  assign \I186.4  = \I1.2  & \I186.1 ;
  assign \I269.3  = I265 & \I269.2 ;
  assign \I269.4  = \X.11  & \I269.1 ;
  assign \I203.3  = I262 & \I203.2 ;
  assign \I203.4  = \X.10  & \I203.1 ;
  assign \I284.3  = \X.9  & \I284.2 ;
  assign \I284.4  = \I1.3  & \I284.1 ;
  assign \I367.3  = I363 & \I367.2 ;
  assign \I367.4  = \X.15  & \I367.1 ;
  assign \I301.3  = I360 & \I301.2 ;
  assign \I301.4  = \X.14  & \I301.1 ;
  assign \I382.3  = \X.13  & \I382.2 ;
  assign \I382.4  = \I1.4  & \I382.1 ;
  assign \I465.3  = I461 & \I465.2 ;
  assign \I465.4  = \X.19  & \I465.1 ;
  assign \I399.3  = I458 & \I399.2 ;
  assign \I399.4  = \X.18  & \I399.1 ;
  assign \I480.3  = \X.17  & \I480.2 ;
  assign \I480.4  = \I1.5  & \I480.1 ;
  assign \I563.3  = I559 & \I563.2 ;
  assign \I563.4  = \X.23  & \I563.1 ;
  assign \I497.3  = I556 & \I497.2 ;
  assign \I497.4  = \X.22  & \I497.1 ;
  assign \I578.3  = \X.21  & \I578.2 ;
  assign \I578.4  = \I1.6  & \I578.1 ;
  assign \I661.3  = I657 & \I661.2 ;
  assign \I661.4  = \X.27  & \I661.1 ;
  assign \I595.3  = I654 & \I595.2 ;
  assign \I595.4  = \X.26  & \I595.1 ;
  assign \I676.3  = \X.25  & \I676.2 ;
  assign \I676.4  = \I1.7  & \I676.1 ;
  assign \I693.3  = I751 & \I693.2 ;
  assign \I693.4  = \X.30  & \I693.1 ;
  assign \I770.3  = \X.29  & \I770.2 ;
  assign \I770.4  = \I1.8  & \I770.1 ;
  assign \I779.1  = I752 & \X.30 ;
  assign \I2.1  = ~I50 & ~I64 & ~I49;
  assign \I2.2  = ~I148 & ~I162 & ~I147;
  assign \I2.3  = ~I246 & ~I260 & ~I245;
  assign \I2.4  = ~I344 & ~I358 & ~I343;
  assign \I2.5  = ~I442 & ~I456 & ~I441;
  assign \I2.6  = ~I540 & ~I554 & ~I539;
  assign \I2.7  = ~I638 & ~I652 & ~I637;
  assign \I803.1  = ~\X.1  & ~\X.3  & ~\X.4  & ~\X.2 ;
  assign \I803.2  = ~\X.5  & ~\X.7  & ~\X.8  & ~\X.6 ;
  assign \I804.2  = \I803.1  & \I803.2 ;
  assign \I803.3  = ~\X.9  & ~\X.11  & ~\X.12  & ~\X.10 ;
  assign \I804.3  = \I804.2  & \I803.3 ;
  assign \I803.4  = ~\X.13  & ~\X.15  & ~\X.16  & ~\X.14 ;
  assign \I804.4  = \I804.3  & \I803.4 ;
  assign \I803.5  = ~\X.17  & ~\X.19  & ~\X.20  & ~\X.18 ;
  assign \I804.5  = \I804.4  & \I803.5 ;
  assign \I803.6  = ~\X.21  & ~\X.23  & ~\X.24  & ~\X.22 ;
  assign \I804.6  = \I804.5  & \I803.6 ;
  assign \I803.7  = ~\X.25  & ~\X.27  & ~\X.28  & ~\X.26 ;
  assign \I804.7  = \I804.6  & \I803.7 ;
  assign \I799.2  = ~I834 & ~I835;
  assign \P.5  = \I803.1  & \I799.2 ;
  assign \I800.2  = ~I836 & ~I846;
  assign \I801.2  = ~I837 & ~I844;
  assign \I802.2  = ~I838 & ~\X.7  & ~I844;
  assign \P.8  = \I803.1  & \I802.2 ;
  assign \I799.3  = ~I862 & ~I863;
  assign \P.9  = \I804.2  & \I799.3 ;
  assign \I800.3  = ~I864 & ~I874;
  assign \I801.3  = ~I865 & ~I872;
  assign \I802.3  = ~I866 & ~\X.11  & ~I872;
  assign \P.12  = \I804.2  & \I802.3 ;
  assign \I799.4  = ~I890 & ~I891;
  assign \P.13  = \I804.3  & \I799.4 ;
  assign \I800.4  = ~I892 & ~I902;
  assign \I801.4  = ~I893 & ~I900;
  assign \I802.4  = ~I894 & ~\X.15  & ~I900;
  assign \P.16  = \I804.3  & \I802.4 ;
  assign \I799.5  = ~I918 & ~I919;
  assign \P.17  = \I804.4  & \I799.5 ;
  assign \I800.5  = ~I920 & ~I930;
  assign \I801.5  = ~I921 & ~I928;
  assign \I802.5  = ~I922 & ~\X.19  & ~I928;
  assign \P.20  = \I804.4  & \I802.5 ;
  assign \I799.6  = ~I946 & ~I947;
  assign \P.21  = \I804.5  & \I799.6 ;
  assign \I800.6  = ~I948 & ~I958;
  assign \I801.6  = ~I949 & ~I956;
  assign \I802.6  = ~I950 & ~\X.23  & ~I956;
  assign \P.24  = \I804.5  & \I802.6 ;
  assign \I799.7  = ~I974 & ~I975;
  assign \P.25  = \I804.6  & \I799.7 ;
  assign \I800.7  = ~I976 & ~I986;
  assign \I801.7  = ~I977 & ~I984;
  assign \I802.7  = ~I978 & ~\X.27  & ~I984;
  assign \P.28  = \I804.6  & \I802.7 ;
  assign \I799.8  = ~I1002 & ~I1003;
  assign \P.29  = \I804.7  & \I799.8 ;
  assign \I800.8  = ~I1004 & ~I1013;
  assign \I801.8  = ~I1005 & ~I1011;
  assign \I802.8  = ~I1006 & ~\X.31  & ~I1011;
  assign \P.32  = \I804.7  & \I802.8 ;
  assign \P.1  = ~I806 & ~I807;
  assign \I1087.1  = \P.1  & \C.1 ;
  assign \I1087.2  = \P.0  & \C.0 ;
  assign \I1111.1  = \P.5  & \C.5 ;
  assign \P.4  = ~I810 & ~\X.3  & ~I816;
  assign \I1111.2  = \P.4  & \C.4 ;
  assign \I1135.1  = \P.9  & \C.9 ;
  assign \I1135.2  = \P.8  & \C.8 ;
  assign \I1159.1  = \P.13  & \C.13 ;
  assign \I1159.2  = \P.12  & \C.12 ;
  assign \I1183.1  = \P.17  & \C.17 ;
  assign \I1183.2  = \P.16  & \C.16 ;
  assign \I1207.1  = \P.21  & \C.21 ;
  assign \I1207.2  = \P.20  & \C.20 ;
  assign \I1231.1  = \P.25  & \C.25 ;
  assign \I1231.2  = \P.24  & \C.24 ;
  assign \I1255.1  = \P.29  & \C.29 ;
  assign \I1255.2  = \P.28  & \C.28 ;
  assign \I1062.9  = \P.32  & \C.32 ;
  assign \I70.1  = I50 | I68 | \X.4 ;
  assign I64 = ~\X.1  | ~\X.2 ;
  assign \I95.1  = I48 | I64 | I50;
  assign \I168.1  = I148 | I166 | \X.8 ;
  assign I162 = ~\X.5  | ~\X.6 ;
  assign \I193.1  = I146 | I162 | I148;
  assign \I266.1  = I246 | I264 | \X.12 ;
  assign I260 = ~\X.9  | ~\X.10 ;
  assign \I291.1  = I244 | I260 | I246;
  assign \I364.1  = I344 | I362 | \X.16 ;
  assign I358 = ~\X.13  | ~\X.14 ;
  assign \I389.1  = I342 | I358 | I344;
  assign \I462.1  = I442 | I460 | \X.20 ;
  assign I456 = ~\X.17  | ~\X.18 ;
  assign \I487.1  = I440 | I456 | I442;
  assign \I560.1  = I540 | I558 | \X.24 ;
  assign I554 = ~\X.21  | ~\X.22 ;
  assign \I585.1  = I538 | I554 | I540;
  assign \I658.1  = I638 | I656 | \X.28 ;
  assign I652 = ~\X.25  | ~\X.26 ;
  assign \I683.1  = I636 | I652 | I638;
  assign I753 = ~I752 | ~\X.30 ;
  assign \I755.1  = I736 | I753 | \X.32 ;
  assign \I758.1  = I753 | \X.31 ;
  assign \I776.1  = I736 | I751 | I737;
  assign \I1083.1  = I1075 | I1079;
  assign \I1083.2  = I1074 | I1078;
  assign \I1107.1  = I1099 | I1103;
  assign \I1107.2  = I1098 | I1102;
  assign \I1131.1  = I1123 | I1127;
  assign \I1131.2  = I1122 | I1126;
  assign \I1155.1  = I1147 | I1151;
  assign \I1155.2  = I1146 | I1150;
  assign \I1179.1  = I1171 | I1175;
  assign \I1179.2  = I1170 | I1174;
  assign \I1203.1  = I1195 | I1199;
  assign \I1203.2  = I1194 | I1198;
  assign \I1227.1  = I1219 | I1223;
  assign \I1227.2  = I1218 | I1222;
  assign \I1251.1  = I1243 | I1247;
  assign \I1251.2  = I1242 | I1246;
  assign \I1061.1  = ~I1082 | ~\I1083.1  | ~\I1083.2 ;
  assign \I1061.2  = ~I1106 | ~\I1107.1  | ~\I1107.2 ;
  assign \I1062.2  = \I1061.1  | \I1061.2 ;
  assign \I1061.3  = ~I1130 | ~\I1131.1  | ~\I1131.2 ;
  assign \I1062.3  = \I1062.2  | \I1061.3 ;
  assign \I1061.4  = ~I1154 | ~\I1155.1  | ~\I1155.2 ;
  assign \I1062.4  = \I1062.3  | \I1061.4 ;
  assign \I1061.5  = ~I1178 | ~\I1179.1  | ~\I1179.2 ;
  assign \I1062.5  = \I1062.4  | \I1061.5 ;
  assign \I1061.6  = ~I1202 | ~\I1203.1  | ~\I1203.2 ;
  assign \I1062.6  = \I1062.5  | \I1061.6 ;
  assign \I1061.7  = ~I1226 | ~\I1227.1  | ~\I1227.2 ;
  assign \I1062.7  = \I1062.6  | \I1061.7 ;
  assign \I1061.8  = ~I1250 | ~\I1251.1  | ~\I1251.2 ;
  assign \I1062.8  = \I1062.7  | \I1061.8 ;
  assign I62 = ~\I95.1  | ~\X.4 ;
  assign I160 = ~\I193.1  | ~\X.8 ;
  assign I258 = ~\I291.1  | ~\X.12 ;
  assign I356 = ~\I389.1  | ~\X.16 ;
  assign I454 = ~\I487.1  | ~\X.20 ;
  assign I552 = ~\I585.1  | ~\X.24 ;
  assign I650 = ~\I683.1  | ~\X.28 ;
  assign I747 = ~\I776.1  | ~\X.32 ;
  assign I816 = ~I819 | ~I808;
  assign I844 = ~I847 | ~I836;
  assign I872 = ~I875 | ~I864;
  assign I900 = ~I903 | ~I892;
  assign I928 = ~I931 | ~I920;
  assign I956 = ~I959 | ~I948;
  assign I984 = ~I987 | ~I976;
  assign I1011 = ~I1014 | ~I1004;
  assign I1082 = ~\I1087.1  & ~\I1087.2 ;
  assign I1106 = ~\I1111.1  & ~\I1111.2 ;
  assign I1130 = ~\I1135.1  & ~\I1135.2 ;
  assign I1154 = ~\I1159.1  & ~\I1159.2 ;
  assign I1178 = ~\I1183.1  & ~\I1183.2 ;
  assign I1202 = ~\I1207.1  & ~\I1207.2 ;
  assign I1226 = ~\I1231.1  & ~\I1231.2 ;
  assign I1250 = ~\I1255.1  & ~\I1255.2 ;
  always @ (posedge clock) begin
    \X.4  <= n72;
    \X.3  <= n77;
    \X.2  <= n82;
    \X.1  <= n87;
    \X.8  <= n92;
    \X.7  <= n97;
    \X.6  <= n102;
    \X.5  <= n107;
    \X.12  <= n112;
    \X.11  <= n117;
    \X.10  <= n122;
    \X.9  <= n127;
    \X.16  <= n132;
    \X.15  <= n137;
    \X.14  <= n142;
    \X.13  <= n147;
    \X.20  <= n152;
    \X.19  <= n157;
    \X.18  <= n162;
    \X.17  <= n167;
    \X.24  <= n172;
    \X.23  <= n177;
    \X.22  <= n182;
    \X.21  <= n187;
    \X.28  <= n192;
    \X.27  <= n197;
    \X.26  <= n202;
    \X.25  <= n207;
    \X.32  <= n212;
    \X.31  <= n217;
    \X.30  <= n222;
    \X.29  <= n227;
  end
endmodule


