// Benchmark "s386.blif" written by ABC on Mon Apr  8 18:05:52 2019

module \s386.blif  ( clock, 
    v6, v5, v4, v3, v2, v1, v0,
    v13_D_12, v13_D_11, v13_D_10, v13_D_9, v13_D_8, v13_D_7, v13_D_6  );
  input  clock;
  input  v6, v5, v4, v3, v2, v1, v0;
  output v13_D_12, v13_D_11, v13_D_10, v13_D_9, v13_D_8, v13_D_7, v13_D_6;
  reg v12, v11, v10, v9, v8, v7;
  wire v3bar, v6bar, v5bar, B35B, B35Bbar, B14B, B14Bbar, B34B, B34Bbar,
    v4bar, v11bar, v8bar, v7bar, v12bar, v0bar, v10bar, v9bar, v1bar,
    Lv13_D_12, I198, Lv13_D_11, I201, Lv13_D_10, I204, Lv13_D_9, I207,
    Lv13_D_8, I210, Lv13_D_7, I213, Lv13_D_6, I216, Lv13_D_5, I219,
    Lv13_D_4, I222, Lv13_D_3, I225, Lv13_D_2, I228, Lv13_D_1, I231,
    Lv13_D_0, I234, I64, I65, I114, I113, I111, I109, I108, I106, I105,
    I103, I102, I100, II98, I96, I89, I94, I93, I91, I90, I97, I98, I87,
    I104, I85, I84, I79, I77, I76, I74, I73, I71, I69, B40B, I124, I66,
    II65, I63, B23B, I62, B42B, I60, B43B, I59, B32B, I57, I56, I54, B27B,
    I53, I51, B21B, I50, I48, I47, B38B, I148, B29B, I44, B30B, I43, B17B,
    I41, B16B, I40, I39, B39B, I158, B25B, B26B, I36, B28B, I35, B15B,
    I164, B33B, I167, B36B, I31, I171, I30, I175, I28, B44B, I27, B22B,
    I25, B24B, I24, B18B, I22, B19B, I21, B31B, I186, B41B, I18, B45B, I17,
    B37B, I192, B20B, I195, n30, n35, n40, n45, n50, n55;
  assign v13_D_12 = ~I198;
  assign v13_D_11 = ~I201;
  assign v13_D_10 = ~I204;
  assign v13_D_9 = ~I207;
  assign v13_D_8 = ~I210;
  assign v13_D_7 = ~I213;
  assign v13_D_6 = ~I216;
  assign n30 = ~I219;
  assign n35 = ~I222;
  assign n40 = ~I225;
  assign n45 = ~I228;
  assign n50 = ~I231;
  assign n55 = ~I234;
  assign v3bar = ~v3;
  assign v6bar = ~v6;
  assign v5bar = ~v5;
  assign B35B = v2 | v7;
  assign B35Bbar = ~B35B;
  assign B14B = v7bar | v8bar;
  assign B14Bbar = ~B14B;
  assign B34B = v8bar | v3;
  assign B34Bbar = ~B34B;
  assign v4bar = ~v4;
  assign v11bar = ~v11;
  assign v8bar = ~v8;
  assign v7bar = ~v7;
  assign v12bar = ~v12;
  assign v0bar = ~v0;
  assign v10bar = ~v10;
  assign v9bar = ~v9;
  assign v1bar = ~v1;
  assign Lv13_D_12 = I64 & I65;
  assign I198 = ~Lv13_D_12;
  assign Lv13_D_11 = I17 | I18;
  assign I201 = ~Lv13_D_11;
  assign Lv13_D_10 = I124 & v12bar & v9 & v11bar;
  assign I204 = ~Lv13_D_10;
  assign Lv13_D_9 = I158 & v11bar & v12bar;
  assign I207 = ~Lv13_D_9;
  assign Lv13_D_8 = I97 & I98;
  assign I210 = ~Lv13_D_8;
  assign Lv13_D_7 = I148 & v12bar & v9bar & v10bar;
  assign I213 = ~Lv13_D_7;
  assign Lv13_D_6 = I192 & v9bar & v10bar;
  assign I216 = ~Lv13_D_6;
  assign Lv13_D_5 = I167 & v9bar & v10bar;
  assign I219 = ~Lv13_D_5;
  assign Lv13_D_4 = I186 & v9bar & v10bar;
  assign I222 = ~Lv13_D_4;
  assign Lv13_D_3 = I35 | I36;
  assign I225 = ~Lv13_D_3;
  assign Lv13_D_2 = I24 | I25;
  assign I228 = ~Lv13_D_2;
  assign Lv13_D_1 = I195 & v9bar & v10bar;
  assign I231 = ~Lv13_D_1;
  assign Lv13_D_0 = I164 & v12bar & v9bar & v10bar;
  assign I234 = ~Lv13_D_0;
  assign I64 = v8bar & v7bar & v0bar & v5;
  assign I65 = v12bar & v11bar & v9 & v10;
  assign I114 = v9bar & v12bar;
  assign I113 = v7bar & v8bar;
  assign I111 = v7bar & v8bar;
  assign I109 = v11bar & v3bar & v4bar;
  assign I108 = v7 & v11;
  assign I106 = v12 & v11 & v5bar & v7bar;
  assign I105 = v12bar & v2 & v11bar;
  assign I103 = v12bar & v8 & v11;
  assign I102 = v12 & v8bar & v11bar;
  assign I100 = v2 & v8bar;
  assign II98 = v0 & v5;
  assign I96 = v1 & v9bar;
  assign I89 = v8bar & v5bar & v7bar;
  assign I94 = I89 & v10 & v11bar;
  assign I93 = v9bar & v10bar;
  assign I91 = v12bar & v0 & v11bar;
  assign I90 = v9bar & v10bar;
  assign I97 = v8bar & v7bar & v0 & v6bar;
  assign I98 = v12bar & v11bar & v9bar & v10;
  assign I87 = v12bar & v11bar & v5bar & v9;
  assign I104 = v8 & v2 & v3;
  assign I85 = I104 & v11bar & v12bar;
  assign I84 = v12 & v8bar & v11;
  assign I79 = v11bar & v12bar;
  assign I77 = v10 & v0 & v8bar;
  assign I76 = B34Bbar & v10bar & v1bar & v4;
  assign I74 = v11 & v7 & v8bar;
  assign I73 = B34Bbar & v4bar & v11bar;
  assign I71 = B34Bbar & v4bar & v11bar;
  assign I69 = v7 & v11bar;
  assign B40B = II98 | v10bar;
  assign I124 = v8bar & v7bar & B40B & v1;
  assign I66 = v4 & v7;
  assign II65 = B35B & B34B;
  assign I63 = v12bar & v9bar & v10bar;
  assign B23B = I90 | I91;
  assign I62 = v8bar & B23B & v7bar;
  assign B42B = I111 | v12bar;
  assign I60 = v1 & B42B;
  assign B43B = I108 | I109;
  assign I59 = v12bar & B43B & v8;
  assign B32B = I84 | I85;
  assign I57 = B32B & v7bar;
  assign I56 = B14Bbar & v11 & v12bar;
  assign I54 = v10bar & v0bar & v9bar;
  assign B27B = I93 | I94;
  assign I53 = B27B & v1;
  assign I51 = v12bar & v9bar & v10bar;
  assign B21B = v10bar | I87;
  assign I50 = v8bar & B21B & v7bar;
  assign I48 = B14B & v11;
  assign I47 = B34Bbar & v4bar & v11bar;
  assign B38B = I73 | I74;
  assign I148 = v1bar & B38B & v0;
  assign B29B = I105 | I106;
  assign I44 = v8bar & B29B;
  assign B30B = I71 | v7;
  assign I43 = B30B & v12bar;
  assign B17B = v7 | I100;
  assign I41 = B17B & v4 & v11bar;
  assign B16B = B35Bbar | I69;
  assign I40 = B16B & v3 & v8;
  assign I39 = v11 & v8bar & v5 & v7;
  assign B39B = I76 | I77;
  assign I158 = v9bar & B39B & v7bar;
  assign B25B = v10bar | I79;
  assign B26B = v0bar | I96;
  assign I36 = B26B & B25B & v7bar & v8bar;
  assign B28B = I53 | I54;
  assign I35 = B28B & v12bar;
  assign B15B = I47 | I48;
  assign I164 = v1bar & B15B & v0;
  assign B33B = I56 | I57;
  assign I167 = v1bar & B33B & v0;
  assign B36B = II65 | I66;
  assign I31 = v12bar & B36B & v11bar;
  assign I171 = v8bar & v5 & v7bar;
  assign I30 = I171 & v11 & v12;
  assign I175 = v8bar & v0 & v7bar;
  assign I28 = I175 & v12bar & v10 & v11bar;
  assign B44B = I59 | I60;
  assign I27 = B44B & v10bar;
  assign B22B = I50 | I51;
  assign I25 = v0bar & B22B;
  assign B24B = I62 | I63;
  assign I24 = B24B & v1;
  assign B18B = I102 | I103;
  assign I22 = v7bar & B18B;
  assign B19B = I41 | I39 | I40;
  assign I21 = B19B & v12bar;
  assign B31B = I43 | I44;
  assign I186 = v1bar & B31B & v0;
  assign B41B = I113 | I114;
  assign I18 = B41B & v0bar & v10bar;
  assign B45B = I27 | I28;
  assign I17 = B45B & v9bar;
  assign B37B = I30 | I31;
  assign I192 = v1bar & B37B & v0;
  assign B20B = I21 | I22;
  assign I195 = v1bar & B20B & v0;
  always @ (posedge clock) begin
    v12 <= n30;
    v11 <= n35;
    v10 <= n40;
    v9 <= n45;
    v8 <= n50;
    v7 <= n55;
  end
endmodule


