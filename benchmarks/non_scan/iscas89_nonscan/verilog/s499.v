// Benchmark "s499.blif" written by ABC on Mon Apr  8 18:06:57 2019

module \s499.blif  ( clock, 
    Enable,
    number_0, number_1, number_2, number_3, number_4, number_5, number_6,
    number_7, number_8, number_9, number_10, number_11, number_12,
    number_13, number_14, number_15, number_16, number_17, number_18,
    number_19, number_20, number_21  );
  input  clock;
  input  Enable;
  output number_0, number_1, number_2, number_3, number_4, number_5, number_6,
    number_7, number_8, number_9, number_10, number_11, number_12,
    number_13, number_14, number_15, number_16, number_17, number_18,
    number_19, number_20, number_21;
  reg number_21, number_20, number_19, number_18, number_17, number_16,
    number_15, number_14, number_13, number_12, number_11, number_10,
    number_9, number_8, number_7, number_6, number_5, number_4, number_3,
    number_2, number_1, number_0;
  wire I116, I127, I117, I112, I128, I125, I122, I124, I121, I132, I115,
    I118, I131, I119, I133, I120, I126, I123, I113, I129, I114, I130, I204,
    I203, I182, I181, I194, I193, I186, I185, I183, I184, I249, I250, I287,
    I288, I291, I292, I289, I290, I293, I294, I229, I320_2, I327_1, I231,
    I316_2, I324_1, I233, I312_1, I320_1, I235, I308_1, I316_1, I237,
    I304_1, I312_2, I239, I308_2, I429_1, I241, I298_1, I304_2, I243,
    I295_1, I301_1, I221, I334_1, I342_1, I223, I330_2, I338_1, I225,
    I334_2, I435_1, I227, I330_1, I432_1, I209, I357_1, I365_2, I211,
    I353_1, I361_1, I213, I357_2, I438_1, I215, I346_2, I353_2, I217,
    I342_2, I350_1, I219, I338_2, I346_1, I205, I365_1, I372_1, I207,
    I361_2, I369_1, I191, I199, I197, I201, I190, I264, I275, I266, I285,
    I280, I187, I283, I282, I196, I277, I256, I258, I260, I274, I252, I254,
    I261, I270, I272, I180, I245, I267, I248, n48, n52, n56, n60, n64, n68,
    n72, n76, n80, n84, n88, n92, n96, n100, n104, n108, n112, n116, n120,
    n124, n128, n132;
  assign n48 = ~I295_1 | ~I245;
  assign n52 = ~I245 | ~I298_1 | ~I243;
  assign n56 = ~I301_1 | ~I288;
  assign n60 = ~I239 | ~I304_1 | ~I304_2;
  assign n64 = ~I237 | ~I308_1 | ~I308_2;
  assign n68 = ~I235 | ~I312_1 | ~I312_2;
  assign n72 = ~I233 | ~I316_1 | ~I316_2;
  assign n76 = ~I231 | ~I320_1 | ~I320_2;
  assign n80 = ~I324_1 | ~I290;
  assign n84 = ~I327_1 | ~I292;
  assign n88 = ~I225 | ~I330_1 | ~I330_2;
  assign n92 = ~I223 | ~I334_1 | ~I334_2;
  assign n96 = ~I221 | ~I338_1 | ~I338_2;
  assign n100 = ~I219 | ~I342_1 | ~I342_2;
  assign n104 = ~I217 | ~I346_1 | ~I346_2;
  assign n108 = ~I350_1 | ~I294;
  assign n112 = ~I213 | ~I353_1 | ~I353_2;
  assign n116 = ~I211 | ~I357_1 | ~I357_2;
  assign n120 = ~I209 | ~I361_1 | ~I361_2;
  assign n124 = ~I207 | ~I365_1 | ~I365_2;
  assign n128 = ~I205 | ~I369_1 | ~I267;
  assign n132 = ~I372_1 | ~I267;
  assign I116 = ~number_18;
  assign I127 = ~number_7;
  assign I117 = ~number_17;
  assign I112 = ~Enable;
  assign I128 = ~number_6;
  assign I125 = ~number_9;
  assign I122 = ~number_12;
  assign I124 = ~number_10;
  assign I121 = ~number_13;
  assign I132 = ~number_2;
  assign I115 = ~number_19;
  assign I118 = ~number_16;
  assign I131 = ~number_3;
  assign I119 = ~number_15;
  assign I133 = ~number_1;
  assign I120 = ~number_14;
  assign I126 = ~number_8;
  assign I123 = ~number_11;
  assign I113 = ~number_21;
  assign I129 = ~number_5;
  assign I114 = ~number_20;
  assign I130 = ~number_4;
  assign I204 = ~number_17 & ~number_16;
  assign I203 = ~I204;
  assign I182 = ~number_0 & ~number_1;
  assign I181 = ~I182;
  assign I194 = ~number_13 & ~number_14;
  assign I193 = ~I194;
  assign I186 = ~I285 & ~I275;
  assign I185 = ~I186;
  assign I183 = ~I264 | ~I282;
  assign I184 = ~I183;
  assign I249 = ~I190 | ~I184 | ~number_8;
  assign I250 = ~I249;
  assign I287 = ~I429_1 | ~I241;
  assign I288 = ~I287;
  assign I291 = ~I435_1 | ~I227;
  assign I292 = ~I291;
  assign I289 = ~I432_1 | ~I229;
  assign I290 = ~I289;
  assign I293 = ~I438_1 | ~I215;
  assign I294 = ~I293;
  assign I229 = ~number_13 | ~I119 | ~I256;
  assign I320_2 = Enable | I229;
  assign I327_1 = I112 | I229;
  assign I231 = ~I121 | ~I256 | ~number_15;
  assign I316_2 = Enable | I231;
  assign I324_1 = I112 | I231;
  assign I233 = ~I258 | ~number_15 | ~I117;
  assign I312_1 = Enable | I233;
  assign I320_1 = I112 | I233;
  assign I235 = ~I119 | ~number_17 | ~I258;
  assign I308_1 = Enable | I235;
  assign I316_1 = I112 | I235;
  assign I237 = ~I260 | ~number_17 | ~I115;
  assign I304_1 = Enable | I237;
  assign I312_2 = I112 | I237;
  assign I239 = ~I260 | ~I117 | ~number_19;
  assign I308_2 = I112 | I239;
  assign I429_1 = Enable | I239;
  assign I241 = ~I274 | ~I113 | ~number_19;
  assign I298_1 = Enable | I241;
  assign I304_2 = I112 | I241;
  assign I243 = ~I274 | ~number_21 | ~I115;
  assign I295_1 = Enable | I243;
  assign I301_1 = I112 | I243;
  assign I221 = ~number_9 | ~I123 | ~I252;
  assign I334_1 = Enable | I221;
  assign I342_1 = I112 | I221;
  assign I223 = ~number_11 | ~I252 | ~I125;
  assign I330_2 = Enable | I223;
  assign I338_1 = I112 | I223;
  assign I225 = ~number_11 | ~I121 | ~I254;
  assign I334_2 = I112 | I225;
  assign I435_1 = Enable | I225;
  assign I227 = ~number_13 | ~I254 | ~I123;
  assign I330_1 = I112 | I227;
  assign I432_1 = Enable | I227;
  assign I209 = ~number_3 | ~I129 | ~I270;
  assign I357_1 = Enable | I209;
  assign I365_2 = I112 | I209;
  assign I211 = ~I270 | ~I131 | ~number_5;
  assign I353_1 = Enable | I211;
  assign I361_1 = I112 | I211;
  assign I213 = ~I127 | ~number_5 | ~I272;
  assign I357_2 = I112 | I213;
  assign I438_1 = Enable | I213;
  assign I215 = ~I272 | ~number_7 | ~I129;
  assign I346_2 = Enable | I215;
  assign I353_2 = I112 | I215;
  assign I217 = ~number_7 | ~I125 | ~I250;
  assign I342_2 = Enable | I217;
  assign I350_1 = I112 | I217;
  assign I219 = ~number_9 | ~I127 | ~I250;
  assign I338_2 = Enable | I219;
  assign I346_1 = I112 | I219;
  assign I205 = ~number_1 | ~I248 | ~I131;
  assign I365_1 = Enable | I205;
  assign I372_1 = I112 | I205;
  assign I207 = ~I133 | ~number_3 | ~I248;
  assign I361_2 = Enable | I207;
  assign I369_1 = I112 | I207;
  assign I191 = ~I125 | ~I124;
  assign I199 = ~I121 | ~I122;
  assign I197 = ~I132 | ~I131;
  assign I201 = ~I113 | ~I114;
  assign I190 = ~I197 & ~I181;
  assign I264 = ~number_4 & ~number_5 & ~number_6;
  assign I275 = ~I127 | ~I190 | ~I264 | ~I126;
  assign I266 = ~number_18 & ~number_19 & ~I201;
  assign I285 = ~I266 | ~I120 | ~I204 | ~I119;
  assign I280 = ~I275 & ~number_12 & ~number_11 & ~I191;
  assign I187 = ~I266 | ~I280;
  assign I283 = ~I119 | ~I194 | ~I118 | ~I280;
  assign I282 = ~I285 & ~number_10 & ~number_11 & ~I199;
  assign I196 = ~number_8 & ~number_9;
  assign I277 = ~I182 | ~I282 | ~I196 | ~I132;
  assign I256 = ~I187 & ~I120 & ~I203;
  assign I258 = ~I118 & ~I193 & ~I187;
  assign I260 = ~I201 & ~I116 & ~I283;
  assign I274 = ~number_17 & ~number_18 & ~I283 & ~I114;
  assign I252 = ~I185 & ~I199 & ~I124;
  assign I254 = ~I185 & ~I122 & ~I191;
  assign I261 = ~I196 | ~I184 | ~I127;
  assign I270 = ~number_7 & ~I130 & ~I277 & ~number_6;
  assign I272 = ~number_3 & ~number_4 & ~I128 & ~I277;
  assign I180 = ~I261 & ~I197;
  assign I245 = ~I180 | ~Enable | ~I182;
  assign I267 = ~number_1 | ~I112 | ~number_0 | ~I180;
  assign I248 = ~I261 & ~number_0 & ~I132;
  always @ (posedge clock) begin
    number_21 <= n48;
    number_20 <= n52;
    number_19 <= n56;
    number_18 <= n60;
    number_17 <= n64;
    number_16 <= n68;
    number_15 <= n72;
    number_14 <= n76;
    number_13 <= n80;
    number_12 <= n84;
    number_11 <= n88;
    number_10 <= n92;
    number_9 <= n96;
    number_8 <= n100;
    number_7 <= n104;
    number_6 <= n108;
    number_5 <= n112;
    number_4 <= n116;
    number_3 <= n120;
    number_2 <= n124;
    number_1 <= n128;
    number_0 <= n132;
  end
endmodule


