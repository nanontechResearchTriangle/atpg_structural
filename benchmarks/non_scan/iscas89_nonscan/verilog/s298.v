// Benchmark "s298.blif" written by ABC on Mon Apr  8 18:05:01 2019

module \s298.blif  ( clock, 
    G0, G1, G2,
    G117, G132, G66, G118, G133, G67  );
  input  clock;
  input  G0, G1, G2;
  output G117, G132, G66, G118, G133, G67;
  reg G10, G11, G12, G13, G14, G15, G16, G17, G18, G19, G20, G21, G22, G23;
  wire G130, G28, G38, G40, G45, G46, G50, G51, G54, G55, G59, G60, G64,
    I155, I158, G76, G82, G87, G91, G93, G96, G99, G103, G112, G108, G114,
    I210, I213, G124, G120, G121, I221, G131, G126, G127, I229, I232, I235,
    I238, G26, G27, G31, G32, G33, G35, G36, G37, G41, G42, G47, G48, G52,
    G49, G61, G57, G65, G58, G62, G63, G74, G75, G88, G89, G90, G94, G95,
    G100, G104, G105, G109, G110, G111, G115, G122, G123, G128, G129, G24,
    G25, G68, G69, G70, G71, G72, G73, G77, G78, G79, G80, G81, G83, G84,
    G85, G43, G97, G101, G106, G116, G53, n20, n25, n30, n35, n40, n45,
    n50, n55, n60, n65, n70, n75, n80, n85;
  assign G117 = ~I210;
  assign G132 = ~I235;
  assign G66 = ~I155;
  assign G118 = ~I213;
  assign G133 = ~I238;
  assign G67 = ~I158;
  assign n20 = ~G10 & ~G130;
  assign n25 = ~G130 & ~G33 & ~G31 & ~G32;
  assign n30 = ~G130 & ~G37 & ~G35 & ~G36;
  assign n35 = ~G42 & ~G43;
  assign n40 = ~G53 & ~G48 & ~G49;
  assign n45 = ~G130 & ~G57 & ~G58;
  assign n50 = ~G112 & ~G90 & ~G88 & ~G89;
  assign n55 = ~G97 & ~G94 & ~G95;
  assign n60 = ~G100 & ~G101;
  assign n65 = ~G105 & ~G106;
  assign n70 = ~G110 & ~G111;
  assign n75 = ~G115 & ~G116;
  assign n80 = ~G130 & ~G122 & ~G123;
  assign n85 = ~G130 & ~G128 & ~G129;
  assign G130 = ~I229;
  assign G28 = ~G130;
  assign G38 = ~G10;
  assign G40 = ~G13;
  assign G45 = ~G12;
  assign G46 = ~G11;
  assign G50 = ~G14;
  assign G51 = ~G23;
  assign G54 = ~G11;
  assign G55 = ~G13;
  assign G59 = ~G12;
  assign G60 = ~G22;
  assign G64 = ~G15;
  assign I155 = ~G16;
  assign I158 = ~G17;
  assign G76 = ~G10;
  assign G82 = ~G11;
  assign G87 = ~G16;
  assign G91 = ~G12;
  assign G93 = ~G17;
  assign G96 = ~G14;
  assign G99 = ~G18;
  assign G103 = ~G13;
  assign G112 = ~G62 & ~G63;
  assign G108 = ~G112;
  assign G114 = ~G21;
  assign I210 = ~G18;
  assign I213 = ~G19;
  assign G124 = ~I221;
  assign G120 = ~G124;
  assign G121 = ~G22;
  assign I221 = ~G2;
  assign G131 = ~I232;
  assign G126 = ~G131;
  assign G127 = ~G23;
  assign I229 = ~G0;
  assign I232 = ~G1;
  assign I235 = ~G20;
  assign I238 = ~G21;
  assign G26 = G28 & G50;
  assign G27 = G51 & G28;
  assign G31 = G13 & G10 & G45;
  assign G32 = G10 & G11;
  assign G33 = G38 & G46;
  assign G35 = G12 & G10 & G11;
  assign G36 = G38 & G45;
  assign G37 = G46 & G45;
  assign G41 = ~G10 | ~G12 | ~G11;
  assign G42 = G40 & G41;
  assign G47 = ~G50 & ~G40;
  assign G48 = G47 & G10 & G45 & G46;
  assign G52 = ~G13 | ~G45 | ~G46 | ~G10;
  assign G49 = G52 & G50 & G51;
  assign G61 = ~G14 & ~G55;
  assign G57 = G61 & G60 & G59 & G11;
  assign G65 = ~G59 | ~G54 | ~G22 | ~G61;
  assign G58 = G64 & G65;
  assign G62 = G61 & G60 & G59 & G11;
  assign G63 = G64 & G65;
  assign G74 = G19 & G12 & G14;
  assign G75 = G14 & G82 & G91;
  assign G88 = G14 & G87;
  assign G89 = G103 & G96;
  assign G90 = G91 & G103;
  assign G94 = G93 & G13;
  assign G95 = G96 & G13;
  assign G100 = G12 & G99 & G14;
  assign G104 = ~G74 & ~G75;
  assign G105 = G104 & G103 & G108;
  assign G109 = ~G71 | ~G72 | ~G73 | ~G14;
  assign G110 = G108 & G109;
  assign G111 = G10 & G112;
  assign G115 = G114 & G14;
  assign G122 = G120 & G121;
  assign G123 = G124 & G22;
  assign G128 = G126 & G127;
  assign G129 = G131 & G23;
  assign G24 = G38 | G46 | G45 | G40;
  assign G25 = G12 | G38 | G11;
  assign G68 = G11 | G12 | G13 | G96;
  assign G69 = G103 | G18;
  assign G70 = G103 | G14;
  assign G71 = G13 | G82 | G12;
  assign G72 = G91 | G20;
  assign G73 = G103 | G20;
  assign G77 = G112 | G103 | G96 | G19;
  assign G78 = G108 | G76;
  assign G79 = G103 | G14;
  assign G80 = G11 | G14;
  assign G81 = G12 | G13;
  assign G83 = G11 | G12 | G13 | G96;
  assign G84 = G14 | G82 | G91;
  assign G85 = G17 | G91 | G96;
  assign G43 = ~G28 | ~G24 | ~G25;
  assign G97 = ~G83 | ~G84 | ~G85 | ~G108;
  assign G101 = ~G68 | ~G69 | ~G70 | ~G108;
  assign G106 = ~G77 | ~G78;
  assign G116 = ~G79 | ~G80 | ~G81 | ~G108;
  assign G53 = ~G26 & ~G27;
  always @ (posedge clock) begin
    G10 <= n20;
    G11 <= n25;
    G12 <= n30;
    G13 <= n35;
    G14 <= n40;
    G15 <= n45;
    G16 <= n50;
    G17 <= n55;
    G18 <= n60;
    G19 <= n65;
    G20 <= n70;
    G21 <= n75;
    G22 <= n80;
    G23 <= n85;
  end
endmodule


