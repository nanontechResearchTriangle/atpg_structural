// Benchmark "s208.1.blif" written by ABC on Mon Apr  8 18:04:45 2019

module \s208.1.blif  ( clock, 
    \P.0 , \C.8 , \C.7 , \C.6 , \C.5 , \C.4 , \C.3 , \C.2 , \C.1 , \C.0 ,
    Z  );
  input  clock;
  input  \P.0 , \C.8 , \C.7 , \C.6 , \C.5 , \C.4 , \C.3 , \C.2 , \C.1 ,
    \C.0 ;
  output Z;
  reg \X.4 , \X.3 , \X.2 , \X.1 , \X.8 , \X.7 , \X.6 , \X.5 ;
  wire I69, \I73.1 , \I73.2 , I66, \I7.1 , \I7.2 , \I88.1 , \I88.2 , I48,
    I49, I50, I68, I163, \I105.1 , \I105.2 , \I182.1 , \I1.2 , \I182.2 ,
    I148, I149, I162, I161, I164, I212, I213, I214, I215, I216, I224, I225,
    I240, I241, I242, I243, I244, I251, I252, \P.2 , I282, \P.3 , I283,
    I286, I287, \P.6 , I306, \P.7 , I307, I310, I311, \I73.3 , \I73.4 ,
    \I7.3 , \I7.4 , \I88.3 , \I88.4 , \I105.3 , \I105.4 , \I182.3 ,
    \I182.4 , \I191.1 , \I2.1 , \I209.1 , \I205.2 , \P.5 , \I206.2 ,
    \I207.2 , \I208.2 , \P.8 , \P.1 , \I295.1 , \I295.2 , \I319.1 , \P.4 ,
    \I319.2 , \I270.3 , \I70.1 , I64, \I95.1 , I165, \I167.1 , \I170.1 ,
    \I188.1 , \I291.1 , \I291.2 , \I315.1 , \I315.2 , \I269.1 , \I269.2 ,
    \I270.2 , I62, I159, I222, I249, I290, I314, n24, n29, n34, n39, n44,
    n49, n54, n59;
  assign Z = \I270.2  | \I270.3 ;
  assign n24 = ~\I70.1  | ~I62;
  assign n29 = \I73.3  | \I73.4 ;
  assign n34 = ~\I7.3  & ~\I7.4 ;
  assign n39 = \I88.3  | \I88.4 ;
  assign n44 = ~\I167.1  | ~I159;
  assign n49 = ~\I170.1  | ~I161;
  assign n54 = ~\I105.3  & ~\I105.4 ;
  assign n59 = \I182.3  | \I182.4 ;
  assign I69 = ~I64 & ~I48;
  assign \I73.1  = ~I69;
  assign \I73.2  = ~\X.3 ;
  assign I66 = ~\X.1  | ~\P.0 ;
  assign \I7.1  = ~I66;
  assign \I7.2  = ~\X.2 ;
  assign \I88.1  = ~\X.1 ;
  assign \I88.2  = ~\P.0 ;
  assign I48 = ~\P.0 ;
  assign I49 = ~\X.4 ;
  assign I50 = ~\X.3 ;
  assign I68 = ~I69;
  assign I163 = ~\X.5  | ~\I1.2 ;
  assign \I105.1  = ~I163;
  assign \I105.2  = ~\X.6 ;
  assign \I182.1  = ~\X.5 ;
  assign \I1.2  = \I2.1  & \P.0 ;
  assign \I182.2  = ~\I1.2 ;
  assign I148 = ~\X.7 ;
  assign I149 = ~\X.6 ;
  assign I162 = ~I148 & ~\I191.1 ;
  assign I161 = ~I162;
  assign I164 = ~I163;
  assign I212 = ~\P.0 ;
  assign I213 = ~\X.1 ;
  assign I214 = ~\X.2 ;
  assign I215 = ~\X.3 ;
  assign I216 = ~\X.4 ;
  assign I224 = ~I213 | ~\P.0 ;
  assign I225 = ~I224;
  assign I240 = ~\P.0 ;
  assign I241 = ~\X.5 ;
  assign I242 = ~\X.6 ;
  assign I243 = ~\X.7 ;
  assign I244 = ~\X.8 ;
  assign I251 = ~I241 | ~\P.0 ;
  assign I252 = ~I251;
  assign \P.2  = ~I214 & ~I224;
  assign I282 = ~\P.2 ;
  assign \P.3  = ~I215 & ~I222;
  assign I283 = ~\P.3 ;
  assign I286 = ~\C.2 ;
  assign I287 = ~\C.3 ;
  assign \P.6  = \I209.1  & \I206.2 ;
  assign I306 = ~\P.6 ;
  assign \P.7  = \I209.1  & \I207.2 ;
  assign I307 = ~\P.7 ;
  assign I310 = ~\C.6 ;
  assign I311 = ~\C.7 ;
  assign \I73.3  = I69 & \I73.2 ;
  assign \I73.4  = \X.3  & \I73.1 ;
  assign \I7.3  = I66 & \I7.2 ;
  assign \I7.4  = \X.2  & \I7.1 ;
  assign \I88.3  = \X.1  & \I88.2 ;
  assign \I88.4  = \P.0  & \I88.1 ;
  assign \I105.3  = I163 & \I105.2 ;
  assign \I105.4  = \X.6  & \I105.1 ;
  assign \I182.3  = \X.5  & \I182.2 ;
  assign \I182.4  = \I1.2  & \I182.1 ;
  assign \I191.1  = I164 & \X.6 ;
  assign \I2.1  = ~I50 & ~I64 & ~I49;
  assign \I209.1  = ~\X.1  & ~\X.3  & ~\X.4  & ~\X.2 ;
  assign \I205.2  = ~I240 & ~I241;
  assign \P.5  = \I209.1  & \I205.2 ;
  assign \I206.2  = ~I242 & ~I251;
  assign \I207.2  = ~I243 & ~I249;
  assign \I208.2  = ~I244 & ~\X.7  & ~I249;
  assign \P.8  = \I209.1  & \I208.2 ;
  assign \P.1  = ~I212 & ~I213;
  assign \I295.1  = \P.1  & \C.1 ;
  assign \I295.2  = \P.0  & \C.0 ;
  assign \I319.1  = \P.5  & \C.5 ;
  assign \P.4  = ~I216 & ~\X.3  & ~I222;
  assign \I319.2  = \P.4  & \C.4 ;
  assign \I270.3  = \P.8  & \C.8 ;
  assign \I70.1  = I50 | I68 | \X.4 ;
  assign I64 = ~\X.1  | ~\X.2 ;
  assign \I95.1  = I48 | I64 | I50;
  assign I165 = ~I164 | ~\X.6 ;
  assign \I167.1  = I148 | I165 | \X.8 ;
  assign \I170.1  = I165 | \X.7 ;
  assign \I188.1  = I148 | I163 | I149;
  assign \I291.1  = I283 | I287;
  assign \I291.2  = I282 | I286;
  assign \I315.1  = I307 | I311;
  assign \I315.2  = I306 | I310;
  assign \I269.1  = ~I290 | ~\I291.1  | ~\I291.2 ;
  assign \I269.2  = ~I314 | ~\I315.1  | ~\I315.2 ;
  assign \I270.2  = \I269.1  | \I269.2 ;
  assign I62 = ~\I95.1  | ~\X.4 ;
  assign I159 = ~\I188.1  | ~\X.8 ;
  assign I222 = ~I225 | ~I214;
  assign I249 = ~I252 | ~I242;
  assign I290 = ~\I295.1  & ~\I295.2 ;
  assign I314 = ~\I319.1  & ~\I319.2 ;
  always @ (posedge clock) begin
    \X.4  <= n24;
    \X.3  <= n29;
    \X.2  <= n34;
    \X.1  <= n39;
    \X.8  <= n44;
    \X.7  <= n49;
    \X.6  <= n54;
    \X.5  <= n59;
  end
endmodule


