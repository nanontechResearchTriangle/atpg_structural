// Benchmark "s820.blif" written by ABC on Mon Apr  8 18:08:21 2019

module \s820.blif  ( clock, 
    G0, G1, G2, G3, G4, G5, G6, G7, G8, G9, G10, G11, G12, G13, G14, G15,
    G16, G18,
    G290, G327, G47, G55, G288, G296, G310, G312, G325, G300, G43, G53,
    G298, G315, G322, G49, G45, G292, G302  );
  input  clock;
  input  G0, G1, G2, G3, G4, G5, G6, G7, G8, G9, G10, G11, G12, G13, G14,
    G15, G16, G18;
  output G290, G327, G47, G55, G288, G296, G310, G312, G325, G300, G43, G53,
    G298, G315, G322, G49, G45, G292, G302;
  reg G38, G39, G40, G41, G42;
  wire G245, G323, G181, G256, G130, G203, G202, G112, G198, G171, G172,
    G168, G201, G267, G317, G281, G313, G328, G88, G91, G94, G97, G100,
    G280, G318, I127, G228, I130, G229, I133, G231, I198, G247, G143, G161,
    G162, G163, G188, G189, G190, G195, G215, G120, G250, G118, G166, G199,
    G170, G169, G129, G265, G142, G279, G103, G164, G167, G191, G200, G214,
    G234, G283, G140, G141, G127, G160, G187, G193, G194, G213, G235, G249,
    G268, G276, G282, G117, G277, G278, G121, G128, G232, G233, G251, G252,
    G270, G271, G209, G210, G225, G226, G176, G175, G196, G197, G262, G263,
    G147, G148, G149, G150, G157, G158, G184, G185, G173, G174, G212, G211,
    G222, G223, G274, G272, G266, G264, G293, G294, G154, G152, G216, G217,
    G218, G153, G151, G275, G273, G257, G258, G220, G219, G260, G259, G89,
    G92, G95, G98, G101, G126, G124, G125, G107, G145, G243, G111, G144,
    G239, G287, G115, G183, G237, G246, G113, G132, G133, G182, G238, G241,
    G136, G116, G286, G108, G109, G240, G242, G244, G110, G134, G135, G114,
    G236, G248, G319, G321, G178, G180, G73, G74, G78, G284, G285, G59,
    G63, G105, G106, G304, G308, G316, G320, G50, G52, G137, G139, G253,
    G255, G204, G205, G207, G305, G309, G57, G58, G62, G303, G307, G81,
    G85, G177, G67, G65, G66, G70, G155, G75, G79, G60, G64, G68, G72, G71,
    G82, G86, G76, G80, G83, G87, G123, G295, G291, G329, G48, G56, G289,
    G297, G311, G314, G326, G119, G301, G44, G54, G156, G299, G179, G224,
    G227, G131, G269, G122, G46, G69, G138, G306, G254, G84, G51, G146,
    G61, G206, G77, G165, G192, G104, G324, G159, G186, G221, G261, n76,
    n81, n86, n91, n96;
  assign G290 = ~G42 & ~G291;
  assign G327 = ~G328 & ~G329;
  assign G47 = ~G48 & ~G42 & ~G41;
  assign G55 = ~G56 & ~G42 & ~G41;
  assign G288 = ~G42 & ~G289;
  assign G296 = ~G42 & ~G297;
  assign G310 = ~G328 & ~G311;
  assign G312 = ~G314 & ~G328 & ~G313;
  assign G325 = ~G328 & ~G326;
  assign G300 = ~G301 & ~G40 & ~G42 & ~G41;
  assign G43 = ~G44 & ~G42 & ~G313;
  assign G53 = ~G42 & ~G54;
  assign G298 = ~G299 & ~G40 & ~G42 & ~G313;
  assign G315 = ~G320 | ~G321;
  assign G322 = ~G324 & ~G323 & ~G41 & ~G38;
  assign G49 = ~G52 | ~G51;
  assign G45 = ~G46 & ~G317 & ~G42 & ~G313;
  assign G292 = ~G295 & ~G294 & ~G328;
  assign G302 = ~G307 | ~G308 | ~G309 | ~G306;
  assign n76 = G89 & G88;
  assign n81 = G92 & G91;
  assign n86 = G95 & G94;
  assign n91 = G98 & G97;
  assign n96 = G101 & G100;
  assign G245 = ~G0;
  assign G323 = ~G1;
  assign G181 = ~G2;
  assign G256 = ~G4;
  assign G130 = ~G5;
  assign G203 = ~G6;
  assign G202 = ~G7;
  assign G112 = ~G8;
  assign G198 = ~G9;
  assign G171 = ~G10;
  assign G172 = ~G11;
  assign G168 = ~G12;
  assign G201 = ~G13;
  assign G267 = ~G15;
  assign G317 = ~G40;
  assign G281 = ~G16;
  assign G313 = ~G41;
  assign G328 = ~G42;
  assign G88 = ~G18;
  assign G91 = ~G18;
  assign G94 = ~G18;
  assign G97 = ~G18;
  assign G100 = ~G18;
  assign G280 = ~G38;
  assign G318 = ~G39;
  assign I127 = ~G38;
  assign G228 = ~I127;
  assign I130 = ~G15;
  assign G229 = ~I130;
  assign I133 = ~G313;
  assign G231 = ~I133;
  assign I198 = ~G38;
  assign G247 = ~I198;
  assign G143 = G40 & G4;
  assign G161 = G3 & G42;
  assign G162 = G1 & G42;
  assign G163 = G41 & G42;
  assign G188 = G3 & G42;
  assign G189 = G1 & G42;
  assign G190 = G41 & G42;
  assign G195 = G41 & G42;
  assign G215 = G41 & G42;
  assign G120 = G42 & G39 & G40;
  assign G250 = G42 & G39 & G40;
  assign G118 = G39 & G245 & G38;
  assign G166 = G42 & G245 & G38;
  assign G199 = G42 & G245 & G38;
  assign G170 = G171 & G172;
  assign G169 = G172 & G168;
  assign G129 = G39 & G317;
  assign G265 = G317 & G267;
  assign G142 = G40 & G281;
  assign G279 = G281 & G42;
  assign G103 = G313 & G38;
  assign G164 = G42 & G313;
  assign G167 = G313 & G256 & G38;
  assign G191 = G42 & G313;
  assign G200 = G313 & G256 & G38;
  assign G214 = G267 & G16;
  assign G234 = G42 & G313 & G15 & G40;
  assign G283 = G317 & G313;
  assign G140 = ~G42 & ~G41;
  assign G141 = G140 & G323 & G317 & G16;
  assign G127 = G328 & G313 & G38 & G39;
  assign G160 = G328 & G5 & G313;
  assign G187 = G328 & G5 & G313;
  assign G193 = G11 & G328;
  assign G194 = G10 & G328;
  assign G213 = G328 & G16 & G313;
  assign G235 = G317 & G328;
  assign G249 = G328 & G40 & G41;
  assign G268 = G328 & G267;
  assign G276 = G328 & G0 & G38;
  assign G282 = G317 & G328;
  assign G117 = G313 & G1 & G39;
  assign G277 = G280 & G323 & G281;
  assign G278 = G280 & G42;
  assign G121 = G328 & G318 & G317;
  assign G128 = G40 & G280 & G318;
  assign G232 = G38 & G318;
  assign G233 = G15 & G318;
  assign G251 = G318 & G313;
  assign G252 = G318 & G317;
  assign G270 = ~G40 & ~G42 & ~G313;
  assign G271 = G270 & G14 & G318 & G15;
  assign G209 = ~G317 & ~G328 & ~G313;
  assign G210 = G209 & G245 & G39 & G38;
  assign G225 = ~G41 | ~G256;
  assign G226 = G318 & G225;
  assign G176 = ~G42 | ~G41 | ~G280 | ~G15;
  assign G175 = G317 & G176;
  assign G196 = ~G198 & ~G280 & ~G267;
  assign G197 = G196 & G6 & G8 & G7;
  assign G262 = ~G113 | ~G317;
  assign G263 = G262 & G39 & G38;
  assign G147 = ~G267 & ~G38 & ~G281;
  assign G148 = ~G39 & ~G317 & ~G42 & ~G313;
  assign G149 = ~G169 & ~G170;
  assign G150 = G149 & G148 & G256 & G147;
  assign G157 = ~G163 & ~G162 & ~G160 & ~G161;
  assign G158 = G280 & G157;
  assign G184 = ~G190 & ~G189 & ~G187 & ~G188;
  assign G185 = G280 & G184;
  assign G173 = ~G193 & ~G194;
  assign G174 = G173 & G15 & G41 & G40;
  assign G212 = ~G215 & ~G213 & ~G214;
  assign G211 = G212 & G256 & G317 & G39;
  assign G222 = ~G234 & ~G235;
  assign G223 = G16 & G222;
  assign G274 = ~G282 & ~G283;
  assign G272 = G274 & G318 & G4;
  assign G266 = ~G109 | ~G110 | ~G111 | ~G40;
  assign G264 = G318 & G266;
  assign G293 = ~G8 | ~G7 | ~G6 | ~G131;
  assign G294 = G16 & G293;
  assign G154 = ~G279 & ~G278 & ~G276 & ~G277;
  assign G152 = G154 & G318 & G313 & G317;
  assign G216 = ~G41 & ~G3;
  assign G217 = ~G236 | ~G237;
  assign G218 = G217 & G216 & G2 & G323;
  assign G153 = ~G252 & ~G251 & ~G249 & ~G250;
  assign G151 = G153 & G256 & G38 & G16;
  assign G275 = ~G287 | ~G285 | ~G286;
  assign G273 = G275 & G40 & G39;
  assign G257 = ~G108 | ~G106 | ~G107;
  assign G258 = G257 & G318 & G280;
  assign G220 = ~G223 & ~G224;
  assign G219 = G318 & G220;
  assign G260 = ~G265 & ~G263 & ~G264;
  assign G259 = G41 & G260;
  assign G89 = G150 | G151 | G152 | G155;
  assign G92 = ~G62 | ~G63 | ~G64 | ~G61;
  assign G95 = ~G70 | ~G71 | ~G72 | ~G69;
  assign G98 = ~G78 | ~G79 | ~G80 | ~G77;
  assign G101 = ~G85 | ~G86 | ~G87 | ~G84;
  assign G126 = G10 | G11;
  assign G124 = G11 | G12;
  assign G125 = G10 | G12;
  assign G107 = G1 | G41 | G40;
  assign G145 = G16 | G41;
  assign G243 = G5 | G41;
  assign G111 = G15 | G42;
  assign G144 = G16 | G42;
  assign G239 = G42 | G40 | G41;
  assign G287 = G42 | G5;
  assign G115 = G39 | G42;
  assign G183 = G41 | G38 | G39;
  assign G237 = G40 | G16 | G39;
  assign G246 = G4 | G39;
  assign G113 = G203 | G202 | G112 | G198;
  assign G132 = G171 | G11 | G12 | G42;
  assign G133 = G10 | G172 | G12 | G42;
  assign G182 = G14 | G267 | G38 | G39;
  assign G238 = G14 | G267 | G40 | G42;
  assign G241 = G256 | G317;
  assign G136 = G4 | G281;
  assign G116 = G39 | G313;
  assign G286 = G42 | G313;
  assign G108 = G328 | G15;
  assign G109 = G328 | G201 | G267;
  assign G240 = G328 | G256 | G313;
  assign G242 = G41 | G328;
  assign G244 = G281 | G328;
  assign G110 = G280 | G42;
  assign G134 = G280 | G42;
  assign G135 = G280 | G40;
  assign G114 = G328 | G267 | G318;
  assign G236 = G328 | G318 | G317;
  assign G248 = G245 | G318;
  assign G319 = ~G42 | ~G41;
  assign G321 = G317 | G318 | G38 | G319;
  assign G178 = ~G1 & ~G181 & ~G16 & ~G3;
  assign G180 = G41 | G178;
  assign G73 = ~G40 | ~G42 | ~G41;
  assign G74 = ~G201 & ~G281 & ~G267;
  assign G78 = G39 | G4 | G73 | G74;
  assign G284 = ~G42 | ~G313;
  assign G285 = G3 | G2 | G1 | G284;
  assign G59 = ~G144 | ~G145;
  assign G63 = G40 | G318 | G4 | G59;
  assign G105 = ~G328 | ~G40 | ~G15 | ~G9;
  assign G106 = G8 | G7 | G203 | G105;
  assign G304 = ~G328 & ~G313;
  assign G308 = G40 | G318 | G16 | G304;
  assign G316 = ~G328 | ~G313;
  assign G320 = G40 | G39 | G38 | G316;
  assign G50 = ~G40 & ~G280;
  assign G52 = G328 | G313 | G39 | G50;
  assign G137 = ~G280 & ~G42 & ~G41;
  assign G139 = G317 | G137;
  assign G253 = ~G280 & ~G42 & ~G41;
  assign G255 = G317 | G253;
  assign G204 = ~G9 | ~G8;
  assign G205 = ~G228 | ~G229;
  assign G207 = G202 | G203 | G204 | G205;
  assign G305 = ~G143 & ~G141 & ~G142;
  assign G309 = G305 | G39 | G38;
  assign G57 = ~G41 | ~G40 | ~G318 | ~G16;
  assign G58 = ~G134 | ~G132 | ~G133;
  assign G62 = G267 | G4 | G57 | G58;
  assign G303 = ~G135 | ~G136;
  assign G307 = G328 | G313 | G39 | G303;
  assign G81 = ~G248 | ~G246 | ~G247;
  assign G85 = G328 | G313 | G317 | G81;
  assign G177 = ~G195 & ~G280;
  assign G67 = G177 | G174 | G175;
  assign G65 = ~G317 | ~G42 | ~G41;
  assign G66 = ~G197 & ~G281;
  assign G70 = G318 | G4 | G65 | G66;
  assign G155 = ~G104 & ~G317 & ~G103 & ~G328;
  assign G75 = ~G207 | ~G206;
  assign G79 = G40 | G281 | G4 | G75;
  assign G60 = ~G158 & ~G159;
  assign G64 = G60 | G317 | G318;
  assign G68 = ~G185 & ~G186;
  assign G72 = G68 | G317 | G318;
  assign G71 = G39 | G281 | G4 | G67;
  assign G82 = ~G273 & ~G271 & ~G272;
  assign G86 = G38 | G82;
  assign G76 = ~G221 & ~G218 & ~G219;
  assign G80 = G38 | G76;
  assign G83 = ~G261 & ~G258 & ~G259;
  assign G87 = G281 | G83;
  assign G123 = ~G124 | ~G125 | ~G126 | ~G256;
  assign G295 = ~G41 | ~G317 | ~G39 | ~G256;
  assign G291 = ~G313 | ~G317 | ~G39 | ~G15;
  assign G329 = ~G313 | ~G317 | ~G39 | ~G15;
  assign G48 = ~G40 | ~G39 | ~G280 | ~G130;
  assign G56 = ~G40 | ~G39 | ~G280 | ~G5;
  assign G289 = ~G313 | ~G40 | ~G39 | ~G280;
  assign G297 = ~G41 | ~G40 | ~G39 | ~G280;
  assign G311 = ~G313 | ~G40 | ~G39 | ~G280;
  assign G314 = ~G40 | ~G39 | ~G280 | ~G16;
  assign G326 = ~G313 | ~G40 | ~G39 | ~G280;
  assign G119 = ~G39 & ~G38;
  assign G301 = ~G281 | ~G3 | ~G323 | ~G119;
  assign G44 = ~G317 | ~G318 | ~G280 | ~G15;
  assign G54 = ~G41 | ~G317 | ~G318 | ~G280;
  assign G156 = ~G281 | ~G318 | ~G280;
  assign G299 = ~G318 | ~G280 | ~G15 | ~G14;
  assign G179 = ~G182 | ~G183;
  assign G224 = ~G238 | ~G239 | ~G240 | ~G241;
  assign G227 = ~G242 | ~G243 | ~G244 | ~G40;
  assign G131 = ~G198 & ~G280 & ~G267;
  assign G269 = ~G114 | ~G115 | ~G116 | ~G317;
  assign G122 = ~G267 & ~G123;
  assign G46 = ~G318 | ~G280 | ~G16 | ~G122;
  assign G69 = ~G180 | ~G328 | ~G317 | ~G179;
  assign G138 = ~G318 & ~G256;
  assign G306 = ~G139 | ~G138;
  assign G254 = ~G318 & ~G256;
  assign G84 = ~G255 | ~G254;
  assign G51 = ~G129 & ~G127 & ~G128;
  assign G146 = ~G156 & ~G1 & ~G3 & ~G181;
  assign G61 = ~G328 | ~G313 | ~G317 | ~G146;
  assign G206 = ~G233 & ~G231 & ~G232;
  assign G77 = ~G210 & ~G211;
  assign G165 = ~G166 & ~G167;
  assign G192 = ~G199 & ~G200;
  assign G104 = ~G117 & ~G118;
  assign G324 = ~G120 & ~G121;
  assign G159 = ~G164 & ~G165;
  assign G186 = ~G191 & ~G192;
  assign G221 = ~G226 & ~G227;
  assign G261 = ~G268 & ~G269;
  always @ (posedge clock) begin
    G38 <= n76;
    G39 <= n81;
    G40 <= n86;
    G41 <= n91;
    G42 <= n96;
  end
endmodule


