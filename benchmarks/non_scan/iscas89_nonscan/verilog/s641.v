// Benchmark "s641.blif" written by ABC on Mon Apr  8 18:07:53 2019

module \s641.blif  ( clock, 
    G1, G2, G3, G4, G5, G6, G8, G9, G10, G11, G12, G13, G14, G15, G16, G17,
    G18, G19, G20, G21, G22, G23, G24, G25, G26, G27, G28, G29, G30, G31,
    G32, G33, G34, G35, G36,
    G91, G94, G107, G83, G84, G85, G100BF, G98BF, G96BF, G92, G87BF, G89BF,
    G101BF, G106BF, G97BF, G104BF, G88BF, G99BF, G105BF, G138, G86BF,
    G95BF, G103BF, G90  );
  input  clock;
  input  G1, G2, G3, G4, G5, G6, G8, G9, G10, G11, G12, G13, G14, G15,
    G16, G17, G18, G19, G20, G21, G22, G23, G24, G25, G26, G27, G28, G29,
    G30, G31, G32, G33, G34, G35, G36;
  output G91, G94, G107, G83, G84, G85, G100BF, G98BF, G96BF, G92, G87BF,
    G89BF, G101BF, G106BF, G97BF, G104BF, G88BF, G99BF, G105BF, G138,
    G86BF, G95BF, G103BF, G90;
  reg G64, G65, G66, G67, G68, G69, G70, G71, G72, G73, G74, G75, G76, G77,
    G78, G79, G80, G81, G82;
  wire I633, G366, G379, I643, I646, I649, I652, I655, I660, I680, I684,
    I687, I165, II178, I169, I172, I175, I178, I181, I184, I187, I190,
    I193, I196, I199, I202, I205, I208, I211, G352, G360, G361, G362, G363,
    G364, G367, G386, G388, G389, G113, G115, G117, G219, G119, G221, G121,
    G223, G209, G109, G211, G111, G213, G215, G217, G110, G114, G118, G216,
    G218, G220, G222, G365, G368, G387, G225, G390, G289, I356, G324, I254,
    I257, G338, I260, I263, G344, I266, I269, G312, I272, G315, I275, G318,
    I278, G321, I281, G143, G166, G325, G194, G339, G202, G345, G313, G316,
    G319, G322, I303, G281, I299, G283, I313, I287, I291, I295, G350, I301,
    I315, G381, G100, G375, G98, G371, G96, G135, G137, G382, G376, G372,
    I321, I324, G329, G333, G87, I406, G89, I422, G173, G183, I335, I338,
    G174, G184, I341, G359, G355, G108, G356, G116, G293, I354, I357, G309,
    I360, I363, G146, G294, G162, G310, G341, I366, I369, G303, I372, I375,
    I378, I382, G198, G342, G154, G304, G383, G101, G396, G106, I386, I390,
    G384, G397, G373, G97, G392, G104, II476, G278, I279, G374, G393, G224,
    G282, I306, G237, I373, G286, II208, I308, I334, G285, I327, I210,
    G136, I336, I329, I442, G331, G88, I414, G178, I449, G179, I452, G357,
    G358, G112, G335, I460, I463, G306, I466, I469, G190, G336, G158, G307,
    I472, I476, G395, G377, G99, G277, II272, G105, G378, G276, I265, G280,
    I292, G235, I440, G284, I294, I320, G279, I285, G134, I322, II287,
    I517, G327, G86, I398, G168, I524, G169, I527, G353, G354, G120, G347,
    I535, I538, G300, I541, I544, G206, G348, G150, G301, I547, I551, G391,
    G369, G95, G103, G370, G275, I258, G271, I230, G239, I511, G288, G272,
    I237, G273, I244, G274, I251, I348, G287, II341, G270, I222, I350,
    I343, I224, G124, I608, G298, G231, G232, G233, G234, G247, G248, G263,
    G264, G214, G210, G266, G229, G245, G249, I533, G227, G243, G265, G236,
    G252, II527, G212, G228, G244, I515, G261, I512, II538, G256, G230,
    G246, G208, G226, G242, I553, I518, I521, II524, I495, G257, I537,
    G258, G259, G260, G241, G267, G238, G254, I546, n120, n125, n130, n135,
    n140, n145, n149, n154, n159, n164, n169, n174, n179, n184, n189, n194,
    n199, n204, n209;
  assign G91 = ~I165;
  assign G94 = ~II178;
  assign G107 = G313 & G18;
  assign G83 = G316 & G19;
  assign G84 = G319 & G20;
  assign G85 = G322 & G21;
  assign G100BF = ~G100;
  assign G98BF = ~G98;
  assign G96BF = ~G96;
  assign G92 = G350 & G28;
  assign G87BF = ~G87;
  assign G89BF = ~G89;
  assign G101BF = ~G101;
  assign G106BF = ~G106;
  assign G97BF = ~G97;
  assign G104BF = ~G104;
  assign G88BF = ~G88;
  assign G99BF = ~G99;
  assign G105BF = ~G105;
  assign G138 = ~I322;
  assign G86BF = ~G86;
  assign G95BF = ~G95;
  assign G103BF = ~G103;
  assign G90 = G298 & G26;
  assign n120 = ~I551;
  assign n125 = G397 & G395 & G366 & G392;
  assign n130 = ~I476;
  assign n135 = G366 & G396;
  assign n140 = ~I210;
  assign n145 = ~II287;
  assign n154 = ~I329;
  assign n159 = ~I336;
  assign n164 = ~I343;
  assign n169 = ~I350;
  assign n174 = ~I230;
  assign n179 = ~I237;
  assign n184 = ~I244;
  assign n189 = ~I251;
  assign n194 = ~I258;
  assign n199 = ~I265;
  assign n204 = ~II272;
  assign n209 = ~I279;
  assign I633 = ~G1;
  assign G366 = ~G2;
  assign G379 = ~G3;
  assign I643 = ~G4;
  assign I646 = ~G5;
  assign I649 = ~G6;
  assign I652 = ~G8;
  assign I655 = ~G9;
  assign I660 = ~G10;
  assign I680 = ~G11;
  assign I684 = ~G12;
  assign I687 = ~G13;
  assign I165 = ~G27;
  assign II178 = ~G29;
  assign I169 = ~G70;
  assign I172 = ~G71;
  assign I175 = ~G72;
  assign I178 = ~G80;
  assign I181 = ~G73;
  assign I184 = ~G81;
  assign I187 = ~G74;
  assign I190 = ~G82;
  assign I193 = ~G75;
  assign I196 = ~G68;
  assign I199 = ~G76;
  assign I202 = ~G69;
  assign I205 = ~G77;
  assign I208 = ~G78;
  assign I211 = ~G79;
  assign G352 = ~I633;
  assign G360 = ~I643;
  assign G361 = ~I646;
  assign G362 = ~I649;
  assign G363 = ~I652;
  assign G364 = ~I655;
  assign G367 = ~I660;
  assign G386 = ~I680;
  assign G388 = ~I684;
  assign G389 = ~I687;
  assign G113 = ~I169;
  assign G115 = ~I172;
  assign G117 = ~I175;
  assign G219 = ~I178;
  assign G119 = ~I181;
  assign G221 = ~I184;
  assign G121 = ~I187;
  assign G223 = ~I190;
  assign G209 = ~I193;
  assign G109 = ~I196;
  assign G211 = ~I199;
  assign G111 = ~I202;
  assign G213 = ~I205;
  assign G215 = ~I208;
  assign G217 = ~I211;
  assign G110 = ~G360;
  assign G114 = ~G360;
  assign G118 = ~G360;
  assign G216 = ~G360;
  assign G218 = ~G360;
  assign G220 = ~G360;
  assign G222 = ~G360;
  assign G365 = ~G364;
  assign G368 = ~G367;
  assign G387 = ~G386;
  assign G225 = ~G388;
  assign G390 = ~G389;
  assign G289 = G389 & G386 & G388;
  assign I356 = ~G289;
  assign G324 = G110 & G111;
  assign I254 = ~G324;
  assign I257 = ~G324;
  assign G338 = G114 & G115;
  assign I260 = ~G338;
  assign I263 = ~G338;
  assign G344 = G118 & G119;
  assign I266 = ~G344;
  assign I269 = ~G344;
  assign G312 = G216 & G217;
  assign I272 = ~G312;
  assign G315 = G218 & G219;
  assign I275 = ~G315;
  assign G318 = G220 & G221;
  assign I278 = ~G318;
  assign G321 = G222 & G223;
  assign I281 = ~G321;
  assign G143 = ~I356;
  assign G166 = ~I254;
  assign G325 = ~I257;
  assign G194 = ~I260;
  assign G339 = ~I263;
  assign G202 = ~I266;
  assign G345 = ~I269;
  assign G313 = ~I272;
  assign G316 = ~I275;
  assign G319 = ~I278;
  assign G322 = ~I281;
  assign I303 = ~G143;
  assign G281 = G65 | G232 | G248;
  assign I299 = ~G281;
  assign G283 = G264 | G234 | G67;
  assign I313 = ~G283;
  assign I287 = ~G166;
  assign I291 = ~G194;
  assign I295 = ~G202;
  assign G350 = ~I303;
  assign I301 = ~I299;
  assign I315 = ~I313;
  assign G381 = ~I287;
  assign G100 = G325 & G35;
  assign G375 = ~I291;
  assign G98 = G339 & G33;
  assign G371 = ~I295;
  assign G96 = G345 & G31;
  assign G135 = ~I301;
  assign G137 = ~I315;
  assign G382 = ~G381;
  assign G376 = ~G375;
  assign G372 = ~G371;
  assign I321 = ~G135;
  assign I324 = ~G137;
  assign G329 = ~I321;
  assign G333 = ~I324;
  assign G87 = G329 & G23;
  assign I406 = ~G87;
  assign G89 = G333 & G25;
  assign I422 = ~G89;
  assign G173 = ~I406;
  assign G183 = ~I422;
  assign I335 = ~G173;
  assign I338 = ~G183;
  assign G174 = ~I335;
  assign G184 = ~I338;
  assign I341 = ~G174;
  assign G359 = ~G184;
  assign G355 = ~I341;
  assign G108 = ~G359;
  assign G356 = ~G355;
  assign G116 = ~G356;
  assign G293 = G108 & G109;
  assign I354 = ~G293;
  assign I357 = ~G293;
  assign G309 = G214 & G215;
  assign I360 = ~G309;
  assign I363 = ~G309;
  assign G146 = ~I354;
  assign G294 = ~I357;
  assign G162 = ~I360;
  assign G310 = ~I363;
  assign G341 = G116 & G117;
  assign I366 = ~G341;
  assign I369 = ~G341;
  assign G303 = G210 & G211;
  assign I372 = ~G303;
  assign I375 = ~G303;
  assign I378 = ~G146;
  assign I382 = ~G162;
  assign G198 = ~I366;
  assign G342 = ~I369;
  assign G154 = ~I372;
  assign G304 = ~I375;
  assign G383 = ~I378;
  assign G101 = G294 & G36;
  assign G396 = ~I382;
  assign G106 = G310 & G17;
  assign I386 = ~G198;
  assign I390 = ~G154;
  assign G384 = ~G383;
  assign G397 = ~G396;
  assign G373 = ~I386;
  assign G97 = G342 & G32;
  assign G392 = ~I390;
  assign G104 = G304 & G15;
  assign II476 = ~G384;
  assign G278 = G366 & G396;
  assign I279 = ~G278;
  assign G374 = ~G373;
  assign G393 = ~G392;
  assign G224 = ~II476;
  assign G282 = G263 | G233 | G249;
  assign I306 = ~G282;
  assign G237 = G374 & G375;
  assign I373 = ~G237;
  assign G286 = ~I373;
  assign II208 = ~G224;
  assign I308 = ~I306;
  assign I334 = ~G286;
  assign G285 = G236 | G252;
  assign I327 = ~G285;
  assign I210 = ~II208;
  assign G136 = ~I308;
  assign I336 = ~I334;
  assign I329 = ~I327;
  assign I442 = ~G136;
  assign G331 = ~I442;
  assign G88 = G331 & G24;
  assign I414 = ~G88;
  assign G178 = ~I414;
  assign I449 = ~G178;
  assign G179 = ~I449;
  assign I452 = ~G179;
  assign G357 = ~I452;
  assign G358 = ~G357;
  assign G112 = ~G358;
  assign G335 = G112 & G113;
  assign I460 = ~G335;
  assign I463 = ~G335;
  assign G306 = G212 & G213;
  assign I466 = ~G306;
  assign I469 = ~G306;
  assign G190 = ~I460;
  assign G336 = ~I463;
  assign G158 = ~I466;
  assign G307 = ~I469;
  assign I472 = ~G190;
  assign I476 = ~G158;
  assign G395 = ~G158;
  assign G377 = ~I472;
  assign G99 = G336 & G34;
  assign G277 = G397 & G366 & G158;
  assign II272 = ~G277;
  assign G105 = G307 & G16;
  assign G378 = ~G377;
  assign G276 = G397 & G395 & G366 & G392;
  assign I265 = ~G276;
  assign G280 = G261 | G231 | G247;
  assign I292 = ~G280;
  assign G235 = G378 & G381;
  assign I440 = ~G235;
  assign G284 = ~I440;
  assign I294 = ~I292;
  assign I320 = ~G284;
  assign G279 = G230 | G246;
  assign I285 = ~G279;
  assign G134 = ~I294;
  assign I322 = ~I320;
  assign II287 = ~I285;
  assign I517 = ~G134;
  assign G327 = ~I517;
  assign G86 = G327 & G22;
  assign I398 = ~G86;
  assign G168 = ~I398;
  assign I524 = ~G168;
  assign G169 = ~I524;
  assign I527 = ~G169;
  assign G353 = ~I527;
  assign G354 = ~G353;
  assign G120 = ~G354;
  assign G347 = G120 & G121;
  assign I535 = ~G347;
  assign I538 = ~G347;
  assign G300 = G208 & G209;
  assign I541 = ~G300;
  assign I544 = ~G300;
  assign G206 = ~I535;
  assign G348 = ~I538;
  assign G150 = ~I541;
  assign G301 = ~I544;
  assign I547 = ~G206;
  assign I551 = ~G150;
  assign G391 = ~G150;
  assign G369 = ~I547;
  assign G95 = G348 & G30;
  assign G103 = G301 & G14;
  assign G370 = ~G369;
  assign G275 = I553 & G395 & G397;
  assign I258 = ~G275;
  assign G271 = G257 | G226 | G242;
  assign I230 = ~G271;
  assign G239 = G370 & G371;
  assign I511 = ~G239;
  assign G288 = ~I511;
  assign G272 = G258 | G227 | G243;
  assign I237 = ~G272;
  assign G273 = G259 | G228 | G244;
  assign I244 = ~G273;
  assign G274 = G260 | G229 | G245;
  assign I251 = ~G274;
  assign I348 = ~G288;
  assign G287 = G238 | G254;
  assign II341 = ~G287;
  assign G270 = G265 | G266 | G267 | I546;
  assign I222 = ~G270;
  assign I350 = ~I348;
  assign I343 = ~II341;
  assign I224 = ~I222;
  assign G124 = ~I224;
  assign I608 = ~G124;
  assign G298 = ~I608;
  assign G231 = G379 & G387;
  assign G232 = G379 & G387;
  assign G233 = G379 & G387;
  assign G234 = G379 & G387;
  assign G247 = G390 & G368 & G379 & G365;
  assign G248 = G390 & G367 & G379 & G365;
  assign G263 = G390 & G368 & G379 & G364;
  assign G264 = G390 & G367 & G379 & G364;
  assign G214 = ~G379 | ~G359;
  assign G210 = ~G379 | ~G356;
  assign G266 = G390 & G383 & G364 & G367;
  assign G229 = G366 & G396;
  assign G245 = G352 & G396;
  assign G249 = G397 & G366 & G66;
  assign I533 = G373 & G365 & G367;
  assign G227 = G366 & G392;
  assign G243 = G392 & G361;
  assign G265 = I533 & G375 & G390;
  assign G236 = G374 & G376;
  assign G252 = G355 & G374;
  assign II527 = G393 & G366 & G64;
  assign G212 = ~G379 | ~G358;
  assign G228 = G366 & G158;
  assign G244 = G158 & G362;
  assign I515 = G397 & G393 & G395;
  assign G261 = II527 & G395 & G397;
  assign I512 = G377 & G364 & G368;
  assign II538 = G387 & G383 & G377 & G381;
  assign G256 = I512 & G381 & G390;
  assign G230 = G378 & G382;
  assign G246 = G357 & G378;
  assign G208 = ~G379 | ~G354;
  assign G226 = G366 & G150;
  assign G242 = G150 & G363;
  assign I553 = G393 & G366 & G150;
  assign I518 = G397 & G391 & G395;
  assign I521 = G397 & G391 & G393;
  assign II524 = G393 & G352 & G391;
  assign I495 = G369 & G365 & G368;
  assign G257 = I515 & G371 & G363 & G369;
  assign I537 = G375 & G373 & G369 & G371;
  assign G258 = I518 & G375 & G361 & G373;
  assign G259 = I521 & G381 & G362 & G377;
  assign G260 = II524 & G395 & G383;
  assign G241 = I495 & G371 & G390;
  assign G267 = I537 & II538;
  assign G238 = G370 & G372;
  assign G254 = G353 & G370;
  assign I546 = G256 | G225 | G241;
  assign n149 = G138;
  always @ (posedge clock) begin
    G64 <= n120;
    G65 <= n125;
    G66 <= n130;
    G67 <= n135;
    G68 <= n140;
    G69 <= n145;
    G70 <= n149;
    G71 <= n154;
    G72 <= n159;
    G73 <= n164;
    G74 <= n169;
    G75 <= n174;
    G76 <= n179;
    G77 <= n184;
    G78 <= n189;
    G79 <= n194;
    G80 <= n199;
    G81 <= n204;
    G82 <= n209;
  end
endmodule


