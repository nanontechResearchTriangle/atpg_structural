// Benchmark "b04s" written by ABC on Mon Apr  8 17:51:07 2019

module b04s ( clock, 
    RESTART, AVERAGE, ENABLE, DATA_IN_7_, DATA_IN_6_, DATA_IN_5_,
    DATA_IN_4_, DATA_IN_3_, DATA_IN_2_, DATA_IN_1_, DATA_IN_0_,
    DATA_OUT_REG_7_, DATA_OUT_REG_6_, DATA_OUT_REG_5_, DATA_OUT_REG_4_,
    DATA_OUT_REG_3_, DATA_OUT_REG_2_, DATA_OUT_REG_1_, DATA_OUT_REG_0_  );
  input  clock;
  input  RESTART, AVERAGE, ENABLE, DATA_IN_7_, DATA_IN_6_, DATA_IN_5_,
    DATA_IN_4_, DATA_IN_3_, DATA_IN_2_, DATA_IN_1_, DATA_IN_0_;
  output DATA_OUT_REG_7_, DATA_OUT_REG_6_, DATA_OUT_REG_5_, DATA_OUT_REG_4_,
    DATA_OUT_REG_3_, DATA_OUT_REG_2_, DATA_OUT_REG_1_, DATA_OUT_REG_0_;
  reg RMAX_REG_7_, RMAX_REG_6_, RMAX_REG_5_, RMAX_REG_4_, RMAX_REG_3_,
    RMAX_REG_2_, RMAX_REG_1_, RMAX_REG_0_, RMIN_REG_7_, RMIN_REG_6_,
    RMIN_REG_5_, RMIN_REG_4_, RMIN_REG_3_, RMIN_REG_2_, RMIN_REG_1_,
    RMIN_REG_0_, RLAST_REG_7_, RLAST_REG_6_, RLAST_REG_5_, RLAST_REG_4_,
    RLAST_REG_3_, RLAST_REG_2_, RLAST_REG_1_, RLAST_REG_0_, REG1_REG_7_,
    REG1_REG_6_, REG1_REG_5_, REG1_REG_4_, REG1_REG_3_, REG1_REG_2_,
    REG1_REG_1_, REG1_REG_0_, REG2_REG_7_, REG2_REG_6_, REG2_REG_5_,
    REG2_REG_4_, REG2_REG_3_, REG2_REG_2_, REG2_REG_1_, REG2_REG_0_,
    REG3_REG_7_, REG3_REG_6_, REG3_REG_5_, REG3_REG_4_, REG3_REG_3_,
    REG3_REG_2_, REG3_REG_1_, REG3_REG_0_, REG4_REG_7_, REG4_REG_6_,
    REG4_REG_5_, REG4_REG_4_, REG4_REG_3_, REG4_REG_2_, REG4_REG_1_,
    REG4_REG_0_, DATA_OUT_REG_7_, DATA_OUT_REG_6_, DATA_OUT_REG_5_,
    DATA_OUT_REG_4_, DATA_OUT_REG_3_, DATA_OUT_REG_2_, DATA_OUT_REG_1_,
    DATA_OUT_REG_0_, STATO_REG_1_, STATO_REG_0_;
  wire R139_U6, U337, U338, U339, U340, U341, U342, U343, U344, U345, U346,
    U347, U348, U349, U350, U351, U352, U353, U354, U355, U356, U357, U358,
    U359, U360, U361, U362, U363, U364, U365, U366, U367, U369, U370, U371,
    U372, U373, U374, U375, U376, U377, U378, U379, U380, U381, U382, U383,
    U384, U385, U386, U387, U388, U389, U390, U391, U392, U393, U394, U395,
    U396, U397, U398, U399, U400, U401, U402, U403, U404, U405, U406, U407,
    U408, U409, U410, U411, U412, U413, U414, U415, U416, U417, U418, U419,
    U420, U421, U422, U423, U424, U425, U426, U427, U428, U429, U430, U431,
    U432, U433, U434, U435, U436, U437, U438, U439, U440, U441, U442, U443,
    U444, U445, U446, U447, U448, U449, U450, U451, U452, U453, U454, U455,
    U456, U457, U458, U459, U460, U461, U462, U463, U464, U465, U466, U467,
    U468, U469, U470, U471, U472, U473, U474, U475, U476, U477, U478, U479,
    U480, U481, U482, U483, U484, U485, U486, U487, U488, U489, U490, U491,
    U492, U493, U494, U495, U496, U497, U498, U499, U500, U501, U502, U503,
    U504, U505, U506, U507, U508, U509, U510, U511, U512, U513, U514, U515,
    U516, U517, U518, U519, U520, U521, U522, U523, U524, U525, U526, U527,
    U528, U529, U530, U531, U532, U533, U534, U535, U536, U537, U538, U539,
    U540, U541, U542, U543, U544, U545, U546, U547, U548, U549, U550, U551,
    U552, U553, U554, U555, U556, U557, U558, U559, U560, U561, U562, U563,
    U564, U565, U566, U567, U568, U569, U570, LT_90_U35, LT_90_U34,
    LT_90_U33, LT_90_U32, LT_90_U31, LT_90_U30, LT_90_U29, R135_U5,
    R135_U6, R135_U7, R135_U8, R135_U9, R135_U10, R135_U11, R135_U12,
    R135_U13, R135_U14, R135_U15, R135_U16, R135_U17, R135_U18, R135_U19,
    R135_U20, R135_U21, R135_U22, R135_U23, R135_U24, R135_U25, R135_U26,
    R135_U27, R135_U28, R135_U29, R135_U30, R135_U31, R135_U32, R135_U33,
    R135_U34, R135_U35, R135_U36, GT_88_U6, GT_88_U7, GT_88_U8, GT_88_U9,
    GT_88_U10, GT_88_U11, GT_88_U12, GT_88_U13, GT_88_U14, GT_88_U15,
    GT_88_U16, GT_88_U17, GT_88_U18, GT_88_U19, GT_88_U20, GT_88_U21,
    GT_88_U22, GT_88_U23, GT_88_U24, GT_88_U25, GT_88_U26, GT_88_U27,
    GT_88_U28, GT_88_U29, GT_88_U30, GT_88_U31, GT_88_U32, GT_88_U33,
    GT_88_U34, GT_88_U35, R81_U6, R81_U7, R81_U8, R81_U9, R81_U10, R81_U11,
    R81_U12, R81_U13, R81_U14, R81_U15, R81_U16, R81_U17, R81_U18, R81_U19,
    R81_U20, R81_U21, R81_U22, R81_U23, R81_U24, R81_U25, R81_U26, R81_U27,
    R81_U28, R81_U29, R57_U5, R57_U6, R57_U7, R57_U8, R57_U9, R57_U10,
    R57_U11, R57_U12, R57_U13, R57_U14, R57_U15, R57_U16, R57_U17, R57_U18,
    R57_U19, R57_U20, R57_U21, R57_U22, R57_U23, R57_U24, R57_U25, R57_U26,
    R57_U27, R57_U28, R57_U29, R57_U30, R57_U31, R57_U32, R57_U33, R57_U34,
    R57_U35, R57_U36, R57_U37, R57_U38, R57_U39, R57_U40, R57_U41, R57_U42,
    R57_U43, R57_U44, R57_U45, R57_U46, R57_U47, R57_U48, R57_U49, R57_U50,
    R57_U51, R57_U52, R57_U53, R57_U54, R57_U55, R57_U56, R57_U57, R57_U58,
    R57_U59, R57_U60, R57_U61, R57_U62, R57_U63, R57_U64, R57_U65, R57_U66,
    R57_U67, R57_U68, R57_U69, R57_U70, R57_U71, R57_U72, R57_U73, R57_U74,
    R57_U75, R57_U76, R57_U77, R57_U78, R134_U6, R134_U7, R134_U8, R134_U9,
    R134_U10, R134_U11, R134_U12, R134_U13, R134_U14, R134_U15, R134_U16,
    R134_U17, R134_U18, R134_U19, R134_U20, R134_U21, R134_U22, R134_U23,
    R134_U24, R134_U25, R134_U26, R134_U27, LT_90_U6, LT_90_U7, LT_90_U8,
    LT_90_U9, LT_90_U10, LT_90_U11, LT_90_U12, LT_90_U13, LT_90_U14,
    LT_90_U15, LT_90_U16, LT_90_U17, LT_90_U18, LT_90_U19, LT_90_U20,
    LT_90_U21, LT_90_U22, LT_90_U23, LT_90_U24, LT_90_U25, LT_90_U26,
    LT_90_U27, LT_90_U28, n40, n45, n50, n55, n60, n65, n70, n75, n80, n85,
    n90, n95, n100, n105, n110, n115, n120, n125, n130, n135, n140, n145,
    n150, n155, n160, n165, n170, n175, n180, n185, n190, n195, n200, n205,
    n210, n215, n220, n225, n230, n235, n240, n245, n250, n255, n260, n265,
    n270, n275, n280, n285, n290, n295, n300, n305, n310, n315, n320, n324,
    n328, n332, n336, n340, n344, n348, n352, n357;
  assign R139_U6 = ~R135_U5;
  assign n40 = ~U377 | ~U376;
  assign n45 = ~U379 | ~U378;
  assign n50 = ~U381 | ~U380;
  assign n55 = ~U383 | ~U382;
  assign n60 = ~U385 | ~U384;
  assign n65 = ~U387 | ~U386;
  assign n70 = ~U389 | ~U388;
  assign n75 = ~U391 | ~U390;
  assign n80 = ~U398 | ~U397;
  assign n85 = ~U400 | ~U399;
  assign n90 = ~U402 | ~U401;
  assign n95 = ~U404 | ~U403;
  assign n100 = ~U406 | ~U405;
  assign n105 = ~U408 | ~U407;
  assign n110 = ~U410 | ~U409;
  assign n115 = ~U412 | ~U411;
  assign n120 = ~U416 | ~U415;
  assign n125 = ~U418 | ~U417;
  assign n130 = ~U420 | ~U419;
  assign n135 = ~U422 | ~U421;
  assign n140 = ~U424 | ~U423;
  assign n145 = ~U426 | ~U425;
  assign n150 = ~U428 | ~U427;
  assign n155 = ~U430 | ~U429;
  assign n160 = ~U432 | ~U431;
  assign n165 = ~U434 | ~U433;
  assign n170 = ~U436 | ~U435;
  assign n175 = ~U438 | ~U437;
  assign n180 = ~U440 | ~U439;
  assign n185 = ~U442 | ~U441;
  assign n190 = ~U444 | ~U443;
  assign n195 = ~U446 | ~U445;
  assign n200 = ~U448 | ~U447;
  assign n205 = ~U450 | ~U449;
  assign n210 = ~U452 | ~U451;
  assign n215 = ~U454 | ~U453;
  assign n220 = ~U456 | ~U455;
  assign n225 = ~U458 | ~U457;
  assign n230 = ~U460 | ~U459;
  assign n235 = ~U462 | ~U461;
  assign n240 = ~U464 | ~U463;
  assign n245 = ~U466 | ~U465;
  assign n250 = ~U468 | ~U467;
  assign n255 = ~U470 | ~U469;
  assign n260 = ~U472 | ~U471;
  assign n265 = ~U474 | ~U473;
  assign n270 = ~U476 | ~U475;
  assign n275 = ~U478 | ~U477;
  assign n280 = ~U480 | ~U479;
  assign n285 = ~U482 | ~U481;
  assign n290 = ~U484 | ~U483;
  assign n295 = ~U486 | ~U485;
  assign n300 = ~U488 | ~U487;
  assign n305 = ~U490 | ~U489;
  assign n310 = ~U492 | ~U491;
  assign n315 = ~U494 | ~U493;
  assign n320 = ~U498 | ~U499 | ~U501 | ~U500;
  assign n352 = U341 | U370;
  assign n324 = ~U502 | ~U503 | ~U505 | ~U504;
  assign n328 = ~U511 | ~U512 | ~U510 | ~U509 | ~U508;
  assign n332 = ~U516 | ~U517 | ~U515 | ~U514 | ~U513;
  assign n336 = ~U521 | ~U522 | ~U520 | ~U519 | ~U518;
  assign n340 = ~U526 | ~U527 | ~U525 | ~U524 | ~U523;
  assign n344 = ~U531 | ~U532 | ~U530 | ~U529 | ~U528;
  assign n348 = ~U536 | ~U537 | ~U535 | ~U534 | ~U533;
  assign U337 = ~STATO_REG_0_;
  assign U338 = ~STATO_REG_1_;
  assign U339 = ~GT_88_U6;
  assign U340 = STATO_REG_1_ & U413;
  assign U341 = STATO_REG_1_ & U337;
  assign U342 = ~U338 & ~U371;
  assign U343 = U342 & U345;
  assign U344 = ~ENABLE;
  assign U345 = ~RESTART;
  assign U346 = U343 & U344;
  assign U347 = AVERAGE & ENABLE & U343;
  assign U348 = U342 & U507;
  assign U349 = U348 & U350;
  assign U350 = ~R139_U6;
  assign U351 = R139_U6 & U348;
  assign U352 = ~U539 | ~U538;
  assign U353 = ~U541 | ~U540;
  assign U354 = ~U543 | ~U542;
  assign U355 = ~U545 | ~U544;
  assign U356 = ~U547 | ~U546;
  assign U357 = ~U549 | ~U548;
  assign U358 = ~U551 | ~U550;
  assign U359 = ~U553 | ~U552;
  assign U360 = ~U555 | ~U554;
  assign U361 = ~U557 | ~U556;
  assign U362 = ~U559 | ~U558;
  assign U363 = ~U561 | ~U560;
  assign U364 = ~U563 | ~U562;
  assign U365 = ~U565 | ~U564;
  assign U366 = ~U567 | ~U566;
  assign U367 = ~U569 | ~U568;
  assign n357 = U338 & U337;
  assign U369 = ~n357;
  assign U370 = STATO_REG_0_ & U338;
  assign U371 = ~n352;
  assign U372 = ~GT_88_U6 | ~STATO_REG_1_;
  assign U373 = ~U337 | ~U372;
  assign U374 = STATO_REG_0_ | GT_88_U6;
  assign U375 = ~U369 | ~U374;
  assign U376 = ~RMAX_REG_7_ | ~U375;
  assign U377 = ~DATA_IN_7_ | ~U373;
  assign U378 = ~RMAX_REG_6_ | ~U375;
  assign U379 = ~DATA_IN_6_ | ~U373;
  assign U380 = ~RMAX_REG_5_ | ~U375;
  assign U381 = ~DATA_IN_5_ | ~U373;
  assign U382 = ~RMAX_REG_4_ | ~U375;
  assign U383 = ~DATA_IN_4_ | ~U373;
  assign U384 = ~RMAX_REG_3_ | ~U375;
  assign U385 = ~DATA_IN_3_ | ~U373;
  assign U386 = ~RMAX_REG_2_ | ~U375;
  assign U387 = ~DATA_IN_2_ | ~U373;
  assign U388 = ~RMAX_REG_1_ | ~U375;
  assign U389 = ~DATA_IN_1_ | ~U373;
  assign U390 = ~RMAX_REG_0_ | ~U375;
  assign U391 = ~DATA_IN_0_ | ~U373;
  assign U392 = ~LT_90_U6 | ~U339;
  assign U393 = ~U392 | ~U337;
  assign U394 = ~U369 | ~U393;
  assign U395 = ~LT_90_U6 | ~STATO_REG_1_ | ~U339;
  assign U396 = ~U337 | ~U395;
  assign U397 = ~DATA_IN_7_ | ~U396;
  assign U398 = ~RMIN_REG_7_ | ~U394;
  assign U399 = ~DATA_IN_6_ | ~U396;
  assign U400 = ~RMIN_REG_6_ | ~U394;
  assign U401 = ~DATA_IN_5_ | ~U396;
  assign U402 = ~RMIN_REG_5_ | ~U394;
  assign U403 = ~DATA_IN_4_ | ~U396;
  assign U404 = ~RMIN_REG_4_ | ~U394;
  assign U405 = ~DATA_IN_3_ | ~U396;
  assign U406 = ~RMIN_REG_3_ | ~U394;
  assign U407 = ~DATA_IN_2_ | ~U396;
  assign U408 = ~RMIN_REG_2_ | ~U394;
  assign U409 = ~DATA_IN_1_ | ~U396;
  assign U410 = ~RMIN_REG_1_ | ~U394;
  assign U411 = ~DATA_IN_0_ | ~U396;
  assign U412 = ~RMIN_REG_0_ | ~U394;
  assign U413 = ENABLE | STATO_REG_0_;
  assign U414 = ~U369 | ~U413;
  assign U415 = ~U340 | ~DATA_IN_7_;
  assign U416 = ~RLAST_REG_7_ | ~U414;
  assign U417 = ~U340 | ~DATA_IN_6_;
  assign U418 = ~RLAST_REG_6_ | ~U414;
  assign U419 = ~U340 | ~DATA_IN_5_;
  assign U420 = ~RLAST_REG_5_ | ~U414;
  assign U421 = ~U340 | ~DATA_IN_4_;
  assign U422 = ~RLAST_REG_4_ | ~U414;
  assign U423 = ~U340 | ~DATA_IN_3_;
  assign U424 = ~RLAST_REG_3_ | ~U414;
  assign U425 = ~U340 | ~DATA_IN_2_;
  assign U426 = ~RLAST_REG_2_ | ~U414;
  assign U427 = ~U340 | ~DATA_IN_1_;
  assign U428 = ~RLAST_REG_1_ | ~U414;
  assign U429 = ~U340 | ~DATA_IN_0_;
  assign U430 = ~RLAST_REG_0_ | ~U414;
  assign U431 = ~U341 | ~DATA_IN_7_;
  assign U432 = ~REG1_REG_7_ | ~U371;
  assign U433 = ~U341 | ~DATA_IN_6_;
  assign U434 = ~REG1_REG_6_ | ~U371;
  assign U435 = ~U341 | ~DATA_IN_5_;
  assign U436 = ~REG1_REG_5_ | ~U371;
  assign U437 = ~U341 | ~DATA_IN_4_;
  assign U438 = ~REG1_REG_4_ | ~U371;
  assign U439 = ~U341 | ~DATA_IN_3_;
  assign U440 = ~REG1_REG_3_ | ~U371;
  assign U441 = ~U341 | ~DATA_IN_2_;
  assign U442 = ~REG1_REG_2_ | ~U371;
  assign U443 = ~U341 | ~DATA_IN_1_;
  assign U444 = ~REG1_REG_1_ | ~U371;
  assign U445 = ~U341 | ~DATA_IN_0_;
  assign U446 = ~REG1_REG_0_ | ~U371;
  assign U447 = ~REG1_REG_7_ | ~U341;
  assign U448 = ~REG2_REG_7_ | ~U371;
  assign U449 = ~REG1_REG_6_ | ~U341;
  assign U450 = ~REG2_REG_6_ | ~U371;
  assign U451 = ~REG1_REG_5_ | ~U341;
  assign U452 = ~REG2_REG_5_ | ~U371;
  assign U453 = ~REG1_REG_4_ | ~U341;
  assign U454 = ~REG2_REG_4_ | ~U371;
  assign U455 = ~REG1_REG_3_ | ~U341;
  assign U456 = ~REG2_REG_3_ | ~U371;
  assign U457 = ~REG1_REG_2_ | ~U341;
  assign U458 = ~REG2_REG_2_ | ~U371;
  assign U459 = ~REG1_REG_1_ | ~U341;
  assign U460 = ~REG2_REG_1_ | ~U371;
  assign U461 = ~REG1_REG_0_ | ~U341;
  assign U462 = ~REG2_REG_0_ | ~U371;
  assign U463 = ~REG2_REG_7_ | ~U341;
  assign U464 = ~REG3_REG_7_ | ~U371;
  assign U465 = ~REG2_REG_6_ | ~U341;
  assign U466 = ~REG3_REG_6_ | ~U371;
  assign U467 = ~REG2_REG_5_ | ~U341;
  assign U468 = ~REG3_REG_5_ | ~U371;
  assign U469 = ~REG2_REG_4_ | ~U341;
  assign U470 = ~REG3_REG_4_ | ~U371;
  assign U471 = ~REG2_REG_3_ | ~U341;
  assign U472 = ~REG3_REG_3_ | ~U371;
  assign U473 = ~REG2_REG_2_ | ~U341;
  assign U474 = ~REG3_REG_2_ | ~U371;
  assign U475 = ~REG2_REG_1_ | ~U341;
  assign U476 = ~REG3_REG_1_ | ~U371;
  assign U477 = ~REG2_REG_0_ | ~U341;
  assign U478 = ~REG3_REG_0_ | ~U371;
  assign U479 = ~REG3_REG_7_ | ~U341;
  assign U480 = ~REG4_REG_7_ | ~U371;
  assign U481 = ~REG3_REG_6_ | ~U341;
  assign U482 = ~REG4_REG_6_ | ~U371;
  assign U483 = ~REG3_REG_5_ | ~U341;
  assign U484 = ~REG4_REG_5_ | ~U371;
  assign U485 = ~REG3_REG_4_ | ~U341;
  assign U486 = ~REG4_REG_4_ | ~U371;
  assign U487 = ~REG3_REG_3_ | ~U341;
  assign U488 = ~REG4_REG_3_ | ~U371;
  assign U489 = ~REG3_REG_2_ | ~U341;
  assign U490 = ~REG4_REG_2_ | ~U371;
  assign U491 = ~REG3_REG_1_ | ~U341;
  assign U492 = ~REG4_REG_1_ | ~U371;
  assign U493 = ~REG3_REG_0_ | ~U341;
  assign U494 = ~REG4_REG_0_ | ~U371;
  assign U495 = U344 | U570 | AVERAGE;
  assign U496 = ~RESTART | ~U342;
  assign U497 = ~U496 | ~U495;
  assign U498 = ~R81_U9 | ~U497 | ~U350;
  assign U499 = ~U346 | ~RLAST_REG_7_;
  assign U500 = ~U347 | ~REG4_REG_7_;
  assign U501 = ~DATA_OUT_REG_7_ | ~U371;
  assign U502 = ~R81_U14 | ~U497 | ~U350;
  assign U503 = ~U346 | ~RLAST_REG_6_;
  assign U504 = ~U347 | ~REG4_REG_6_;
  assign U505 = ~DATA_OUT_REG_6_ | ~U371;
  assign U506 = U344 | AVERAGE;
  assign U507 = ~U345 | ~U506;
  assign U508 = ~R81_U8 | ~U349;
  assign U509 = ~R57_U6 | ~U351;
  assign U510 = ~U346 | ~RLAST_REG_5_;
  assign U511 = ~U347 | ~REG4_REG_5_;
  assign U512 = ~DATA_OUT_REG_5_ | ~U371;
  assign U513 = ~R81_U7 | ~U349;
  assign U514 = ~R57_U22 | ~U351;
  assign U515 = ~U346 | ~RLAST_REG_4_;
  assign U516 = ~U347 | ~REG4_REG_4_;
  assign U517 = ~DATA_OUT_REG_4_ | ~U371;
  assign U518 = ~R81_U16 | ~U349;
  assign U519 = ~R57_U23 | ~U351;
  assign U520 = ~U346 | ~RLAST_REG_3_;
  assign U521 = ~U347 | ~REG4_REG_3_;
  assign U522 = ~DATA_OUT_REG_3_ | ~U371;
  assign U523 = ~R81_U6 | ~U349;
  assign U524 = ~R57_U5 | ~U351;
  assign U525 = ~U346 | ~RLAST_REG_2_;
  assign U526 = ~U347 | ~REG4_REG_2_;
  assign U527 = ~DATA_OUT_REG_2_ | ~U371;
  assign U528 = ~R81_U12 | ~U349;
  assign U529 = ~R57_U24 | ~U351;
  assign U530 = ~U346 | ~RLAST_REG_1_;
  assign U531 = ~U347 | ~REG4_REG_1_;
  assign U532 = ~DATA_OUT_REG_1_ | ~U371;
  assign U533 = ~R134_U14 | ~U349;
  assign U534 = ~R57_U7 | ~U351;
  assign U535 = ~U346 | ~RLAST_REG_0_;
  assign U536 = ~U347 | ~REG4_REG_0_;
  assign U537 = ~DATA_OUT_REG_0_ | ~U371;
  assign U538 = ~REG4_REG_6_ | ~U345;
  assign U539 = ~RESTART | ~RMIN_REG_6_;
  assign U540 = ~REG4_REG_5_ | ~U345;
  assign U541 = ~RESTART | ~RMIN_REG_5_;
  assign U542 = ~REG4_REG_4_ | ~U345;
  assign U543 = ~RESTART | ~RMIN_REG_4_;
  assign U544 = ~REG4_REG_3_ | ~U345;
  assign U545 = ~RESTART | ~RMIN_REG_3_;
  assign U546 = ~REG4_REG_2_ | ~U345;
  assign U547 = ~RESTART | ~RMIN_REG_2_;
  assign U548 = ~REG4_REG_1_ | ~U345;
  assign U549 = ~RESTART | ~RMIN_REG_1_;
  assign U550 = ~REG4_REG_0_ | ~U345;
  assign U551 = ~RESTART | ~RMIN_REG_0_;
  assign U552 = ~DATA_IN_6_ | ~U345;
  assign U553 = ~RESTART | ~RMAX_REG_6_;
  assign U554 = ~DATA_IN_5_ | ~U345;
  assign U555 = ~RESTART | ~RMAX_REG_5_;
  assign U556 = ~DATA_IN_4_ | ~U345;
  assign U557 = ~RESTART | ~RMAX_REG_4_;
  assign U558 = ~DATA_IN_3_ | ~U345;
  assign U559 = ~RESTART | ~RMAX_REG_3_;
  assign U560 = ~DATA_IN_2_ | ~U345;
  assign U561 = ~RESTART | ~RMAX_REG_2_;
  assign U562 = ~DATA_IN_1_ | ~U345;
  assign U563 = ~RESTART | ~RMAX_REG_1_;
  assign U564 = ~DATA_IN_0_ | ~U345;
  assign U565 = ~RESTART | ~RMAX_REG_0_;
  assign U566 = ~REG4_REG_7_ | ~U345;
  assign U567 = ~RESTART | ~RMIN_REG_7_;
  assign U568 = ~DATA_IN_7_ | ~U345;
  assign U569 = ~RESTART | ~RMAX_REG_7_;
  assign U570 = ~U343;
  assign LT_90_U35 = ~DATA_IN_7_ | ~LT_90_U13;
  assign LT_90_U34 = ~LT_90_U33 | ~LT_90_U32 | ~LT_90_U31;
  assign LT_90_U33 = LT_90_U13 | DATA_IN_7_;
  assign LT_90_U32 = LT_90_U12 | RMIN_REG_6_;
  assign LT_90_U31 = ~LT_90_U30 | ~LT_90_U29 | ~LT_90_U28;
  assign LT_90_U30 = ~RMIN_REG_6_ | ~LT_90_U12;
  assign LT_90_U29 = LT_90_U11 | DATA_IN_5_;
  assign R135_U5 = ~R135_U36 | ~R135_U35;
  assign R135_U6 = ~U366;
  assign R135_U7 = ~U367;
  assign R135_U8 = ~R135_U31 | ~R135_U30;
  assign R135_U9 = ~U357 | ~U364;
  assign R135_U10 = ~U358 | ~U365;
  assign R135_U11 = ~R135_U10 | ~R135_U9;
  assign R135_U12 = U357 | U364;
  assign R135_U13 = U356 | U363;
  assign R135_U14 = ~R135_U13 | ~R135_U12 | ~R135_U11;
  assign R135_U15 = ~U355 | ~U362;
  assign R135_U16 = ~U356 | ~U363;
  assign R135_U17 = ~R135_U16 | ~R135_U15 | ~R135_U14;
  assign R135_U18 = U355 | U362;
  assign R135_U19 = U354 | U361;
  assign R135_U20 = ~R135_U19 | ~R135_U18 | ~R135_U17;
  assign R135_U21 = ~U353 | ~U360;
  assign R135_U22 = ~U354 | ~U361;
  assign R135_U23 = ~R135_U22 | ~R135_U21 | ~R135_U20;
  assign R135_U24 = U353 | U360;
  assign R135_U25 = U352 | U359;
  assign R135_U26 = ~R135_U25 | ~R135_U24 | ~R135_U23;
  assign R135_U27 = ~U352 | ~U359;
  assign R135_U28 = ~R135_U27 | ~R135_U26;
  assign R135_U29 = U366 | U367;
  assign R135_U30 = ~R135_U29 | ~R135_U28;
  assign R135_U31 = ~U366 | ~U367;
  assign R135_U32 = R135_U6 | U367;
  assign R135_U33 = R135_U7 | U366;
  assign R135_U34 = R135_U33 & R135_U32;
  assign R135_U35 = ~R135_U8 | ~R135_U33 | ~R135_U32;
  assign R135_U36 = R135_U8 | R135_U34;
  assign GT_88_U6 = ~GT_88_U34 | ~GT_88_U35;
  assign GT_88_U7 = ~DATA_IN_1_;
  assign GT_88_U8 = ~RMAX_REG_2_;
  assign GT_88_U9 = ~DATA_IN_3_;
  assign GT_88_U10 = ~RMAX_REG_4_;
  assign GT_88_U11 = ~DATA_IN_5_;
  assign GT_88_U12 = ~RMAX_REG_6_;
  assign GT_88_U13 = ~DATA_IN_7_;
  assign GT_88_U14 = ~RMAX_REG_0_;
  assign GT_88_U15 = ~RMAX_REG_1_ | ~GT_88_U7;
  assign GT_88_U16 = ~DATA_IN_0_ | ~GT_88_U15 | ~GT_88_U14;
  assign GT_88_U17 = GT_88_U7 | RMAX_REG_1_;
  assign GT_88_U18 = ~DATA_IN_2_ | ~GT_88_U8;
  assign GT_88_U19 = ~GT_88_U18 | ~GT_88_U17 | ~GT_88_U16;
  assign GT_88_U20 = GT_88_U8 | DATA_IN_2_;
  assign GT_88_U21 = ~RMAX_REG_3_ | ~GT_88_U9;
  assign GT_88_U22 = ~GT_88_U21 | ~GT_88_U20 | ~GT_88_U19;
  assign GT_88_U23 = GT_88_U9 | RMAX_REG_3_;
  assign GT_88_U24 = ~DATA_IN_4_ | ~GT_88_U10;
  assign GT_88_U25 = ~GT_88_U24 | ~GT_88_U23 | ~GT_88_U22;
  assign GT_88_U26 = GT_88_U10 | DATA_IN_4_;
  assign GT_88_U27 = ~RMAX_REG_5_ | ~GT_88_U11;
  assign GT_88_U28 = ~GT_88_U27 | ~GT_88_U26 | ~GT_88_U25;
  assign GT_88_U29 = GT_88_U11 | RMAX_REG_5_;
  assign GT_88_U30 = ~DATA_IN_6_ | ~GT_88_U12;
  assign GT_88_U31 = ~GT_88_U30 | ~GT_88_U29 | ~GT_88_U28;
  assign GT_88_U32 = GT_88_U12 | DATA_IN_6_;
  assign GT_88_U33 = GT_88_U13 | RMAX_REG_7_;
  assign GT_88_U34 = ~GT_88_U33 | ~GT_88_U32 | ~GT_88_U31;
  assign GT_88_U35 = ~RMAX_REG_7_ | ~GT_88_U13;
  assign R81_U6 = R81_U22 & R81_U10;
  assign R81_U7 = R81_U20 & R81_U11;
  assign R81_U8 = R81_U19 & R81_U13;
  assign R81_U9 = ~R81_U29 & ~R134_U10;
  assign R81_U10 = R134_U16 | R134_U6 | R134_U14;
  assign R81_U11 = R81_U10 | R134_U8 | R134_U7;
  assign R81_U12 = ~R81_U28 | ~R81_U27;
  assign R81_U13 = R81_U11 | R134_U9;
  assign R81_U14 = R81_U24 & R81_U23;
  assign R81_U15 = ~R134_U7;
  assign R81_U16 = R81_U26 & R81_U25;
  assign R81_U17 = ~R134_U6;
  assign R81_U18 = ~R81_U10;
  assign R81_U19 = ~R134_U9 | ~R81_U11;
  assign R81_U20 = ~R134_U8 | ~R81_U25;
  assign R81_U21 = R134_U6 | R134_U14;
  assign R81_U22 = ~R134_U16 | ~R81_U21;
  assign R81_U23 = ~R134_U10 | ~R81_U13;
  assign R81_U24 = R81_U13 | R134_U10;
  assign R81_U25 = ~R81_U18 | ~R81_U15;
  assign R81_U26 = ~R134_U7 | ~R81_U10;
  assign R81_U27 = R81_U17 | R134_U14;
  assign R81_U28 = ~R134_U14 | ~R81_U17;
  assign R81_U29 = ~R81_U13;
  assign R57_U5 = R57_U51 & R57_U50;
  assign R57_U6 = R57_U47 & R57_U45;
  assign R57_U7 = ~R57_U52 | ~R57_U75 | ~R57_U74;
  assign R57_U8 = ~U365;
  assign R57_U9 = ~U365 | ~U358;
  assign R57_U10 = ~U357;
  assign R57_U11 = ~U363;
  assign R57_U12 = ~U356;
  assign R57_U13 = ~U354;
  assign R57_U14 = ~U361;
  assign R57_U15 = ~R57_U20 & ~R57_U78;
  assign R57_U16 = R57_U40 & R57_U39;
  assign R57_U17 = ~U353;
  assign R57_U18 = ~U360;
  assign R57_U19 = R57_U36 & R57_U35;
  assign R57_U20 = ~R57_U19 & ~R57_U37;
  assign R57_U21 = ~R57_U77 | ~R57_U76;
  assign R57_U22 = ~R57_U60 | ~R57_U59;
  assign R57_U23 = ~R57_U65 | ~R57_U64;
  assign R57_U24 = ~R57_U70 | ~R57_U69;
  assign R57_U25 = ~U352;
  assign R57_U26 = ~U359;
  assign R57_U27 = R57_U28 | R57_U33;
  assign R57_U28 = ~R57_U10 & ~R57_U9;
  assign R57_U29 = ~R57_U15;
  assign R57_U30 = ~R57_U9;
  assign R57_U31 = ~U355 | ~U362;
  assign R57_U32 = U357 | R57_U30;
  assign R57_U33 = U364 & R57_U32;
  assign R57_U34 = ~R57_U11 | ~R57_U12;
  assign R57_U35 = ~R57_U34 | ~R57_U27;
  assign R57_U36 = R57_U12 | R57_U11;
  assign R57_U37 = ~U355 & ~U362;
  assign R57_U38 = R57_U14 | R57_U13;
  assign R57_U39 = ~R57_U15 | ~R57_U38;
  assign R57_U40 = ~R57_U14 | ~R57_U13;
  assign R57_U41 = ~R57_U16;
  assign R57_U42 = ~R57_U17 | ~R57_U18;
  assign R57_U43 = ~R57_U16 | ~R57_U42;
  assign R57_U44 = R57_U18 | R57_U17;
  assign R57_U45 = ~R57_U53 | ~R57_U54 | ~R57_U44 | ~R57_U43;
  assign R57_U46 = ~R57_U44 | ~R57_U41;
  assign R57_U47 = ~R57_U55 | ~R57_U42 | ~R57_U46;
  assign R57_U48 = U362 | U355;
  assign R57_U49 = ~R57_U48 | ~R57_U31;
  assign R57_U50 = ~R57_U19 | ~R57_U49;
  assign R57_U51 = ~R57_U20 | ~R57_U31;
  assign R57_U52 = ~R57_U73 | ~R57_U10;
  assign R57_U53 = R57_U25 | U359;
  assign R57_U54 = R57_U26 | U352;
  assign R57_U55 = ~R57_U54 | ~R57_U53;
  assign R57_U56 = R57_U18 | U353;
  assign R57_U57 = R57_U17 | U360;
  assign R57_U58 = ~R57_U57 | ~R57_U56;
  assign R57_U59 = ~R57_U58 | ~R57_U41;
  assign R57_U60 = ~R57_U16 | ~R57_U57 | ~R57_U56;
  assign R57_U61 = R57_U14 | U354;
  assign R57_U62 = R57_U13 | U361;
  assign R57_U63 = ~R57_U62 | ~R57_U61;
  assign R57_U64 = ~R57_U29 | ~R57_U62 | ~R57_U61;
  assign R57_U65 = ~R57_U63 | ~R57_U15;
  assign R57_U66 = R57_U12 | U363;
  assign R57_U67 = R57_U11 | U356;
  assign R57_U68 = R57_U67 & R57_U66;
  assign R57_U69 = ~R57_U27 | ~R57_U67 | ~R57_U66;
  assign R57_U70 = R57_U27 | R57_U68;
  assign R57_U71 = ~U364 | ~R57_U9;
  assign R57_U72 = R57_U9 | U364;
  assign R57_U73 = ~R57_U72 | ~R57_U71;
  assign R57_U74 = U364 | R57_U10 | R57_U30;
  assign R57_U75 = ~R57_U28 | ~U364;
  assign R57_U76 = R57_U8 | U358;
  assign R57_U77 = ~U358 | ~R57_U8;
  assign R57_U78 = ~R57_U31;
  assign R134_U6 = R134_U23 & R134_U11;
  assign R134_U7 = R134_U21 & R134_U12;
  assign R134_U8 = R134_U20 & R134_U13;
  assign R134_U9 = R134_U19 & R134_U10;
  assign R134_U10 = R134_U13 | R57_U6;
  assign R134_U11 = R57_U24 | R57_U21 | R57_U7;
  assign R134_U12 = R134_U11 | R57_U23 | R57_U5;
  assign R134_U13 = R134_U12 | R57_U22;
  assign R134_U14 = ~R134_U27 | ~R134_U26;
  assign R134_U15 = ~R57_U5;
  assign R134_U16 = R134_U25 & R134_U24;
  assign R134_U17 = ~R57_U21;
  assign R134_U18 = ~R134_U11;
  assign R134_U19 = ~R57_U6 | ~R134_U13;
  assign R134_U20 = ~R57_U22 | ~R134_U12;
  assign R134_U21 = ~R57_U23 | ~R134_U24;
  assign R134_U22 = R57_U21 | R57_U7;
  assign R134_U23 = ~R57_U24 | ~R134_U22;
  assign R134_U24 = ~R134_U18 | ~R134_U15;
  assign R134_U25 = ~R57_U5 | ~R134_U11;
  assign R134_U26 = R134_U17 | R57_U7;
  assign R134_U27 = ~R57_U7 | ~R134_U17;
  assign LT_90_U6 = ~LT_90_U34 | ~LT_90_U35;
  assign LT_90_U7 = ~RMIN_REG_1_;
  assign LT_90_U8 = ~DATA_IN_2_;
  assign LT_90_U9 = ~RMIN_REG_3_;
  assign LT_90_U10 = ~DATA_IN_4_;
  assign LT_90_U11 = ~RMIN_REG_5_;
  assign LT_90_U12 = ~DATA_IN_6_;
  assign LT_90_U13 = ~RMIN_REG_7_;
  assign LT_90_U14 = ~DATA_IN_0_;
  assign LT_90_U15 = ~DATA_IN_1_ | ~LT_90_U7;
  assign LT_90_U16 = ~RMIN_REG_0_ | ~LT_90_U15 | ~LT_90_U14;
  assign LT_90_U17 = LT_90_U7 | DATA_IN_1_;
  assign LT_90_U18 = ~RMIN_REG_2_ | ~LT_90_U8;
  assign LT_90_U19 = ~LT_90_U18 | ~LT_90_U17 | ~LT_90_U16;
  assign LT_90_U20 = LT_90_U8 | RMIN_REG_2_;
  assign LT_90_U21 = ~DATA_IN_3_ | ~LT_90_U9;
  assign LT_90_U22 = ~LT_90_U21 | ~LT_90_U20 | ~LT_90_U19;
  assign LT_90_U23 = LT_90_U9 | DATA_IN_3_;
  assign LT_90_U24 = ~RMIN_REG_4_ | ~LT_90_U10;
  assign LT_90_U25 = ~LT_90_U24 | ~LT_90_U23 | ~LT_90_U22;
  assign LT_90_U26 = LT_90_U10 | RMIN_REG_4_;
  assign LT_90_U27 = ~DATA_IN_5_ | ~LT_90_U11;
  assign LT_90_U28 = ~LT_90_U27 | ~LT_90_U26 | ~LT_90_U25;
  always @ (posedge clock) begin
    RMAX_REG_7_ <= n40;
    RMAX_REG_6_ <= n45;
    RMAX_REG_5_ <= n50;
    RMAX_REG_4_ <= n55;
    RMAX_REG_3_ <= n60;
    RMAX_REG_2_ <= n65;
    RMAX_REG_1_ <= n70;
    RMAX_REG_0_ <= n75;
    RMIN_REG_7_ <= n80;
    RMIN_REG_6_ <= n85;
    RMIN_REG_5_ <= n90;
    RMIN_REG_4_ <= n95;
    RMIN_REG_3_ <= n100;
    RMIN_REG_2_ <= n105;
    RMIN_REG_1_ <= n110;
    RMIN_REG_0_ <= n115;
    RLAST_REG_7_ <= n120;
    RLAST_REG_6_ <= n125;
    RLAST_REG_5_ <= n130;
    RLAST_REG_4_ <= n135;
    RLAST_REG_3_ <= n140;
    RLAST_REG_2_ <= n145;
    RLAST_REG_1_ <= n150;
    RLAST_REG_0_ <= n155;
    REG1_REG_7_ <= n160;
    REG1_REG_6_ <= n165;
    REG1_REG_5_ <= n170;
    REG1_REG_4_ <= n175;
    REG1_REG_3_ <= n180;
    REG1_REG_2_ <= n185;
    REG1_REG_1_ <= n190;
    REG1_REG_0_ <= n195;
    REG2_REG_7_ <= n200;
    REG2_REG_6_ <= n205;
    REG2_REG_5_ <= n210;
    REG2_REG_4_ <= n215;
    REG2_REG_3_ <= n220;
    REG2_REG_2_ <= n225;
    REG2_REG_1_ <= n230;
    REG2_REG_0_ <= n235;
    REG3_REG_7_ <= n240;
    REG3_REG_6_ <= n245;
    REG3_REG_5_ <= n250;
    REG3_REG_4_ <= n255;
    REG3_REG_3_ <= n260;
    REG3_REG_2_ <= n265;
    REG3_REG_1_ <= n270;
    REG3_REG_0_ <= n275;
    REG4_REG_7_ <= n280;
    REG4_REG_6_ <= n285;
    REG4_REG_5_ <= n290;
    REG4_REG_4_ <= n295;
    REG4_REG_3_ <= n300;
    REG4_REG_2_ <= n305;
    REG4_REG_1_ <= n310;
    REG4_REG_0_ <= n315;
    DATA_OUT_REG_7_ <= n320;
    DATA_OUT_REG_6_ <= n324;
    DATA_OUT_REG_5_ <= n328;
    DATA_OUT_REG_4_ <= n332;
    DATA_OUT_REG_3_ <= n336;
    DATA_OUT_REG_2_ <= n340;
    DATA_OUT_REG_1_ <= n344;
    DATA_OUT_REG_0_ <= n348;
    STATO_REG_1_ <= n352;
    STATO_REG_0_ <= n357;
  end
endmodule


