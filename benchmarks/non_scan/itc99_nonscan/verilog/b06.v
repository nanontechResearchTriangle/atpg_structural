// Benchmark "b05s" written by ABC on Mon Apr  8 17:53:28 2019

module b05s ( clock, 
    START,
    U592, U797, U798, U717, U717, U798, U798, U798, U620, U621, U622, U623,
    U624, U625, U626, U603, U604, U605, U606, U607, U608, U609, U797, U799,
    U717, U717, U799, U799, U799, U613, U614, U615, U616, U617, U618, U619  );
  input  clock;
  input  START;
  output U592, U797, U798, U717, U717, U798, U798, U798, U620, U621, U622,
    U623, U624, U625, U626, U603, U604, U605, U606, U607, U608, U609, U797,
    U799, U717, U717, U799, U799, U799, U613, U614, U615, U616, U617, U618,
    U619;
  reg NUM_REG_4_, NUM_REG_3_, NUM_REG_2_, NUM_REG_1_, NUM_REG_0_,
    MAR_REG_4_, MAR_REG_3_, MAR_REG_2_, MAR_REG_1_, MAR_REG_0_,
    TEMP_REG_8_, TEMP_REG_7_, TEMP_REG_6_, TEMP_REG_5_, TEMP_REG_4_,
    TEMP_REG_3_, TEMP_REG_2_, TEMP_REG_1_, TEMP_REG_0_, MAX_REG_8_,
    MAX_REG_7_, MAX_REG_6_, MAX_REG_5_, MAX_REG_4_, MAX_REG_3_, MAX_REG_2_,
    MAX_REG_1_, MAX_REG_0_, EN_DISP_REG, RES_DISP_REG, FLAG_REG,
    STATO_REG_0_, STATO_REG_1_, STATO_REG_2_;
  wire GT_122_U10, GT_122_U9, U583, U584, U585, U586, U587, U588, U589, U590,
    U591, U593, U594, U595, U596, U597, U598, U599, U600, U601, U602, U655,
    U656, U657, U658, U659, U660, U661, U662, U663, U664, U665, U666, U667,
    U668, U669, U670, U671, U672, U673, U674, U675, U676, U677, U678, U679,
    U680, U681, U682, U683, U684, U685, U686, U687, U688, U689, U690, U691,
    U692, U693, U694, U695, U696, U697, U698, U699, U700, U701, U702, U703,
    U704, U705, U706, U707, U708, U709, U710, U711, U712, U713, U714, U715,
    U716, U718, U719, U720, U721, U722, U723, U724, U725, U726, U727, U728,
    U729, U730, U731, U732, U733, U734, U735, U737, U740, U741, U742, U743,
    U744, U745, U746, U747, U748, U749, U750, U751, U752, U753, U754, U755,
    U756, U757, U758, U759, U760, U761, U762, U763, U764, U765, U766, U767,
    U768, U769, U770, U771, U772, U773, U774, U775, U776, U777, U778, U779,
    U780, U781, U782, U783, U784, U785, U786, U787, U788, U789, U790, U791,
    U792, U793, U794, U795, U796, U800, U801, U802, U803, U804, U805, U806,
    U807, U808, U809, U810, U811, U812, U813, U814, U815, U816, U817, U818,
    U819, U820, U821, U822, U823, U824, U825, U826, U827, U828, U829, U830,
    U831, U832, U833, U834, U835, U836, U837, U838, U839, U840, U841, U842,
    U843, U844, U845, U846, U847, U848, U849, U850, U851, U852, U853, U854,
    U855, U856, U857, U858, U859, U860, U861, U862, U863, U864, U865, U866,
    U867, U868, U869, U870, U871, U872, U873, U874, U875, U876, U877, U878,
    U879, U880, U881, U882, U883, U884, U885, U886, U887, U888, U889, U890,
    U891, U892, U893, U894, U895, U896, U897, U898, U899, U900, U901, U902,
    U903, U904, U905, U906, U907, U908, U909, U910, U911, U912, U913, U914,
    U915, U916, U917, U918, U919, U920, U921, U922, U923, U924, U925, U926,
    U927, U928, U929, U930, U931, U932, U933, U934, U935, U936, U937, U938,
    U939, U940, U941, U942, U943, U944, U945, U946, U947, U948, U949, U950,
    U951, U952, U953, U954, U955, U956, U957, U958, U959, U960, U961, U962,
    U963, U964, U965, U966, U967, U968, U969, U970, U971, U972, U973, U974,
    U975, U976, U977, U978, U979, U980, U981, U982, U983, U984, U985, U986,
    U987, U988, U989, U990, U991, U992, U993, U994, U995, U996, U997, U998,
    U999, U1000, U1001, U1002, U1003, U1004, U1005, U1006, U1007, U1008,
    U1009, U1010, U1011, U1012, U1013, U1014, U1015, U1016, U1017, U1018,
    U1019, U1020, U1021, U1022, U1023, U1024, U1025, U1026, U1027, U1028,
    U1029, U1030, U1031, U1032, U1033, U1034, U1035, U1036, U1037, U1038,
    U1039, U1040, U1041, U1042, U1043, U1044, U1045, U1046, U1047, U1048,
    U1049, U1050, U1051, U1052, U1053, U1054, U1055, U1056, U1057, U1058,
    U1059, U1060, GT_122_U8, GT_122_U7, GT_122_U6, GT_166_U9, GT_166_U8,
    GT_166_U7, GT_166_U6, GT_142_U9, GT_142_U8, GT_142_U7, GT_142_U6,
    GT_175_U8, GT_175_U7, GT_175_U6, GT_134_U9, GT_134_U8, GT_134_U7,
    GT_134_U6, GT_209_U9, GT_209_U8, GT_209_U7, GT_209_U6, SUB_73_U47,
    SUB_73_U46, SUB_73_U45, SUB_73_U44, SUB_73_U43, SUB_73_U42, SUB_73_U41,
    SUB_73_U40, SUB_73_U39, SUB_73_U38, SUB_73_U37, SUB_73_U36, SUB_73_U35,
    SUB_73_U34, SUB_73_U33, SUB_73_U32, SUB_73_U31, GT_218_U6, GT_218_U7,
    GT_218_U8, GT_146_U6, GT_146_U7, GT_146_U8, GT_146_U9, GT_126_U6,
    GT_126_U7, GT_126_U8, GT_203_U6, GT_203_U7, GT_203_U8, GT_203_U9,
    GT_172_U6, GT_172_U7, GT_172_U8, GT_172_U9, GT_172_U10, GT_108_U6,
    GT_108_U7, GT_108_U8, GT_108_U9, GT_181_U6, GT_181_U7, GT_181_U8,
    SUB_110_U6, SUB_110_U7, SUB_110_U8, SUB_110_U9, SUB_110_U10,
    SUB_110_U11, SUB_110_U12, SUB_110_U13, SUB_110_U14, SUB_110_U15,
    SUB_110_U16, SUB_110_U17, SUB_110_U18, SUB_110_U19, SUB_110_U20,
    SUB_110_U21, SUB_110_U22, SUB_110_U23, SUB_110_U24, SUB_110_U25,
    SUB_110_U26, SUB_110_U27, SUB_110_U28, SUB_110_U29, R277_U6, R277_U7,
    R277_U8, R277_U9, R277_U10, R277_U11, R277_U12, R277_U13, R277_U14,
    R277_U15, R277_U16, R277_U17, R277_U18, R277_U19, R277_U20, R277_U21,
    R277_U22, R277_U23, GT_163_U6, GT_163_U7, GT_224_U6, GT_224_U7,
    GT_224_U8, GT_114_U6, GT_114_U7, GT_114_U8, GT_114_U9, GT_114_U10,
    GT_114_U11, GT_118_U6, GT_118_U7, GT_118_U8, GT_118_U9, GT_184_U6,
    GT_184_U7, GT_184_U8, GT_212_U6, GT_212_U7, GT_212_U8, GT_212_U9,
    SUB_60_U6, SUB_60_U7, SUB_60_U8, SUB_60_U9, SUB_60_U10, SUB_60_U11,
    SUB_60_U12, SUB_60_U13, SUB_60_U14, SUB_60_U15, SUB_60_U16, SUB_60_U17,
    SUB_60_U18, SUB_60_U19, SUB_60_U20, SUB_60_U21, SUB_60_U22, SUB_60_U23,
    SUB_60_U24, SUB_60_U25, SUB_60_U26, SUB_60_U27, SUB_60_U28, SUB_60_U29,
    SUB_60_U30, SUB_60_U31, SUB_60_U32, SUB_60_U33, SUB_60_U34, SUB_60_U35,
    SUB_60_U36, SUB_60_U37, SUB_60_U38, SUB_60_U39, SUB_60_U40, SUB_60_U41,
    SUB_60_U42, SUB_60_U43, SUB_60_U44, SUB_60_U45, SUB_60_U46, SUB_60_U47,
    SUB_60_U48, SUB_60_U49, SUB_60_U50, SUB_60_U51, SUB_60_U52, SUB_60_U53,
    SUB_60_U54, SUB_60_U55, SUB_60_U56, SUB_60_U57, SUB_60_U58, SUB_60_U59,
    SUB_60_U60, SUB_60_U61, SUB_60_U62, SUB_60_U63, SUB_60_U64, SUB_60_U65,
    SUB_60_U66, SUB_60_U67, SUB_60_U68, SUB_60_U69, SUB_60_U70, SUB_60_U71,
    SUB_60_U72, SUB_60_U73, SUB_60_U74, SUB_60_U75, SUB_60_U76, SUB_60_U77,
    SUB_60_U78, SUB_60_U79, SUB_60_U80, SUB_60_U81, SUB_60_U82, SUB_60_U83,
    SUB_60_U84, SUB_60_U85, SUB_60_U86, SUB_60_U87, SUB_60_U88, SUB_60_U89,
    SUB_60_U90, SUB_60_U91, SUB_60_U92, SUB_60_U93, SUB_60_U94, SUB_60_U95,
    SUB_60_U96, R429_U6, R429_U7, R429_U8, R429_U9, R429_U10, R429_U11,
    R429_U12, R429_U13, R429_U14, R429_U15, R429_U16, R429_U17, R429_U18,
    R429_U19, R429_U20, R429_U21, R429_U22, R429_U23, R429_U24, R429_U25,
    R429_U26, R429_U27, R429_U28, R429_U29, R429_U30, R429_U31, R429_U32,
    R429_U33, R429_U34, R429_U35, R429_U36, R429_U37, R429_U38, R429_U39,
    R429_U40, R429_U41, R429_U42, R429_U43, R429_U44, R429_U45, R429_U46,
    R429_U47, R429_U48, R429_U49, R429_U50, R429_U51, R429_U52, R429_U53,
    R429_U54, R429_U55, R429_U56, R429_U57, R429_U58, R429_U59, R429_U60,
    R429_U61, R429_U62, R429_U63, R429_U64, R429_U65, R429_U66, R429_U67,
    R429_U68, R429_U69, R429_U70, R429_U71, GT_138_U6, GT_138_U7,
    GT_138_U8, GT_206_U6, GT_206_U7, GT_160_U6, GT_160_U7, GT_160_U8,
    GT_160_U9, GT_169_U6, GT_169_U7, GT_169_U8, GT_169_U9, SUB_103_U6,
    SUB_103_U7, SUB_103_U8, SUB_103_U9, SUB_103_U10, SUB_103_U11,
    SUB_103_U12, SUB_103_U13, SUB_103_U14, SUB_103_U15, SUB_103_U16,
    SUB_103_U17, SUB_103_U18, SUB_103_U19, GT_227_U6, GT_227_U7, GT_227_U8,
    GT_197_U6, GT_197_U7, GT_197_U8, GT_130_U6, GT_130_U7, GT_130_U8,
    GT_130_U9, GT_178_U6, GT_178_U7, GT_178_U8, GT_178_U9, SUB_199_U6,
    SUB_199_U7, SUB_199_U8, SUB_199_U9, SUB_199_U10, SUB_199_U11,
    SUB_199_U12, SUB_199_U13, SUB_199_U14, SUB_199_U15, SUB_199_U16,
    SUB_199_U17, SUB_199_U18, SUB_199_U19, SUB_199_U20, SUB_199_U21,
    SUB_199_U22, SUB_199_U23, SUB_199_U24, SUB_199_U25, SUB_199_U26,
    SUB_199_U27, SUB_199_U28, GT_221_U6, GT_221_U7, GT_221_U8, GT_221_U9,
    GT_215_U6, GT_215_U7, GT_215_U8, GT_215_U9, GT_215_U10, SUB_73_U6,
    SUB_73_U7, SUB_73_U8, SUB_73_U9, SUB_73_U10, SUB_73_U11, SUB_73_U12,
    SUB_73_U13, SUB_73_U14, SUB_73_U15, SUB_73_U16, SUB_73_U17, SUB_73_U18,
    SUB_73_U19, SUB_73_U20, SUB_73_U21, SUB_73_U22, SUB_73_U23, SUB_73_U24,
    SUB_73_U25, SUB_73_U26, SUB_73_U27, SUB_73_U28, SUB_73_U29, SUB_73_U30,
    n66, n71, n76, n81, n86, n91, n96, n101, n106, n111, n116, n121, n126,
    n131, n136, n141, n146, n151, n156, n161, n166, n171, n176, n181, n186,
    n191, n196, n201, n206, n211, n216, n221, n226, n231;
  assign GT_122_U10 = U596 | U746 | U748 | GT_122_U7;
  assign GT_122_U9 = ~GT_122_U8 & ~U594;
  assign U583 = U700 & U676;
  assign U584 = ~U701 | ~U681 | ~U583;
  assign U585 = ~U781 | ~U583 | ~U702 | ~U693;
  assign U586 = ~U770 | ~U702 | ~U700 | ~U691;
  assign U587 = U844 | U846;
  assign U588 = ~U703 | ~U695 | ~U800;
  assign U589 = ~U702 | ~U692 | ~U676;
  assign U590 = ~U690 | ~U694 | ~U676 | ~U780 | ~U778;
  assign U591 = ~U704 | ~U701 | ~U692;
  assign U592 = U797 | U877;
  assign U593 = ~U803 | ~U865;
  assign U594 = ~U803 | ~U866;
  assign U595 = ~U803 | ~U867;
  assign U596 = ~U803 | ~U868;
  assign U597 = U709 & U735;
  assign U598 = ~U938 | ~U790 | ~U788;
  assign U599 = ~U733 | ~U709 | ~U788;
  assign U600 = ~U734 | ~U940 | ~U790;
  assign U601 = ~U733 | ~U941;
  assign U602 = ~U734 | ~U942 | ~U788;
  assign U603 = U902 & U699;
  assign U604 = U717 & U905;
  assign U605 = U717 & U906;
  assign U606 = U717 & U907;
  assign U607 = U717 & U909;
  assign U608 = U717 & U911;
  assign U609 = U717 & U917 & U785;
  assign n231 = ~U873 | ~U872;
  assign n226 = ~U668 | ~U807;
  assign n221 = ~U804 | ~U807 | ~U876 | ~U875;
  assign U613 = U882 & U699;
  assign U614 = U717 & U885;
  assign U615 = U717 & U886;
  assign U616 = U717 & U887;
  assign U617 = U717 & U889;
  assign U618 = U717 & U891;
  assign U619 = U717 & U897 & U794;
  assign U620 = U920 & U699;
  assign U621 = U717 & U924;
  assign U622 = U717 & U789;
  assign U623 = U717 & U925;
  assign U624 = U717 & U927;
  assign U625 = U717 & U931;
  assign U626 = U717 & U937 & U787;
  assign n66 = ~U811 | ~U810;
  assign n71 = ~U813 | ~U812;
  assign n76 = ~U815 | ~U814;
  assign n81 = ~U817 | ~U816;
  assign n86 = ~U819 | ~U818;
  assign n91 = ~U821 | ~U820;
  assign n96 = ~U823 | ~U822;
  assign n101 = ~U825 | ~U824;
  assign n106 = ~U827 | ~U826;
  assign n111 = ~U829 | ~U828;
  assign n121 = ~U682 | ~U963 | ~U962;
  assign n126 = ~U682 | ~U965 | ~U964;
  assign n131 = ~U946 | ~U969 | ~U968;
  assign n141 = ~U947 | ~U973 | ~U972;
  assign n146 = ~U974 | ~U975 | ~U682 | ~U948;
  assign n151 = ~U949 | ~U977 | ~U976;
  assign n156 = ~U950 | ~U979 | ~U978;
  assign n166 = ~U801 | ~U983 | ~U982;
  assign n171 = ~U801 | ~U985 | ~U984;
  assign n176 = ~U951 | ~U987 | ~U986;
  assign n181 = ~U953 | ~U989 | ~U988;
  assign n186 = ~U952 | ~U991 | ~U990;
  assign n191 = ~U992 | ~U993 | ~U953 | ~U801;
  assign n196 = ~U954 | ~U995 | ~U994;
  assign n201 = ~U955 | ~U997 | ~U996;
  assign n206 = ~U859 | ~U858 | ~U802;
  assign n211 = ~U802 | ~U861;
  assign n216 = ~U863 | ~U862;
  assign U655 = ~STATO_REG_1_;
  assign U656 = ~U870 & ~U871 & ~GT_146_U6;
  assign U657 = ~STATO_REG_0_;
  assign U658 = STATO_REG_1_ & FLAG_REG & SUB_60_U6 & STATO_REG_0_;
  assign U659 = ~STATO_REG_0_ | ~U957 | ~U956;
  assign U660 = ~STATO_REG_2_;
  assign U661 = ~MAR_REG_1_;
  assign U662 = ~MAR_REG_3_;
  assign U663 = ~MAR_REG_0_;
  assign U664 = MAR_REG_0_ & MAR_REG_3_;
  assign U665 = MAR_REG_2_ & MAR_REG_1_;
  assign U666 = ~U665 | ~U664;
  assign U667 = ~MAR_REG_4_;
  assign U668 = U802 & U804;
  assign U669 = U663 & U662;
  assign U670 = U669 & MAR_REG_2_ & U661;
  assign U671 = ~MAR_REG_2_ & ~MAR_REG_1_;
  assign U672 = ~U671 | ~U669;
  assign U673 = U671 & U664;
  assign U674 = ~U661 & ~MAR_REG_2_;
  assign U675 = U778 & U689;
  assign U676 = U783 & U779 & U775;
  assign U677 = MAR_REG_0_ & U662;
  assign U678 = U677 & U665;
  assign U679 = U677 & U674;
  assign U680 = ~U662 & ~MAR_REG_4_ & ~MAR_REG_0_;
  assign U681 = U806 & U780 & U776;
  assign U682 = U838 & U837;
  assign U683 = U669 & U665;
  assign U684 = ~U667 & ~U662 & ~MAR_REG_0_;
  assign U685 = ~U677 | ~MAR_REG_2_ | ~U661;
  assign U686 = U806 & U841;
  assign U687 = U776 & U781;
  assign U688 = U775 & U672 & U686 & U666;
  assign U689 = ~U674 | ~U664;
  assign U690 = U686 & U845 & U697;
  assign U691 = U690 & U775;
  assign U692 = U687 & U780 & U777;
  assign U693 = U782 & U847;
  assign U694 = U693 & U848 & U784;
  assign U695 = U694 & U783;
  assign U696 = ~U655 & ~U852 & ~SUB_60_U6;
  assign U697 = U667 | U666;
  assign U698 = ~U697 & ~U660;
  assign U699 = ~EN_DISP_REG;
  assign U700 = U784 & U778 & U777;
  assign U701 = U737 & U805;
  assign U702 = U701 & U771;
  assign U703 = U806 & U781 & U779;
  assign U704 = U693 & U778;
  assign U705 = ~GT_197_U6;
  assign U706 = ~GT_108_U6;
  assign U707 = ~MAX_REG_8_;
  assign U708 = MAX_REG_8_ & SUB_103_U9;
  assign U709 = ~GT_114_U6 & ~GT_118_U6;
  assign U710 = ~GT_126_U6 & ~GT_130_U6;
  assign U711 = ~GT_138_U6 & ~GT_142_U6 & ~GT_134_U6;
  assign U712 = ~GT_203_U6 & ~GT_206_U6;
  assign U713 = ~GT_218_U6;
  assign U714 = U712 & U878;
  assign U715 = U714 & U879;
  assign U716 = GT_227_U7 & U1057;
  assign U717 = RES_DISP_REG & U699;
  assign U718 = GT_212_U6 | GT_215_U6;
  assign U719 = ~U718 & ~U1058 & ~GT_209_U6;
  assign U720 = ~GT_163_U6 & ~GT_160_U6;
  assign U721 = ~GT_175_U6;
  assign U722 = U720 & U898;
  assign U723 = U722 & U899;
  assign U724 = GT_184_U7 & U1059;
  assign U725 = GT_169_U6 | GT_172_U6;
  assign U726 = ~U725 & ~U1060 & ~GT_166_U6;
  assign U727 = U709 & U918;
  assign U728 = ~U1056 & ~GT_142_U6;
  assign U729 = U710 & U597;
  assign U730 = ~GT_126_U6;
  assign U731 = ~GT_134_U6;
  assign U732 = ~GT_138_U6;
  assign U733 = U791 & U790 & U789;
  assign U734 = U939 & U791 & U787;
  assign U735 = ~GT_122_U6;
  assign n116 = ~U959 | ~U958;
  assign U737 = U961 & U960;
  assign n136 = ~U971 | ~U970;
  assign n161 = ~U981 | ~U980;
  assign U740 = ~U999 | ~U998;
  assign U741 = ~U1001 | ~U1000;
  assign U742 = ~U1003 | ~U1002;
  assign U743 = ~U1005 | ~U1004;
  assign U744 = ~U1007 | ~U1006;
  assign U745 = ~U1009 | ~U1008;
  assign U746 = ~U1011 | ~U1010;
  assign U747 = ~U1013 | ~U1012;
  assign U748 = ~U1015 | ~U1014;
  assign U749 = ~U1017 | ~U1016;
  assign U750 = ~U1019 | ~U1018;
  assign U751 = ~U1021 | ~U1020;
  assign U752 = ~U1023 | ~U1022;
  assign U753 = ~U1025 | ~U1024;
  assign U754 = ~U1027 | ~U1026;
  assign U755 = ~U1029 | ~U1028;
  assign U756 = ~U1031 | ~U1030;
  assign U757 = ~U1033 | ~U1032;
  assign U758 = ~U1035 | ~U1034;
  assign U759 = ~U1037 | ~U1036;
  assign U760 = ~U1039 | ~U1038;
  assign U761 = ~U1041 | ~U1040;
  assign U762 = ~U1043 | ~U1042;
  assign U763 = ~U1045 | ~U1044;
  assign U764 = ~U1047 | ~U1046;
  assign U765 = ~U1049 | ~U1048;
  assign U766 = ~U1051 | ~U1050;
  assign U767 = ~U1053 | ~U1052;
  assign U768 = ~U1055 | ~U1054;
  assign U769 = SUB_60_U40 | SUB_60_U38 | SUB_60_U36 | SUB_60_U7 | SUB_60_U24;
  assign U770 = U687 & U800 & U779;
  assign U771 = U685 | U667;
  assign U772 = U807 & U853;
  assign U773 = U656 & U597;
  assign U774 = U709 & U656 & U735;
  assign U775 = ~U670 | ~U667;
  assign U776 = ~U678 | ~U667;
  assign U777 = U689 | MAR_REG_4_;
  assign U778 = U666 | MAR_REG_4_;
  assign U779 = U672 | U667;
  assign U780 = ~U679 | ~MAR_REG_4_;
  assign U781 = ~U683 | ~MAR_REG_4_;
  assign U782 = ~U684 | ~U671;
  assign U783 = ~U673 | ~MAR_REG_4_;
  assign U784 = U689 | U667;
  assign U785 = ~GT_160_U6;
  assign U786 = ~GT_166_U6;
  assign U787 = ~GT_114_U6;
  assign U788 = ~GT_130_U6 | ~U597 | ~U730;
  assign U789 = ~U731 | ~U732 | ~GT_142_U6 | ~U729;
  assign U790 = U730 | U869;
  assign U791 = ~GT_138_U6 | ~U729 | ~U731;
  assign U792 = ~GT_221_U6;
  assign U793 = ~GT_178_U6;
  assign U794 = ~GT_203_U6;
  assign U795 = ~GT_209_U6;
  assign U796 = GT_197_U6 & SUB_199_U7;
  assign U797 = ~RES_DISP_REG & ~EN_DISP_REG;
  assign U798 = U717 & U706;
  assign U799 = U717 & U705;
  assign U800 = ~U966 | ~U967 | ~MAR_REG_0_ | ~U661;
  assign U801 = ~U855 | ~U854;
  assign U802 = ~START | ~STATO_REG_0_ | ~U655;
  assign U803 = ~U708 | ~U706;
  assign U804 = ~STATO_REG_2_ | ~U697;
  assign U805 = ~MAR_REG_4_ | ~U836;
  assign U806 = ~U680 | ~U831;
  assign U807 = U655 | STATO_REG_0_;
  assign U808 = ~U655 & ~FLAG_REG;
  assign U809 = U659 | U808;
  assign U810 = ~R277_U11 | ~U658;
  assign U811 = ~NUM_REG_4_ | ~U809;
  assign U812 = ~R277_U12 | ~U658;
  assign U813 = ~NUM_REG_3_ | ~U809;
  assign U814 = ~R277_U13 | ~U658;
  assign U815 = ~NUM_REG_2_ | ~U809;
  assign U816 = ~R277_U14 | ~U658;
  assign U817 = ~NUM_REG_1_ | ~U809;
  assign U818 = ~R277_U6 | ~U658;
  assign U819 = ~NUM_REG_0_ | ~U809;
  assign U820 = ~STATO_REG_2_ | ~R277_U11;
  assign U821 = ~U668 | ~MAR_REG_4_;
  assign U822 = ~STATO_REG_2_ | ~R277_U12;
  assign U823 = ~U668 | ~MAR_REG_3_;
  assign U824 = ~STATO_REG_2_ | ~R277_U13;
  assign U825 = ~U668 | ~MAR_REG_2_;
  assign U826 = ~STATO_REG_2_ | ~R277_U14;
  assign U827 = ~U668 | ~MAR_REG_1_;
  assign U828 = ~STATO_REG_2_ | ~R277_U6;
  assign U829 = ~U668 | ~MAR_REG_0_;
  assign U830 = ~U676 | ~U675;
  assign U831 = U671 | U674;
  assign U832 = ~U681 | ~U675;
  assign U833 = U683 | U679 | U673;
  assign U834 = ~U674 | ~U669;
  assign U835 = ~U677 | ~U671;
  assign U836 = ~U835 | ~U834;
  assign U837 = U655 | U701;
  assign U838 = U655 | U676;
  assign U839 = ~U781 | ~U782 | ~U675 | ~U685;
  assign U840 = U680 | U684;
  assign U841 = ~U665 | ~U840;
  assign U842 = ~U771 | ~U689 | ~U688 | ~U687;
  assign U843 = ~U701 | ~U800;
  assign U844 = ~U692;
  assign U845 = U672 | MAR_REG_4_;
  assign U846 = ~U691;
  assign U847 = U685 | MAR_REG_4_;
  assign U848 = ~U678 | ~MAR_REG_4_;
  assign U849 = ~U695 | ~U800;
  assign U850 = ~U688 | ~U780;
  assign U851 = ~U692 | ~U701;
  assign U852 = ~U769 & ~SUB_60_U44 & ~SUB_60_U42 & ~SUB_60_U34 & ~SUB_60_U6;
  assign U853 = ~SUB_73_U6 | ~U696;
  assign U854 = ~U772;
  assign U855 = ~U737 | ~U676 | ~U805;
  assign U856 = U655 | STATO_REG_2_;
  assign U857 = ~U856 | ~U804 | ~U657;
  assign U858 = ~U698 | ~STATO_REG_0_;
  assign U859 = ~EN_DISP_REG | ~U857;
  assign U860 = ~U657 | ~U660 | ~U655;
  assign U861 = ~RES_DISP_REG | ~U860;
  assign U862 = ~U696 | ~STATO_REG_0_;
  assign U863 = ~FLAG_REG | ~U659;
  assign U864 = ~U583;
  assign U865 = ~GT_108_U6 | ~SUB_110_U12;
  assign U866 = ~SUB_110_U16 | ~GT_108_U6;
  assign U867 = ~SUB_110_U13 | ~GT_108_U6;
  assign U868 = ~SUB_110_U18 | ~GT_108_U6;
  assign U869 = ~U597;
  assign U870 = ~U711;
  assign U871 = ~U710;
  assign U872 = ~U698 | ~START;
  assign U873 = U657 | U655;
  assign U874 = ~U698 & ~U655;
  assign U875 = U874 | START;
  assign U876 = ~U660 | ~U657;
  assign U877 = ~U707 & ~EN_DISP_REG;
  assign U878 = ~U718 | ~U795;
  assign U879 = U713 | GT_209_U6;
  assign U880 = GT_224_U6 | GT_221_U6;
  assign U881 = ~U880 | ~U795;
  assign U882 = ~U715 | ~RES_DISP_REG | ~U881;
  assign U883 = ~U716 | ~U792;
  assign U884 = ~U883 | ~U795 | ~U713;
  assign U885 = ~U714 | ~U884;
  assign U886 = ~U713 | ~U792 | ~GT_224_U6 | ~U719;
  assign U887 = ~U712 | ~U718 | ~U795;
  assign U888 = U716 | GT_221_U6 | GT_218_U6;
  assign U889 = ~U719 | ~U888;
  assign U890 = GT_227_U7 | GT_221_U6 | GT_209_U6 | GT_224_U6;
  assign U891 = ~U715 | ~U890;
  assign U892 = GT_227_U7 | GT_224_U6;
  assign U893 = U792 & U892;
  assign U894 = ~U893 & ~GT_215_U6 & ~GT_218_U6;
  assign U895 = GT_212_U6 | U894;
  assign U896 = U895 & U795;
  assign U897 = GT_206_U6 | U896;
  assign U898 = ~U725 | ~U786;
  assign U899 = U721 | GT_166_U6;
  assign U900 = GT_181_U6 | GT_178_U6;
  assign U901 = ~U900 | ~U786;
  assign U902 = ~U723 | ~RES_DISP_REG | ~U901;
  assign U903 = ~U724 | ~U793;
  assign U904 = ~U903 | ~U721 | ~U786;
  assign U905 = ~U722 | ~U904;
  assign U906 = ~U721 | ~U793 | ~GT_181_U6 | ~U726;
  assign U907 = ~U720 | ~U725 | ~U786;
  assign U908 = U724 | GT_178_U6 | GT_175_U6;
  assign U909 = ~U726 | ~U908;
  assign U910 = GT_184_U7 | GT_166_U6 | GT_178_U6 | GT_181_U6;
  assign U911 = ~U723 | ~U910;
  assign U912 = GT_184_U7 | GT_181_U6;
  assign U913 = U793 & U912;
  assign U914 = ~U913 & ~GT_175_U6 & ~GT_172_U6;
  assign U915 = GT_169_U6 | U914;
  assign U916 = U915 & U786;
  assign U917 = GT_163_U6 | U916;
  assign U918 = U710 | GT_122_U6;
  assign U919 = U711 | GT_122_U6;
  assign U920 = ~U727 | ~RES_DISP_REG | ~U919;
  assign U921 = ~U728;
  assign U922 = U921 | GT_138_U6;
  assign U923 = ~U922 | ~U735 | ~U731;
  assign U924 = ~U727 | ~U923;
  assign U925 = U710 | U869;
  assign U926 = U728 | GT_138_U6 | GT_134_U6;
  assign U927 = ~U729 | ~U926;
  assign U928 = GT_138_U6 | GT_142_U6 | GT_146_U6;
  assign U929 = ~U731 | ~U928;
  assign U930 = ~U929 | ~U735;
  assign U931 = ~U727 | ~U930;
  assign U932 = GT_142_U6 | GT_146_U6;
  assign U933 = U732 & U932;
  assign U934 = U933 | GT_130_U6 | GT_134_U6;
  assign U935 = ~U730 | ~U934;
  assign U936 = U935 & U735;
  assign U937 = GT_118_U6 | U936;
  assign U938 = ~GT_134_U6 | ~U729;
  assign U939 = ~U729 | ~U711;
  assign U940 = GT_118_U6 | GT_122_U6 | U731 | U871;
  assign U941 = ~GT_122_U6 | ~U709;
  assign U942 = U735 | GT_118_U6;
  assign U943 = ~MAR_REG_4_ & ~MAR_REG_3_;
  assign U944 = ~U774;
  assign U945 = ~U773;
  assign U946 = ~STATO_REG_1_ | ~U843;
  assign U947 = ~STATO_REG_1_ | ~U849;
  assign U948 = U655 | U692;
  assign U949 = U655 | U695;
  assign U950 = ~STATO_REG_1_ | ~U851;
  assign U951 = ~U854 | ~U843;
  assign U952 = ~U854 | ~U849;
  assign U953 = U772 | U692;
  assign U954 = U772 | U695;
  assign U955 = ~U854 | ~U851;
  assign U956 = U655 | SUB_60_U6;
  assign U957 = START | STATO_REG_1_;
  assign U958 = ~TEMP_REG_8_ | ~U655;
  assign U959 = ~STATO_REG_1_ | ~U830;
  assign U960 = ~U670 | ~MAR_REG_4_;
  assign U961 = ~U833 | ~U667;
  assign U962 = ~TEMP_REG_7_ | ~U655;
  assign U963 = ~STATO_REG_1_ | ~U832;
  assign U964 = ~TEMP_REG_6_ | ~U655;
  assign U965 = ~STATO_REG_1_ | ~U839;
  assign U966 = ~MAR_REG_2_ | ~U662;
  assign U967 = U943 | MAR_REG_2_;
  assign U968 = ~TEMP_REG_5_ | ~U655;
  assign U969 = ~STATO_REG_1_ | ~U842;
  assign U970 = ~TEMP_REG_4_ | ~U655;
  assign U971 = ~STATO_REG_1_ | ~U587;
  assign U972 = ~TEMP_REG_3_ | ~U655;
  assign U973 = U655 | U703;
  assign U974 = ~TEMP_REG_2_ | ~U655;
  assign U975 = U771 | U655;
  assign U976 = ~TEMP_REG_1_ | ~U655;
  assign U977 = ~STATO_REG_1_ | ~U850;
  assign U978 = ~TEMP_REG_0_ | ~U655;
  assign U979 = U655 | U704;
  assign U980 = ~MAX_REG_8_ | ~U772;
  assign U981 = ~U854 | ~U830;
  assign U982 = ~MAX_REG_7_ | ~U772;
  assign U983 = ~U854 | ~U832;
  assign U984 = ~MAX_REG_6_ | ~U772;
  assign U985 = ~U854 | ~U839;
  assign U986 = ~MAX_REG_5_ | ~U772;
  assign U987 = ~U854 | ~U842;
  assign U988 = ~MAX_REG_4_ | ~U772;
  assign U989 = U772 | U691;
  assign U990 = ~MAX_REG_3_ | ~U772;
  assign U991 = U772 | U703;
  assign U992 = ~MAX_REG_2_ | ~U772;
  assign U993 = U771 | U772;
  assign U994 = ~MAX_REG_1_ | ~U772;
  assign U995 = ~U854 | ~U850;
  assign U996 = ~MAX_REG_0_ | ~U772;
  assign U997 = U772 | U704;
  assign U998 = ~NUM_REG_4_ | ~U705;
  assign U999 = ~SUB_199_U15 | ~GT_197_U6;
  assign U1000 = ~NUM_REG_3_ | ~U705;
  assign U1001 = ~SUB_199_U12 | ~GT_197_U6;
  assign U1002 = ~NUM_REG_2_ | ~U705;
  assign U1003 = ~SUB_199_U11 | ~GT_197_U6;
  assign U1004 = ~NUM_REG_1_ | ~U705;
  assign U1005 = ~SUB_199_U6 | ~GT_197_U6;
  assign U1006 = ~NUM_REG_0_ | ~U705;
  assign U1007 = ~NUM_REG_0_ | ~GT_197_U6;
  assign U1008 = ~MAX_REG_4_ | ~U707;
  assign U1009 = ~SUB_103_U8 | ~MAX_REG_8_;
  assign U1010 = ~U745 | ~U706;
  assign U1011 = ~SUB_110_U8 | ~GT_108_U6;
  assign U1012 = ~MAX_REG_3_ | ~U707;
  assign U1013 = ~SUB_103_U7 | ~MAX_REG_8_;
  assign U1014 = ~U747 | ~U706;
  assign U1015 = ~SUB_110_U7 | ~GT_108_U6;
  assign U1016 = ~MAX_REG_2_ | ~U707;
  assign U1017 = ~SUB_103_U6 | ~MAX_REG_8_;
  assign U1018 = ~U749 | ~U706;
  assign U1019 = ~SUB_110_U6 | ~GT_108_U6;
  assign U1020 = ~MAX_REG_1_ | ~U707;
  assign U1021 = ~SUB_103_U12 | ~MAX_REG_8_;
  assign U1022 = ~U751 | ~U706;
  assign U1023 = ~U751 | ~GT_108_U6;
  assign U1024 = ~MAX_REG_0_ | ~U707;
  assign U1025 = ~MAX_REG_0_ | ~MAX_REG_8_;
  assign U1026 = ~U753 | ~U706;
  assign U1027 = ~U753 | ~GT_108_U6;
  assign U1028 = ~U773 | ~U593;
  assign U1029 = ~R429_U21 | ~U945;
  assign U1030 = ~U773 | ~U594;
  assign U1031 = ~R429_U6 | ~U945;
  assign U1032 = ~U773 | ~U595;
  assign U1033 = ~R429_U29 | ~U945;
  assign U1034 = ~U773 | ~U596;
  assign U1035 = ~R429_U31 | ~U945;
  assign U1036 = ~R429_U33 | ~U944;
  assign U1037 = ~U774 | ~U746;
  assign U1038 = ~R429_U35 | ~U944;
  assign U1039 = ~U774 | ~U748;
  assign U1040 = ~R429_U22 | ~U944;
  assign U1041 = ~U774 | ~U750;
  assign U1042 = ~R429_U7 | ~U944;
  assign U1043 = ~U774 | ~U752;
  assign U1044 = ~U754 | ~U944;
  assign U1045 = ~U774 | ~U754;
  assign U1046 = ~NUM_REG_4_ | ~U660;
  assign U1047 = U667 | U660;
  assign U1048 = ~NUM_REG_3_ | ~U660;
  assign U1049 = U662 | U660;
  assign U1050 = ~NUM_REG_2_ | ~U660;
  assign U1051 = ~MAR_REG_2_ | ~STATO_REG_2_;
  assign U1052 = ~NUM_REG_1_ | ~U660;
  assign U1053 = U661 | U660;
  assign U1054 = ~NUM_REG_0_ | ~U660;
  assign U1055 = U663 | U660;
  assign U1056 = ~GT_146_U6;
  assign U1057 = ~GT_224_U6;
  assign U1058 = ~U712;
  assign U1059 = ~GT_181_U6;
  assign U1060 = ~U720;
  assign GT_122_U8 = U595 & GT_122_U10;
  assign GT_122_U7 = U750 & U752;
  assign GT_122_U6 = ~U593 & ~GT_122_U9;
  assign GT_166_U9 = U759 | U758 | U760;
  assign GT_166_U8 = ~U756 & ~U757 & ~GT_166_U9 & ~GT_166_U7;
  assign GT_166_U7 = U763 & U762 & U761;
  assign GT_166_U6 = ~U755 & ~GT_166_U8;
  assign GT_142_U9 = U750 | U748;
  assign GT_142_U8 = ~GT_142_U7 & ~U594 & ~U596 & ~U595;
  assign GT_142_U7 = U746 & GT_142_U9;
  assign GT_142_U6 = ~U593 & ~GT_142_U8;
  assign GT_175_U8 = U760 | U758 | U761;
  assign GT_175_U7 = ~U759 & ~U756 & ~GT_175_U8 & ~U757;
  assign GT_175_U6 = ~U755 & ~GT_175_U7;
  assign GT_134_U9 = U746 | U748;
  assign GT_134_U8 = ~U594 & ~GT_134_U7 & ~U595;
  assign GT_134_U7 = U596 & GT_134_U9;
  assign GT_134_U6 = ~U593 & ~GT_134_U8;
  assign GT_209_U9 = U740 | GT_209_U7 | U741;
  assign GT_209_U8 = ~U796 & ~U796 & ~GT_209_U9 & ~U796;
  assign GT_209_U7 = U744 & U743 & U742;
  assign GT_209_U6 = ~U796 & ~GT_209_U8;
  assign SUB_73_U47 = ~SUB_73_U46 | ~SUB_73_U45;
  assign SUB_73_U46 = ~MAX_REG_8_ | ~SUB_73_U13;
  assign SUB_73_U45 = ~U864 | ~SUB_73_U14;
  assign SUB_73_U44 = ~SUB_73_U45 | ~SUB_73_U46 | ~SUB_73_U43 | ~SUB_73_U39;
  assign SUB_73_U43 = ~SUB_73_U42 | ~SUB_73_U12;
  assign SUB_73_U42 = ~SUB_73_U36 | ~SUB_73_U37;
  assign SUB_73_U41 = ~SUB_73_U47 | ~SUB_73_U40;
  assign SUB_73_U40 = ~SUB_73_U43 | ~SUB_73_U39;
  assign SUB_73_U39 = ~U584 | ~SUB_73_U38;
  assign SUB_73_U38 = ~MAX_REG_7_ | ~SUB_73_U37 | ~SUB_73_U36;
  assign SUB_73_U37 = SUB_73_U11 | MAX_REG_6_;
  assign SUB_73_U36 = ~SUB_73_U35 | ~SUB_73_U34;
  assign SUB_73_U35 = ~MAX_REG_6_ | ~SUB_73_U11;
  assign SUB_73_U34 = ~SUB_73_U33 | ~SUB_73_U32;
  assign SUB_73_U33 = SUB_73_U10 | MAX_REG_5_;
  assign SUB_73_U32 = ~SUB_73_U31 | ~SUB_73_U30;
  assign SUB_73_U31 = ~MAX_REG_5_ | ~SUB_73_U10;
  assign GT_218_U6 = ~U796 & ~GT_218_U7;
  assign GT_218_U7 = ~U796 & ~U796 & ~GT_218_U8 & ~U796;
  assign GT_218_U8 = U741 | U740 | U742;
  assign GT_146_U6 = ~U593 & ~GT_146_U8;
  assign GT_146_U7 = U748 & GT_146_U9;
  assign GT_146_U8 = ~U595 & ~U594 & ~U596 & ~U746 & ~GT_146_U7;
  assign GT_146_U9 = U750 | U752;
  assign GT_126_U6 = ~U593 & ~GT_126_U8;
  assign GT_126_U7 = U748 & U596 & U750 & U746;
  assign GT_126_U8 = ~U594 & ~GT_126_U7 & ~U595;
  assign GT_203_U6 = ~U796 & ~GT_203_U8;
  assign GT_203_U7 = U741 & GT_203_U9;
  assign GT_203_U8 = ~U796 & ~U796 & ~U796 & ~U740 & ~GT_203_U7;
  assign GT_203_U9 = U743 | U742 | U744;
  assign GT_172_U6 = ~U755 & ~GT_172_U8;
  assign GT_172_U7 = U761 & GT_172_U10;
  assign GT_172_U8 = ~U756 & ~U757 & ~GT_172_U9 & ~GT_172_U7;
  assign GT_172_U9 = U759 | U758 | U760;
  assign GT_172_U10 = U763 | U762;
  assign GT_108_U6 = ~U708 & ~GT_108_U8;
  assign GT_108_U7 = U708 & U708 & GT_108_U9;
  assign GT_108_U8 = ~GT_108_U7 & ~U708;
  assign GT_108_U9 = U745 | U749 | U747;
  assign GT_181_U6 = ~U755 & ~GT_181_U7;
  assign GT_181_U7 = ~U757 & ~U756 & ~GT_181_U8 & ~U762;
  assign GT_181_U8 = U759 | U760 | U758 | U761;
  assign SUB_110_U6 = ~U749;
  assign SUB_110_U7 = ~SUB_110_U9 | ~SUB_110_U21;
  assign SUB_110_U8 = ~SUB_110_U17 | ~SUB_110_U20;
  assign SUB_110_U9 = U749 | U747;
  assign SUB_110_U10 = ~U708;
  assign SUB_110_U11 = U708 & SUB_110_U17;
  assign SUB_110_U12 = ~SUB_110_U23 | ~SUB_110_U22;
  assign SUB_110_U13 = ~SUB_110_U27 | ~SUB_110_U26;
  assign SUB_110_U14 = ~SUB_110_U10 | ~SUB_110_U15;
  assign SUB_110_U15 = ~U708 | ~SUB_110_U11;
  assign SUB_110_U16 = SUB_110_U25 & SUB_110_U24;
  assign SUB_110_U17 = SUB_110_U9 | U745;
  assign SUB_110_U18 = SUB_110_U29 & SUB_110_U28;
  assign SUB_110_U19 = ~SUB_110_U17;
  assign SUB_110_U20 = ~U745 | ~SUB_110_U9;
  assign SUB_110_U21 = ~U747 | ~U749;
  assign SUB_110_U22 = ~U708 | ~SUB_110_U14;
  assign SUB_110_U23 = SUB_110_U14 | U708;
  assign SUB_110_U24 = ~U708 | ~SUB_110_U15;
  assign SUB_110_U25 = SUB_110_U15 | U708;
  assign SUB_110_U26 = SUB_110_U10 | SUB_110_U11;
  assign SUB_110_U27 = ~SUB_110_U11 | ~SUB_110_U10;
  assign SUB_110_U28 = ~U708 | ~SUB_110_U17;
  assign SUB_110_U29 = ~SUB_110_U19 | ~SUB_110_U10;
  assign R277_U6 = ~U768;
  assign R277_U7 = U767 & U768;
  assign R277_U8 = ~U766;
  assign R277_U9 = U766 & R277_U7;
  assign R277_U10 = ~U765;
  assign R277_U11 = ~R277_U17 | ~R277_U16;
  assign R277_U12 = ~R277_U19 | ~R277_U18;
  assign R277_U13 = ~R277_U21 | ~R277_U20;
  assign R277_U14 = ~R277_U23 | ~R277_U22;
  assign R277_U15 = ~U765 | ~R277_U9;
  assign R277_U16 = ~U764 | ~R277_U15;
  assign R277_U17 = R277_U15 | U764;
  assign R277_U18 = R277_U10 | R277_U9;
  assign R277_U19 = ~R277_U9 | ~R277_U10;
  assign R277_U20 = R277_U8 | R277_U7;
  assign R277_U21 = ~R277_U7 | ~R277_U8;
  assign R277_U22 = R277_U6 | U767;
  assign R277_U23 = ~U767 | ~R277_U6;
  assign GT_163_U6 = ~U755 & ~GT_163_U7;
  assign GT_163_U7 = ~U756 & ~U757 & ~U759 & ~U758 & ~U760;
  assign GT_224_U6 = ~U796 & ~GT_224_U7;
  assign GT_224_U7 = ~U796 & ~U796 & ~GT_224_U8 & ~U796;
  assign GT_224_U8 = U740 | U741 | U743 | U742;
  assign GT_114_U6 = ~U593 & ~GT_114_U9;
  assign GT_114_U7 = U748 & U746 & GT_114_U10;
  assign GT_114_U8 = U595 & GT_114_U11;
  assign GT_114_U9 = ~GT_114_U8 & ~U594;
  assign GT_114_U10 = U750 | U752;
  assign GT_114_U11 = GT_114_U7 | U596;
  assign GT_118_U6 = ~U593 & ~GT_118_U8;
  assign GT_118_U7 = U595 & GT_118_U9;
  assign GT_118_U8 = ~GT_118_U7 & ~U594;
  assign GT_118_U9 = U746 | U596;
  assign GT_184_U6 = ~U756 & ~U762 & ~U763 & ~GT_184_U8 & ~U757;
  assign GT_184_U7 = ~GT_184_U6 & ~U755;
  assign GT_184_U8 = U759 | U760 | U758 | U761;
  assign GT_212_U6 = ~U796 & ~GT_212_U8;
  assign GT_212_U7 = U742 & U743;
  assign GT_212_U8 = ~U796 & ~U796 & ~GT_212_U9 & ~U796;
  assign GT_212_U9 = GT_212_U7 | U741 | U740;
  assign SUB_60_U6 = ~SUB_60_U62 | ~SUB_60_U64;
  assign SUB_60_U7 = ~SUB_60_U9 | ~SUB_60_U65;
  assign SUB_60_U8 = ~TEMP_REG_0_;
  assign SUB_60_U9 = SUB_60_U8 | U591;
  assign SUB_60_U10 = ~TEMP_REG_1_;
  assign SUB_60_U11 = ~U589;
  assign SUB_60_U12 = ~TEMP_REG_2_;
  assign SUB_60_U13 = ~U588;
  assign SUB_60_U14 = ~TEMP_REG_3_;
  assign SUB_60_U15 = ~U587;
  assign SUB_60_U16 = ~TEMP_REG_4_;
  assign SUB_60_U17 = ~U586;
  assign SUB_60_U18 = ~TEMP_REG_5_;
  assign SUB_60_U19 = ~U585;
  assign SUB_60_U20 = ~TEMP_REG_6_;
  assign SUB_60_U21 = ~TEMP_REG_7_;
  assign SUB_60_U22 = SUB_60_U74 & SUB_60_U59;
  assign SUB_60_U23 = ~U584;
  assign SUB_60_U24 = ~SUB_60_U96 | ~SUB_60_U95;
  assign SUB_60_U25 = SUB_60_U70 & SUB_60_U69;
  assign SUB_60_U26 = SUB_60_U74 & SUB_60_U73;
  assign SUB_60_U27 = SUB_60_U78 & SUB_60_U77;
  assign SUB_60_U28 = SUB_60_U82 & SUB_60_U81;
  assign SUB_60_U29 = SUB_60_U86 & SUB_60_U85;
  assign SUB_60_U30 = SUB_60_U90 & SUB_60_U89;
  assign SUB_60_U31 = SUB_60_U94 & SUB_60_U93;
  assign SUB_60_U32 = ~U864;
  assign SUB_60_U33 = ~TEMP_REG_8_;
  assign SUB_60_U34 = SUB_60_U72 & SUB_60_U71;
  assign SUB_60_U35 = SUB_60_U78 & SUB_60_U57;
  assign SUB_60_U36 = SUB_60_U76 & SUB_60_U75;
  assign SUB_60_U37 = SUB_60_U82 & SUB_60_U55;
  assign SUB_60_U38 = SUB_60_U80 & SUB_60_U79;
  assign SUB_60_U39 = SUB_60_U86 & SUB_60_U53;
  assign SUB_60_U40 = SUB_60_U84 & SUB_60_U83;
  assign SUB_60_U41 = SUB_60_U90 & SUB_60_U51;
  assign SUB_60_U42 = SUB_60_U88 & SUB_60_U87;
  assign SUB_60_U43 = SUB_60_U49 & SUB_60_U48;
  assign SUB_60_U44 = SUB_60_U92 & SUB_60_U91;
  assign SUB_60_U45 = ~U590;
  assign SUB_60_U46 = ~SUB_60_U9;
  assign SUB_60_U47 = ~SUB_60_U10 | ~SUB_60_U9;
  assign SUB_60_U48 = ~SUB_60_U47 | ~SUB_60_U45;
  assign SUB_60_U49 = ~TEMP_REG_1_ | ~SUB_60_U46;
  assign SUB_60_U50 = U589 & SUB_60_U12;
  assign SUB_60_U51 = SUB_60_U50 | SUB_60_U43;
  assign SUB_60_U52 = U588 & SUB_60_U14;
  assign SUB_60_U53 = SUB_60_U52 | SUB_60_U41;
  assign SUB_60_U54 = U587 & SUB_60_U16;
  assign SUB_60_U55 = SUB_60_U54 | SUB_60_U39;
  assign SUB_60_U56 = U586 & SUB_60_U18;
  assign SUB_60_U57 = SUB_60_U56 | SUB_60_U37;
  assign SUB_60_U58 = U585 & SUB_60_U20;
  assign SUB_60_U59 = SUB_60_U58 | SUB_60_U35;
  assign SUB_60_U60 = U584 & SUB_60_U21;
  assign SUB_60_U61 = SUB_60_U60 | SUB_60_U22;
  assign SUB_60_U62 = ~SUB_60_U68 | ~SUB_60_U70 | ~SUB_60_U61;
  assign SUB_60_U63 = ~SUB_60_U22 | ~SUB_60_U70;
  assign SUB_60_U64 = ~SUB_60_U66 | ~SUB_60_U67 | ~SUB_60_U69 | ~SUB_60_U63;
  assign SUB_60_U65 = ~U591 | ~SUB_60_U8;
  assign SUB_60_U66 = ~U864 | ~SUB_60_U33;
  assign SUB_60_U67 = ~TEMP_REG_8_ | ~SUB_60_U32;
  assign SUB_60_U68 = ~SUB_60_U67 | ~SUB_60_U66;
  assign SUB_60_U69 = ~U584 | ~SUB_60_U21;
  assign SUB_60_U70 = ~TEMP_REG_7_ | ~SUB_60_U23;
  assign SUB_60_U71 = ~SUB_60_U25 | ~SUB_60_U22;
  assign SUB_60_U72 = SUB_60_U25 | SUB_60_U22;
  assign SUB_60_U73 = ~U585 | ~SUB_60_U20;
  assign SUB_60_U74 = ~TEMP_REG_6_ | ~SUB_60_U19;
  assign SUB_60_U75 = ~SUB_60_U35 | ~SUB_60_U26;
  assign SUB_60_U76 = SUB_60_U26 | SUB_60_U35;
  assign SUB_60_U77 = ~U586 | ~SUB_60_U18;
  assign SUB_60_U78 = ~TEMP_REG_5_ | ~SUB_60_U17;
  assign SUB_60_U79 = ~SUB_60_U37 | ~SUB_60_U27;
  assign SUB_60_U80 = SUB_60_U27 | SUB_60_U37;
  assign SUB_60_U81 = ~U587 | ~SUB_60_U16;
  assign SUB_60_U82 = ~TEMP_REG_4_ | ~SUB_60_U15;
  assign SUB_60_U83 = ~SUB_60_U39 | ~SUB_60_U28;
  assign SUB_60_U84 = SUB_60_U28 | SUB_60_U39;
  assign SUB_60_U85 = ~U588 | ~SUB_60_U14;
  assign SUB_60_U86 = ~TEMP_REG_3_ | ~SUB_60_U13;
  assign SUB_60_U87 = ~SUB_60_U41 | ~SUB_60_U29;
  assign SUB_60_U88 = SUB_60_U29 | SUB_60_U41;
  assign SUB_60_U89 = ~U589 | ~SUB_60_U12;
  assign SUB_60_U90 = ~TEMP_REG_2_ | ~SUB_60_U11;
  assign SUB_60_U91 = ~SUB_60_U43 | ~SUB_60_U30;
  assign SUB_60_U92 = SUB_60_U30 | SUB_60_U43;
  assign SUB_60_U93 = ~U590 | ~SUB_60_U10;
  assign SUB_60_U94 = ~TEMP_REG_1_ | ~SUB_60_U45;
  assign SUB_60_U95 = ~SUB_60_U31 | ~SUB_60_U46;
  assign SUB_60_U96 = SUB_60_U31 | SUB_60_U46;
  assign R429_U6 = ~R429_U28 | ~R429_U48;
  assign R429_U7 = ~R429_U37 | ~R429_U49;
  assign R429_U8 = ~U602;
  assign R429_U9 = ~R429_U8 & ~U752;
  assign R429_U10 = ~U601;
  assign R429_U11 = ~U748;
  assign R429_U12 = ~U600;
  assign R429_U13 = ~U746;
  assign R429_U14 = ~U599;
  assign R429_U15 = ~U596;
  assign R429_U16 = ~U598;
  assign R429_U17 = ~U869;
  assign R429_U18 = R429_U56 & R429_U46;
  assign R429_U19 = ~U595;
  assign R429_U20 = ~R429_U53 | ~R429_U47;
  assign R429_U21 = ~R429_U51 | ~R429_U50;
  assign R429_U22 = ~R429_U71 | ~R429_U70;
  assign R429_U23 = R429_U53 & R429_U52;
  assign R429_U24 = R429_U57 & R429_U56;
  assign R429_U25 = R429_U61 & R429_U60;
  assign R429_U26 = R429_U65 & R429_U64;
  assign R429_U27 = R429_U69 & R429_U68;
  assign R429_U28 = R429_U20 | U594;
  assign R429_U29 = R429_U55 & R429_U54;
  assign R429_U30 = R429_U61 & R429_U44;
  assign R429_U31 = R429_U59 & R429_U58;
  assign R429_U32 = R429_U64 & R429_U42;
  assign R429_U33 = R429_U63 & R429_U62;
  assign R429_U34 = R429_U40 & R429_U39;
  assign R429_U35 = R429_U67 & R429_U66;
  assign R429_U36 = ~U750;
  assign R429_U37 = ~R429_U9;
  assign R429_U38 = R429_U9 | U601;
  assign R429_U39 = ~R429_U38 | ~R429_U36;
  assign R429_U40 = R429_U10 | R429_U37;
  assign R429_U41 = ~R429_U11 & ~U600;
  assign R429_U42 = R429_U41 | R429_U34;
  assign R429_U43 = ~R429_U13 & ~U599;
  assign R429_U44 = R429_U43 | R429_U32;
  assign R429_U45 = ~R429_U15 & ~U598;
  assign R429_U46 = R429_U45 | R429_U30;
  assign R429_U47 = ~R429_U18 | ~R429_U52;
  assign R429_U48 = ~U594 | ~R429_U20;
  assign R429_U49 = ~U752 | ~R429_U8;
  assign R429_U50 = ~U593 | ~R429_U28;
  assign R429_U51 = R429_U28 | U593;
  assign R429_U52 = R429_U17 | U595;
  assign R429_U53 = R429_U19 | U869;
  assign R429_U54 = ~R429_U23 | ~R429_U18;
  assign R429_U55 = R429_U23 | R429_U18;
  assign R429_U56 = R429_U16 | U596;
  assign R429_U57 = R429_U15 | U598;
  assign R429_U58 = ~R429_U30 | ~R429_U24;
  assign R429_U59 = R429_U24 | R429_U30;
  assign R429_U60 = R429_U13 | U599;
  assign R429_U61 = R429_U14 | U746;
  assign R429_U62 = ~R429_U32 | ~R429_U25;
  assign R429_U63 = R429_U25 | R429_U32;
  assign R429_U64 = R429_U12 | U748;
  assign R429_U65 = R429_U11 | U600;
  assign R429_U66 = ~R429_U34 | ~R429_U26;
  assign R429_U67 = R429_U26 | R429_U34;
  assign R429_U68 = R429_U36 | U601;
  assign R429_U69 = R429_U10 | U750;
  assign R429_U70 = ~R429_U27 | ~R429_U9;
  assign R429_U71 = R429_U27 | R429_U9;
  assign GT_138_U6 = ~U593 & ~GT_138_U8;
  assign GT_138_U7 = U748 & U752 & U750 & U746;
  assign GT_138_U8 = ~GT_138_U7 & ~U594 & ~U596 & ~U595;
  assign GT_206_U6 = ~U796 & ~GT_206_U7;
  assign GT_206_U7 = ~U796 & ~U796 & ~U740 & ~U796 & ~U741;
  assign GT_160_U6 = ~U755 & ~GT_160_U8;
  assign GT_160_U7 = U760 & GT_160_U9;
  assign GT_160_U8 = ~U757 & ~U756 & ~U758 & ~U759 & ~GT_160_U7;
  assign GT_160_U9 = U762 | U761 | U763;
  assign GT_169_U6 = ~U755 & ~GT_169_U8;
  assign GT_169_U7 = U761 & U762;
  assign GT_169_U8 = ~U756 & ~U757 & ~GT_169_U9 & ~GT_169_U7;
  assign GT_169_U9 = U759 | U758 | U760;
  assign SUB_103_U6 = SUB_103_U17 & SUB_103_U10;
  assign SUB_103_U7 = SUB_103_U15 & SUB_103_U11;
  assign SUB_103_U8 = SUB_103_U14 & SUB_103_U9;
  assign SUB_103_U9 = SUB_103_U11 | MAX_REG_4_;
  assign SUB_103_U10 = MAX_REG_2_ | MAX_REG_0_ | MAX_REG_1_;
  assign SUB_103_U11 = SUB_103_U10 | MAX_REG_3_;
  assign SUB_103_U12 = ~SUB_103_U19 | ~SUB_103_U18;
  assign SUB_103_U13 = ~MAX_REG_0_;
  assign SUB_103_U14 = ~MAX_REG_4_ | ~SUB_103_U11;
  assign SUB_103_U15 = ~MAX_REG_3_ | ~SUB_103_U10;
  assign SUB_103_U16 = MAX_REG_0_ | MAX_REG_1_;
  assign SUB_103_U17 = ~MAX_REG_2_ | ~SUB_103_U16;
  assign SUB_103_U18 = SUB_103_U13 | MAX_REG_1_;
  assign SUB_103_U19 = ~MAX_REG_1_ | ~SUB_103_U13;
  assign GT_227_U6 = ~U796 & ~U743 & ~U744 & ~GT_227_U8 & ~U796;
  assign GT_227_U7 = ~GT_227_U6 & ~U796;
  assign GT_227_U8 = U740 | U741 | U796 | U742;
  assign GT_197_U6 = GT_197_U7 | NUM_REG_4_;
  assign GT_197_U7 = NUM_REG_3_ & GT_197_U8;
  assign GT_197_U8 = NUM_REG_2_ | NUM_REG_1_;
  assign GT_130_U6 = ~U593 & ~GT_130_U8;
  assign GT_130_U7 = U746 & U596 & GT_130_U9;
  assign GT_130_U8 = ~U594 & ~GT_130_U7 & ~U595;
  assign GT_130_U9 = U752 | U750 | U748;
  assign GT_178_U6 = ~U755 & ~GT_178_U8;
  assign GT_178_U7 = U763 & U762;
  assign GT_178_U8 = ~U756 & ~U757 & ~GT_178_U9 & ~GT_178_U7;
  assign GT_178_U9 = U759 | U760 | U758 | U761;
  assign SUB_199_U6 = ~NUM_REG_1_;
  assign SUB_199_U7 = SUB_199_U20 & SUB_199_U16 & SUB_199_U13;
  assign SUB_199_U8 = ~NUM_REG_3_;
  assign SUB_199_U9 = NUM_REG_2_ & NUM_REG_1_;
  assign SUB_199_U10 = SUB_199_U16 & SUB_199_U17;
  assign SUB_199_U11 = SUB_199_U24 & SUB_199_U23;
  assign SUB_199_U12 = ~SUB_199_U28 | ~SUB_199_U27;
  assign SUB_199_U13 = ~NUM_REG_4_;
  assign SUB_199_U14 = NUM_REG_3_ & SUB_199_U21;
  assign SUB_199_U15 = SUB_199_U26 & SUB_199_U25;
  assign SUB_199_U16 = SUB_199_U8 | SUB_199_U11;
  assign SUB_199_U17 = ~SUB_199_U11 | ~SUB_199_U8;
  assign SUB_199_U18 = ~SUB_199_U10;
  assign SUB_199_U19 = ~SUB_199_U9;
  assign SUB_199_U20 = SUB_199_U18 | SUB_199_U19;
  assign SUB_199_U21 = ~SUB_199_U11 | ~SUB_199_U19;
  assign SUB_199_U22 = ~SUB_199_U14;
  assign SUB_199_U23 = ~NUM_REG_2_ | ~SUB_199_U6;
  assign SUB_199_U24 = NUM_REG_2_ | SUB_199_U6;
  assign SUB_199_U25 = SUB_199_U13 | SUB_199_U14;
  assign SUB_199_U26 = SUB_199_U22 | NUM_REG_4_;
  assign SUB_199_U27 = SUB_199_U18 | SUB_199_U9;
  assign SUB_199_U28 = SUB_199_U19 | SUB_199_U10;
  assign GT_221_U6 = ~U796 & ~GT_221_U8;
  assign GT_221_U7 = U744 & U743;
  assign GT_221_U8 = ~U796 & ~U796 & ~GT_221_U9 & ~U796;
  assign GT_221_U9 = U740 | U741 | GT_221_U7 | U742;
  assign GT_215_U6 = ~U796 & ~GT_215_U8;
  assign GT_215_U7 = U742 & GT_215_U10;
  assign GT_215_U8 = ~U796 & ~U796 & ~GT_215_U9 & ~U796;
  assign GT_215_U9 = U740 | GT_215_U7 | U741;
  assign GT_215_U10 = U744 | U743;
  assign SUB_73_U6 = ~SUB_73_U41 | ~SUB_73_U44;
  assign SUB_73_U7 = ~MAX_REG_1_;
  assign SUB_73_U8 = ~MAX_REG_2_;
  assign SUB_73_U9 = ~U588;
  assign SUB_73_U10 = ~U586;
  assign SUB_73_U11 = ~U585;
  assign SUB_73_U12 = ~MAX_REG_7_;
  assign SUB_73_U13 = ~U864;
  assign SUB_73_U14 = ~MAX_REG_8_;
  assign SUB_73_U15 = ~U591;
  assign SUB_73_U16 = ~U590 | ~SUB_73_U7;
  assign SUB_73_U17 = ~MAX_REG_0_ | ~SUB_73_U16 | ~SUB_73_U15;
  assign SUB_73_U18 = SUB_73_U7 | U590;
  assign SUB_73_U19 = ~SUB_73_U18 | ~SUB_73_U17;
  assign SUB_73_U20 = ~U589 | ~SUB_73_U8;
  assign SUB_73_U21 = ~SUB_73_U20 | ~SUB_73_U19;
  assign SUB_73_U22 = SUB_73_U8 | U589;
  assign SUB_73_U23 = ~MAX_REG_3_ | ~SUB_73_U9;
  assign SUB_73_U24 = ~SUB_73_U23 | ~SUB_73_U22 | ~SUB_73_U21;
  assign SUB_73_U25 = SUB_73_U9 | MAX_REG_3_;
  assign SUB_73_U26 = ~MAX_REG_4_ | ~SUB_73_U25 | ~SUB_73_U24;
  assign SUB_73_U27 = ~U587 | ~SUB_73_U26;
  assign SUB_73_U28 = SUB_73_U25 & SUB_73_U24;
  assign SUB_73_U29 = MAX_REG_4_ | SUB_73_U28;
  assign SUB_73_U30 = ~SUB_73_U29 | ~SUB_73_U27;
  always @ (posedge clock) begin
    NUM_REG_4_ <= n66;
    NUM_REG_3_ <= n71;
    NUM_REG_2_ <= n76;
    NUM_REG_1_ <= n81;
    NUM_REG_0_ <= n86;
    MAR_REG_4_ <= n91;
    MAR_REG_3_ <= n96;
    MAR_REG_2_ <= n101;
    MAR_REG_1_ <= n106;
    MAR_REG_0_ <= n111;
    TEMP_REG_8_ <= n116;
    TEMP_REG_7_ <= n121;
    TEMP_REG_6_ <= n126;
    TEMP_REG_5_ <= n131;
    TEMP_REG_4_ <= n136;
    TEMP_REG_3_ <= n141;
    TEMP_REG_2_ <= n146;
    TEMP_REG_1_ <= n151;
    TEMP_REG_0_ <= n156;
    MAX_REG_8_ <= n161;
    MAX_REG_7_ <= n166;
    MAX_REG_6_ <= n171;
    MAX_REG_5_ <= n176;
    MAX_REG_4_ <= n181;
    MAX_REG_3_ <= n186;
    MAX_REG_2_ <= n191;
    MAX_REG_1_ <= n196;
    MAX_REG_0_ <= n201;
    EN_DISP_REG <= n206;
    RES_DISP_REG <= n211;
    FLAG_REG <= n216;
    STATO_REG_0_ <= n221;
    STATO_REG_1_ <= n226;
    STATO_REG_2_ <= n231;
  end
endmodule


