// Benchmark "b13s" written by ABC on Mon Apr  8 17:54:37 2019

module b13s ( clock, 
    EOC, DATA_IN_7_, DATA_IN_6_, DATA_IN_5_, DATA_IN_4_, DATA_IN_3_,
    DATA_IN_2_, DATA_IN_1_, DATA_IN_0_, DSR,
    SOC_REG, LOAD_DATO_REG, ADD_MPX2_REG, CANALE_REG_3_, CANALE_REG_2_,
    CANALE_REG_1_, CANALE_REG_0_, MUX_EN_REG, ERROR_REG, DATA_OUT_REG  );
  input  clock;
  input  EOC, DATA_IN_7_, DATA_IN_6_, DATA_IN_5_, DATA_IN_4_, DATA_IN_3_,
    DATA_IN_2_, DATA_IN_1_, DATA_IN_0_, DSR;
  output SOC_REG, LOAD_DATO_REG, ADD_MPX2_REG, CANALE_REG_3_, CANALE_REG_2_,
    CANALE_REG_1_, CANALE_REG_0_, MUX_EN_REG, ERROR_REG, DATA_OUT_REG;
  reg CANALE_REG_3_, CANALE_REG_2_, CANALE_REG_1_, CANALE_REG_0_,
    CONTA_TMP_REG_3_, CONTA_TMP_REG_2_, CONTA_TMP_REG_1_, CONTA_TMP_REG_0_,
    OUT_REG_REG_7_, OUT_REG_REG_6_, OUT_REG_REG_5_, OUT_REG_REG_4_,
    OUT_REG_REG_3_, OUT_REG_REG_2_, OUT_REG_REG_1_, OUT_REG_REG_0_,
    NEXT_BIT_REG_3_, NEXT_BIT_REG_2_, NEXT_BIT_REG_1_, NEXT_BIT_REG_0_,
    TX_CONTA_REG_9_, TX_CONTA_REG_8_, TX_CONTA_REG_7_, TX_CONTA_REG_6_,
    TX_CONTA_REG_5_, TX_CONTA_REG_4_, TX_CONTA_REG_3_, TX_CONTA_REG_2_,
    TX_CONTA_REG_1_, TX_CONTA_REG_0_, LOAD_REG, ITFC_STATE_REG_0_,
    SEND_DATA_REG, SEND_EN_REG, MUX_EN_REG, ITFC_STATE_REG_1_, TRE_REG,
    LOAD_DATO_REG, SOC_REG, SEND_REG, MPX_REG, CONFIRM_REG, SHOT_REG,
    ADD_MPX2_REG, RDY_REG, ERROR_REG, S1_REG_2_, S1_REG_1_, S1_REG_0_,
    S2_REG_1_, S2_REG_0_, TX_END_REG, DATA_OUT_REG;
  wire GT_255_U10, GT_255_U9, U409, U410, U411, U412, U413, U414, U415, U416,
    U417, U418, U419, U420, U421, U422, U423, U424, U425, U426, U427, U428,
    U429, U430, U431, U432, U433, U434, U435, U436, U437, U453, U454, U455,
    U456, U457, U458, U459, U460, U461, U462, U463, U464, U465, U466, U467,
    U468, U469, U470, U471, U472, U473, U474, U475, U476, U477, U478, U479,
    U480, U481, U482, U483, U484, U485, U486, U487, U488, U489, U490, U491,
    U492, U493, U494, U495, U496, U497, U498, U499, U500, U501, U502, U503,
    U504, U505, U506, U507, U508, U509, U510, U511, U512, U513, U514, U515,
    U516, U517, U518, U519, U520, U521, U522, U523, U524, U525, U526, U527,
    U528, U529, U530, U531, U532, U533, U534, U535, U536, U537, U538, U539,
    U540, U541, U542, U543, U544, U545, U546, U547, U548, U549, U550, U551,
    U552, U553, U554, U555, U556, U557, U558, U559, U560, U561, U562, U563,
    U564, U565, U566, U567, U568, U569, U570, U571, U572, U573, U574, U575,
    U576, U577, U578, U579, U580, U581, U582, U583, U584, U585, U586, U587,
    U588, GT_255_U8, ADD_291_U6, ADD_291_U7, ADD_291_U8, ADD_291_U9,
    ADD_291_U10, ADD_291_U11, ADD_291_U12, ADD_291_U13, ADD_291_U14,
    ADD_291_U15, ADD_291_U16, ADD_291_U17, ADD_291_U18, ADD_291_U19,
    ADD_291_U20, ADD_291_U21, ADD_291_U22, ADD_291_U23, ADD_291_U24,
    ADD_291_U25, ADD_291_U26, ADD_291_U27, ADD_291_U28, ADD_291_U29,
    ADD_291_U30, ADD_291_U31, ADD_291_U32, ADD_291_U33, ADD_291_U34,
    ADD_291_U35, ADD_291_U36, ADD_291_U37, ADD_291_U38, ADD_291_U39,
    ADD_291_U40, ADD_291_U41, ADD_291_U42, ADD_291_U43, ADD_291_U44,
    ADD_291_U45, ADD_291_U46, ADD_291_U47, ADD_291_U48, GT_255_U6,
    GT_255_U7, n42, n46, n50, n54, n58, n63, n68, n73, n78, n83, n88, n93,
    n98, n103, n108, n113, n118, n123, n128, n133, n138, n143, n148, n153,
    n158, n163, n168, n173, n178, n183, n188, n193, n198, n203, n208, n212,
    n217, n222, n226, n230, n235, n240, n245, n250, n254, n259, n263, n268,
    n273, n278, n283, n288, n293;
  assign GT_255_U10 = GT_255_U7 | TX_CONTA_REG_4_;
  assign GT_255_U9 = TX_CONTA_REG_1_ | TX_CONTA_REG_2_ | TX_CONTA_REG_0_;
  assign n263 = ~U424 | ~U543;
  assign n273 = ~S1_REG_0_ | ~U587 | ~U586;
  assign n293 = ~U411 | ~U581 | ~U580;
  assign n288 = ~U458 & ~U413 & ~NEXT_BIT_REG_1_;
  assign n283 = ~U491 | ~U459;
  assign n42 = ~U503 | ~U502;
  assign n46 = ~U505 | ~U504 | ~U454;
  assign n50 = ~U507 | ~U506 | ~U455;
  assign n54 = ~U433 | ~U508;
  assign n63 = ~U454 | ~U511;
  assign n68 = ~U455 | ~U513;
  assign n73 = ~U433 | ~U514;
  assign n123 = ~U457 | ~U575 | ~U574;
  assign n128 = ~U517 | ~U516;
  assign n138 = ~U522 | ~U521;
  assign n143 = ~U524 | ~U523;
  assign n148 = ~U526 | ~U525;
  assign n153 = ~U528 | ~U527;
  assign n158 = ~U530 | ~U529;
  assign n163 = ~U462 | ~U461;
  assign n168 = ~U464 | ~U463;
  assign n173 = ~U466 | ~U465;
  assign n178 = ~U468 | ~U467;
  assign n183 = ~U470 | ~U469;
  assign n188 = ~U473 | ~U472;
  assign n193 = ~U476 | ~U475;
  assign n198 = ~U479 | ~U478;
  assign n203 = ~U481 | ~U480;
  assign n208 = ~U484 | ~U483;
  assign n217 = LOAD_REG | TX_END_REG | TRE_REG;
  assign n222 = ~U482 | ~U487;
  assign n230 = ~U490 | ~U489;
  assign n235 = U456 | U492;
  assign n240 = ~U495 | ~U494;
  assign n245 = ~U583 | ~U497;
  assign n250 = U456 | ADD_MPX2_REG;
  assign n254 = ~U459 | ~U499;
  assign n58 = CONTA_TMP_REG_3_ & U509;
  assign U409 = CONTA_TMP_REG_1_ & CONTA_TMP_REG_0_;
  assign U410 = ~SEND_EN_REG;
  assign U411 = SEND_EN_REG & GT_255_U6;
  assign U412 = ~NEXT_BIT_REG_0_;
  assign U413 = ~NEXT_BIT_REG_0_ | ~U411;
  assign U414 = ~NEXT_BIT_REG_3_ & ~NEXT_BIT_REG_2_;
  assign U415 = ~U410 & ~GT_255_U6;
  assign U416 = ~ITFC_STATE_REG_0_;
  assign U417 = ~LOAD_REG;
  assign U418 = ~TX_END_REG;
  assign U419 = ~ITFC_STATE_REG_1_;
  assign U420 = ~S1_REG_0_;
  assign U421 = ~S1_REG_1_;
  assign U422 = ~S1_REG_2_;
  assign U423 = RDY_REG & S1_REG_1_;
  assign U424 = U421 | S1_REG_0_;
  assign U425 = ~S2_REG_1_;
  assign U426 = ~CONFIRM_REG;
  assign U427 = ~MPX_REG;
  assign U428 = ~U426 & ~U425 & ~S2_REG_0_;
  assign U429 = ~S2_REG_0_;
  assign U430 = ~CONTA_TMP_REG_1_;
  assign U431 = ~U424 & ~U422;
  assign U432 = U431 & U501;
  assign U433 = ~U432 | ~U588;
  assign U434 = ~NEXT_BIT_REG_1_;
  assign U435 = NEXT_BIT_REG_2_ & NEXT_BIT_REG_1_;
  assign U436 = ~U434 & ~NEXT_BIT_REG_2_;
  assign U437 = NEXT_BIT_REG_2_ & U434;
  assign n212 = ~U548 | ~U547;
  assign n226 = ~U550 | ~U549;
  assign n259 = ~U555 | ~U554;
  assign n78 = ~U557 | ~U556;
  assign n83 = ~U559 | ~U558;
  assign n88 = ~U561 | ~U560;
  assign n93 = ~U563 | ~U562;
  assign n98 = ~U565 | ~U564;
  assign n103 = ~U567 | ~U566;
  assign n108 = ~U569 | ~U568;
  assign n113 = ~U571 | ~U570;
  assign n118 = ~U573 | ~U572;
  assign n133 = ~U579 | ~U578;
  assign n278 = ~U583 | ~U582;
  assign n268 = ~U585 | ~U584;
  assign U453 = ~U417 & ~TRE_REG;
  assign U454 = ~U432 | ~U409;
  assign U455 = ~U432 | ~CONTA_TMP_REG_0_ | ~U430;
  assign U456 = ~U491 & ~MPX_REG;
  assign U457 = ~U437;
  assign U458 = ~U414;
  assign U459 = ~SEND_DATA_REG | ~U429 | ~U425;
  assign U460 = ~U413;
  assign U461 = ~ADD_291_U22 | ~U415;
  assign U462 = ~TX_CONTA_REG_4_ | ~U410;
  assign U463 = ~ADD_291_U23 | ~U415;
  assign U464 = ~TX_CONTA_REG_3_ | ~U410;
  assign U465 = ~ADD_291_U24 | ~U415;
  assign U466 = ~TX_CONTA_REG_2_ | ~U410;
  assign U467 = ~ADD_291_U25 | ~U415;
  assign U468 = ~TX_CONTA_REG_1_ | ~U410;
  assign U469 = ~ADD_291_U6 | ~U415;
  assign U470 = ~TX_CONTA_REG_0_ | ~U410;
  assign U471 = U416 | ITFC_STATE_REG_1_;
  assign U472 = ~SHOT_REG | ~U416 | ~U419;
  assign U473 = ~LOAD_REG | ~U471;
  assign U474 = U418 | U416;
  assign U475 = ~ITFC_STATE_REG_1_ | ~U474;
  assign U476 = ~SHOT_REG | ~U416;
  assign U477 = ~U423 | ~S1_REG_0_;
  assign U478 = U422 | U421 | U420;
  assign U479 = ~SEND_DATA_REG | ~U477;
  assign U480 = ~TRE_REG | ~DSR | ~SEND_REG;
  assign U481 = U410 | TX_END_REG;
  assign U482 = S1_REG_1_ | EOC | U422 | U420;
  assign U483 = ~U420 | ~U422 | ~U421;
  assign U484 = ~MUX_EN_REG | ~U482;
  assign U485 = U419 | U418;
  assign U486 = ~U431;
  assign U487 = ~LOAD_DATO_REG | ~U486;
  assign U488 = SEND_REG | ITFC_STATE_REG_0_;
  assign U489 = ~U488 | ~U419;
  assign U490 = ~SEND_REG | ~ITFC_STATE_REG_0_;
  assign U491 = ~U428;
  assign U492 = ~U427 & ~U428;
  assign U493 = ~U416 | ~U419;
  assign U494 = U419 | U418 | U416;
  assign U495 = ~CONFIRM_REG | ~U493;
  assign U496 = U425 | S2_REG_0_ | CONFIRM_REG;
  assign U497 = ~SHOT_REG | ~U496;
  assign U498 = U491 | U427;
  assign U499 = ~RDY_REG | ~U498;
  assign U500 = ~TRE_REG | ~DSR;
  assign U501 = ~U409 | ~CONTA_TMP_REG_2_;
  assign U502 = ~CONTA_TMP_REG_3_ | ~U432;
  assign U503 = ~CANALE_REG_3_ | ~U486;
  assign U504 = ~U432 | ~CONTA_TMP_REG_2_;
  assign U505 = ~CANALE_REG_2_ | ~U486;
  assign U506 = U433 | U430;
  assign U507 = ~CANALE_REG_1_ | ~U486;
  assign U508 = ~CANALE_REG_0_ | ~U486;
  assign U509 = ~U431 | ~U409 | ~CONTA_TMP_REG_2_;
  assign U510 = ~U431 | ~U409;
  assign U511 = ~U510 | ~CONTA_TMP_REG_2_;
  assign U512 = U486 | U588;
  assign U513 = ~CONTA_TMP_REG_1_ | ~U512;
  assign U514 = U588 | U431;
  assign U515 = ~U453;
  assign U516 = ~U411 | ~U577 | ~U576;
  assign U517 = U434 | U460;
  assign U518 = NEXT_BIT_REG_1_ | NEXT_BIT_REG_2_;
  assign U519 = U518 & U412;
  assign U520 = NEXT_BIT_REG_3_ | U519;
  assign U521 = ~ADD_291_U17 | ~U415;
  assign U522 = ~TX_CONTA_REG_9_ | ~U410;
  assign U523 = ~ADD_291_U18 | ~U415;
  assign U524 = ~TX_CONTA_REG_8_ | ~U410;
  assign U525 = ~ADD_291_U19 | ~U415;
  assign U526 = ~TX_CONTA_REG_7_ | ~U410;
  assign U527 = ~ADD_291_U20 | ~U415;
  assign U528 = ~TX_CONTA_REG_6_ | ~U410;
  assign U529 = ~ADD_291_U21 | ~U415;
  assign U530 = ~TX_CONTA_REG_5_ | ~U410;
  assign U531 = ~U436 | ~OUT_REG_REG_6_;
  assign U532 = ~U435 | ~OUT_REG_REG_2_;
  assign U533 = U458 | NEXT_BIT_REG_1_;
  assign U534 = ~U437 | ~OUT_REG_REG_4_;
  assign U535 = ~NEXT_BIT_REG_3_ | ~OUT_REG_REG_0_;
  assign U536 = ~U534 | ~U535 | ~U533 | ~U532 | ~U531;
  assign U537 = ~U436 | ~OUT_REG_REG_7_;
  assign U538 = ~U435 | ~OUT_REG_REG_3_;
  assign U539 = ~U437 | ~OUT_REG_REG_5_;
  assign U540 = ~NEXT_BIT_REG_3_ | ~OUT_REG_REG_1_;
  assign U541 = ~U537 | ~U538 | ~U540 | ~U539;
  assign U542 = U426 | MPX_REG;
  assign U543 = U422 | U420;
  assign U544 = U423 | S1_REG_2_;
  assign U545 = S1_REG_1_ | EOC;
  assign U546 = ~U545 | ~U544;
  assign U547 = U419 | ITFC_STATE_REG_0_;
  assign U548 = ~ITFC_STATE_REG_0_ | ~U485;
  assign U549 = U424 | S1_REG_2_;
  assign U550 = ~SOC_REG | ~U424;
  assign U551 = ~ERROR_REG | ~U417;
  assign U552 = ~TRE_REG | ~LOAD_REG;
  assign U553 = U552 & U551;
  assign U554 = ~SEND_REG | ~U500;
  assign U555 = U553 | SEND_REG;
  assign U556 = ~OUT_REG_REG_7_ | ~U515;
  assign U557 = ~DATA_IN_7_ | ~U453;
  assign U558 = ~OUT_REG_REG_6_ | ~U515;
  assign U559 = ~DATA_IN_6_ | ~U453;
  assign U560 = ~OUT_REG_REG_5_ | ~U515;
  assign U561 = ~DATA_IN_5_ | ~U453;
  assign U562 = ~OUT_REG_REG_4_ | ~U515;
  assign U563 = ~DATA_IN_4_ | ~U453;
  assign U564 = ~OUT_REG_REG_3_ | ~U515;
  assign U565 = ~DATA_IN_3_ | ~U453;
  assign U566 = ~OUT_REG_REG_2_ | ~U515;
  assign U567 = ~DATA_IN_2_ | ~U453;
  assign U568 = ~OUT_REG_REG_1_ | ~U515;
  assign U569 = ~DATA_IN_1_ | ~U453;
  assign U570 = ~OUT_REG_REG_0_ | ~U515;
  assign U571 = ~DATA_IN_0_ | ~U453;
  assign U572 = ~NEXT_BIT_REG_3_ | ~U413;
  assign U573 = ~U435 | ~U460;
  assign U574 = ~NEXT_BIT_REG_2_ | ~U413;
  assign U575 = ~U436 | ~U460;
  assign U576 = U412 | U437;
  assign U577 = U414 | NEXT_BIT_REG_0_;
  assign U578 = U412 | U411;
  assign U579 = ~U411 | ~U520;
  assign U580 = ~NEXT_BIT_REG_0_ | ~U536;
  assign U581 = ~U541 | ~U412;
  assign U582 = ~U429 | ~S2_REG_1_ | ~U542;
  assign U583 = U429 | S2_REG_1_;
  assign U584 = U422 | S1_REG_0_;
  assign U585 = ~S1_REG_0_ | ~U546;
  assign U586 = ~U421 | ~EOC | ~S1_REG_2_;
  assign U587 = U421 | S1_REG_2_ | RDY_REG;
  assign U588 = ~CONTA_TMP_REG_0_;
  assign GT_255_U8 = TX_CONTA_REG_6_ & TX_CONTA_REG_5_ & GT_255_U10;
  assign ADD_291_U6 = ~TX_CONTA_REG_0_;
  assign ADD_291_U7 = ~TX_CONTA_REG_2_;
  assign ADD_291_U8 = ~TX_CONTA_REG_4_;
  assign ADD_291_U9 = TX_CONTA_REG_1_ & TX_CONTA_REG_2_ & TX_CONTA_REG_0_;
  assign ADD_291_U10 = ~TX_CONTA_REG_3_;
  assign ADD_291_U11 = TX_CONTA_REG_3_ & ADD_291_U9 & TX_CONTA_REG_4_;
  assign ADD_291_U12 = ~TX_CONTA_REG_5_;
  assign ADD_291_U13 = TX_CONTA_REG_5_ & ADD_291_U11;
  assign ADD_291_U14 = ~TX_CONTA_REG_6_;
  assign ADD_291_U15 = ~TX_CONTA_REG_7_;
  assign ADD_291_U16 = ~TX_CONTA_REG_7_ | ~TX_CONTA_REG_6_ | ~ADD_291_U13;
  assign ADD_291_U17 = ~ADD_291_U31 | ~ADD_291_U30;
  assign ADD_291_U18 = ~ADD_291_U33 | ~ADD_291_U32;
  assign ADD_291_U19 = ~ADD_291_U35 | ~ADD_291_U34;
  assign ADD_291_U20 = ~ADD_291_U37 | ~ADD_291_U36;
  assign ADD_291_U21 = ~ADD_291_U39 | ~ADD_291_U38;
  assign ADD_291_U22 = ~ADD_291_U41 | ~ADD_291_U40;
  assign ADD_291_U23 = ~ADD_291_U43 | ~ADD_291_U42;
  assign ADD_291_U24 = ~ADD_291_U45 | ~ADD_291_U44;
  assign ADD_291_U25 = ~ADD_291_U47 | ~ADD_291_U46;
  assign ADD_291_U26 = ADD_291_U16 | ADD_291_U48;
  assign ADD_291_U27 = TX_CONTA_REG_6_ & ADD_291_U13;
  assign ADD_291_U28 = TX_CONTA_REG_3_ & ADD_291_U9;
  assign ADD_291_U29 = TX_CONTA_REG_1_ & TX_CONTA_REG_0_;
  assign ADD_291_U30 = ~TX_CONTA_REG_9_ | ~ADD_291_U26;
  assign ADD_291_U31 = ADD_291_U26 | TX_CONTA_REG_9_;
  assign ADD_291_U32 = ADD_291_U16 | TX_CONTA_REG_8_;
  assign ADD_291_U33 = ~TX_CONTA_REG_8_ | ~ADD_291_U16;
  assign ADD_291_U34 = ADD_291_U15 | ADD_291_U27;
  assign ADD_291_U35 = ~ADD_291_U27 | ~ADD_291_U15;
  assign ADD_291_U36 = ADD_291_U14 | ADD_291_U13;
  assign ADD_291_U37 = ~ADD_291_U13 | ~ADD_291_U14;
  assign ADD_291_U38 = ADD_291_U12 | ADD_291_U11;
  assign ADD_291_U39 = ~ADD_291_U11 | ~ADD_291_U12;
  assign ADD_291_U40 = ADD_291_U8 | ADD_291_U28;
  assign ADD_291_U41 = ~ADD_291_U28 | ~ADD_291_U8;
  assign ADD_291_U42 = ADD_291_U10 | ADD_291_U9;
  assign ADD_291_U43 = ~ADD_291_U9 | ~ADD_291_U10;
  assign ADD_291_U44 = ADD_291_U7 | ADD_291_U29;
  assign ADD_291_U45 = ~ADD_291_U29 | ~ADD_291_U7;
  assign ADD_291_U46 = ADD_291_U6 | TX_CONTA_REG_1_;
  assign ADD_291_U47 = ~TX_CONTA_REG_1_ | ~ADD_291_U6;
  assign ADD_291_U48 = ~TX_CONTA_REG_8_;
  assign GT_255_U6 = TX_CONTA_REG_9_ | TX_CONTA_REG_7_ | TX_CONTA_REG_8_ | GT_255_U8;
  assign GT_255_U7 = TX_CONTA_REG_3_ & GT_255_U9;
  always @ (posedge clock) begin
    CANALE_REG_3_ <= n42;
    CANALE_REG_2_ <= n46;
    CANALE_REG_1_ <= n50;
    CANALE_REG_0_ <= n54;
    CONTA_TMP_REG_3_ <= n58;
    CONTA_TMP_REG_2_ <= n63;
    CONTA_TMP_REG_1_ <= n68;
    CONTA_TMP_REG_0_ <= n73;
    OUT_REG_REG_7_ <= n78;
    OUT_REG_REG_6_ <= n83;
    OUT_REG_REG_5_ <= n88;
    OUT_REG_REG_4_ <= n93;
    OUT_REG_REG_3_ <= n98;
    OUT_REG_REG_2_ <= n103;
    OUT_REG_REG_1_ <= n108;
    OUT_REG_REG_0_ <= n113;
    NEXT_BIT_REG_3_ <= n118;
    NEXT_BIT_REG_2_ <= n123;
    NEXT_BIT_REG_1_ <= n128;
    NEXT_BIT_REG_0_ <= n133;
    TX_CONTA_REG_9_ <= n138;
    TX_CONTA_REG_8_ <= n143;
    TX_CONTA_REG_7_ <= n148;
    TX_CONTA_REG_6_ <= n153;
    TX_CONTA_REG_5_ <= n158;
    TX_CONTA_REG_4_ <= n163;
    TX_CONTA_REG_3_ <= n168;
    TX_CONTA_REG_2_ <= n173;
    TX_CONTA_REG_1_ <= n178;
    TX_CONTA_REG_0_ <= n183;
    LOAD_REG <= n188;
    ITFC_STATE_REG_0_ <= n193;
    SEND_DATA_REG <= n198;
    SEND_EN_REG <= n203;
    MUX_EN_REG <= n208;
    ITFC_STATE_REG_1_ <= n212;
    TRE_REG <= n217;
    LOAD_DATO_REG <= n222;
    SOC_REG <= n226;
    SEND_REG <= n230;
    MPX_REG <= n235;
    CONFIRM_REG <= n240;
    SHOT_REG <= n245;
    ADD_MPX2_REG <= n250;
    RDY_REG <= n254;
    ERROR_REG <= n259;
    S1_REG_2_ <= n263;
    S1_REG_1_ <= n268;
    S1_REG_0_ <= n273;
    S2_REG_1_ <= n278;
    S2_REG_0_ <= n283;
    TX_END_REG <= n288;
    DATA_OUT_REG <= n293;
  end
endmodule


