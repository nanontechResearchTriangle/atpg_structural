// Benchmark "b07s" written by ABC on Mon Apr  8 17:53:37 2019

module b07s ( clock, 
    START,
    PUNTI_RETTA_REG_7_, PUNTI_RETTA_REG_6_, PUNTI_RETTA_REG_5_,
    PUNTI_RETTA_REG_4_, PUNTI_RETTA_REG_3_, PUNTI_RETTA_REG_2_,
    PUNTI_RETTA_REG_1_, PUNTI_RETTA_REG_0_  );
  input  clock;
  input  START;
  output PUNTI_RETTA_REG_7_, PUNTI_RETTA_REG_6_, PUNTI_RETTA_REG_5_,
    PUNTI_RETTA_REG_4_, PUNTI_RETTA_REG_3_, PUNTI_RETTA_REG_2_,
    PUNTI_RETTA_REG_1_, PUNTI_RETTA_REG_0_;
  reg PUNTI_RETTA_REG_7_, PUNTI_RETTA_REG_6_, PUNTI_RETTA_REG_5_,
    PUNTI_RETTA_REG_4_, PUNTI_RETTA_REG_3_, PUNTI_RETTA_REG_2_,
    PUNTI_RETTA_REG_1_, PUNTI_RETTA_REG_0_, CONT_REG_7_, CONT_REG_6_,
    CONT_REG_5_, CONT_REG_4_, CONT_REG_3_, CONT_REG_2_, CONT_REG_1_,
    CONT_REG_0_, MAR_REG_7_, MAR_REG_6_, MAR_REG_5_, MAR_REG_4_,
    MAR_REG_3_, MAR_REG_2_, MAR_REG_1_, MAR_REG_0_, X_REG_7_, X_REG_6_,
    X_REG_5_, X_REG_4_, X_REG_3_, X_REG_2_, X_REG_1_, X_REG_0_, Y_REG_3_,
    Y_REG_1_, Y_REG_5_, T_REG_3_, T_REG_5_, T_REG_1_, T_REG_0_, T_REG_4_,
    T_REG_6_, T_REG_2_, Y_REG_4_, Y_REG_0_, Y_REG_2_, Y_REG_6_,
    STATO_REG_2_, STATO_REG_1_, STATO_REG_0_;
  wire U296, U297, U298, U299, U300, U301, U302, U303, U304, U305, U306,
    U307, U308, U309, U310, U346, U347, U348, U349, U350, U351, U352, U353,
    U354, U355, U356, U357, U358, U359, U360, U361, U362, U363, U364, U365,
    U366, U367, U368, U369, U370, U371, U372, U373, U374, U375, U376, U377,
    U378, U393, U394, U395, U396, U397, U398, U399, U400, U401, U402, U403,
    U404, U405, U406, U407, U408, U409, U410, U411, U412, U413, U414, U415,
    U416, U417, U418, U419, U420, U421, U422, U423, U424, U425, U426, U427,
    U428, U429, U430, U431, U432, U433, U434, U435, U436, U437, U438, U439,
    U440, U441, U442, U443, U444, U445, U446, U447, U448, U449, U450, U451,
    U452, U453, U454, U455, U456, U457, U458, U459, U460, U461, U462, U463,
    U464, U465, U466, U467, U468, U469, U470, U471, U472, U473, U474, U475,
    U476, U477, U478, U479, U480, U481, U482, U483, U484, U485, U486, U487,
    U488, U489, U490, U491, U492, U493, U494, U495, U496, U497, U498, U499,
    U500, U501, U502, U503, U504, U505, U506, U507, U508, U509, U510, U511,
    U512, U513, U514, U515, U516, U517, U518, U519, U520, U521, U522, U523,
    U524, U525, U526, U527, U528, U529, U530, U531, U532, U533, U534, U535,
    U536, U537, U538, U539, U540, U541, U542, U543, U544, U545, U546, U547,
    U548, U549, U550, U551, U552, U553, U554, U555, U556, U557, U558, U559,
    U560, U561, U562, U563, U564, U565, U566, U567, U568, U569, U570, U571,
    U572, U573, R81_U88, R81_U5, R81_U6, R81_U7, R81_U8, R81_U9, R81_U10,
    R81_U11, R81_U12, R81_U13, R81_U14, R81_U15, R81_U16, R81_U17, R81_U18,
    R81_U19, R81_U20, R81_U21, R81_U22, R81_U23, R81_U24, R81_U25, R81_U26,
    R81_U27, R81_U28, R81_U29, R81_U30, R81_U31, R81_U32, R81_U33, R81_U34,
    R81_U35, R81_U36, R81_U37, R81_U38, R81_U39, R81_U40, R81_U41, R81_U42,
    R81_U43, R81_U44, R81_U45, R81_U46, R81_U47, R81_U48, R81_U49, R81_U50,
    R81_U51, R81_U52, R81_U53, R81_U54, R81_U55, R81_U56, R81_U57, R81_U58,
    R81_U59, R81_U60, R81_U61, R81_U62, R81_U63, R81_U64, R81_U65, R81_U66,
    R81_U67, R81_U68, R81_U69, R81_U70, R81_U71, R81_U72, R81_U73, R81_U74,
    R81_U75, R81_U76, R81_U77, R81_U78, R81_U79, R81_U80, R81_U81, R81_U82,
    R81_U83, R81_U84, R81_U85, R81_U86, R81_U87, n20, n24, n28, n32, n36,
    n40, n44, n48, n52, n57, n62, n67, n72, n77, n82, n87, n92, n97, n102,
    n107, n112, n117, n122, n127, n132, n137, n142, n147, n152, n157, n162,
    n167, n172, n177, n182, n187, n192, n197, n202, n207, n212, n217, n222,
    n227, n232, n237, n242, n247, n252;
  assign U296 = CONT_REG_7_ & U521;
  assign U297 = ~U524 | ~U523;
  assign U298 = ~U526 | ~U525;
  assign U299 = ~U528 | ~U527;
  assign U300 = ~U530 | ~U529;
  assign U301 = ~U532 | ~U531;
  assign U302 = ~U534 | ~U533;
  assign U303 = ~U536 | ~U535;
  assign U304 = ~U502 | ~U501 | ~U500;
  assign U305 = ~U505 | ~U504 | ~U503;
  assign U306 = ~U508 | ~U507 | ~U506;
  assign U307 = ~U511 | ~U510 | ~U509;
  assign U308 = ~U514 | ~U513 | ~U512;
  assign U309 = ~U517 | ~U516 | ~U515;
  assign U310 = ~U358 | ~U518 | ~U520 | ~U519;
  assign n242 = ~U403 | ~U407;
  assign n247 = ~U399 | ~U538 | ~U537;
  assign n252 = ~U413 | ~U571 | ~U397;
  assign n20 = ~U439 | ~U438 | ~U437;
  assign n24 = ~U442 | ~U441 | ~U440;
  assign n28 = ~U445 | ~U444 | ~U443;
  assign n32 = ~U448 | ~U447 | ~U446;
  assign n36 = ~U451 | ~U450 | ~U449;
  assign n40 = ~U454 | ~U453 | ~U452;
  assign n44 = ~U457 | ~U456 | ~U455;
  assign n48 = ~U460 | ~U459 | ~U458;
  assign n52 = ~U464 | ~U463;
  assign n57 = ~U466 | ~U465;
  assign n62 = ~U468 | ~U467;
  assign n67 = ~U470 | ~U469;
  assign n72 = ~U472 | ~U471;
  assign n77 = ~U474 | ~U473;
  assign n82 = ~U476 | ~U475;
  assign n87 = ~U478 | ~U477;
  assign n92 = MAR_REG_7_ & U480;
  assign n97 = MAR_REG_6_ & U480;
  assign n102 = MAR_REG_5_ & U480;
  assign n107 = MAR_REG_4_ & U480;
  assign n132 = ~U488 | ~U487 | ~U401;
  assign n137 = ~U490 | ~U489 | ~U401;
  assign n142 = ~U492 | ~U491 | ~U401;
  assign n147 = ~U494 | ~U493 | ~U401;
  assign n152 = ~U496 | ~U495 | ~U401;
  assign n157 = ~U499 | ~U498 | ~U497;
  assign n162 = ~U401 | ~U419 | ~U421 | ~U420;
  assign n167 = ~U427 | ~U426 | ~U425;
  assign n172 = ~U402 | ~U429;
  assign n182 = ~U402 | ~U432;
  assign n222 = ~U402 | ~U433;
  assign n237 = ~U402 | ~U434;
  assign U346 = ~MAR_REG_1_;
  assign U347 = ~MAR_REG_0_;
  assign U348 = MAR_REG_0_ & MAR_REG_1_;
  assign U349 = ~MAR_REG_2_;
  assign U350 = MAR_REG_2_ & U348;
  assign U351 = ~MAR_REG_3_;
  assign U352 = U393 | U351 | U572 | MAR_REG_4_;
  assign U353 = ~START;
  assign U354 = ~STATO_REG_1_;
  assign U355 = ~STATO_REG_2_;
  assign U356 = ~STATO_REG_0_;
  assign U357 = ~U356 & ~U353;
  assign U358 = ~STATO_REG_2_ | ~STATO_REG_1_;
  assign U359 = ~MAR_REG_0_ | ~U346 | ~U349;
  assign U360 = ~U378 & ~STATO_REG_2_;
  assign U361 = ~U378 & ~U395;
  assign U362 = ~U400 | ~U414;
  assign U363 = U415 & U422;
  assign U364 = U354 & U356;
  assign U365 = U364 & STATO_REG_2_;
  assign U366 = U354 & U355;
  assign U367 = ~U436 | ~U353;
  assign U368 = U394 | X_REG_3_ | X_REG_2_ | U569 | X_REG_0_;
  assign U369 = ~U367 & ~U355;
  assign U370 = U369 & U573;
  assign U371 = U369 & U368;
  assign U372 = U358 | U404;
  assign U373 = ~U355 & ~U374;
  assign U374 = U462 & U461;
  assign U375 = ~U479 | ~U403 | ~U372;
  assign U376 = ~U480 & ~U366;
  assign U377 = STATO_REG_0_ & STATO_REG_2_;
  assign U378 = U540 & U539;
  assign n177 = ~U542 | ~U541;
  assign n187 = ~U544 | ~U543;
  assign n192 = ~U546 | ~U545;
  assign n197 = ~U548 | ~U547;
  assign n202 = ~U550 | ~U549;
  assign n207 = ~U552 | ~U551;
  assign n212 = ~U554 | ~U553;
  assign n217 = ~U556 | ~U555;
  assign n227 = ~U558 | ~U557;
  assign n232 = ~U560 | ~U559;
  assign n112 = ~U562 | ~U561;
  assign n117 = ~U564 | ~U563;
  assign n122 = ~U566 | ~U565;
  assign n127 = ~U568 | ~U567;
  assign U393 = MAR_REG_6_ | MAR_REG_5_ | MAR_REG_7_;
  assign U394 = X_REG_4_ | X_REG_5_ | X_REG_7_ | X_REG_6_;
  assign U395 = ~STATO_REG_0_ & ~STATO_REG_2_;
  assign U396 = STATO_REG_0_ & STATO_REG_1_;
  assign U397 = ~U364;
  assign U398 = U358 | U352;
  assign U399 = ~U377;
  assign U400 = ~U349 | ~U347 | ~MAR_REG_3_ | ~MAR_REG_1_;
  assign U401 = U570 | U415;
  assign U402 = ~U365 | ~U362;
  assign U403 = ~U396;
  assign U404 = ~U352;
  assign U405 = ~START | ~U404;
  assign U406 = ~STATO_REG_1_ | ~U405;
  assign U407 = ~STATO_REG_2_ | ~U406;
  assign U408 = ~U404 | ~U353;
  assign U409 = ~U408 | ~U356;
  assign U410 = ~U571 | ~U409;
  assign U411 = ~U366;
  assign U412 = ~U398 | ~U411;
  assign U413 = ~U412 | ~U353;
  assign U414 = U359 | MAR_REG_3_;
  assign U415 = ~U362;
  assign U416 = ~MAR_REG_1_ | ~U351;
  assign U417 = ~MAR_REG_2_ | ~U416;
  assign U418 = ~MAR_REG_1_ | ~U417;
  assign U419 = ~U360 | ~MAR_REG_0_ | ~U418;
  assign U420 = ~R81_U6 | ~U361;
  assign U421 = ~X_REG_1_ | ~U378;
  assign U422 = ~MAR_REG_3_ | ~U348 | ~U349;
  assign U423 = MAR_REG_1_ | MAR_REG_3_ | MAR_REG_2_;
  assign U424 = ~U363 | ~U423;
  assign U425 = ~U360 | ~U424;
  assign U426 = ~R81_U22 | ~U361;
  assign U427 = ~X_REG_0_ | ~U378;
  assign U428 = ~U365;
  assign U429 = ~Y_REG_3_ | ~U428;
  assign U430 = ~MAR_REG_2_ | ~MAR_REG_0_ | ~U416;
  assign U431 = ~U430 | ~U359 | ~U400;
  assign U432 = ~Y_REG_5_ | ~U428;
  assign U433 = ~Y_REG_4_ | ~U428;
  assign U434 = ~Y_REG_6_ | ~U428;
  assign U435 = ~U366 | ~STATO_REG_0_;
  assign U436 = ~U398 | ~U435;
  assign U437 = ~R81_U23 | ~U370;
  assign U438 = ~CONT_REG_7_ | ~U371;
  assign U439 = ~PUNTI_RETTA_REG_7_ | ~U367;
  assign U440 = ~U370 | ~R81_U24;
  assign U441 = ~CONT_REG_6_ | ~U371;
  assign U442 = ~PUNTI_RETTA_REG_6_ | ~U367;
  assign U443 = ~U370 | ~R81_U25;
  assign U444 = ~CONT_REG_5_ | ~U371;
  assign U445 = ~PUNTI_RETTA_REG_5_ | ~U367;
  assign U446 = ~U370 | ~R81_U26;
  assign U447 = ~CONT_REG_4_ | ~U371;
  assign U448 = ~PUNTI_RETTA_REG_4_ | ~U367;
  assign U449 = ~U370 | ~R81_U5;
  assign U450 = ~CONT_REG_3_ | ~U371;
  assign U451 = ~PUNTI_RETTA_REG_3_ | ~U367;
  assign U452 = ~U370 | ~R81_U27;
  assign U453 = ~CONT_REG_2_ | ~U371;
  assign U454 = ~PUNTI_RETTA_REG_2_ | ~U367;
  assign U455 = ~U370 | ~R81_U6;
  assign U456 = ~CONT_REG_1_ | ~U371;
  assign U457 = ~PUNTI_RETTA_REG_1_ | ~U367;
  assign U458 = ~U370 | ~R81_U22;
  assign U459 = ~CONT_REG_0_ | ~U371;
  assign U460 = ~PUNTI_RETTA_REG_0_ | ~U367;
  assign U461 = ~U366 | ~U357;
  assign U462 = U372 | U368;
  assign U463 = ~U373 | ~R81_U23;
  assign U464 = ~U374 | ~CONT_REG_7_;
  assign U465 = ~U373 | ~R81_U24;
  assign U466 = ~U374 | ~CONT_REG_6_;
  assign U467 = ~U373 | ~R81_U25;
  assign U468 = ~U374 | ~CONT_REG_5_;
  assign U469 = ~U373 | ~R81_U26;
  assign U470 = ~U374 | ~CONT_REG_4_;
  assign U471 = ~U373 | ~R81_U5;
  assign U472 = ~U374 | ~CONT_REG_3_;
  assign U473 = ~U373 | ~R81_U27;
  assign U474 = ~U374 | ~CONT_REG_2_;
  assign U475 = ~U373 | ~R81_U6;
  assign U476 = ~U374 | ~CONT_REG_1_;
  assign U477 = ~U373 | ~R81_U22;
  assign U478 = ~U374 | ~CONT_REG_0_;
  assign U479 = ~U357 | ~U355;
  assign U480 = ~U375;
  assign U481 = ~U411 | ~U572;
  assign U482 = ~U375 | ~U481;
  assign U483 = U366 | U348;
  assign U484 = ~U375 | ~U483;
  assign U485 = ~U411 | ~U347;
  assign U486 = ~U375 | ~U485;
  assign U487 = ~R81_U23 | ~U361;
  assign U488 = ~X_REG_7_ | ~U378;
  assign U489 = ~R81_U24 | ~U361;
  assign U490 = ~X_REG_6_ | ~U378;
  assign U491 = ~R81_U25 | ~U361;
  assign U492 = ~X_REG_5_ | ~U378;
  assign U493 = ~R81_U26 | ~U361;
  assign U494 = ~X_REG_4_ | ~U378;
  assign U495 = ~R81_U5 | ~U361;
  assign U496 = ~X_REG_3_ | ~U378;
  assign U497 = U570 | U363;
  assign U498 = ~R81_U27 | ~U361;
  assign U499 = ~X_REG_2_ | ~U378;
  assign U500 = ~U377 | ~Y_REG_6_;
  assign U501 = ~T_REG_6_ | ~U364;
  assign U502 = ~X_REG_6_ | ~U355;
  assign U503 = ~U377 | ~Y_REG_5_;
  assign U504 = ~T_REG_5_ | ~U364;
  assign U505 = ~X_REG_5_ | ~U355;
  assign U506 = ~U377 | ~Y_REG_4_;
  assign U507 = ~T_REG_4_ | ~U364;
  assign U508 = ~X_REG_4_ | ~U355;
  assign U509 = ~U377 | ~Y_REG_3_;
  assign U510 = ~T_REG_3_ | ~U364;
  assign U511 = ~X_REG_3_ | ~U355;
  assign U512 = ~U377 | ~Y_REG_2_;
  assign U513 = ~T_REG_2_ | ~U364;
  assign U514 = ~X_REG_2_ | ~U355;
  assign U515 = ~U377 | ~Y_REG_1_;
  assign U516 = ~T_REG_1_ | ~U364;
  assign U517 = ~X_REG_1_ | ~U355;
  assign U518 = ~U377 | ~Y_REG_0_;
  assign U519 = ~T_REG_0_ | ~U364;
  assign U520 = ~X_REG_0_ | ~U355;
  assign U521 = ~U398 | ~U372;
  assign U522 = ~STATO_REG_2_ | ~U399 | ~U397;
  assign U523 = ~CONT_REG_6_ | ~U521;
  assign U524 = ~X_REG_6_ | ~U522;
  assign U525 = ~CONT_REG_5_ | ~U521;
  assign U526 = ~X_REG_5_ | ~U522;
  assign U527 = ~CONT_REG_4_ | ~U521;
  assign U528 = ~X_REG_4_ | ~U522;
  assign U529 = ~CONT_REG_3_ | ~U521;
  assign U530 = ~X_REG_3_ | ~U522;
  assign U531 = ~CONT_REG_2_ | ~U521;
  assign U532 = ~X_REG_2_ | ~U522;
  assign U533 = ~CONT_REG_1_ | ~U521;
  assign U534 = ~X_REG_1_ | ~U522;
  assign U535 = ~CONT_REG_0_ | ~U521;
  assign U536 = ~X_REG_0_ | ~U522;
  assign U537 = ~U357 | ~U354;
  assign U538 = ~STATO_REG_1_ | ~U410;
  assign U539 = ~STATO_REG_2_ | ~U354;
  assign U540 = ~U395 | ~STATO_REG_1_;
  assign U541 = ~Y_REG_1_ | ~U428;
  assign U542 = ~U365 | ~U431;
  assign U543 = ~T_REG_3_ | ~U403;
  assign U544 = ~U396 | ~R81_U5;
  assign U545 = ~T_REG_5_ | ~U403;
  assign U546 = ~U396 | ~R81_U25;
  assign U547 = ~T_REG_1_ | ~U403;
  assign U548 = ~U396 | ~R81_U6;
  assign U549 = ~T_REG_0_ | ~U403;
  assign U550 = ~U396 | ~R81_U22;
  assign U551 = ~T_REG_4_ | ~U403;
  assign U552 = ~U396 | ~R81_U26;
  assign U553 = ~T_REG_6_ | ~U403;
  assign U554 = ~U396 | ~R81_U24;
  assign U555 = ~T_REG_2_ | ~U403;
  assign U556 = ~U396 | ~R81_U27;
  assign U557 = ~U365 | ~U424;
  assign U558 = ~Y_REG_0_ | ~U428;
  assign U559 = U428 | U363;
  assign U560 = ~Y_REG_2_ | ~U428;
  assign U561 = ~MAR_REG_3_ | ~U482;
  assign U562 = ~U351 | ~U376 | ~U350;
  assign U563 = ~MAR_REG_2_ | ~U484;
  assign U564 = ~U349 | ~U376 | ~U348;
  assign U565 = ~U486 | ~MAR_REG_1_;
  assign U566 = ~U346 | ~U376 | ~MAR_REG_0_;
  assign U567 = ~U376 | ~U347;
  assign U568 = ~U480 | ~MAR_REG_0_;
  assign U569 = ~X_REG_1_;
  assign U570 = ~U360;
  assign U571 = ~U395;
  assign U572 = ~U350;
  assign U573 = ~U368;
  assign R81_U88 = ~R81_U33;
  assign R81_U5 = R81_U55 & R81_U54;
  assign R81_U6 = ~R81_U58 | ~R81_U85 | ~R81_U84;
  assign R81_U7 = ~U303;
  assign R81_U8 = ~U303 | ~U310;
  assign R81_U9 = ~U309;
  assign R81_U10 = ~U301;
  assign R81_U11 = ~U308;
  assign R81_U12 = ~U306;
  assign R81_U13 = ~U299;
  assign R81_U14 = ~R81_U21 & ~R81_U88;
  assign R81_U15 = ~U305;
  assign R81_U16 = ~U298;
  assign R81_U17 = R81_U46 & R81_U45;
  assign R81_U18 = ~U304;
  assign R81_U19 = ~U297;
  assign R81_U20 = R81_U38 & R81_U37;
  assign R81_U21 = ~R81_U20 & ~R81_U39;
  assign R81_U22 = ~R81_U87 | ~R81_U86;
  assign R81_U23 = ~R81_U60 | ~R81_U59;
  assign R81_U24 = ~R81_U65 | ~R81_U64;
  assign R81_U25 = ~R81_U70 | ~R81_U69;
  assign R81_U26 = ~R81_U75 | ~R81_U74;
  assign R81_U27 = ~R81_U80 | ~R81_U79;
  assign R81_U28 = R81_U42 & R81_U41;
  assign R81_U29 = R81_U30 | R81_U35;
  assign R81_U30 = ~R81_U9 & ~R81_U8;
  assign R81_U31 = ~R81_U14;
  assign R81_U32 = ~R81_U8;
  assign R81_U33 = ~U307 | ~U300;
  assign R81_U34 = U309 | R81_U32;
  assign R81_U35 = U302 & R81_U34;
  assign R81_U36 = ~R81_U10 | ~R81_U11;
  assign R81_U37 = ~R81_U36 | ~R81_U29;
  assign R81_U38 = R81_U11 | R81_U10;
  assign R81_U39 = ~U307 & ~U300;
  assign R81_U40 = R81_U13 | R81_U12;
  assign R81_U41 = ~R81_U14 | ~R81_U40;
  assign R81_U42 = ~R81_U13 | ~R81_U12;
  assign R81_U43 = ~R81_U28;
  assign R81_U44 = R81_U16 | R81_U15;
  assign R81_U45 = ~R81_U44 | ~R81_U43;
  assign R81_U46 = ~R81_U16 | ~R81_U15;
  assign R81_U47 = ~R81_U17;
  assign R81_U48 = R81_U19 | R81_U18;
  assign R81_U49 = ~R81_U48 | ~R81_U47;
  assign R81_U50 = ~R81_U19 | ~R81_U18;
  assign R81_U51 = ~R81_U17 | ~R81_U50;
  assign R81_U52 = U300 | U307;
  assign R81_U53 = ~R81_U52 | ~R81_U33;
  assign R81_U54 = ~R81_U20 | ~R81_U53;
  assign R81_U55 = ~R81_U21 | ~R81_U33;
  assign R81_U56 = ~R81_U50 | ~R81_U49;
  assign R81_U57 = R81_U48 & R81_U51;
  assign R81_U58 = ~R81_U83 | ~R81_U9;
  assign R81_U59 = ~U296 | ~R81_U56;
  assign R81_U60 = R81_U57 | U296;
  assign R81_U61 = R81_U19 | U304;
  assign R81_U62 = R81_U18 | U297;
  assign R81_U63 = ~R81_U62 | ~R81_U61;
  assign R81_U64 = ~R81_U63 | ~R81_U47;
  assign R81_U65 = ~R81_U17 | ~R81_U62 | ~R81_U61;
  assign R81_U66 = R81_U16 | U305;
  assign R81_U67 = R81_U15 | U298;
  assign R81_U68 = ~R81_U67 | ~R81_U66;
  assign R81_U69 = ~R81_U68 | ~R81_U43;
  assign R81_U70 = ~R81_U28 | ~R81_U67 | ~R81_U66;
  assign R81_U71 = R81_U13 | U306;
  assign R81_U72 = R81_U12 | U299;
  assign R81_U73 = ~R81_U72 | ~R81_U71;
  assign R81_U74 = ~R81_U31 | ~R81_U72 | ~R81_U71;
  assign R81_U75 = ~R81_U73 | ~R81_U14;
  assign R81_U76 = R81_U11 | U301;
  assign R81_U77 = R81_U10 | U308;
  assign R81_U78 = R81_U77 & R81_U76;
  assign R81_U79 = ~R81_U29 | ~R81_U77 | ~R81_U76;
  assign R81_U80 = R81_U29 | R81_U78;
  assign R81_U81 = ~U302 | ~R81_U8;
  assign R81_U82 = R81_U8 | U302;
  assign R81_U83 = ~R81_U82 | ~R81_U81;
  assign R81_U84 = U302 | R81_U9 | R81_U32;
  assign R81_U85 = ~R81_U30 | ~U302;
  assign R81_U86 = R81_U7 | U310;
  assign R81_U87 = ~U310 | ~R81_U7;
  always @ (posedge clock) begin
    PUNTI_RETTA_REG_7_ <= n20;
    PUNTI_RETTA_REG_6_ <= n24;
    PUNTI_RETTA_REG_5_ <= n28;
    PUNTI_RETTA_REG_4_ <= n32;
    PUNTI_RETTA_REG_3_ <= n36;
    PUNTI_RETTA_REG_2_ <= n40;
    PUNTI_RETTA_REG_1_ <= n44;
    PUNTI_RETTA_REG_0_ <= n48;
    CONT_REG_7_ <= n52;
    CONT_REG_6_ <= n57;
    CONT_REG_5_ <= n62;
    CONT_REG_4_ <= n67;
    CONT_REG_3_ <= n72;
    CONT_REG_2_ <= n77;
    CONT_REG_1_ <= n82;
    CONT_REG_0_ <= n87;
    MAR_REG_7_ <= n92;
    MAR_REG_6_ <= n97;
    MAR_REG_5_ <= n102;
    MAR_REG_4_ <= n107;
    MAR_REG_3_ <= n112;
    MAR_REG_2_ <= n117;
    MAR_REG_1_ <= n122;
    MAR_REG_0_ <= n127;
    X_REG_7_ <= n132;
    X_REG_6_ <= n137;
    X_REG_5_ <= n142;
    X_REG_4_ <= n147;
    X_REG_3_ <= n152;
    X_REG_2_ <= n157;
    X_REG_1_ <= n162;
    X_REG_0_ <= n167;
    Y_REG_3_ <= n172;
    Y_REG_1_ <= n177;
    Y_REG_5_ <= n182;
    T_REG_3_ <= n187;
    T_REG_5_ <= n192;
    T_REG_1_ <= n197;
    T_REG_0_ <= n202;
    T_REG_4_ <= n207;
    T_REG_6_ <= n212;
    T_REG_2_ <= n217;
    Y_REG_4_ <= n222;
    Y_REG_0_ <= n227;
    Y_REG_2_ <= n232;
    Y_REG_6_ <= n237;
    STATO_REG_2_ <= n242;
    STATO_REG_1_ <= n247;
    STATO_REG_0_ <= n252;
  end
endmodule


