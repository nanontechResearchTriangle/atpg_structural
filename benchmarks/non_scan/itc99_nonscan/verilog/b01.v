// Benchmark "b01" written by ABC on Mon Apr  8 17:49:33 2019

module b01 ( clock, 
    LINE1, LINE2,
    OUTP_REG, OVERFLW_REG  );
  input  clock;
  input  LINE1, LINE2;
  output OUTP_REG, OVERFLW_REG;
  reg STATO_REG_2_, STATO_REG_1_, STATO_REG_0_, OUTP_REG, OVERFLW_REG;
  wire U38, U39, U40, U41, U42, U43, U44, U45, U46, U47, U49, U50, U51, U52,
    U53, U54, U55, U56, U57, U58, U59, U60, U61, U62, U63, U64, U65, U66,
    U67, U68, U69, U70, n10, n15, n20, n25, n29;
  assign n10 = ~U49 | ~U50 | ~U53 | ~U52;
  assign n15 = ~U49 | ~U56 | ~U45 | ~U64 | ~U63;
  assign n20 = ~U50 | ~U59 | ~U45 | ~U66 | ~U65;
  assign n25 = ~U47 | ~U68 | ~U67;
  assign U38 = STATO_REG_1_ & U43;
  assign U39 = ~LINE1;
  assign U40 = ~U42 & ~STATO_REG_1_;
  assign U41 = ~STATO_REG_2_;
  assign U42 = LINE1 & LINE2;
  assign U43 = ~STATO_REG_0_;
  assign U44 = ~U43 & ~U41;
  assign U45 = U55 & U54;
  assign U46 = ~U62 | ~U61;
  assign U47 = U46 | U41 | STATO_REG_1_;
  assign n29 = STATO_REG_0_ & STATO_REG_1_ & U41;
  assign U49 = ~U38 | ~U41;
  assign U50 = ~STATO_REG_2_ | ~U40 | ~U46;
  assign U51 = ~STATO_REG_2_ | ~STATO_REG_1_;
  assign U52 = ~U42 | ~U51;
  assign U53 = ~U44 | ~U40;
  assign U54 = ~U44 | ~U46;
  assign U55 = ~U42 | ~U38;
  assign U56 = ~STATO_REG_0_ | ~U40;
  assign U57 = U43 | STATO_REG_1_;
  assign U58 = ~U41 | ~U57;
  assign U59 = ~U40 | ~U43 | ~U41;
  assign U60 = U41 | U38;
  assign U61 = U39 | LINE2;
  assign U62 = ~LINE2 | ~U39;
  assign U63 = U47 | U42;
  assign U64 = U70 | U69;
  assign U65 = ~n29 | ~U69;
  assign U66 = ~U42 | ~U58;
  assign U67 = U46 | U70;
  assign U68 = ~U46 | ~U60;
  assign U69 = ~U42;
  assign U70 = ~U44;
  always @ (posedge clock) begin
    STATO_REG_2_ <= n10;
    STATO_REG_1_ <= n15;
    STATO_REG_0_ <= n20;
    OUTP_REG <= n25;
    OVERFLW_REG <= n29;
  end
endmodule


