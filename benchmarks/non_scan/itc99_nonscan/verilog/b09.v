// Benchmark "b09" written by ABC on Mon Apr  8 17:53:59 2019

module b09 ( clock, 
    X,
    Y_REG  );
  input  clock;
  input  X;
  output Y_REG;
  reg D_OUT_REG_7_, D_OUT_REG_6_, D_OUT_REG_5_, D_OUT_REG_4_, D_OUT_REG_3_,
    D_OUT_REG_2_, D_OUT_REG_1_, D_OUT_REG_0_, OLD_REG_7_, OLD_REG_6_,
    OLD_REG_5_, OLD_REG_4_, OLD_REG_3_, OLD_REG_2_, OLD_REG_1_, OLD_REG_0_,
    Y_REG, STATO_REG_1_, STATO_REG_0_, D_IN_REG_8_, D_IN_REG_7_,
    D_IN_REG_6_, D_IN_REG_5_, D_IN_REG_4_, D_IN_REG_3_, D_IN_REG_2_,
    D_IN_REG_1_, D_IN_REG_0_;
  wire U107, U108, U109, U110, U111, U112, U113, U114, U115, U116, U117,
    U118, U119, U120, U121, U122, U123, U124, U125, U126, U127, U128, U129,
    U130, U131, U132, U133, U134, U135, U136, U137, U138, U139, U140, U141,
    U142, U143, U144, U145, U146, U147, U148, U149, U150, U151, U152, U153,
    U154, U155, U156, U157, U158, U159, U160, U161, U162, U163, U164, U165,
    U166, U167, U168, U169, U170, U171, U172, U173, U174, U175, U176, U177,
    U178, U179, U180, U181, U182, U183, U184, U185, U186, U187, U188, U189,
    U190, U191, U192, U193, U194, U195, U196, U197, U198, U199, U200, U201,
    U202, U203, U204, U205, U206, U207, n6, n11, n16, n21, n26, n31, n36,
    n41, n46, n51, n56, n61, n66, n71, n76, n81, n86, n90, n95, n100, n105,
    n110, n115, n120, n125, n130, n135, n140;
  assign n95 = ~U135 | ~U204 | ~U159;
  assign n90 = STATO_REG_1_ | U120;
  assign n140 = D_IN_REG_1_ & U187;
  assign n135 = D_IN_REG_2_ & U187;
  assign n130 = D_IN_REG_3_ & U187;
  assign n125 = D_IN_REG_4_ & U187;
  assign n120 = D_IN_REG_5_ & U187;
  assign n115 = D_IN_REG_6_ & U187;
  assign n110 = D_IN_REG_7_ & U187;
  assign n105 = D_IN_REG_8_ & U187;
  assign n100 = ~U185 | ~U184;
  assign n6 = ~U137 | ~U136;
  assign n11 = ~U140 | ~U139 | ~U138;
  assign n16 = ~U143 | ~U142 | ~U141;
  assign n21 = ~U146 | ~U145 | ~U144;
  assign n26 = ~U149 | ~U148 | ~U147;
  assign n31 = ~U152 | ~U151 | ~U150;
  assign n36 = ~U155 | ~U154 | ~U153;
  assign n41 = ~U158 | ~U157 | ~U156;
  assign n46 = ~U162 | ~U161;
  assign n51 = ~U164 | ~U163;
  assign n56 = ~U166 | ~U165;
  assign n61 = ~U168 | ~U167;
  assign n66 = ~U170 | ~U169;
  assign n71 = ~U172 | ~U171;
  assign n76 = ~U174 | ~U173;
  assign n81 = ~U176 | ~U175;
  assign n86 = ~U179 | ~U178 | ~U133;
  assign U107 = ~STATO_REG_0_;
  assign U108 = ~D_IN_REG_0_;
  assign U109 = ~OLD_REG_1_;
  assign U110 = ~OLD_REG_6_;
  assign U111 = ~OLD_REG_0_;
  assign U112 = ~OLD_REG_3_;
  assign U113 = ~OLD_REG_5_;
  assign U114 = ~OLD_REG_2_;
  assign U115 = ~OLD_REG_4_;
  assign U116 = ~OLD_REG_7_;
  assign U117 = ~STATO_REG_1_;
  assign U118 = ~U117 & ~U107;
  assign U119 = ~U127 | ~U128 | ~U130 | ~U129;
  assign U120 = D_IN_REG_0_ & STATO_REG_0_;
  assign U121 = ~U107 & ~U122;
  assign U122 = U133 & U206 & U205;
  assign U123 = STATO_REG_1_ & U107;
  assign U124 = ~U132 & ~U122;
  assign U125 = STATO_REG_0_ & U117;
  assign U126 = U132 & U182;
  assign U127 = U188 & U189 & U191 & U190;
  assign U128 = U192 & U193 & U195 & U194;
  assign U129 = U196 & U197 & U199 & U198;
  assign U130 = U200 & U201 & U203 & U202;
  assign U131 = ~U125;
  assign U132 = ~U123;
  assign U133 = ~U120 | ~U119;
  assign U134 = ~STATO_REG_1_ | ~U108;
  assign U135 = U119 | U207;
  assign U136 = ~U121 | ~D_IN_REG_8_;
  assign U137 = ~D_OUT_REG_7_ | ~U122;
  assign U138 = ~U124 | ~D_OUT_REG_7_;
  assign U139 = ~U121 | ~D_IN_REG_7_;
  assign U140 = ~D_OUT_REG_6_ | ~U122;
  assign U141 = ~D_OUT_REG_6_ | ~U124;
  assign U142 = ~U121 | ~D_IN_REG_6_;
  assign U143 = ~D_OUT_REG_5_ | ~U122;
  assign U144 = ~D_OUT_REG_5_ | ~U124;
  assign U145 = ~U121 | ~D_IN_REG_5_;
  assign U146 = ~D_OUT_REG_4_ | ~U122;
  assign U147 = ~D_OUT_REG_4_ | ~U124;
  assign U148 = ~U121 | ~D_IN_REG_4_;
  assign U149 = ~D_OUT_REG_3_ | ~U122;
  assign U150 = ~D_OUT_REG_3_ | ~U124;
  assign U151 = ~U121 | ~D_IN_REG_3_;
  assign U152 = ~D_OUT_REG_2_ | ~U122;
  assign U153 = ~D_OUT_REG_2_ | ~U124;
  assign U154 = ~U121 | ~D_IN_REG_2_;
  assign U155 = ~D_OUT_REG_1_ | ~U122;
  assign U156 = ~D_OUT_REG_1_ | ~U124;
  assign U157 = ~U121 | ~D_IN_REG_1_;
  assign U158 = ~D_OUT_REG_0_ | ~U122;
  assign U159 = ~STATO_REG_0_ | ~U108;
  assign U160 = ~U132 | ~U159;
  assign U161 = ~U120 | ~D_IN_REG_8_;
  assign U162 = ~OLD_REG_7_ | ~U160;
  assign U163 = ~U120 | ~D_IN_REG_7_;
  assign U164 = ~OLD_REG_6_ | ~U160;
  assign U165 = ~U120 | ~D_IN_REG_6_;
  assign U166 = ~OLD_REG_5_ | ~U160;
  assign U167 = ~U120 | ~D_IN_REG_5_;
  assign U168 = ~OLD_REG_4_ | ~U160;
  assign U169 = ~U120 | ~D_IN_REG_4_;
  assign U170 = ~OLD_REG_3_ | ~U160;
  assign U171 = ~U120 | ~D_IN_REG_3_;
  assign U172 = ~OLD_REG_2_ | ~U160;
  assign U173 = ~U120 | ~D_IN_REG_2_;
  assign U174 = ~OLD_REG_1_ | ~U160;
  assign U175 = ~U120 | ~D_IN_REG_1_;
  assign U176 = ~OLD_REG_0_ | ~U160;
  assign U177 = Y_REG | D_IN_REG_0_;
  assign U178 = ~D_OUT_REG_0_ | ~U123 | ~U108;
  assign U179 = ~U125 | ~U177;
  assign U180 = ~U118 | ~U119;
  assign U181 = ~U131 | ~U180;
  assign U182 = U207 | D_IN_REG_0_;
  assign U183 = ~U126 | ~U131;
  assign U184 = ~X | ~U183;
  assign U185 = ~D_IN_REG_0_ | ~U181;
  assign U186 = ~U125 | ~U108;
  assign U187 = ~U126 | ~U186;
  assign U188 = ~D_IN_REG_2_ | ~U109;
  assign U189 = U109 | D_IN_REG_2_;
  assign U190 = ~D_IN_REG_7_ | ~U110;
  assign U191 = U110 | D_IN_REG_7_;
  assign U192 = ~D_IN_REG_1_ | ~U111;
  assign U193 = U111 | D_IN_REG_1_;
  assign U194 = ~D_IN_REG_4_ | ~U112;
  assign U195 = U112 | D_IN_REG_4_;
  assign U196 = ~D_IN_REG_6_ | ~U113;
  assign U197 = U113 | D_IN_REG_6_;
  assign U198 = ~D_IN_REG_3_ | ~U114;
  assign U199 = U114 | D_IN_REG_3_;
  assign U200 = ~D_IN_REG_5_ | ~U115;
  assign U201 = U115 | D_IN_REG_5_;
  assign U202 = ~D_IN_REG_8_ | ~U116;
  assign U203 = U116 | D_IN_REG_8_;
  assign U204 = ~U134 | ~U107;
  assign U205 = ~D_IN_REG_0_ | ~U117;
  assign U206 = D_IN_REG_0_ | STATO_REG_0_;
  assign U207 = ~U118;
  always @ (posedge clock) begin
    D_OUT_REG_7_ <= n6;
    D_OUT_REG_6_ <= n11;
    D_OUT_REG_5_ <= n16;
    D_OUT_REG_4_ <= n21;
    D_OUT_REG_3_ <= n26;
    D_OUT_REG_2_ <= n31;
    D_OUT_REG_1_ <= n36;
    D_OUT_REG_0_ <= n41;
    OLD_REG_7_ <= n46;
    OLD_REG_6_ <= n51;
    OLD_REG_5_ <= n56;
    OLD_REG_4_ <= n61;
    OLD_REG_3_ <= n66;
    OLD_REG_2_ <= n71;
    OLD_REG_1_ <= n76;
    OLD_REG_0_ <= n81;
    Y_REG <= n86;
    STATO_REG_1_ <= n90;
    STATO_REG_0_ <= n95;
    D_IN_REG_8_ <= n100;
    D_IN_REG_7_ <= n105;
    D_IN_REG_6_ <= n110;
    D_IN_REG_5_ <= n115;
    D_IN_REG_4_ <= n120;
    D_IN_REG_3_ <= n125;
    D_IN_REG_2_ <= n130;
    D_IN_REG_1_ <= n135;
    D_IN_REG_0_ <= n140;
  end
endmodule


