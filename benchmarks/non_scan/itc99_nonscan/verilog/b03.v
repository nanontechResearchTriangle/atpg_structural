// Benchmark "b03" written by ABC on Mon Apr  8 17:50:54 2019

module b03 ( clock, 
    REQUEST1, REQUEST2, REQUEST3, REQUEST4,
    GRANT_O_REG_3_, GRANT_O_REG_2_, GRANT_O_REG_1_, GRANT_O_REG_0_  );
  input  clock;
  input  REQUEST1, REQUEST2, REQUEST3, REQUEST4;
  output GRANT_O_REG_3_, GRANT_O_REG_2_, GRANT_O_REG_1_, GRANT_O_REG_0_;
  reg CODA0_REG_2_, CODA0_REG_1_, CODA0_REG_0_, CODA1_REG_2_, CODA1_REG_1_,
    CODA1_REG_0_, CODA2_REG_2_, CODA2_REG_1_, CODA2_REG_0_, CODA3_REG_2_,
    CODA3_REG_1_, CODA3_REG_0_, GRANT_REG_3_, GRANT_REG_2_, GRANT_REG_1_,
    GRANT_REG_0_, GRANT_O_REG_3_, GRANT_O_REG_2_, GRANT_O_REG_1_,
    GRANT_O_REG_0_, RU3_REG, FU1_REG, FU3_REG, RU1_REG, RU4_REG, FU2_REG,
    FU4_REG, RU2_REG, STATO_REG_1_, STATO_REG_0_;
  wire U185, U186, U187, U188, U189, U190, U191, U192, U193, U208, U209,
    U210, U211, U212, U213, U214, U215, U216, U217, U218, U219, U220, U221,
    U222, U223, U224, U225, U226, U227, U228, U229, U230, U231, U232, U233,
    U234, U235, U236, U237, U238, U239, U240, U241, U242, U243, U244, U245,
    U246, U247, U248, U249, U250, U251, U252, U253, U254, U255, U256, U257,
    U258, U259, U260, U261, U262, U263, U264, U265, U266, U267, U268, U269,
    U270, U271, U272, U273, U274, U275, U276, U277, U278, U279, U280, n18,
    n23, n28, n33, n38, n43, n48, n53, n58, n63, n68, n73, n78, n83, n88,
    n93, n98, n102, n106, n110, n114, n119, n124, n129, n134, n139, n144,
    n149, n154, n159;
  assign n159 = ~STATO_REG_0_;
  assign n18 = ~U219 | ~U218 | ~U217;
  assign n23 = ~U222 | ~U221 | ~U220;
  assign n28 = ~U225 | ~U224 | ~U223;
  assign n33 = ~U228 | ~U227 | ~U226;
  assign n38 = ~U231 | ~U230 | ~U229;
  assign n43 = ~U234 | ~U233 | ~U232;
  assign n48 = ~U237 | ~U236 | ~U235;
  assign n53 = ~U240 | ~U239 | ~U238;
  assign n58 = ~U243 | ~U242 | ~U241;
  assign n63 = ~U245 | ~U244;
  assign n68 = ~U247 | ~U246;
  assign n73 = ~U249 | ~U248;
  assign n119 = ~U187 | ~U250;
  assign n129 = ~U187 | ~U251;
  assign U185 = ~RU3_REG | ~U208;
  assign U186 = ~RU1_REG;
  assign U187 = ~RU1_REG | ~STATO_REG_0_;
  assign U188 = ~U190 & ~STATO_REG_1_;
  assign U189 = ~U280 & ~U190;
  assign U190 = U214 & U213 & U209;
  assign U191 = ~CODA0_REG_2_;
  assign U192 = ~CODA0_REG_1_;
  assign U193 = ~CODA0_REG_0_;
  assign n78 = ~U255 | ~U254;
  assign n83 = ~U257 | ~U256;
  assign n88 = ~U259 | ~U258;
  assign n93 = ~U261 | ~U260;
  assign n98 = ~U263 | ~U262;
  assign n102 = ~U265 | ~U264;
  assign n106 = ~U267 | ~U266;
  assign n110 = ~U269 | ~U268;
  assign n114 = ~U272 | ~U270;
  assign n124 = ~U272 | ~U271;
  assign n134 = ~U276 | ~U273;
  assign n139 = ~U278 | ~U274;
  assign n144 = ~U276 | ~U275;
  assign n149 = ~U278 | ~U277;
  assign U208 = ~RU2_REG;
  assign U209 = ~STATO_REG_1_ | ~U210;
  assign U210 = FU4_REG | FU1_REG | FU3_REG | FU2_REG;
  assign U211 = U185 | FU3_REG;
  assign U212 = ~U211 | ~U253 | ~U252;
  assign U213 = ~U212 | ~U186 | ~STATO_REG_0_;
  assign U214 = U187 | FU1_REG;
  assign U215 = RU2_REG | RU3_REG;
  assign U216 = ~U186 | ~U215;
  assign U217 = ~U188 | ~U216;
  assign U218 = ~CODA1_REG_2_ | ~U189;
  assign U219 = ~CODA0_REG_2_ | ~U190;
  assign U220 = ~U188 | ~U185 | ~U186;
  assign U221 = ~CODA1_REG_1_ | ~U189;
  assign U222 = ~CODA0_REG_1_ | ~U190;
  assign U223 = ~U188 | ~U208 | ~U186;
  assign U224 = ~CODA1_REG_0_ | ~U189;
  assign U225 = ~CODA0_REG_0_ | ~U190;
  assign U226 = ~CODA2_REG_2_ | ~U189;
  assign U227 = ~CODA0_REG_2_ | ~U188;
  assign U228 = ~U190 | ~CODA1_REG_2_;
  assign U229 = ~CODA2_REG_1_ | ~U189;
  assign U230 = ~CODA0_REG_1_ | ~U188;
  assign U231 = ~CODA1_REG_1_ | ~U190;
  assign U232 = ~CODA2_REG_0_ | ~U189;
  assign U233 = ~CODA0_REG_0_ | ~U188;
  assign U234 = ~CODA1_REG_0_ | ~U190;
  assign U235 = ~CODA3_REG_2_ | ~U189;
  assign U236 = ~CODA1_REG_2_ | ~U188;
  assign U237 = ~CODA2_REG_2_ | ~U190;
  assign U238 = ~CODA3_REG_1_ | ~U189;
  assign U239 = ~CODA1_REG_1_ | ~U188;
  assign U240 = ~CODA2_REG_1_ | ~U190;
  assign U241 = ~CODA3_REG_0_ | ~U189;
  assign U242 = ~CODA1_REG_0_ | ~U188;
  assign U243 = ~CODA2_REG_0_ | ~U190;
  assign U244 = ~CODA2_REG_2_ | ~U188;
  assign U245 = ~CODA3_REG_2_ | ~U190;
  assign U246 = ~CODA2_REG_1_ | ~U188;
  assign U247 = ~CODA3_REG_1_ | ~U190;
  assign U248 = ~CODA2_REG_0_ | ~U188;
  assign U249 = ~CODA3_REG_0_ | ~U190;
  assign U250 = ~FU1_REG | ~n159;
  assign U251 = ~REQUEST1 | ~n159;
  assign U252 = U208 | FU2_REG;
  assign U253 = RU2_REG | U279 | RU3_REG | FU4_REG;
  assign U254 = ~GRANT_REG_3_ | ~U209;
  assign U255 = U209 | U191 | CODA0_REG_0_ | CODA0_REG_1_;
  assign U256 = ~GRANT_REG_2_ | ~U209;
  assign U257 = U209 | U192 | CODA0_REG_0_ | CODA0_REG_2_;
  assign U258 = ~GRANT_REG_1_ | ~U209;
  assign U259 = U209 | U193 | CODA0_REG_1_ | CODA0_REG_2_;
  assign U260 = ~GRANT_REG_0_ | ~U209;
  assign U261 = U209 | U193 | U192 | U191;
  assign U262 = ~GRANT_O_REG_3_ | ~n159;
  assign U263 = ~GRANT_REG_3_ | ~STATO_REG_0_;
  assign U264 = ~GRANT_O_REG_2_ | ~n159;
  assign U265 = ~GRANT_REG_2_ | ~STATO_REG_0_;
  assign U266 = ~GRANT_O_REG_1_ | ~n159;
  assign U267 = ~GRANT_REG_1_ | ~STATO_REG_0_;
  assign U268 = ~GRANT_O_REG_0_ | ~n159;
  assign U269 = ~GRANT_REG_0_ | ~STATO_REG_0_;
  assign U270 = ~REQUEST3 | ~n159;
  assign U271 = ~FU3_REG | ~n159;
  assign U272 = ~RU3_REG | ~STATO_REG_0_;
  assign U273 = ~REQUEST4 | ~n159;
  assign U274 = ~FU2_REG | ~n159;
  assign U275 = ~FU4_REG | ~n159;
  assign U276 = ~RU4_REG | ~STATO_REG_0_;
  assign U277 = ~REQUEST2 | ~n159;
  assign U278 = ~RU2_REG | ~STATO_REG_0_;
  assign U279 = ~RU4_REG;
  assign U280 = ~STATO_REG_1_;
  assign n154 = STATO_REG_0_;
  always @ (posedge clock) begin
    CODA0_REG_2_ <= n18;
    CODA0_REG_1_ <= n23;
    CODA0_REG_0_ <= n28;
    CODA1_REG_2_ <= n33;
    CODA1_REG_1_ <= n38;
    CODA1_REG_0_ <= n43;
    CODA2_REG_2_ <= n48;
    CODA2_REG_1_ <= n53;
    CODA2_REG_0_ <= n58;
    CODA3_REG_2_ <= n63;
    CODA3_REG_1_ <= n68;
    CODA3_REG_0_ <= n73;
    GRANT_REG_3_ <= n78;
    GRANT_REG_2_ <= n83;
    GRANT_REG_1_ <= n88;
    GRANT_REG_0_ <= n93;
    GRANT_O_REG_3_ <= n98;
    GRANT_O_REG_2_ <= n102;
    GRANT_O_REG_1_ <= n106;
    GRANT_O_REG_0_ <= n110;
    RU3_REG <= n114;
    FU1_REG <= n119;
    FU3_REG <= n124;
    RU1_REG <= n129;
    RU4_REG <= n134;
    FU2_REG <= n139;
    FU4_REG <= n144;
    RU2_REG <= n149;
    STATO_REG_1_ <= n154;
    STATO_REG_0_ <= n159;
  end
endmodule


