// Benchmark "b02" written by ABC on Mon Apr  8 17:50:44 2019

module b02 ( clock, 
    LINEA,
    U_REG  );
  input  clock;
  input  LINEA;
  output U_REG;
  reg STATO_REG_2_, STATO_REG_1_, STATO_REG_0_, U_REG;
  wire U34, U35, U36, U38, U39, U40, U41, U42, U43, U44, U45, U46, U47, U48,
    U49, U50, U51, U52, n6, n11, n16, n21;
  assign n21 = ~U34 & ~STATO_REG_0_ & ~STATO_REG_1_;
  assign n6 = ~U44 | ~U43 | ~U38;
  assign n16 = ~U50 | ~U49;
  assign U34 = ~STATO_REG_2_;
  assign U35 = ~STATO_REG_0_;
  assign U36 = ~STATO_REG_1_;
  assign n11 = ~U52 | ~U51;
  assign U38 = U36 | U35;
  assign U39 = U36 | STATO_REG_2_;
  assign U40 = ~U35 | ~U39;
  assign U41 = U36 | LINEA;
  assign U42 = ~U35 | ~U41;
  assign U43 = ~STATO_REG_2_ | ~U42;
  assign U44 = ~LINEA | ~U40;
  assign U45 = LINEA | STATO_REG_1_;
  assign U46 = ~U34 | ~U45;
  assign U47 = ~LINEA | ~U34;
  assign U48 = ~STATO_REG_0_ | ~U47;
  assign U49 = STATO_REG_2_ | LINEA | STATO_REG_0_;
  assign U50 = ~U48 | ~U36;
  assign U51 = ~STATO_REG_0_ | ~U46;
  assign U52 = STATO_REG_0_ | U36 | STATO_REG_2_;
  always @ (posedge clock) begin
    STATO_REG_2_ <= n6;
    STATO_REG_1_ <= n11;
    STATO_REG_0_ <= n16;
    U_REG <= n21;
  end
endmodule


