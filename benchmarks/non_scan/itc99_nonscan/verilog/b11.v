// Benchmark "b11s" written by ABC on Mon Apr  8 17:54:19 2019

module b11s ( clock, 
    X_IN_5_, X_IN_4_, X_IN_3_, X_IN_2_, X_IN_1_, X_IN_0_, STBI,
    X_OUT_REG_5_, X_OUT_REG_4_, X_OUT_REG_3_, X_OUT_REG_2_, X_OUT_REG_1_,
    X_OUT_REG_0_  );
  input  clock;
  input  X_IN_5_, X_IN_4_, X_IN_3_, X_IN_2_, X_IN_1_, X_IN_0_, STBI;
  output X_OUT_REG_5_, X_OUT_REG_4_, X_OUT_REG_3_, X_OUT_REG_2_, X_OUT_REG_1_,
    X_OUT_REG_0_;
  reg R_IN_REG_5_, R_IN_REG_4_, R_IN_REG_3_, R_IN_REG_2_, R_IN_REG_1_,
    R_IN_REG_0_, CONT_REG_5_, CONT_REG_4_, CONT_REG_3_, CONT_REG_2_,
    CONT_REG_1_, CONT_REG_0_, CONT1_REG_8_, CONT1_REG_7_, CONT1_REG_6_,
    CONT1_REG_5_, CONT1_REG_4_, CONT1_REG_3_, CONT1_REG_2_, CONT1_REG_1_,
    CONT1_REG_0_, X_OUT_REG_5_, X_OUT_REG_4_, X_OUT_REG_3_, X_OUT_REG_2_,
    X_OUT_REG_1_, X_OUT_REG_0_, STATO_REG_3_, STATO_REG_2_, STATO_REG_1_,
    STATO_REG_0_;
  wire U308, U309, U310, U311, U312, U313, U314, U315, U316, U317, U318,
    U319, U320, U321, U322, U323, U324, U325, U326, U327, U328, U329, U330,
    U331, U332, U333, U334, U335, U336, U337, U338, U339, U340, U341, U342,
    U343, U344, U345, U371, U372, U373, U374, U375, U376, U377, U378, U379,
    U380, U381, U382, U383, U384, U385, U386, U387, U388, U389, U390, U391,
    U392, U393, U394, U395, U396, U397, U398, U399, U400, U401, U402, U403,
    U404, U405, U406, U407, U408, U409, U410, U411, U418, U419, U420, U421,
    U422, U423, U424, U425, U426, U427, U428, U429, U430, U431, U432, U433,
    U434, U435, U436, U437, U438, U439, U440, U441, U442, U443, U444, U445,
    U446, U447, U448, U449, U450, U451, U452, U453, U454, U455, U456, U457,
    U458, U459, U460, U461, U462, U463, U464, U465, U466, U467, U468, U469,
    U470, U471, U472, U473, U474, U475, U476, U477, U478, U479, U480, U481,
    U482, U483, U484, U485, U486, U487, U488, U489, U490, U491, U492, U493,
    U494, U495, U496, U497, U498, U499, U500, U501, U502, U503, U504, U505,
    U506, U507, U508, U509, U510, U511, U512, U513, U514, U515, U516, U517,
    U518, U519, U520, U521, U522, U523, U524, U525, U526, U527, U528, U529,
    U530, U531, U532, U533, U534, U535, U536, U537, U538, U539, U540, U541,
    U542, U543, U544, U545, U546, U547, U548, U549, U550, U551, U552, U553,
    U554, U555, U556, U557, U558, U559, U560, U561, U562, U563, U564, U565,
    U566, U567, U568, U569, U570, U571, U572, U573, U574, U575, U576, U577,
    U578, U579, U580, U581, U582, R81_U44, R81_U43, R78_U5, R78_U6, R78_U7,
    R78_U8, R78_U9, R78_U10, R78_U11, R78_U12, R78_U13, R78_U14, R78_U15,
    R78_U16, R78_U17, R78_U18, R78_U19, R78_U20, R78_U21, R78_U22, R78_U23,
    R78_U24, R78_U25, R78_U26, R78_U27, R78_U28, R78_U29, R78_U30, R78_U31,
    R78_U32, R78_U33, R78_U34, R78_U35, R78_U36, R78_U37, R78_U38, R78_U39,
    R78_U40, R78_U41, R78_U42, R78_U43, R78_U44, R78_U45, R78_U46, R78_U47,
    R78_U48, R78_U49, R78_U50, R78_U51, R78_U52, R78_U53, R78_U54, R78_U55,
    R78_U56, R78_U57, R78_U58, R78_U59, R78_U60, R78_U61, R78_U62, R78_U63,
    R78_U64, R78_U65, R78_U66, R78_U67, R78_U68, R78_U69, R78_U70, R78_U71,
    R78_U72, R78_U73, R78_U74, R78_U75, R78_U76, R78_U77, R78_U78, R78_U79,
    R78_U80, R78_U81, R78_U82, R78_U83, R78_U84, R78_U85, R78_U86, R78_U87,
    R78_U88, R78_U89, R78_U90, R78_U91, R78_U92, R78_U93, R78_U94, R78_U95,
    R78_U96, R78_U97, R78_U98, R78_U99, R78_U100, R78_U101, R78_U102,
    R78_U103, R78_U104, R78_U105, R78_U106, R78_U107, R78_U108, R78_U109,
    R78_U110, R78_U111, R78_U112, R78_U113, R78_U114, R78_U115, R78_U116,
    R78_U117, R78_U118, R78_U119, R78_U120, R78_U121, R78_U122, R78_U123,
    R78_U124, R78_U125, R78_U126, R78_U127, R81_U6, R81_U7, R81_U8, R81_U9,
    R81_U10, R81_U11, R81_U12, R81_U13, R81_U14, R81_U15, R81_U16, R81_U17,
    R81_U18, R81_U19, R81_U20, R81_U21, R81_U22, R81_U23, R81_U24, R81_U25,
    R81_U26, R81_U27, R81_U28, R81_U29, R81_U30, R81_U31, R81_U32, R81_U33,
    R81_U34, R81_U35, R81_U36, R81_U37, R81_U38, R81_U39, R81_U40, R81_U41,
    R81_U42, n28, n33, n38, n43, n48, n53, n58, n63, n68, n73, n78, n83,
    n88, n93, n98, n103, n108, n113, n118, n123, n128, n133, n137, n141,
    n145, n149, n153, n157, n162, n167, n172;
  assign U308 = ~U394 | ~U384 | ~U557 | ~U556;
  assign U309 = ~U425 | ~U535;
  assign U310 = ~U425 | ~U536;
  assign U311 = ~U425 | ~U537;
  assign U312 = ~U420 | ~U539 | ~U541 | ~U540;
  assign U313 = ~U543 | ~U544 | ~U542 | ~U384 | ~U422;
  assign U314 = ~U545 | ~U546 | ~U407 | ~U547;
  assign U315 = ~U422 | ~U548 | ~U550 | ~U549;
  assign U316 = ~U551 | ~U552 | ~U407 | ~U411;
  assign U317 = ~U422 | ~U553 | ~U555 | ~U554;
  assign U318 = CONT1_REG_8_ & U523;
  assign U319 = CONT1_REG_7_ & U523;
  assign U320 = CONT1_REG_6_ & U523;
  assign U321 = ~U525 | ~U524;
  assign U322 = ~U409 | ~U527 | ~U526;
  assign U323 = ~U409 | ~U529 | ~U528;
  assign U324 = ~U423 | ~U424 | ~U531 | ~U530;
  assign U325 = ~U532 | ~U386 | ~U424;
  assign U326 = ~U421 | ~U424 | ~U534 | ~U533;
  assign U327 = ~U373 | ~U394;
  assign U328 = STATO_REG_3_ & CONT1_REG_8_;
  assign U329 = STATO_REG_3_ & CONT1_REG_7_;
  assign U330 = STATO_REG_3_ & CONT1_REG_6_;
  assign U331 = ~U540 | ~U425 | ~U381;
  assign U332 = ~U405 | ~U543 | ~U518;
  assign U333 = ~U405 | ~U546 | ~U519;
  assign U334 = ~U549 | ~U520 | ~U381;
  assign U335 = ~U405 | ~U552 | ~U521;
  assign U336 = ~U554 | ~U522 | ~U381;
  assign U337 = CONT1_REG_8_ & U404;
  assign U338 = CONT1_REG_7_ & U404;
  assign U339 = CONT1_REG_6_ & U404;
  assign U340 = ~U507 | ~U506;
  assign U341 = ~U509 | ~U508 | ~U421;
  assign U342 = ~U511 | ~U510 | ~U421;
  assign U343 = ~U513 | ~U512;
  assign U344 = ~U515 | ~U514;
  assign U345 = ~U517 | ~U516 | ~U421;
  assign n157 = ~U427 | ~U493;
  assign n162 = ~U385 | ~U565 | ~U564;
  assign n167 = ~U566 | ~U567 | ~U431 | ~U386;
  assign n172 = ~U419 | ~U433 | ~U432 | ~U434;
  assign n58 = ~U495 | ~U494;
  assign n63 = ~U497 | ~U496;
  assign n68 = ~U499 | ~U498;
  assign n73 = ~U501 | ~U500;
  assign n78 = ~U503 | ~U502;
  assign n83 = ~U505 | ~U504;
  assign n88 = ~U439 | ~U438;
  assign n93 = ~U441 | ~U440;
  assign n98 = ~U444 | ~U443 | ~U442;
  assign n103 = ~U448 | ~U449 | ~U447 | ~U446 | ~U445;
  assign n108 = ~U453 | ~U454 | ~U452 | ~U451 | ~U450;
  assign n113 = ~U458 | ~U459 | ~U457 | ~U456 | ~U455;
  assign n118 = ~U463 | ~U464 | ~U462 | ~U461 | ~U460;
  assign n123 = ~U468 | ~U469 | ~U467 | ~U466 | ~U465;
  assign n128 = ~U470 | ~U471 | ~U473 | ~U472;
  assign n133 = ~U476 | ~U475 | ~U474;
  assign n137 = ~U479 | ~U478 | ~U477;
  assign n141 = ~U482 | ~U481 | ~U480;
  assign n145 = ~U485 | ~U484 | ~U483;
  assign n149 = ~U488 | ~U487 | ~U486;
  assign n153 = ~U491 | ~U490 | ~U489;
  assign U371 = U418 & U559 & U558;
  assign U372 = ~STATO_REG_1_;
  assign U373 = ~U372 & ~STATO_REG_2_;
  assign U374 = ~R_IN_REG_3_;
  assign U375 = ~R_IN_REG_1_;
  assign U376 = ~R_IN_REG_2_;
  assign U377 = ~R_IN_REG_0_;
  assign U378 = ~R_IN_REG_4_;
  assign U379 = U373 & U382;
  assign U380 = ~STATO_REG_2_;
  assign U381 = U380 | U372;
  assign U382 = ~STATO_REG_0_;
  assign U383 = ~R81_U6;
  assign U384 = U382 | STATO_REG_1_;
  assign U385 = U380 | STATO_REG_1_;
  assign U386 = U381 | STATO_REG_0_;
  assign U387 = ~U388 & ~U373;
  assign U388 = U406 & U435 & U437 & U436;
  assign U389 = ~U382 & ~U388 & ~STATO_REG_2_;
  assign U390 = U389 & R_IN_REG_0_;
  assign U391 = U389 & U377;
  assign U392 = ~U388 & ~STATO_REG_2_ & ~STATO_REG_0_;
  assign U393 = ~U380 | ~U372 | ~U382;
  assign U394 = ~STATO_REG_3_;
  assign U395 = ~U394 & ~U393;
  assign U396 = U395 & U383;
  assign U397 = U395 & R81_U6;
  assign U398 = STATO_REG_1_ | STATO_REG_3_;
  assign U399 = ~U398 & ~STATO_REG_2_;
  assign U400 = ~U372 & ~U401 & ~R81_U6;
  assign U401 = U493 & U492;
  assign U402 = ~U371 & ~STATO_REG_2_ & ~STATO_REG_3_;
  assign U403 = ~U426 & ~STATO_REG_2_ & ~STATO_REG_3_;
  assign U404 = ~U381 | ~U398;
  assign U405 = ~U404 & ~U402;
  assign U406 = U382 | U372;
  assign U407 = U420 & U384;
  assign U408 = U398 | R_IN_REG_1_ | STATO_REG_0_;
  assign U409 = U423 & U386;
  assign U410 = ~U409 | ~U424 | ~U408;
  assign U411 = U398 | U375 | STATO_REG_0_;
  assign n28 = ~U571 | ~U570;
  assign n33 = ~U573 | ~U572;
  assign n38 = ~U575 | ~U574;
  assign n43 = ~U577 | ~U576;
  assign n48 = ~U579 | ~U578;
  assign n53 = ~U581 | ~U580;
  assign U418 = U560 & U561 & U563 & U562;
  assign U419 = U393 & U569 & U568;
  assign U420 = U406 | U376 | R_IN_REG_3_;
  assign U421 = ~U403;
  assign U422 = U406 | R_IN_REG_2_ | R_IN_REG_3_;
  assign U423 = U406 | U376 | U374;
  assign U424 = U406 | U374 | R_IN_REG_2_;
  assign U425 = ~STATO_REG_3_ | ~CONT1_REG_5_;
  assign U426 = ~U371;
  assign U427 = U382 | U381;
  assign U428 = U384 | U383;
  assign U429 = ~U382 | ~U375;
  assign U430 = ~U429 | ~U428;
  assign U431 = ~R81_U6 | ~U379 | ~U426;
  assign U432 = ~U379 | ~U426;
  assign U433 = U385 | U375;
  assign U434 = U386 | U383;
  assign U435 = U385 | STATO_REG_0_;
  assign U436 = ~U373 | ~U371;
  assign U437 = U380 | R81_U6;
  assign U438 = ~R78_U6 | ~U387;
  assign U439 = ~CONT1_REG_8_ | ~U388;
  assign U440 = ~R78_U5 | ~U387;
  assign U441 = ~CONT1_REG_7_ | ~U388;
  assign U442 = ~CONT_REG_5_ | ~U390;
  assign U443 = ~R78_U19 | ~U387;
  assign U444 = ~CONT1_REG_6_ | ~U388;
  assign U445 = ~U391 | ~CONT_REG_5_;
  assign U446 = ~CONT_REG_4_ | ~U390;
  assign U447 = ~R78_U20 | ~U387;
  assign U448 = ~U392 | ~R_IN_REG_5_;
  assign U449 = ~CONT1_REG_5_ | ~U388;
  assign U450 = ~CONT_REG_4_ | ~U391;
  assign U451 = ~CONT_REG_3_ | ~U390;
  assign U452 = ~R78_U21 | ~U387;
  assign U453 = ~U392 | ~R_IN_REG_4_;
  assign U454 = ~CONT1_REG_4_ | ~U388;
  assign U455 = ~CONT_REG_3_ | ~U391;
  assign U456 = ~CONT_REG_2_ | ~U390;
  assign U457 = ~R78_U22 | ~U387;
  assign U458 = ~U392 | ~R_IN_REG_3_;
  assign U459 = ~CONT1_REG_3_ | ~U388;
  assign U460 = ~CONT_REG_2_ | ~U391;
  assign U461 = ~CONT_REG_1_ | ~U390;
  assign U462 = ~R78_U23 | ~U387;
  assign U463 = ~U392 | ~R_IN_REG_2_;
  assign U464 = ~CONT1_REG_2_ | ~U388;
  assign U465 = ~CONT_REG_1_ | ~U391;
  assign U466 = ~CONT_REG_0_ | ~U390;
  assign U467 = ~R78_U24 | ~U387;
  assign U468 = ~U392 | ~R_IN_REG_1_;
  assign U469 = ~CONT1_REG_1_ | ~U388;
  assign U470 = ~CONT_REG_0_ | ~U391;
  assign U471 = ~R78_U18 | ~U387;
  assign U472 = ~U392 | ~R_IN_REG_0_;
  assign U473 = ~CONT1_REG_0_ | ~U388;
  assign U474 = ~U396 | ~R78_U20;
  assign U475 = ~U397 | ~CONT1_REG_5_;
  assign U476 = ~X_OUT_REG_5_ | ~U393;
  assign U477 = ~U396 | ~R78_U21;
  assign U478 = ~U397 | ~CONT1_REG_4_;
  assign U479 = ~X_OUT_REG_4_ | ~U393;
  assign U480 = ~U396 | ~R78_U22;
  assign U481 = ~U397 | ~CONT1_REG_3_;
  assign U482 = ~X_OUT_REG_3_ | ~U393;
  assign U483 = ~U396 | ~R78_U23;
  assign U484 = ~U397 | ~CONT1_REG_2_;
  assign U485 = ~X_OUT_REG_2_ | ~U393;
  assign U486 = ~U396 | ~R78_U24;
  assign U487 = ~U397 | ~CONT1_REG_1_;
  assign U488 = ~X_OUT_REG_1_ | ~U393;
  assign U489 = ~U396 | ~R78_U18;
  assign U490 = ~U397 | ~CONT1_REG_0_;
  assign U491 = ~X_OUT_REG_0_ | ~U393;
  assign U492 = ~U399 | ~U382;
  assign U493 = ~U379 | ~U371;
  assign U494 = ~U400 | ~R78_U20;
  assign U495 = ~U401 | ~CONT_REG_5_;
  assign U496 = ~U400 | ~R78_U21;
  assign U497 = ~U401 | ~CONT_REG_4_;
  assign U498 = ~U400 | ~R78_U22;
  assign U499 = ~U401 | ~CONT_REG_3_;
  assign U500 = ~U400 | ~R78_U23;
  assign U501 = ~U401 | ~CONT_REG_2_;
  assign U502 = ~U400 | ~R78_U24;
  assign U503 = ~U401 | ~CONT_REG_1_;
  assign U504 = ~U400 | ~R78_U18;
  assign U505 = ~U401 | ~CONT_REG_0_;
  assign U506 = ~CONT1_REG_5_ | ~U404;
  assign U507 = ~U402 | ~R_IN_REG_5_;
  assign U508 = ~CONT1_REG_4_ | ~U404;
  assign U509 = ~U402 | ~R_IN_REG_4_;
  assign U510 = ~CONT1_REG_3_ | ~U404;
  assign U511 = ~U402 | ~R_IN_REG_3_;
  assign U512 = ~CONT1_REG_2_ | ~U404;
  assign U513 = ~U402 | ~R_IN_REG_2_;
  assign U514 = ~CONT1_REG_1_ | ~U404;
  assign U515 = ~U402 | ~R_IN_REG_1_;
  assign U516 = ~CONT1_REG_0_ | ~U404;
  assign U517 = ~U402 | ~R_IN_REG_0_;
  assign U518 = ~STATO_REG_3_ | ~CONT1_REG_4_;
  assign U519 = ~STATO_REG_3_ | ~CONT1_REG_3_;
  assign U520 = ~STATO_REG_3_ | ~CONT1_REG_2_;
  assign U521 = ~STATO_REG_3_ | ~CONT1_REG_1_;
  assign U522 = ~STATO_REG_3_ | ~CONT1_REG_0_;
  assign U523 = ~U407 | ~U411 | ~U422;
  assign U524 = ~CONT1_REG_5_ | ~U523;
  assign U525 = U408 | U582;
  assign U526 = ~CONT1_REG_4_ | ~U523;
  assign U527 = U408 | U378;
  assign U528 = ~CONT1_REG_3_ | ~U523;
  assign U529 = U408 | U374;
  assign U530 = ~CONT1_REG_2_ | ~U523;
  assign U531 = U408 | U376;
  assign U532 = ~CONT1_REG_1_ | ~U523;
  assign U533 = ~CONT1_REG_0_ | ~U523;
  assign U534 = U408 | U377;
  assign U535 = ~CONT1_REG_8_ | ~U410;
  assign U536 = ~CONT1_REG_7_ | ~U410;
  assign U537 = ~CONT1_REG_6_ | ~U410;
  assign U538 = U410 | STATO_REG_3_;
  assign U539 = ~CONT1_REG_5_ | ~U538;
  assign U540 = ~U403 | ~CONT_REG_5_;
  assign U541 = U411 | U582;
  assign U542 = ~CONT1_REG_4_ | ~U538;
  assign U543 = ~U403 | ~CONT_REG_4_;
  assign U544 = U411 | U378;
  assign U545 = ~CONT1_REG_3_ | ~U538;
  assign U546 = ~U403 | ~CONT_REG_3_;
  assign U547 = U411 | U374;
  assign U548 = ~CONT1_REG_2_ | ~U538;
  assign U549 = ~U403 | ~CONT_REG_2_;
  assign U550 = U411 | U376;
  assign U551 = ~CONT1_REG_1_ | ~U538;
  assign U552 = ~U403 | ~CONT_REG_1_;
  assign U553 = ~CONT1_REG_0_ | ~U538;
  assign U554 = ~U403 | ~CONT_REG_0_;
  assign U555 = U411 | U377;
  assign U556 = ~U372 | ~U375;
  assign U557 = U382 | R_IN_REG_3_;
  assign U558 = U582 | R_IN_REG_3_;
  assign U559 = ~R_IN_REG_4_ | ~U582;
  assign U560 = U375 | R_IN_REG_2_;
  assign U561 = U374 | R_IN_REG_1_;
  assign U562 = U377 | R_IN_REG_4_;
  assign U563 = U376 | R_IN_REG_0_;
  assign U564 = U380 | STATO_REG_0_;
  assign U565 = ~STATO_REG_0_ | ~U373;
  assign U566 = ~STATO_REG_2_ | ~U430;
  assign U567 = STATO_REG_2_ | U384 | STBI;
  assign U568 = ~U380 | ~STBI | ~U372;
  assign U569 = U384 | U380;
  assign U570 = U582 | U399;
  assign U571 = ~X_IN_5_ | ~U399;
  assign U572 = U378 | U399;
  assign U573 = ~X_IN_4_ | ~U399;
  assign U574 = U374 | U399;
  assign U575 = ~X_IN_3_ | ~U399;
  assign U576 = U376 | U399;
  assign U577 = ~X_IN_2_ | ~U399;
  assign U578 = U375 | U399;
  assign U579 = ~X_IN_1_ | ~U399;
  assign U580 = U377 | U399;
  assign U581 = ~X_IN_0_ | ~U399;
  assign U582 = ~R_IN_REG_5_;
  assign R81_U44 = ~U327 | ~R81_U15;
  assign R81_U43 = U328 | U327;
  assign R78_U5 = R78_U78 & R78_U76;
  assign R78_U6 = R78_U75 & R78_U73;
  assign R78_U7 = ~U308;
  assign R78_U8 = ~U319;
  assign R78_U9 = ~U326;
  assign R78_U10 = ~U325;
  assign R78_U11 = ~U324;
  assign R78_U12 = ~U323;
  assign R78_U13 = ~U322;
  assign R78_U14 = ~U321;
  assign R78_U15 = ~U320;
  assign R78_U16 = ~R78_U17 & ~R78_U69;
  assign R78_U17 = R78_U67 & R78_U68;
  assign R78_U18 = ~R78_U126 | ~R78_U125;
  assign R78_U19 = ~R78_U114 | ~R78_U113;
  assign R78_U20 = ~R78_U116 | ~R78_U115;
  assign R78_U21 = ~R78_U118 | ~R78_U117;
  assign R78_U22 = ~R78_U120 | ~R78_U119;
  assign R78_U23 = ~R78_U122 | ~R78_U121;
  assign R78_U24 = ~R78_U124 | ~R78_U123;
  assign R78_U25 = ~U309;
  assign R78_U26 = ~U310;
  assign R78_U27 = ~U317;
  assign R78_U28 = ~U316;
  assign R78_U29 = ~U315;
  assign R78_U30 = ~U314;
  assign R78_U31 = ~U313;
  assign R78_U32 = ~U312;
  assign R78_U33 = ~U311;
  assign R78_U34 = ~R78_U63 | ~R78_U64;
  assign R78_U35 = ~R78_U59 | ~R78_U60;
  assign R78_U36 = ~R78_U55 | ~R78_U56;
  assign R78_U37 = ~R78_U51 | ~R78_U52;
  assign R78_U38 = ~R78_U47 | ~R78_U48;
  assign R78_U39 = ~R78_U43 | ~R78_U44;
  assign R78_U40 = ~R78_U42 | ~R78_U43;
  assign R78_U41 = U319 & R78_U89;
  assign R78_U42 = ~R78_U9 | ~R78_U93 | ~R78_U92;
  assign R78_U43 = ~U326 | ~R78_U94;
  assign R78_U44 = ~U308 | ~R78_U42;
  assign R78_U45 = ~R78_U39;
  assign R78_U46 = ~R78_U10 | ~R78_U96 | ~R78_U95;
  assign R78_U47 = ~U325 | ~R78_U97;
  assign R78_U48 = ~R78_U46 | ~R78_U39;
  assign R78_U49 = ~R78_U38;
  assign R78_U50 = ~R78_U11 | ~R78_U99 | ~R78_U98;
  assign R78_U51 = ~U324 | ~R78_U100;
  assign R78_U52 = ~R78_U50 | ~R78_U38;
  assign R78_U53 = ~R78_U37;
  assign R78_U54 = ~R78_U12 | ~R78_U102 | ~R78_U101;
  assign R78_U55 = ~U323 | ~R78_U103;
  assign R78_U56 = ~R78_U54 | ~R78_U37;
  assign R78_U57 = ~R78_U36;
  assign R78_U58 = ~R78_U13 | ~R78_U105 | ~R78_U104;
  assign R78_U59 = ~U322 | ~R78_U106;
  assign R78_U60 = ~R78_U58 | ~R78_U36;
  assign R78_U61 = ~R78_U35;
  assign R78_U62 = ~R78_U14 | ~R78_U108 | ~R78_U107;
  assign R78_U63 = ~U321 | ~R78_U109;
  assign R78_U64 = ~R78_U62 | ~R78_U35;
  assign R78_U65 = ~R78_U34;
  assign R78_U66 = ~R78_U15 | ~R78_U111 | ~R78_U110;
  assign R78_U67 = ~U320 | ~R78_U112;
  assign R78_U68 = ~R78_U66 | ~R78_U34;
  assign R78_U69 = R78_U8 & R78_U91 & R78_U90;
  assign R78_U70 = R78_U16 | R78_U41;
  assign R78_U71 = ~U318 | ~R78_U70;
  assign R78_U72 = R78_U16 | R78_U41 | U318;
  assign R78_U73 = ~R78_U88 | ~R78_U72 | ~R78_U71;
  assign R78_U74 = ~R78_U72 | ~R78_U71;
  assign R78_U75 = ~R78_U74 | ~R78_U87 | ~R78_U86;
  assign R78_U76 = R78_U127 | R78_U41;
  assign R78_U77 = R78_U69 | R78_U41;
  assign R78_U78 = ~R78_U17 | ~R78_U77;
  assign R78_U79 = ~R78_U40;
  assign R78_U80 = ~R78_U67 | ~R78_U66;
  assign R78_U81 = ~R78_U63 | ~R78_U62;
  assign R78_U82 = ~R78_U59 | ~R78_U58;
  assign R78_U83 = ~R78_U55 | ~R78_U54;
  assign R78_U84 = ~R78_U51 | ~R78_U50;
  assign R78_U85 = ~R78_U47 | ~R78_U46;
  assign R78_U86 = ~U308 | ~R78_U25;
  assign R78_U87 = ~U309 | ~R78_U7;
  assign R78_U88 = ~R78_U87 | ~R78_U86;
  assign R78_U89 = ~R78_U91 | ~R78_U90;
  assign R78_U90 = ~U308 | ~R78_U26;
  assign R78_U91 = ~U310 | ~R78_U7;
  assign R78_U92 = ~U308 | ~R78_U27;
  assign R78_U93 = ~U317 | ~R78_U7;
  assign R78_U94 = ~R78_U93 | ~R78_U92;
  assign R78_U95 = ~U308 | ~R78_U28;
  assign R78_U96 = ~U316 | ~R78_U7;
  assign R78_U97 = ~R78_U96 | ~R78_U95;
  assign R78_U98 = ~U308 | ~R78_U29;
  assign R78_U99 = ~U315 | ~R78_U7;
  assign R78_U100 = ~R78_U99 | ~R78_U98;
  assign R78_U101 = ~U308 | ~R78_U30;
  assign R78_U102 = ~U314 | ~R78_U7;
  assign R78_U103 = ~R78_U102 | ~R78_U101;
  assign R78_U104 = ~U308 | ~R78_U31;
  assign R78_U105 = ~U313 | ~R78_U7;
  assign R78_U106 = ~R78_U105 | ~R78_U104;
  assign R78_U107 = ~U308 | ~R78_U32;
  assign R78_U108 = ~U312 | ~R78_U7;
  assign R78_U109 = ~R78_U108 | ~R78_U107;
  assign R78_U110 = ~U308 | ~R78_U33;
  assign R78_U111 = ~U311 | ~R78_U7;
  assign R78_U112 = ~R78_U111 | ~R78_U110;
  assign R78_U113 = ~R78_U80 | ~R78_U34;
  assign R78_U114 = ~R78_U65 | ~R78_U67 | ~R78_U66;
  assign R78_U115 = ~R78_U81 | ~R78_U35;
  assign R78_U116 = ~R78_U61 | ~R78_U63 | ~R78_U62;
  assign R78_U117 = ~R78_U82 | ~R78_U36;
  assign R78_U118 = ~R78_U57 | ~R78_U59 | ~R78_U58;
  assign R78_U119 = ~R78_U83 | ~R78_U37;
  assign R78_U120 = ~R78_U53 | ~R78_U55 | ~R78_U54;
  assign R78_U121 = ~R78_U84 | ~R78_U38;
  assign R78_U122 = ~R78_U49 | ~R78_U51 | ~R78_U50;
  assign R78_U123 = ~R78_U85 | ~R78_U39;
  assign R78_U124 = ~R78_U45 | ~R78_U47 | ~R78_U46;
  assign R78_U125 = ~U308 | ~R78_U40;
  assign R78_U126 = ~R78_U79 | ~R78_U7;
  assign R78_U127 = ~R78_U16;
  assign R81_U6 = ~R81_U39 | ~R81_U38;
  assign R81_U7 = ~U344;
  assign R81_U8 = ~U334;
  assign R81_U9 = ~U342;
  assign R81_U10 = ~U332;
  assign R81_U11 = ~U340;
  assign R81_U12 = ~U330;
  assign R81_U13 = ~U338;
  assign R81_U14 = ~U328;
  assign R81_U15 = ~U337;
  assign R81_U16 = ~U336;
  assign R81_U17 = ~U335 | ~R81_U7;
  assign R81_U18 = ~U345 | ~R81_U17 | ~R81_U16;
  assign R81_U19 = R81_U7 | U335;
  assign R81_U20 = ~U343 | ~R81_U8;
  assign R81_U21 = ~R81_U20 | ~R81_U19 | ~R81_U18;
  assign R81_U22 = R81_U8 | U343;
  assign R81_U23 = ~U333 | ~R81_U9;
  assign R81_U24 = ~R81_U23 | ~R81_U22 | ~R81_U21;
  assign R81_U25 = R81_U9 | U333;
  assign R81_U26 = ~U341 | ~R81_U10;
  assign R81_U27 = ~R81_U26 | ~R81_U25 | ~R81_U24;
  assign R81_U28 = R81_U10 | U341;
  assign R81_U29 = ~U331 | ~R81_U11;
  assign R81_U30 = ~R81_U29 | ~R81_U28 | ~R81_U27;
  assign R81_U31 = R81_U11 | U331;
  assign R81_U32 = ~U339 | ~R81_U12;
  assign R81_U33 = ~R81_U32 | ~R81_U31 | ~R81_U30;
  assign R81_U34 = R81_U12 | U339;
  assign R81_U35 = ~U329 | ~R81_U13;
  assign R81_U36 = ~R81_U35 | ~R81_U34 | ~R81_U33;
  assign R81_U37 = R81_U13 | U329;
  assign R81_U38 = ~R81_U41 | ~R81_U42 | ~R81_U37 | ~R81_U36;
  assign R81_U39 = ~R81_U40 | ~R81_U44 | ~R81_U43;
  assign R81_U40 = R81_U15 | R81_U14;
  assign R81_U41 = R81_U14 | U337;
  assign R81_U42 = R81_U15 | U328;
  always @ (posedge clock) begin
    R_IN_REG_5_ <= n28;
    R_IN_REG_4_ <= n33;
    R_IN_REG_3_ <= n38;
    R_IN_REG_2_ <= n43;
    R_IN_REG_1_ <= n48;
    R_IN_REG_0_ <= n53;
    CONT_REG_5_ <= n58;
    CONT_REG_4_ <= n63;
    CONT_REG_3_ <= n68;
    CONT_REG_2_ <= n73;
    CONT_REG_1_ <= n78;
    CONT_REG_0_ <= n83;
    CONT1_REG_8_ <= n88;
    CONT1_REG_7_ <= n93;
    CONT1_REG_6_ <= n98;
    CONT1_REG_5_ <= n103;
    CONT1_REG_4_ <= n108;
    CONT1_REG_3_ <= n113;
    CONT1_REG_2_ <= n118;
    CONT1_REG_1_ <= n123;
    CONT1_REG_0_ <= n128;
    X_OUT_REG_5_ <= n133;
    X_OUT_REG_4_ <= n137;
    X_OUT_REG_3_ <= n141;
    X_OUT_REG_2_ <= n145;
    X_OUT_REG_1_ <= n149;
    X_OUT_REG_0_ <= n153;
    STATO_REG_3_ <= n157;
    STATO_REG_2_ <= n162;
    STATO_REG_1_ <= n167;
    STATO_REG_0_ <= n172;
  end
endmodule


