// Benchmark "c499.blif" written by ABC on Mon Apr  8 18:19:54 2019

module \c499.blif  ( 
    Gid0, Gid1, Gid2, Gid3, Gid4, Gid5, Gid6, Gid7, Gid8, Gid9, Gid10,
    Gid11, Gid12, Gid13, Gid14, Gid15, Gid16, Gid17, Gid18, Gid19, Gid20,
    Gid21, Gid22, Gid23, Gid24, Gid25, Gid26, Gid27, Gid28, Gid29, Gid30,
    Gid31, Gic0, Gic1, Gic2, Gic3, Gic4, Gic5, Gic6, Gic7, Gr,
    God0, God1, God2, God3, God4, God5, God6, God7, God8, God9, God10,
    God11, God12, God13, God14, God15, God16, God17, God18, God19, God20,
    God21, God22, God23, God24, God25, God26, God27, God28, God29, God30,
    God31  );
  input  Gid0, Gid1, Gid2, Gid3, Gid4, Gid5, Gid6, Gid7, Gid8, Gid9,
    Gid10, Gid11, Gid12, Gid13, Gid14, Gid15, Gid16, Gid17, Gid18, Gid19,
    Gid20, Gid21, Gid22, Gid23, Gid24, Gid25, Gid26, Gid27, Gid28, Gid29,
    Gid30, Gid31, Gic0, Gic1, Gic2, Gic3, Gic4, Gic5, Gic6, Gic7, Gr;
  output God0, God1, God2, God3, God4, God5, God6, God7, God8, God9, God10,
    God11, God12, God13, God14, God15, God16, God17, God18, God19, God20,
    God21, God22, God23, God24, God25, God26, God27, God28, God29, God30,
    God31;
  wire Gxa0, Gxa1, Gxa2, Gxa3, Gxa4, Gxa5, Gxa6, Gxa7, Gxa8, Gxa9, Gxa10,
    Gxa11, Gxa12, Gxa13, Gxa14, Gxa15, Gh0, Gh1, Gh2, Gh3, Gh4, Gh5, Gh6,
    Gh7, Gxb0, Gxc0, Gxb1, Gxc1, Gxb2, Gxc2, Gxb3, Gxc3, Gxb4, Gxc4, Gxb5,
    Gxc5, Gxb6, Gxc6, Gxb7, Gxc7, Gf0, Gf1, Gf2, Gf3, Gf4, Gf5, Gf6, Gf7,
    Gxe0, Gxe1, Gxe2, Gxe3, Gxe4, Gxe5, Gxe6, Gxe7, Gg0, Gg1, Gg2, Gg3,
    Gg4, Gg5, Gg6, Gg7, Gxd0, Gxd1, Gxd2, Gxd3, Gxd4, Gxd5, Gxd6, Gxd7,
    Gs0, Gs1, Gs2, Gs3, Gs4, Gs5, Gs6, Gs7, Gy0a, Gy1a, Gy2a, Gy0b, Gy1b,
    Gy3b, Gy0c, Gy2c, Gy3c, Gy1d, Gy2d, Gy3d, Gy5i, Gy7i, Gy5j, Gy6j, Gy4k,
    Gy7k, Gy4l, Gy6l, Gy4a, Gy5a, Gy6a, Gy4b, Gy5b, Gy7b, Gy4c, Gy6c, Gy7c,
    Gy5d, Gy6d, Gy7d, Gy1i, Gy3i, Gy1j, Gy2j, Gy0k, Gy3k, Gy0l, Gy2l, Gt0,
    Gt1, Gt2, Gt3, Gt4, Gt5, Gt6, Gt7, Gu0, Gu1, Gwa, Gwb, Gwc, Gwd, Gwe,
    Gwf, Gwg, Gwh, Ge0, Ge1, Ge2, Ge3, Ge4, Ge5, Ge6, Ge7, Ge8, Ge9, Ge10,
    Ge11, Ge12, Ge13, Ge14, Ge15, Ge16, Ge17, Ge18, Ge19, Ge20, Ge21, Ge22,
    Ge23, Ge24, Ge25, Ge26, Ge27, Ge28, Ge29, Ge30, Ge31;
  assign God0 = Gid0 ^ Ge0;
  assign God1 = Gid1 ^ Ge1;
  assign God2 = Gid2 ^ Ge2;
  assign God3 = Gid3 ^ Ge3;
  assign God4 = Gid4 ^ Ge4;
  assign God5 = Gid5 ^ Ge5;
  assign God6 = Gid6 ^ Ge6;
  assign God7 = Gid7 ^ Ge7;
  assign God8 = Gid8 ^ Ge8;
  assign God9 = Gid9 ^ Ge9;
  assign God10 = Gid10 ^ Ge10;
  assign God11 = Gid11 ^ Ge11;
  assign God12 = Gid12 ^ Ge12;
  assign God13 = Gid13 ^ Ge13;
  assign God14 = Gid14 ^ Ge14;
  assign God15 = Gid15 ^ Ge15;
  assign God16 = Gid16 ^ Ge16;
  assign God17 = Gid17 ^ Ge17;
  assign God18 = Gid18 ^ Ge18;
  assign God19 = Gid19 ^ Ge19;
  assign God20 = Gid20 ^ Ge20;
  assign God21 = Gid21 ^ Ge21;
  assign God22 = Gid22 ^ Ge22;
  assign God23 = Gid23 ^ Ge23;
  assign God24 = Gid24 ^ Ge24;
  assign God25 = Gid25 ^ Ge25;
  assign God26 = Gid26 ^ Ge26;
  assign God27 = Gid27 ^ Ge27;
  assign God28 = Gid28 ^ Ge28;
  assign God29 = Gid29 ^ Ge29;
  assign God30 = Gid30 ^ Ge30;
  assign God31 = Gid31 ^ Ge31;
  assign Gxa0 = Gid0 ^ Gid1;
  assign Gxa1 = Gid2 ^ Gid3;
  assign Gxa2 = Gid4 ^ Gid5;
  assign Gxa3 = Gid6 ^ Gid7;
  assign Gxa4 = Gid8 ^ Gid9;
  assign Gxa5 = Gid10 ^ Gid11;
  assign Gxa6 = Gid12 ^ Gid13;
  assign Gxa7 = Gid14 ^ Gid15;
  assign Gxa8 = Gid16 ^ Gid17;
  assign Gxa9 = Gid18 ^ Gid19;
  assign Gxa10 = Gid20 ^ Gid21;
  assign Gxa11 = Gid22 ^ Gid23;
  assign Gxa12 = Gid24 ^ Gid25;
  assign Gxa13 = Gid26 ^ Gid27;
  assign Gxa14 = Gid28 ^ Gid29;
  assign Gxa15 = Gid30 ^ Gid31;
  assign Gh0 = Gic0 & Gr;
  assign Gh1 = Gic1 & Gr;
  assign Gh2 = Gic2 & Gr;
  assign Gh3 = Gic3 & Gr;
  assign Gh4 = Gic4 & Gr;
  assign Gh5 = Gic5 & Gr;
  assign Gh6 = Gic6 & Gr;
  assign Gh7 = Gic7 & Gr;
  assign Gxb0 = Gid0 ^ Gid4;
  assign Gxc0 = Gid8 ^ Gid12;
  assign Gxb1 = Gid1 ^ Gid5;
  assign Gxc1 = Gid9 ^ Gid13;
  assign Gxb2 = Gid2 ^ Gid6;
  assign Gxc2 = Gid10 ^ Gid14;
  assign Gxb3 = Gid3 ^ Gid7;
  assign Gxc3 = Gid11 ^ Gid15;
  assign Gxb4 = Gid16 ^ Gid20;
  assign Gxc4 = Gid24 ^ Gid28;
  assign Gxb5 = Gid17 ^ Gid21;
  assign Gxc5 = Gid25 ^ Gid29;
  assign Gxb6 = Gid18 ^ Gid22;
  assign Gxc6 = Gid26 ^ Gid30;
  assign Gxb7 = Gid19 ^ Gid23;
  assign Gxc7 = Gid27 ^ Gid31;
  assign Gf0 = Gxa0 ^ Gxa1;
  assign Gf1 = Gxa2 ^ Gxa3;
  assign Gf2 = Gxa4 ^ Gxa5;
  assign Gf3 = Gxa6 ^ Gxa7;
  assign Gf4 = Gxa8 ^ Gxa9;
  assign Gf5 = Gxa10 ^ Gxa11;
  assign Gf6 = Gxa12 ^ Gxa13;
  assign Gf7 = Gxa14 ^ Gxa15;
  assign Gxe0 = Gxb0 ^ Gxc0;
  assign Gxe1 = Gxb1 ^ Gxc1;
  assign Gxe2 = Gxb2 ^ Gxc2;
  assign Gxe3 = Gxb3 ^ Gxc3;
  assign Gxe4 = Gxb4 ^ Gxc4;
  assign Gxe5 = Gxb5 ^ Gxc5;
  assign Gxe6 = Gxb6 ^ Gxc6;
  assign Gxe7 = Gxb7 ^ Gxc7;
  assign Gg0 = Gf0 ^ Gf1;
  assign Gg1 = Gf2 ^ Gf3;
  assign Gg2 = Gf0 ^ Gf2;
  assign Gg3 = Gf1 ^ Gf3;
  assign Gg4 = Gf4 ^ Gf5;
  assign Gg5 = Gf6 ^ Gf7;
  assign Gg6 = Gf4 ^ Gf6;
  assign Gg7 = Gf5 ^ Gf7;
  assign Gxd0 = Gh0 ^ Gg4;
  assign Gxd1 = Gh1 ^ Gg5;
  assign Gxd2 = Gh2 ^ Gg6;
  assign Gxd3 = Gh3 ^ Gg7;
  assign Gxd4 = Gh4 ^ Gg0;
  assign Gxd5 = Gh5 ^ Gg1;
  assign Gxd6 = Gh6 ^ Gg2;
  assign Gxd7 = Gh7 ^ Gg3;
  assign Gs0 = Gxe0 ^ Gxd0;
  assign Gs1 = Gxe1 ^ Gxd1;
  assign Gs2 = Gxe2 ^ Gxd2;
  assign Gs3 = Gxe3 ^ Gxd3;
  assign Gs4 = Gxe4 ^ Gxd4;
  assign Gs5 = Gxe5 ^ Gxd5;
  assign Gs6 = Gxe6 ^ Gxd6;
  assign Gs7 = Gxe7 ^ Gxd7;
  assign Gy0a = ~Gs0;
  assign Gy1a = ~Gs1;
  assign Gy2a = ~Gs2;
  assign Gy0b = ~Gs0;
  assign Gy1b = ~Gs1;
  assign Gy3b = ~Gs3;
  assign Gy0c = ~Gs0;
  assign Gy2c = ~Gs2;
  assign Gy3c = ~Gs3;
  assign Gy1d = ~Gs1;
  assign Gy2d = ~Gs2;
  assign Gy3d = ~Gs3;
  assign Gy5i = ~Gs5;
  assign Gy7i = ~Gs7;
  assign Gy5j = ~Gs5;
  assign Gy6j = ~Gs6;
  assign Gy4k = ~Gs4;
  assign Gy7k = ~Gs7;
  assign Gy4l = ~Gs4;
  assign Gy6l = ~Gs6;
  assign Gy4a = ~Gs4;
  assign Gy5a = ~Gs5;
  assign Gy6a = ~Gs6;
  assign Gy4b = ~Gs4;
  assign Gy5b = ~Gs5;
  assign Gy7b = ~Gs7;
  assign Gy4c = ~Gs4;
  assign Gy6c = ~Gs6;
  assign Gy7c = ~Gs7;
  assign Gy5d = ~Gs5;
  assign Gy6d = ~Gs6;
  assign Gy7d = ~Gs7;
  assign Gy1i = ~Gs1;
  assign Gy3i = ~Gs3;
  assign Gy1j = ~Gs1;
  assign Gy2j = ~Gs2;
  assign Gy0k = ~Gs0;
  assign Gy3k = ~Gs3;
  assign Gy0l = ~Gs0;
  assign Gy2l = ~Gs2;
  assign Gt0 = Gs3 & Gy2a & Gy0a & Gy1a;
  assign Gt1 = Gy3b & Gs2 & Gy0b & Gy1b;
  assign Gt2 = Gy3c & Gy2c & Gy0c & Gs1;
  assign Gt3 = Gy3d & Gy2d & Gs0 & Gy1d;
  assign Gt4 = Gs7 & Gy6a & Gy4a & Gy5a;
  assign Gt5 = Gy7b & Gs6 & Gy4b & Gy5b;
  assign Gt6 = Gy7c & Gy6c & Gy4c & Gs5;
  assign Gt7 = Gy7d & Gy6d & Gs4 & Gy5d;
  assign Gu0 = Gt2 | Gt3 | Gt0 | Gt1;
  assign Gu1 = Gt6 | Gt7 | Gt4 | Gt5;
  assign Gwa = Gu0 & Gy7i & Gs6 & Gs4 & Gy5i;
  assign Gwb = Gu0 & Gs7 & Gy6j & Gs4 & Gy5j;
  assign Gwc = Gu0 & Gy7k & Gs6 & Gy4k & Gs5;
  assign Gwd = Gu0 & Gs7 & Gy6l & Gy4l & Gs5;
  assign Gwe = Gu1 & Gy3i & Gs2 & Gs0 & Gy1i;
  assign Gwf = Gu1 & Gs3 & Gy2j & Gs0 & Gy1j;
  assign Gwg = Gu1 & Gy3k & Gs2 & Gy0k & Gs1;
  assign Gwh = Gu1 & Gs3 & Gy2l & Gy0l & Gs1;
  assign Ge0 = Gs0 & Gwa;
  assign Ge1 = Gs1 & Gwa;
  assign Ge2 = Gs2 & Gwa;
  assign Ge3 = Gs3 & Gwa;
  assign Ge4 = Gs0 & Gwb;
  assign Ge5 = Gs1 & Gwb;
  assign Ge6 = Gs2 & Gwb;
  assign Ge7 = Gs3 & Gwb;
  assign Ge8 = Gs0 & Gwc;
  assign Ge9 = Gs1 & Gwc;
  assign Ge10 = Gs2 & Gwc;
  assign Ge11 = Gs3 & Gwc;
  assign Ge12 = Gs0 & Gwd;
  assign Ge13 = Gs1 & Gwd;
  assign Ge14 = Gs2 & Gwd;
  assign Ge15 = Gs3 & Gwd;
  assign Ge16 = Gs4 & Gwe;
  assign Ge17 = Gs5 & Gwe;
  assign Ge18 = Gs6 & Gwe;
  assign Ge19 = Gs7 & Gwe;
  assign Ge20 = Gs4 & Gwf;
  assign Ge21 = Gs5 & Gwf;
  assign Ge22 = Gs6 & Gwf;
  assign Ge23 = Gs7 & Gwf;
  assign Ge24 = Gs4 & Gwg;
  assign Ge25 = Gs5 & Gwg;
  assign Ge26 = Gs6 & Gwg;
  assign Ge27 = Gs7 & Gwg;
  assign Ge28 = Gs4 & Gwh;
  assign Ge29 = Gs5 & Gwh;
  assign Ge30 = Gs6 & Gwh;
  assign Ge31 = Gs7 & Gwh;
endmodule


