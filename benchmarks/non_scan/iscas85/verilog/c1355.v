// Benchmark "c1355.blif" written by ABC on Mon Apr  8 18:20:21 2019

module \c1355.blif  ( 
    G1gat, G8gat, G15gat, G22gat, G29gat, G36gat, G43gat, G50gat, G57gat,
    G64gat, G71gat, G78gat, G85gat, G92gat, G99gat, G106gat, G113gat,
    G120gat, G127gat, G134gat, G141gat, G148gat, G155gat, G162gat, G169gat,
    G176gat, G183gat, G190gat, G197gat, G204gat, G211gat, G218gat, G225gat,
    G226gat, G227gat, G228gat, G229gat, G230gat, G231gat, G232gat, G233gat,
    G1324gat, G1325gat, G1326gat, G1327gat, G1328gat, G1329gat, G1330gat,
    G1331gat, G1332gat, G1333gat, G1334gat, G1335gat, G1336gat, G1337gat,
    G1338gat, G1339gat, G1340gat, G1341gat, G1342gat, G1343gat, G1344gat,
    G1345gat, G1346gat, G1347gat, G1348gat, G1349gat, G1350gat, G1351gat,
    G1352gat, G1353gat, G1354gat, G1355gat  );
  input  G1gat, G8gat, G15gat, G22gat, G29gat, G36gat, G43gat, G50gat,
    G57gat, G64gat, G71gat, G78gat, G85gat, G92gat, G99gat, G106gat,
    G113gat, G120gat, G127gat, G134gat, G141gat, G148gat, G155gat, G162gat,
    G169gat, G176gat, G183gat, G190gat, G197gat, G204gat, G211gat, G218gat,
    G225gat, G226gat, G227gat, G228gat, G229gat, G230gat, G231gat, G232gat,
    G233gat;
  output G1324gat, G1325gat, G1326gat, G1327gat, G1328gat, G1329gat, G1330gat,
    G1331gat, G1332gat, G1333gat, G1334gat, G1335gat, G1336gat, G1337gat,
    G1338gat, G1339gat, G1340gat, G1341gat, G1342gat, G1343gat, G1344gat,
    G1345gat, G1346gat, G1347gat, G1348gat, G1349gat, G1350gat, G1351gat,
    G1352gat, G1353gat, G1354gat, G1355gat;
  wire G242gat, G245gat, G248gat, G251gat, G254gat, G257gat, G260gat,
    G263gat, G266gat, G269gat, G272gat, G275gat, G278gat, G281gat, G284gat,
    G287gat, G290gat, G293gat, G296gat, G299gat, G302gat, G305gat, G308gat,
    G311gat, G314gat, G317gat, G320gat, G323gat, G326gat, G329gat, G332gat,
    G335gat, G338gat, G341gat, G344gat, G347gat, G350gat, G353gat, G356gat,
    G359gat, G362gat, G363gat, G364gat, G365gat, G366gat, G367gat, G368gat,
    G369gat, G370gat, G371gat, G372gat, G373gat, G374gat, G375gat, G376gat,
    G377gat, G378gat, G379gat, G380gat, G381gat, G382gat, G383gat, G384gat,
    G385gat, G386gat, G387gat, G388gat, G389gat, G390gat, G391gat, G392gat,
    G393gat, G394gat, G395gat, G396gat, G397gat, G398gat, G399gat, G400gat,
    G401gat, G402gat, G403gat, G404gat, G405gat, G406gat, G407gat, G408gat,
    G409gat, G410gat, G411gat, G412gat, G413gat, G414gat, G415gat, G416gat,
    G417gat, G418gat, G419gat, G420gat, G421gat, G422gat, G423gat, G424gat,
    G425gat, G426gat, G429gat, G432gat, G435gat, G438gat, G441gat, G444gat,
    G447gat, G450gat, G453gat, G456gat, G459gat, G462gat, G465gat, G468gat,
    G471gat, G474gat, G477gat, G480gat, G483gat, G486gat, G489gat, G492gat,
    G495gat, G498gat, G501gat, G504gat, G507gat, G510gat, G513gat, G516gat,
    G519gat, G522gat, G525gat, G528gat, G531gat, G534gat, G537gat, G540gat,
    G543gat, G546gat, G549gat, G552gat, G555gat, G558gat, G561gat, G564gat,
    G567gat, G570gat, G571gat, G572gat, G573gat, G574gat, G575gat, G576gat,
    G577gat, G578gat, G579gat, G580gat, G581gat, G582gat, G583gat, G584gat,
    G585gat, G586gat, G587gat, G588gat, G589gat, G590gat, G591gat, G592gat,
    G593gat, G594gat, G595gat, G596gat, G597gat, G598gat, G599gat, G600gat,
    G601gat, G602gat, G607gat, G612gat, G617gat, G622gat, G627gat, G632gat,
    G637gat, G642gat, G645gat, G648gat, G651gat, G654gat, G657gat, G660gat,
    G663gat, G666gat, G669gat, G672gat, G675gat, G678gat, G681gat, G684gat,
    G687gat, G690gat, G691gat, G692gat, G693gat, G694gat, G695gat, G696gat,
    G697gat, G698gat, G699gat, G700gat, G701gat, G702gat, G703gat, G704gat,
    G705gat, G706gat, G709gat, G712gat, G715gat, G718gat, G721gat, G724gat,
    G727gat, G730gat, G733gat, G736gat, G739gat, G742gat, G745gat, G748gat,
    G751gat, G754gat, G755gat, G756gat, G757gat, G758gat, G759gat, G760gat,
    G761gat, G762gat, G763gat, G764gat, G765gat, G766gat, G767gat, G768gat,
    G769gat, G770gat, G773gat, G776gat, G779gat, G782gat, G785gat, G788gat,
    G791gat, G794gat, G797gat, G800gat, G803gat, G806gat, G809gat, G812gat,
    G815gat, G818gat, G819gat, G820gat, G821gat, G822gat, G823gat, G824gat,
    G825gat, G826gat, G827gat, G828gat, G829gat, G830gat, G831gat, G832gat,
    G833gat, G834gat, G847gat, G860gat, G873gat, G886gat, G899gat, G912gat,
    G925gat, G938gat, G939gat, G940gat, G941gat, G942gat, G943gat, G944gat,
    G945gat, G946gat, G947gat, G948gat, G949gat, G950gat, G951gat, G952gat,
    G953gat, G954gat, G955gat, G956gat, G957gat, G958gat, G959gat, G960gat,
    G961gat, G962gat, G963gat, G964gat, G965gat, G966gat, G967gat, G968gat,
    G969gat, G970gat, G971gat, G972gat, G973gat, G974gat, G975gat, G976gat,
    G977gat, G978gat, G979gat, G980gat, G981gat, G982gat, G983gat, G984gat,
    G985gat, G986gat, G991gat, G996gat, G1001gat, G1006gat, G1011gat,
    G1016gat, G1021gat, G1026gat, G1031gat, G1036gat, G1039gat, G1042gat,
    G1045gat, G1048gat, G1051gat, G1054gat, G1057gat, G1060gat, G1063gat,
    G1066gat, G1069gat, G1072gat, G1075gat, G1078gat, G1081gat, G1084gat,
    G1087gat, G1090gat, G1093gat, G1096gat, G1099gat, G1102gat, G1105gat,
    G1108gat, G1111gat, G1114gat, G1117gat, G1120gat, G1123gat, G1126gat,
    G1129gat, G1132gat, G1135gat, G1138gat, G1141gat, G1144gat, G1147gat,
    G1150gat, G1153gat, G1156gat, G1159gat, G1162gat, G1165gat, G1168gat,
    G1171gat, G1174gat, G1177gat, G1180gat, G1183gat, G1186gat, G1189gat,
    G1192gat, G1195gat, G1198gat, G1201gat, G1204gat, G1207gat, G1210gat,
    G1213gat, G1216gat, G1219gat, G1222gat, G1225gat, G1228gat, G1229gat,
    G1230gat, G1231gat, G1232gat, G1233gat, G1234gat, G1235gat, G1236gat,
    G1237gat, G1238gat, G1239gat, G1240gat, G1241gat, G1242gat, G1243gat,
    G1244gat, G1245gat, G1246gat, G1247gat, G1248gat, G1249gat, G1250gat,
    G1251gat, G1252gat, G1253gat, G1254gat, G1255gat, G1256gat, G1257gat,
    G1258gat, G1259gat, G1260gat, G1261gat, G1262gat, G1263gat, G1264gat,
    G1265gat, G1266gat, G1267gat, G1268gat, G1269gat, G1270gat, G1271gat,
    G1272gat, G1273gat, G1274gat, G1275gat, G1276gat, G1277gat, G1278gat,
    G1279gat, G1280gat, G1281gat, G1282gat, G1283gat, G1284gat, G1285gat,
    G1286gat, G1287gat, G1288gat, G1289gat, G1290gat, G1291gat;
  assign G242gat = G225gat & G233gat;
  assign G245gat = G226gat & G233gat;
  assign G248gat = G227gat & G233gat;
  assign G251gat = G228gat & G233gat;
  assign G254gat = G229gat & G233gat;
  assign G257gat = G230gat & G233gat;
  assign G260gat = G231gat & G233gat;
  assign G263gat = G232gat & G233gat;
  assign G266gat = ~G1gat | ~G8gat;
  assign G269gat = ~G15gat | ~G22gat;
  assign G272gat = ~G29gat | ~G36gat;
  assign G275gat = ~G43gat | ~G50gat;
  assign G278gat = ~G57gat | ~G64gat;
  assign G281gat = ~G71gat | ~G78gat;
  assign G284gat = ~G85gat | ~G92gat;
  assign G287gat = ~G99gat | ~G106gat;
  assign G290gat = ~G113gat | ~G120gat;
  assign G293gat = ~G127gat | ~G134gat;
  assign G296gat = ~G141gat | ~G148gat;
  assign G299gat = ~G155gat | ~G162gat;
  assign G302gat = ~G169gat | ~G176gat;
  assign G305gat = ~G183gat | ~G190gat;
  assign G308gat = ~G197gat | ~G204gat;
  assign G311gat = ~G211gat | ~G218gat;
  assign G314gat = ~G1gat | ~G29gat;
  assign G317gat = ~G57gat | ~G85gat;
  assign G320gat = ~G8gat | ~G36gat;
  assign G323gat = ~G64gat | ~G92gat;
  assign G326gat = ~G15gat | ~G43gat;
  assign G329gat = ~G71gat | ~G99gat;
  assign G332gat = ~G22gat | ~G50gat;
  assign G335gat = ~G78gat | ~G106gat;
  assign G338gat = ~G113gat | ~G141gat;
  assign G341gat = ~G169gat | ~G197gat;
  assign G344gat = ~G120gat | ~G148gat;
  assign G347gat = ~G176gat | ~G204gat;
  assign G350gat = ~G127gat | ~G155gat;
  assign G353gat = ~G183gat | ~G211gat;
  assign G356gat = ~G134gat | ~G162gat;
  assign G359gat = ~G190gat | ~G218gat;
  assign G362gat = ~G1gat | ~G266gat;
  assign G363gat = ~G8gat | ~G266gat;
  assign G364gat = ~G15gat | ~G269gat;
  assign G365gat = ~G22gat | ~G269gat;
  assign G366gat = ~G29gat | ~G272gat;
  assign G367gat = ~G36gat | ~G272gat;
  assign G368gat = ~G43gat | ~G275gat;
  assign G369gat = ~G50gat | ~G275gat;
  assign G370gat = ~G57gat | ~G278gat;
  assign G371gat = ~G64gat | ~G278gat;
  assign G372gat = ~G71gat | ~G281gat;
  assign G373gat = ~G78gat | ~G281gat;
  assign G374gat = ~G85gat | ~G284gat;
  assign G375gat = ~G92gat | ~G284gat;
  assign G376gat = ~G99gat | ~G287gat;
  assign G377gat = ~G106gat | ~G287gat;
  assign G378gat = ~G113gat | ~G290gat;
  assign G379gat = ~G120gat | ~G290gat;
  assign G380gat = ~G127gat | ~G293gat;
  assign G381gat = ~G134gat | ~G293gat;
  assign G382gat = ~G141gat | ~G296gat;
  assign G383gat = ~G148gat | ~G296gat;
  assign G384gat = ~G155gat | ~G299gat;
  assign G385gat = ~G162gat | ~G299gat;
  assign G386gat = ~G169gat | ~G302gat;
  assign G387gat = ~G176gat | ~G302gat;
  assign G388gat = ~G183gat | ~G305gat;
  assign G389gat = ~G190gat | ~G305gat;
  assign G390gat = ~G197gat | ~G308gat;
  assign G391gat = ~G204gat | ~G308gat;
  assign G392gat = ~G211gat | ~G311gat;
  assign G393gat = ~G218gat | ~G311gat;
  assign G394gat = ~G1gat | ~G314gat;
  assign G395gat = ~G29gat | ~G314gat;
  assign G396gat = ~G57gat | ~G317gat;
  assign G397gat = ~G85gat | ~G317gat;
  assign G398gat = ~G8gat | ~G320gat;
  assign G399gat = ~G36gat | ~G320gat;
  assign G400gat = ~G64gat | ~G323gat;
  assign G401gat = ~G92gat | ~G323gat;
  assign G402gat = ~G15gat | ~G326gat;
  assign G403gat = ~G43gat | ~G326gat;
  assign G404gat = ~G71gat | ~G329gat;
  assign G405gat = ~G99gat | ~G329gat;
  assign G406gat = ~G22gat | ~G332gat;
  assign G407gat = ~G50gat | ~G332gat;
  assign G408gat = ~G78gat | ~G335gat;
  assign G409gat = ~G106gat | ~G335gat;
  assign G410gat = ~G113gat | ~G338gat;
  assign G411gat = ~G141gat | ~G338gat;
  assign G412gat = ~G169gat | ~G341gat;
  assign G413gat = ~G197gat | ~G341gat;
  assign G414gat = ~G120gat | ~G344gat;
  assign G415gat = ~G148gat | ~G344gat;
  assign G416gat = ~G176gat | ~G347gat;
  assign G417gat = ~G204gat | ~G347gat;
  assign G418gat = ~G127gat | ~G350gat;
  assign G419gat = ~G155gat | ~G350gat;
  assign G420gat = ~G183gat | ~G353gat;
  assign G421gat = ~G211gat | ~G353gat;
  assign G422gat = ~G134gat | ~G356gat;
  assign G423gat = ~G162gat | ~G356gat;
  assign G424gat = ~G190gat | ~G359gat;
  assign G425gat = ~G218gat | ~G359gat;
  assign G426gat = ~G362gat | ~G363gat;
  assign G429gat = ~G364gat | ~G365gat;
  assign G432gat = ~G366gat | ~G367gat;
  assign G435gat = ~G368gat | ~G369gat;
  assign G438gat = ~G370gat | ~G371gat;
  assign G441gat = ~G372gat | ~G373gat;
  assign G444gat = ~G374gat | ~G375gat;
  assign G447gat = ~G376gat | ~G377gat;
  assign G450gat = ~G378gat | ~G379gat;
  assign G453gat = ~G380gat | ~G381gat;
  assign G456gat = ~G382gat | ~G383gat;
  assign G459gat = ~G384gat | ~G385gat;
  assign G462gat = ~G386gat | ~G387gat;
  assign G465gat = ~G388gat | ~G389gat;
  assign G468gat = ~G390gat | ~G391gat;
  assign G471gat = ~G392gat | ~G393gat;
  assign G474gat = ~G394gat | ~G395gat;
  assign G477gat = ~G396gat | ~G397gat;
  assign G480gat = ~G398gat | ~G399gat;
  assign G483gat = ~G400gat | ~G401gat;
  assign G486gat = ~G402gat | ~G403gat;
  assign G489gat = ~G404gat | ~G405gat;
  assign G492gat = ~G406gat | ~G407gat;
  assign G495gat = ~G408gat | ~G409gat;
  assign G498gat = ~G410gat | ~G411gat;
  assign G501gat = ~G412gat | ~G413gat;
  assign G504gat = ~G414gat | ~G415gat;
  assign G507gat = ~G416gat | ~G417gat;
  assign G510gat = ~G418gat | ~G419gat;
  assign G513gat = ~G420gat | ~G421gat;
  assign G516gat = ~G422gat | ~G423gat;
  assign G519gat = ~G424gat | ~G425gat;
  assign G522gat = ~G426gat | ~G429gat;
  assign G525gat = ~G432gat | ~G435gat;
  assign G528gat = ~G438gat | ~G441gat;
  assign G531gat = ~G444gat | ~G447gat;
  assign G534gat = ~G450gat | ~G453gat;
  assign G537gat = ~G456gat | ~G459gat;
  assign G540gat = ~G462gat | ~G465gat;
  assign G543gat = ~G468gat | ~G471gat;
  assign G546gat = ~G474gat | ~G477gat;
  assign G549gat = ~G480gat | ~G483gat;
  assign G552gat = ~G486gat | ~G489gat;
  assign G555gat = ~G492gat | ~G495gat;
  assign G558gat = ~G498gat | ~G501gat;
  assign G561gat = ~G504gat | ~G507gat;
  assign G564gat = ~G510gat | ~G513gat;
  assign G567gat = ~G516gat | ~G519gat;
  assign G570gat = ~G426gat | ~G522gat;
  assign G571gat = ~G429gat | ~G522gat;
  assign G572gat = ~G432gat | ~G525gat;
  assign G573gat = ~G435gat | ~G525gat;
  assign G574gat = ~G438gat | ~G528gat;
  assign G575gat = ~G441gat | ~G528gat;
  assign G576gat = ~G444gat | ~G531gat;
  assign G577gat = ~G447gat | ~G531gat;
  assign G578gat = ~G450gat | ~G534gat;
  assign G579gat = ~G453gat | ~G534gat;
  assign G580gat = ~G456gat | ~G537gat;
  assign G581gat = ~G459gat | ~G537gat;
  assign G582gat = ~G462gat | ~G540gat;
  assign G583gat = ~G465gat | ~G540gat;
  assign G584gat = ~G468gat | ~G543gat;
  assign G585gat = ~G471gat | ~G543gat;
  assign G586gat = ~G474gat | ~G546gat;
  assign G587gat = ~G477gat | ~G546gat;
  assign G588gat = ~G480gat | ~G549gat;
  assign G589gat = ~G483gat | ~G549gat;
  assign G590gat = ~G486gat | ~G552gat;
  assign G591gat = ~G489gat | ~G552gat;
  assign G592gat = ~G492gat | ~G555gat;
  assign G593gat = ~G495gat | ~G555gat;
  assign G594gat = ~G498gat | ~G558gat;
  assign G595gat = ~G501gat | ~G558gat;
  assign G596gat = ~G504gat | ~G561gat;
  assign G597gat = ~G507gat | ~G561gat;
  assign G598gat = ~G510gat | ~G564gat;
  assign G599gat = ~G513gat | ~G564gat;
  assign G600gat = ~G516gat | ~G567gat;
  assign G601gat = ~G519gat | ~G567gat;
  assign G602gat = ~G570gat | ~G571gat;
  assign G607gat = ~G572gat | ~G573gat;
  assign G612gat = ~G574gat | ~G575gat;
  assign G617gat = ~G576gat | ~G577gat;
  assign G622gat = ~G578gat | ~G579gat;
  assign G627gat = ~G580gat | ~G581gat;
  assign G632gat = ~G582gat | ~G583gat;
  assign G637gat = ~G584gat | ~G585gat;
  assign G642gat = ~G586gat | ~G587gat;
  assign G645gat = ~G588gat | ~G589gat;
  assign G648gat = ~G590gat | ~G591gat;
  assign G651gat = ~G592gat | ~G593gat;
  assign G654gat = ~G594gat | ~G595gat;
  assign G657gat = ~G596gat | ~G597gat;
  assign G660gat = ~G598gat | ~G599gat;
  assign G663gat = ~G600gat | ~G601gat;
  assign G666gat = ~G602gat | ~G607gat;
  assign G669gat = ~G612gat | ~G617gat;
  assign G672gat = ~G602gat | ~G612gat;
  assign G675gat = ~G607gat | ~G617gat;
  assign G678gat = ~G622gat | ~G627gat;
  assign G681gat = ~G632gat | ~G637gat;
  assign G684gat = ~G622gat | ~G632gat;
  assign G687gat = ~G627gat | ~G637gat;
  assign G690gat = ~G602gat | ~G666gat;
  assign G691gat = ~G607gat | ~G666gat;
  assign G692gat = ~G612gat | ~G669gat;
  assign G693gat = ~G617gat | ~G669gat;
  assign G694gat = ~G602gat | ~G672gat;
  assign G695gat = ~G612gat | ~G672gat;
  assign G696gat = ~G607gat | ~G675gat;
  assign G697gat = ~G617gat | ~G675gat;
  assign G698gat = ~G622gat | ~G678gat;
  assign G699gat = ~G627gat | ~G678gat;
  assign G700gat = ~G632gat | ~G681gat;
  assign G701gat = ~G637gat | ~G681gat;
  assign G702gat = ~G622gat | ~G684gat;
  assign G703gat = ~G632gat | ~G684gat;
  assign G704gat = ~G627gat | ~G687gat;
  assign G705gat = ~G637gat | ~G687gat;
  assign G706gat = ~G690gat | ~G691gat;
  assign G709gat = ~G692gat | ~G693gat;
  assign G712gat = ~G694gat | ~G695gat;
  assign G715gat = ~G696gat | ~G697gat;
  assign G718gat = ~G698gat | ~G699gat;
  assign G721gat = ~G700gat | ~G701gat;
  assign G724gat = ~G702gat | ~G703gat;
  assign G727gat = ~G704gat | ~G705gat;
  assign G730gat = ~G242gat | ~G718gat;
  assign G733gat = ~G245gat | ~G721gat;
  assign G736gat = ~G248gat | ~G724gat;
  assign G739gat = ~G251gat | ~G727gat;
  assign G742gat = ~G254gat | ~G706gat;
  assign G745gat = ~G257gat | ~G709gat;
  assign G748gat = ~G260gat | ~G712gat;
  assign G751gat = ~G263gat | ~G715gat;
  assign G754gat = ~G242gat | ~G730gat;
  assign G755gat = ~G718gat | ~G730gat;
  assign G756gat = ~G245gat | ~G733gat;
  assign G757gat = ~G721gat | ~G733gat;
  assign G758gat = ~G248gat | ~G736gat;
  assign G759gat = ~G724gat | ~G736gat;
  assign G760gat = ~G251gat | ~G739gat;
  assign G761gat = ~G727gat | ~G739gat;
  assign G762gat = ~G254gat | ~G742gat;
  assign G763gat = ~G706gat | ~G742gat;
  assign G764gat = ~G257gat | ~G745gat;
  assign G765gat = ~G709gat | ~G745gat;
  assign G766gat = ~G260gat | ~G748gat;
  assign G767gat = ~G712gat | ~G748gat;
  assign G768gat = ~G263gat | ~G751gat;
  assign G769gat = ~G715gat | ~G751gat;
  assign G770gat = ~G754gat | ~G755gat;
  assign G773gat = ~G756gat | ~G757gat;
  assign G776gat = ~G758gat | ~G759gat;
  assign G779gat = ~G760gat | ~G761gat;
  assign G782gat = ~G762gat | ~G763gat;
  assign G785gat = ~G764gat | ~G765gat;
  assign G788gat = ~G766gat | ~G767gat;
  assign G791gat = ~G768gat | ~G769gat;
  assign G794gat = ~G642gat | ~G770gat;
  assign G797gat = ~G645gat | ~G773gat;
  assign G800gat = ~G648gat | ~G776gat;
  assign G803gat = ~G651gat | ~G779gat;
  assign G806gat = ~G654gat | ~G782gat;
  assign G809gat = ~G657gat | ~G785gat;
  assign G812gat = ~G660gat | ~G788gat;
  assign G815gat = ~G663gat | ~G791gat;
  assign G818gat = ~G642gat | ~G794gat;
  assign G819gat = ~G770gat | ~G794gat;
  assign G820gat = ~G645gat | ~G797gat;
  assign G821gat = ~G773gat | ~G797gat;
  assign G822gat = ~G648gat | ~G800gat;
  assign G823gat = ~G776gat | ~G800gat;
  assign G824gat = ~G651gat | ~G803gat;
  assign G825gat = ~G779gat | ~G803gat;
  assign G826gat = ~G654gat | ~G806gat;
  assign G827gat = ~G782gat | ~G806gat;
  assign G828gat = ~G657gat | ~G809gat;
  assign G829gat = ~G785gat | ~G809gat;
  assign G830gat = ~G660gat | ~G812gat;
  assign G831gat = ~G788gat | ~G812gat;
  assign G832gat = ~G663gat | ~G815gat;
  assign G833gat = ~G791gat | ~G815gat;
  assign G834gat = ~G818gat | ~G819gat;
  assign G847gat = ~G820gat | ~G821gat;
  assign G860gat = ~G822gat | ~G823gat;
  assign G873gat = ~G824gat | ~G825gat;
  assign G886gat = ~G828gat | ~G829gat;
  assign G899gat = ~G832gat | ~G833gat;
  assign G912gat = ~G830gat | ~G831gat;
  assign G925gat = ~G826gat | ~G827gat;
  assign G938gat = ~G834gat;
  assign G939gat = ~G847gat;
  assign G940gat = ~G860gat;
  assign G941gat = ~G834gat;
  assign G942gat = ~G847gat;
  assign G943gat = ~G873gat;
  assign G944gat = ~G834gat;
  assign G945gat = ~G860gat;
  assign G946gat = ~G873gat;
  assign G947gat = ~G847gat;
  assign G948gat = ~G860gat;
  assign G949gat = ~G873gat;
  assign G950gat = ~G886gat;
  assign G951gat = ~G899gat;
  assign G952gat = ~G886gat;
  assign G953gat = ~G912gat;
  assign G954gat = ~G925gat;
  assign G955gat = ~G899gat;
  assign G956gat = ~G925gat;
  assign G957gat = ~G912gat;
  assign G958gat = ~G925gat;
  assign G959gat = ~G886gat;
  assign G960gat = ~G912gat;
  assign G961gat = ~G925gat;
  assign G962gat = ~G886gat;
  assign G963gat = ~G899gat;
  assign G964gat = ~G925gat;
  assign G965gat = ~G912gat;
  assign G966gat = ~G899gat;
  assign G967gat = ~G886gat;
  assign G968gat = ~G912gat;
  assign G969gat = ~G899gat;
  assign G970gat = ~G847gat;
  assign G971gat = ~G873gat;
  assign G972gat = ~G847gat;
  assign G973gat = ~G860gat;
  assign G974gat = ~G834gat;
  assign G975gat = ~G873gat;
  assign G976gat = ~G834gat;
  assign G977gat = ~G860gat;
  assign G978gat = G873gat & G940gat & G938gat & G939gat;
  assign G979gat = G943gat & G860gat & G941gat & G942gat;
  assign G980gat = G946gat & G945gat & G944gat & G847gat;
  assign G981gat = G949gat & G948gat & G834gat & G947gat;
  assign G982gat = G899gat & G960gat & G958gat & G959gat;
  assign G983gat = G963gat & G912gat & G961gat & G962gat;
  assign G984gat = G966gat & G965gat & G964gat & G886gat;
  assign G985gat = G969gat & G968gat & G925gat & G967gat;
  assign G986gat = G980gat | G981gat | G978gat | G979gat;
  assign G991gat = G984gat | G985gat | G982gat | G983gat;
  assign G996gat = G986gat & G951gat & G912gat & G925gat & G950gat;
  assign G1001gat = G986gat & G899gat & G953gat & G925gat & G952gat;
  assign G1006gat = G986gat & G955gat & G912gat & G954gat & G886gat;
  assign G1011gat = G986gat & G899gat & G957gat & G956gat & G886gat;
  assign G1016gat = G991gat & G971gat & G860gat & G834gat & G970gat;
  assign G1021gat = G991gat & G873gat & G973gat & G834gat & G972gat;
  assign G1026gat = G991gat & G975gat & G860gat & G974gat & G847gat;
  assign G1031gat = G991gat & G873gat & G977gat & G976gat & G847gat;
  assign G1036gat = G834gat & G996gat;
  assign G1039gat = G847gat & G996gat;
  assign G1042gat = G860gat & G996gat;
  assign G1045gat = G873gat & G996gat;
  assign G1048gat = G834gat & G1001gat;
  assign G1051gat = G847gat & G1001gat;
  assign G1054gat = G860gat & G1001gat;
  assign G1057gat = G873gat & G1001gat;
  assign G1060gat = G834gat & G1006gat;
  assign G1063gat = G847gat & G1006gat;
  assign G1066gat = G860gat & G1006gat;
  assign G1069gat = G873gat & G1006gat;
  assign G1072gat = G834gat & G1011gat;
  assign G1075gat = G847gat & G1011gat;
  assign G1078gat = G860gat & G1011gat;
  assign G1081gat = G873gat & G1011gat;
  assign G1084gat = G925gat & G1016gat;
  assign G1087gat = G886gat & G1016gat;
  assign G1090gat = G912gat & G1016gat;
  assign G1093gat = G899gat & G1016gat;
  assign G1096gat = G925gat & G1021gat;
  assign G1099gat = G886gat & G1021gat;
  assign G1102gat = G912gat & G1021gat;
  assign G1105gat = G899gat & G1021gat;
  assign G1108gat = G925gat & G1026gat;
  assign G1111gat = G886gat & G1026gat;
  assign G1114gat = G912gat & G1026gat;
  assign G1117gat = G899gat & G1026gat;
  assign G1120gat = G925gat & G1031gat;
  assign G1123gat = G886gat & G1031gat;
  assign G1126gat = G912gat & G1031gat;
  assign G1129gat = G899gat & G1031gat;
  assign G1132gat = ~G1gat | ~G1036gat;
  assign G1135gat = ~G8gat | ~G1039gat;
  assign G1138gat = ~G15gat | ~G1042gat;
  assign G1141gat = ~G22gat | ~G1045gat;
  assign G1144gat = ~G29gat | ~G1048gat;
  assign G1147gat = ~G36gat | ~G1051gat;
  assign G1150gat = ~G43gat | ~G1054gat;
  assign G1153gat = ~G50gat | ~G1057gat;
  assign G1156gat = ~G57gat | ~G1060gat;
  assign G1159gat = ~G64gat | ~G1063gat;
  assign G1162gat = ~G71gat | ~G1066gat;
  assign G1165gat = ~G78gat | ~G1069gat;
  assign G1168gat = ~G85gat | ~G1072gat;
  assign G1171gat = ~G92gat | ~G1075gat;
  assign G1174gat = ~G99gat | ~G1078gat;
  assign G1177gat = ~G106gat | ~G1081gat;
  assign G1180gat = ~G113gat | ~G1084gat;
  assign G1183gat = ~G120gat | ~G1087gat;
  assign G1186gat = ~G127gat | ~G1090gat;
  assign G1189gat = ~G134gat | ~G1093gat;
  assign G1192gat = ~G141gat | ~G1096gat;
  assign G1195gat = ~G148gat | ~G1099gat;
  assign G1198gat = ~G155gat | ~G1102gat;
  assign G1201gat = ~G162gat | ~G1105gat;
  assign G1204gat = ~G169gat | ~G1108gat;
  assign G1207gat = ~G176gat | ~G1111gat;
  assign G1210gat = ~G183gat | ~G1114gat;
  assign G1213gat = ~G190gat | ~G1117gat;
  assign G1216gat = ~G197gat | ~G1120gat;
  assign G1219gat = ~G204gat | ~G1123gat;
  assign G1222gat = ~G211gat | ~G1126gat;
  assign G1225gat = ~G218gat | ~G1129gat;
  assign G1228gat = ~G1gat | ~G1132gat;
  assign G1229gat = ~G1036gat | ~G1132gat;
  assign G1230gat = ~G8gat | ~G1135gat;
  assign G1231gat = ~G1039gat | ~G1135gat;
  assign G1232gat = ~G15gat | ~G1138gat;
  assign G1233gat = ~G1042gat | ~G1138gat;
  assign G1234gat = ~G22gat | ~G1141gat;
  assign G1235gat = ~G1045gat | ~G1141gat;
  assign G1236gat = ~G29gat | ~G1144gat;
  assign G1237gat = ~G1048gat | ~G1144gat;
  assign G1238gat = ~G36gat | ~G1147gat;
  assign G1239gat = ~G1051gat | ~G1147gat;
  assign G1240gat = ~G43gat | ~G1150gat;
  assign G1241gat = ~G1054gat | ~G1150gat;
  assign G1242gat = ~G50gat | ~G1153gat;
  assign G1243gat = ~G1057gat | ~G1153gat;
  assign G1244gat = ~G57gat | ~G1156gat;
  assign G1245gat = ~G1060gat | ~G1156gat;
  assign G1246gat = ~G64gat | ~G1159gat;
  assign G1247gat = ~G1063gat | ~G1159gat;
  assign G1248gat = ~G71gat | ~G1162gat;
  assign G1249gat = ~G1066gat | ~G1162gat;
  assign G1250gat = ~G78gat | ~G1165gat;
  assign G1251gat = ~G1069gat | ~G1165gat;
  assign G1252gat = ~G85gat | ~G1168gat;
  assign G1253gat = ~G1072gat | ~G1168gat;
  assign G1254gat = ~G92gat | ~G1171gat;
  assign G1255gat = ~G1075gat | ~G1171gat;
  assign G1256gat = ~G99gat | ~G1174gat;
  assign G1257gat = ~G1078gat | ~G1174gat;
  assign G1258gat = ~G106gat | ~G1177gat;
  assign G1259gat = ~G1081gat | ~G1177gat;
  assign G1260gat = ~G113gat | ~G1180gat;
  assign G1261gat = ~G1084gat | ~G1180gat;
  assign G1262gat = ~G120gat | ~G1183gat;
  assign G1263gat = ~G1087gat | ~G1183gat;
  assign G1264gat = ~G127gat | ~G1186gat;
  assign G1265gat = ~G1090gat | ~G1186gat;
  assign G1266gat = ~G134gat | ~G1189gat;
  assign G1267gat = ~G1093gat | ~G1189gat;
  assign G1268gat = ~G141gat | ~G1192gat;
  assign G1269gat = ~G1096gat | ~G1192gat;
  assign G1270gat = ~G148gat | ~G1195gat;
  assign G1271gat = ~G1099gat | ~G1195gat;
  assign G1272gat = ~G155gat | ~G1198gat;
  assign G1273gat = ~G1102gat | ~G1198gat;
  assign G1274gat = ~G162gat | ~G1201gat;
  assign G1275gat = ~G1105gat | ~G1201gat;
  assign G1276gat = ~G169gat | ~G1204gat;
  assign G1277gat = ~G1108gat | ~G1204gat;
  assign G1278gat = ~G176gat | ~G1207gat;
  assign G1279gat = ~G1111gat | ~G1207gat;
  assign G1280gat = ~G183gat | ~G1210gat;
  assign G1281gat = ~G1114gat | ~G1210gat;
  assign G1282gat = ~G190gat | ~G1213gat;
  assign G1283gat = ~G1117gat | ~G1213gat;
  assign G1284gat = ~G197gat | ~G1216gat;
  assign G1285gat = ~G1120gat | ~G1216gat;
  assign G1286gat = ~G204gat | ~G1219gat;
  assign G1287gat = ~G1123gat | ~G1219gat;
  assign G1288gat = ~G211gat | ~G1222gat;
  assign G1289gat = ~G1126gat | ~G1222gat;
  assign G1290gat = ~G218gat | ~G1225gat;
  assign G1291gat = ~G1129gat | ~G1225gat;
  assign G1324gat = ~G1228gat | ~G1229gat;
  assign G1325gat = ~G1230gat | ~G1231gat;
  assign G1326gat = ~G1232gat | ~G1233gat;
  assign G1327gat = ~G1234gat | ~G1235gat;
  assign G1328gat = ~G1236gat | ~G1237gat;
  assign G1329gat = ~G1238gat | ~G1239gat;
  assign G1330gat = ~G1240gat | ~G1241gat;
  assign G1331gat = ~G1242gat | ~G1243gat;
  assign G1332gat = ~G1244gat | ~G1245gat;
  assign G1333gat = ~G1246gat | ~G1247gat;
  assign G1334gat = ~G1248gat | ~G1249gat;
  assign G1335gat = ~G1250gat | ~G1251gat;
  assign G1336gat = ~G1252gat | ~G1253gat;
  assign G1337gat = ~G1254gat | ~G1255gat;
  assign G1338gat = ~G1256gat | ~G1257gat;
  assign G1339gat = ~G1258gat | ~G1259gat;
  assign G1340gat = ~G1260gat | ~G1261gat;
  assign G1341gat = ~G1262gat | ~G1263gat;
  assign G1342gat = ~G1264gat | ~G1265gat;
  assign G1343gat = ~G1266gat | ~G1267gat;
  assign G1344gat = ~G1268gat | ~G1269gat;
  assign G1345gat = ~G1270gat | ~G1271gat;
  assign G1346gat = ~G1272gat | ~G1273gat;
  assign G1347gat = ~G1274gat | ~G1275gat;
  assign G1348gat = ~G1276gat | ~G1277gat;
  assign G1349gat = ~G1278gat | ~G1279gat;
  assign G1350gat = ~G1280gat | ~G1281gat;
  assign G1351gat = ~G1282gat | ~G1283gat;
  assign G1352gat = ~G1284gat | ~G1285gat;
  assign G1353gat = ~G1286gat | ~G1287gat;
  assign G1354gat = ~G1288gat | ~G1289gat;
  assign G1355gat = ~G1290gat | ~G1291gat;
endmodule


