This repository has experimental artifacts of paper titled "Fault Coverage of a Test Set on Structure-Preserving
Siblings of a Circuit-Under-Test" which has been accepted at 28th Asian Test Symposium, 2019.

If one is interested to replicate and/or use it for experimental and validation purpose, please cite the following
paper :-

```
@article{mondal2018semi,
  title={Fault Coverage of a Test Set on Structure-Preserving
Siblings of a Circuit-Under-Test},
  author={Mondal, Manobendra Nath; Basak Chowdhury, Animesh; Pradhan, Manjari; Sur-Kolay, Susmita; Bhattacharya, Bhargab B.},
  conference={28th Asian Test Symposium},
  year={2019}
}
```

