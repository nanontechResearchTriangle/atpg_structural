import re,os,copy
import random

hopeExec = "/home/animesh/currentResearch/ISI/benchmarksAndTools/hope/hope"
atalantaExec = "/home/animesh/currentResearch/ISI/benchmarksAndTools/atalanta/atalanta"

#listOfSCkt = ["s208", "s298", "s344" ,"s349", "s382", "s386", "s400", "s420", "s444", "s510","s526","s641","s713","s820","s832","s838","s953","s1196","s1238","s1423","s1488","s1494","s5378","s9234","s13207","s15850","s35932"]

#listOfSCkt = ["s13207","s15850","s35932","s38417","s38584"]
#listOfSCkt = ["c432"]
listOfSCkt = ["c432","c499","c880","c1355","c1908","c2670","c3540","c5315","c6288","c7552"]


#inputCktName = "c7552"
seriesName = "iscas85"
#seriesName = "iscas89"
rootData = "/home/animesh/currentResearch/ISI/atpg_structural/code"
tempDataPath = rootData+os.sep+"tempData"
testDataRootFolder = rootData+os.sep+"testData"+os.sep+"atalanta"
dumpDataRootFolder = rootData+os.sep+"dumpData"+os.sep+"atalanta"+os.sep+seriesName
csvDelimiter = " "
numCircuitToBeGenerated = 100



for inputCktName in listOfSCkt:
    cktFileName = testDataRootFolder+os.sep+"bench"+os.sep+seriesName+os.sep+inputCktName +".bench"
    testFileName = testDataRootFolder+os.sep+"tests"+os.sep+seriesName+os.sep+inputCktName +".test"
    dumpDataFolder = dumpDataRootFolder+os.sep+inputCktName

    if(not os.path.exists(dumpDataFolder)):
        os.mkdir(dumpDataFolder)

    inputCktFileHandler = open(cktFileName,'r+')
    inputFileLines = inputCktFileHandler.readlines()
    #setOfGates = ["nor","nand","and","or"]
    #setOfGates = ["NOR","NAND","AND","OR"]
    #setOfGates_WORD = ["NOR(","NAND(","AND(","OR("]
    setOfGates = ["nor","nand","and","or"]
    setOfGates_WORD = ["nor(","nand(","and(","or("]
    setOfTargetNodes = {}
    thresholdPercent = {0.1 : "tenPercent" ,
                    0.3 : "thirtyPercent",
                    0.5 : "fiftyPercent",
                    0.7 : "seventyPercent",
                    0.9 : "ninetyPercent"}
     #               1 : "hundredPercent"}

    countGates = 0
    countReplacableGates = 0
    for line in inputFileLines:
        if(line.__contains__("=")):countGates+=1
        words = re.findall('\w+',line)
        #print(words)
        if(len(words)> 2):
            if(setOfGates.__contains__(words[1])):
                #print(words)
                setOfTargetNodes[words[0]] = words[1]+"("
                countReplacableGates+=1

    inputCktFileHandler.close()
    print(countGates)
    print(countReplacableGates)
    print(setOfTargetNodes)

    resultLogFilePath = dumpDataFolder+os.sep+"results_"+inputCktName+".csv"
    resultLogFileHandler = open(resultLogFilePath,'w+')
    resultLogFileHandler.write("cktName"+csvDelimiter+"percentChange"+csvDelimiter+"hopeUndetectedFault"+csvDelimiter+"hopeDetectedFault"+csvDelimiter+"hopeNumCollapsedFault"+csvDelimiter+"hopeFaultCoverage"+csvDelimiter+"atpgAbortedFault"+csvDelimiter+"atpgRedundantFault"+csvDelimiter+"atpgNumCollapsedFault"+csvDelimiter+"atpgFaultCoverage"+csvDelimiter)
    resultLogFileHandler.write("\n")

    for percentValue in thresholdPercent.keys():
        numTargetNodesToBeReplace = int(percentValue*countReplacableGates)
        circuitDumpPath = dumpDataFolder+os.sep+thresholdPercent[percentValue]
        if(not os.path.exists(circuitDumpPath)): os.mkdir(circuitDumpPath)
        for loopVar in range(numCircuitToBeGenerated):
            randomCktName = inputCktName+"_"+thresholdPercent[percentValue]+"_"+str(loopVar)
            targetNodeSet = copy.deepcopy(setOfTargetNodes)
            listOfTargetNodeSet = list(targetNodeSet.keys())
            for numNodeToBeChanged in range(numTargetNodesToBeReplace):
                outputGateIdentifier = random.choice(listOfTargetNodeSet)
                #outputGateFunctionality = random.choice(setOfGates)
                #outputGateFunctionality = random.choice(setOfGates_WORD)
                #targetNodeSet[outputGateIdentifier] = outputGateFunctionality
                '''
                if (targetNodeSet[outputGateIdentifier] == "NOR("):
                    targetNodeSet[outputGateIdentifier] = "NAND("
                elif (targetNodeSet[outputGateIdentifier] == "NAND("):
                    targetNodeSet[outputGateIdentifier] = "NOR("
                elif (targetNodeSet[outputGateIdentifier] == "AND("):
                    targetNodeSet[outputGateIdentifier] = "OR("
                elif (targetNodeSet[outputGateIdentifier] == "OR("):
                    targetNodeSet[outputGateIdentifier] = "AND("
                listOfTargetNodeSet.remove(outputGateIdentifier)
                '''
                if (targetNodeSet[outputGateIdentifier] == "nor("):
                    targetNodeSet[outputGateIdentifier] = "nand("
                elif (targetNodeSet[outputGateIdentifier] == "nand("):
                    targetNodeSet[outputGateIdentifier] = "nor("
                elif (targetNodeSet[outputGateIdentifier] == "and("):
                    targetNodeSet[outputGateIdentifier] = "or("
                elif (targetNodeSet[outputGateIdentifier] == "or("):
                    targetNodeSet[outputGateIdentifier] = "and("
                listOfTargetNodeSet.remove(outputGateIdentifier)
            writeCktFile = circuitDumpPath+os.sep+randomCktName+".bench"
            writeCktFileHandler = open(writeCktFile,'w+')
            setOfTargetNodeNames = set(targetNodeSet.keys())
            #print(setOfTargetNodeNames)
            for writeLine in inputFileLines:
                words = re.findall('\w+', writeLine)
                if(len(words)>0):
                    #print(words[0])
                    if(setOfTargetNodeNames.__contains__(words[0])):
                        #print(words[1],targetNodeSet[words[0]])
                        #print(writeLine)
                        wordToBeChanged = words[1]+"("
                        # For adding dual logics

                        writeLine = writeLine.replace(wordToBeChanged,targetNodeSet[words[0]])
                        #print(writeLine)
                writeCktFileHandler.write(writeLine)
            writeCktFileHandler.close()

            hopeRunLogFile =  circuitDumpPath+os.sep+inputCktName+"_"+thresholdPercent[percentValue]+"_"+str(loopVar)+"hopeSimulator.log"
            hopeRunCmd = hopeExec+" -t "+testFileName+" "+writeCktFile+" > "+hopeRunLogFile

            atalantaRunLogFile = circuitDumpPath+os.sep+inputCktName+"_"+thresholdPercent[percentValue]+"_"+str(loopVar)+"atalantaATPG.log"
            atalantaRunCmd = atalantaExec+" "+writeCktFile+" > "+atalantaRunLogFile

            #print(hopeRunCmd)
            os.system(hopeRunCmd)
            os.system(atalantaRunCmd)

            hopeRunLogFileHandler = open(hopeRunLogFile,'r+')
            atalantaRunLogFileHandler = open(atalantaRunLogFile,'r+')
            hopeLogFileLines = hopeRunLogFileHandler.readlines()
            atalantaLogFileLines = atalantaRunLogFileHandler.readlines()
            resultLogFileHandler.write(randomCktName+csvDelimiter+str(percentValue)+csvDelimiter)

            for line in range(8,12):
                lineContent = hopeLogFileLines[-line]
                words = re.findall('[a-zA-Z0-9.]+',lineContent)
                resultLogFileHandler.write(words[len(words)-1]+csvDelimiter)

            for line in range(10,14):
                if(len(atalantaLogFileLines)>0):
                    lineContent = atalantaLogFileLines[-line]
                    words = re.findall('[a-zA-Z0-9.]+',lineContent)
                    resultLogFileHandler.write(words[len(words)-1]+csvDelimiter)
                else:
                    resultLogFileHandler.write("0"+csvDelimiter)

            atalantaRunLogFileHandler.close()
            hopeRunLogFileHandler.close()
            resultLogFileHandler.write("\n")

        # Testability on siblings
        siblingResultLogFile = dumpDataFolder + os.sep + "resultSiblingsCoverage_" + inputCktName + "_" + \
                               thresholdPercent[percentValue] + ".csv"
        siblingResultLogFileHandler = open(siblingResultLogFile, 'w+')
        siblingResultLogFileHandler.write(
            "cktName" + csvDelimiter + "hopeUndetectedFault" + csvDelimiter + "hopeDetectedFault" + csvDelimiter + "hopeNumCollapsedFault" + csvDelimiter + "hopeFaultCoverage" + csvDelimiter + "atpgAbortedFault" + csvDelimiter + "atpgRedundantFault" + csvDelimiter + "atpgNumCollapsedFault" + csvDelimiter + "atpgFaultCoverage" + csvDelimiter)
        siblingResultLogFileHandler.write("\n")
        for loopVar1 in range(numCircuitToBeGenerated):
            #print(loopVar1)


            #atalantaRunLogFile = circuitDumpPath + os.sep + inputCktName + "_" + thresholdPercent[percentValue] + "_" + str(loopVar1) + "atalantaATPG.log"
            #atalantaRunLogFileHandler = open(atalantaRunLogFile,'r+')
            #atalantaLogFileLines = atalantaRunLogFileHandler.readlines()

            baseRandomCktName = inputCktName + "_" + thresholdPercent[percentValue] + "_" + str(loopVar1)
            baseTestFile = rootData+os.sep+baseRandomCktName+".test"
            for loopVar2 in range(numCircuitToBeGenerated):
                atalantaRunLogFile = circuitDumpPath + os.sep + inputCktName + "_" + thresholdPercent[percentValue] + "_" + str(loopVar2) + "atalantaATPG.log"
                atalantaRunLogFileHandler = open(atalantaRunLogFile, 'r+')
                atalantaLogFileLines = atalantaRunLogFileHandler.readlines()
                #print(loopVar1,loopVar2)
                siblingCktFile =  circuitDumpPath+os.sep+inputCktName + "_" + thresholdPercent[percentValue] + "_" + str(loopVar2) +".bench"
                siblingFromToCktInfo = inputCktName + "_" + thresholdPercent[percentValue] + "_" + str(loopVar1) + "_" + str(loopVar2)
                #if(loopVar1 == loopVar2):
                #    continue
                #else:
                if(True):
                    hopeRunLogFileForSibling = tempDataPath + os.sep + inputCktName + "_" + thresholdPercent[percentValue] + "_" + str(loopVar1) + "_" + str(loopVar2) + "_" + "hopeSimulator.log"
                    hopeRunCmdForSibling = hopeExec + " -t " + baseTestFile + " " + siblingCktFile + " > " + hopeRunLogFileForSibling

                    os.system(hopeRunCmdForSibling)

                    hopeRunLogFileHandlerForSibling = open(hopeRunLogFileForSibling, 'r+')
                    logFileLines = hopeRunLogFileHandlerForSibling.readlines()
                    siblingResultLogFileHandler.write(siblingFromToCktInfo + csvDelimiter)

                    for line in range(8, 12):
                        lineContent = logFileLines[-line]
                        words = re.findall('[a-zA-Z0-9.]+', lineContent)
                        siblingResultLogFileHandler.write(words[len(words) - 1] + csvDelimiter)

                    for line in range(10, 14):
                        if (len(atalantaLogFileLines) > 0):
                            lineContent = atalantaLogFileLines[-line]
                            words = re.findall('[a-zA-Z0-9.]+', lineContent)
                            siblingResultLogFileHandler.write(words[len(words) - 1] + csvDelimiter)
                        else:
                            siblingResultLogFileHandler.write("0" + csvDelimiter)

                    atalantaRunLogFileHandler.close()
                    hopeRunLogFileHandlerForSibling.close()
                    siblingResultLogFileHandler.write("\n")
        siblingResultLogFileHandler.close()



    resultLogFileHandler.close()