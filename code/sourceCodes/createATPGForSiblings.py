import re, os, copy
import random

tessentExec = "tessent"

# listOfSCkt = ["s208", "s298", "s344" ,"s349", "s382", "s386", "s400", "s420", "s444", "s510","s526","s641","s713","s820","s832","s838","s953","s1196","s1238","s1423","s1488","s1494","s5378","s9234","s13207","s15850","s35932"]

# listOfSCkt = ["s13207","s15850","s35932","s38417","s38584"]
# listOfSCkt = ["c432","c499","c1908","c3540","c7552"]
# listOfSCkt = ["s832","s838","s1494"]
# listOfSCkt = ["s9234","s15850","s38584"]
# listOfSCkt = ["c432","c499","c880","c1355","c1908","c2670","c3540","c5315","c6288","c7552"]
# listOfSCkt = ["c432_synth","c499_synth","c1908_synth","c3540_synth","c7552_synth"]
# listOfSCkt = ["s526_synth","s832_synth","s838_synth","s1494_synth","s9234_synth","s15850_synth","s38584_synth"]
# listOfSCkt = ["b10_synth","b14_synth","b15_synth","b19_synth","b22_synth"]
# listOfSCkt = ["b10","b14","b15"]
# listOfSCkt = ["b22"]
# listOfSCkt = ["adder_synth","bar_synth","div_synth","hyp_synth","log2_synth","max_synth","multiplier_synth","sin_synth","sqrt_synth"]
#listOfSCkt = ["max", "multiplier", "sin"]
listOfSCkt = ["c7552"]


#Big Circuits :
# Divisor, Hypotenuse, Mem_ctrl, b19

# listOfEPFLCtrlCkt = ["arbiter_synth","cavlc_synth","ctrl_synth","dec_synth","i2c_synth","int2float_synth","mem_ctrl_synth","priority_synth","router_synth","voter_synth"]


# inputCktName = "c7552"
seriesName = "iscas85"
# seriesName = "iscas89"
# seriesName = "itc99"
#seriesName = "epfl" + os.sep + "arithmetic"
rootData = "/home/animesh/currentResearch/ISI/atpg_structural/code"
tempDataPath = rootData + os.sep + "tempData"
testDataRootFolder = rootData + os.sep + "testData" + os.sep + "tessent"
dumpDataRootFolder = rootData + os.sep + "dumpData" + os.sep + "tessent" + os.sep + seriesName
csvDelimiter = " "
verilogExtension = ".v"
testFileExtension = ".spf"
atpgLogFilePathExtension = ".atpg.log"
atpgResultFilePathExtension = ".atpg.result"

doFileExtension = ".dofile"
cellLibraryPath = testDataRootFolder + os.sep + "cellLibrary.atpg"
numCircuitToBeGenerated = 40

# Choice (Random or with Dual Gates)
choice = 1

thresholdPercent = {0.1: "tenPercent",
                    0.3: "thirtyPercent",
                    0.5: "fiftyPercent",
                    0.7: "seventyPercent",
                    0.9: "ninetyPercent"}
#               1 : "hundredPercent"}

for inputCkt in listOfSCkt:
    inputCktNameSynth = inputCkt + "_synth"
    dumpDataFolder = dumpDataRootFolder + os.sep + inputCktNameSynth
    # resultLogFilePath = dumpDataFolder+os.sep+"results_"+inputCktName+".csv"
    # resultLogFileHandler = open(resultLogFilePath,'w+')
    # resultLogFileHandler.write("cktName"+csvDelimiter+"percentChange"+csvDelimiter+"hopeUndetectedFault"+csvDelimiter+"hopeDetectedFault"+csvDelimiter+"hopeNumCollapsedFault"+csvDelimiter+"hopeFaultCoverage"+csvDelimiter+"atpgAbortedFault"+csvDelimiter+"atpgRedundantFault"+csvDelimiter+"atpgNumCollapsedFault"+csvDelimiter+"atpgFaultCoverage"+csvDelimiter)
    # resultLogFileHandler.write("\n")

    for percentValue in thresholdPercent.keys():
        circuitDumpPath = dumpDataFolder + os.sep + thresholdPercent[percentValue]
        for loopVar in range(numCircuitToBeGenerated):
            inputCktName = inputCktNameSynth + "_" + thresholdPercent[percentValue] + "_" + str(loopVar)
            inputCktFilePath = circuitDumpPath + os.sep + inputCktName + verilogExtension
            inputCktFileTestPath = circuitDumpPath + os.sep + inputCktName + testFileExtension
            inputCktFileATPGLogPath = circuitDumpPath + os.sep + inputCktName + atpgLogFilePathExtension
            inputCktFileATPGResultPath = circuitDumpPath + os.sep + inputCktName + atpgResultFilePathExtension
            inputCktATPGDoFile = circuitDumpPath + os.sep + inputCktName + doFileExtension
            paraGraph = "set_context patterns -scan\n" \
                    "read_verilog " + inputCktFilePath +" \n" \
                    "read_cell_library "+cellLibraryPath + "\n" \
                    "set_logfile_handling "+inputCktFileATPGLogPath+" -rep\n" \
                    "set_current_design "+ inputCkt+"\n" \
                    "set_system_mode analysis\n" \
                    "add_fault -all\n" \
                    "set_fault_mode collapsed\n" \
                    "set_fault_type stuck\n" \
                    "create_patterns\n" \
                    "write_patterns "+inputCktFileTestPath+" -stil -rep\n" \
                    "report_statistics > "+inputCktFileATPGResultPath+"\n" \
                    "exit\n"

            doFileWriter = open(inputCktATPGDoFile,'w+')
            doFileWriter.write(paraGraph)
            doFileWriter.close()

            tessantRunCmd = "tessent -shell -dofile "+inputCktATPGDoFile
            os.system(tessantRunCmd)
