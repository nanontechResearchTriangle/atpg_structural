import re,os,copy
import random

hopeExec = "/home/animesh/currentResearch/ISI/benchmarksAndTools/hope/hope"
atalantaExec = "/home/animesh/currentResearch/ISI/benchmarksAndTools/atalanta/atalanta"

#listOfSCkt = ["s208", "s298", "s344" ,"s349", "s382", "s386", "s400", "s420", "s444", "s510","s526","s641","s713","s820","s832","s838","s953","s1196","s1238","s1423","s1488","s1494","s5378","s9234","s13207","s15850","s35932"]

#listOfSCkt = ["s13207","s15850","s35932","s38417","s38584"]
#listOfSCkt = ["c432_synth"]
#listOfSCkt = ["c432","c499","c880","c1355","c1908","c2670","c3540","c5315","c6288","c7552"]
#listOfSCkt = ["c432_synth","c499_synth","c1908_synth","c3540_synth","c7552_synth"]
#listOfSCkt = ["s526_synth","s832_synth","s838_synth","s1494_synth","s9234_synth","s15850_synth","s38584_synth"]
#listOfSCkt = ["b10_synth","b14_synth","b15_synth","b19_synth","b22_synth"]
#listOfSCkt = ["b19_synth"]
#listOfSCkt = ["adder_synth","bar_synth","div_synth","hyp_synth","log2_synth","max_synth","multiplier_synth","sin_synth","sqrt_synth"]
#listOfSCkt = ["log2_synth","max_synth","multiplier_synth","sin_synth","sqrt_synth","square_synth"]
#listOfSCkt = ["arbiter_synth","cavlc_synth","ctrl_synth","dec_synth","i2c_synth","int2float_synth","priority_synth","router_synth","voter_synth"]
#listOfEPFLCtrlCkt = ["arbiter_synth","cavlc_synth","ctrl_synth","dec_synth","i2c_synth","int2float_synth","priority_synth","router_synth","voter_synth"]
#listOfSCkt = ["c7552_synth"]
#listOfSCkt = ["s9234_synth","s15850_synth","s38584_synth"]
listOfSCkt = ["i2c_synth"]




#inputCktName = "c7552"
#seriesName = "iscas85"
#seriesName = "iscas89"
#seriesName = "itc99"
seriesName = "epfl"+os.sep+"random_control"
rootData = "/home/animesh/currentResearch/ISI/atpg_structural/code"
tempDataPath = rootData+os.sep+"tempData"
testDataRootFolder = rootData+os.sep+"testData"+os.sep+"tessent"
dumpDataRootFolder = rootData+os.sep+"dumpData"+os.sep+"tessent"+os.sep+seriesName
csvDelimiter = " "
numCircuitToBeGenerated = 40

#Choice (Random or with Dual Gates)
choice = 1



for inputCktName in listOfSCkt:
    cktFileName = testDataRootFolder+os.sep+"synthesizedVerilog"+os.sep+seriesName+os.sep+inputCktName +".v"
    #testFileName = testDataRootFolder+os.sep+"tests"+os.sep+seriesName+os.sep+inputCktName +".test"
    dumpDataFolder = dumpDataRootFolder+os.sep+inputCktName

    if(not os.path.exists(dumpDataFolder)):
        os.mkdir(dumpDataFolder)

    inputCktFileHandler = open(cktFileName,'r+')
    inputFileLines = inputCktFileHandler.readlines()

    setOfGates = ["inv01","buf01","nor02","nand02","or02","and02","xor2","xnor2"]
    setOfReplacableGates = ["nor02","nand02","or02","and02"]
    setOfTargetGates = {}


    thresholdPercent = {0.1 : "tenPercent" ,
                    0.3 : "thirtyPercent",
                    0.5 : "fiftyPercent",
                    0.7 : "seventyPercent",
                    0.9 : "ninetyPercent"}
     #               1 : "hundredPercent"}

    '''
    thresholdPercent = {0.1: "tenPercent",
                        0.3: "thirtyPercent",
                        0.5: "fiftyPercent"}
    #               1 : "hundredPercent"}
    '''

    countGates = 0
    countReplacableGates = 0
    for line in inputFileLines:
        words = re.findall('\w+',line)
        #print(words)
        if(len(words)> 2):
            if(setOfGates.__contains__(words[0])):
                countGates+=1
                if (setOfReplacableGates.__contains__(words[0])):
                    setOfTargetGates[words[1]] = words[0]
                    countReplacableGates+=1

    inputCktFileHandler.close()
    print(countGates)
    print(countReplacableGates)
    print(setOfTargetGates)

    #resultLogFilePath = dumpDataFolder+os.sep+"results_"+inputCktName+".csv"
    #resultLogFileHandler = open(resultLogFilePath,'w+')
    #resultLogFileHandler.write("cktName"+csvDelimiter+"percentChange"+csvDelimiter+"hopeUndetectedFault"+csvDelimiter+"hopeDetectedFault"+csvDelimiter+"hopeNumCollapsedFault"+csvDelimiter+"hopeFaultCoverage"+csvDelimiter+"atpgAbortedFault"+csvDelimiter+"atpgRedundantFault"+csvDelimiter+"atpgNumCollapsedFault"+csvDelimiter+"atpgFaultCoverage"+csvDelimiter)
    #resultLogFileHandler.write("\n")

    for percentValue in thresholdPercent.keys():
        numTargetGatesToBeReplace = int(percentValue*countReplacableGates)
        circuitDumpPath = dumpDataFolder+os.sep+thresholdPercent[percentValue]
        if(not os.path.exists(circuitDumpPath)): os.mkdir(circuitDumpPath)
        for loopVar in range(numCircuitToBeGenerated):
            randomCktName = inputCktName+"_"+thresholdPercent[percentValue]+"_"+str(loopVar)
            targetGateSet = copy.deepcopy(setOfTargetGates)
            listOfTargetGateSet = list(targetGateSet.keys())
            for numGateToBeChanged in range(numTargetGatesToBeReplace):
                outputGateIdentifier = random.choice(listOfTargetGateSet)
                if(choice == 1):
                    if (targetGateSet[outputGateIdentifier] == "nor02"):
                        targetGateSet[outputGateIdentifier] = "nand02"
                    elif (targetGateSet[outputGateIdentifier] == "nand02"):
                        targetGateSet[outputGateIdentifier] = "nor02"
                    elif (targetGateSet[outputGateIdentifier] == "and02"):
                        targetGateSet[outputGateIdentifier] = "or02"
                    elif (targetGateSet[outputGateIdentifier] == "or02"):
                        targetGateSet[outputGateIdentifier] = "and02"
                else:
                    outputGateFunctionality = random.choice(setOfReplacableGates)
                    targetGateSet[outputGateIdentifier] = outputGateFunctionality

                listOfTargetGateSet.remove(outputGateIdentifier)

            writeCktFile = circuitDumpPath+os.sep+randomCktName+".v"
            writeCktFileHandler = open(writeCktFile,'w+')
            setOfTargetGateNames = set(targetGateSet.keys())
            #print(setOfTargetNodeNames)
            for writeLine in inputFileLines:
                words = re.findall('\w+', writeLine)
                if(len(words)>2):
                    #print(words[0])
                    if(setOfTargetGateNames.__contains__(words[1])):
                        #print(words[1],targetNodeSet[words[0]])
                        #print(writeLine)
                        wordToBeChanged = words[0]
                        # For adding dual logics

                        writeLine = writeLine.replace(wordToBeChanged,targetGateSet[words[1]])
                        #print(writeLine)
                writeCktFileHandler.write(writeLine)
            writeCktFileHandler.close()