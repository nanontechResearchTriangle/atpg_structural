import re, os, copy
import random
import pandas as pd
import shutil
import seaborn as sns
import matplotlib.pyplot as plt

def doBoxPlot(fileName,xAxis,yAxis,xLab,yLab,hueCol,graphDf,graphTitle):
    if(hueCol == None):
        ax2 = sns.boxplot(y=yAxis, x=xAxis, data=graphDf)
    else:
        ax2 = sns.boxplot(y=yAxis, x=xAxis, hue = hueCol,data=graphDf)
    ax2.set_xlabel(xLab, fontsize=14)
    ax2.set_ylabel(yLab, fontsize=14)
    ax2.set_title(graphTitle, fontsize=14)
    ax2.tick_params(axis='x', rotation=10)
    fig = ax2.get_figure()
    fig.savefig(fileName, format='pdf', dpi=1500)
    ax2.clear()


def doSwarmPlot(fileName,xAxis,yAxis,xLab,yLab,graphDf,graphTitle):
    ax2 = sns.swarmplot(y=yAxis, x=xAxis, data=graphDf,order=["tenPercent", "thirtyPercent","fiftyPercent","seventyPercent","ninetyPercent"])
    ax2.set_xlabel(xLab, fontsize=14)
    ax2.set_ylabel(yLab, fontsize=14)
    ax2.set_title(graphTitle, fontsize=14)
    fig = ax2.get_figure()
    fig.savefig(fileName, format='pdf', dpi=1500)
    ax2.clear()

# listOfSCkt = ["s208", "s298", "s344" ,"s349", "s382", "s386", "s400", "s420", "s444", "s510","s526","s641","s713","s820","s832","s838","s953","s1196","s1238","s1423","s1488","s1494","s5378","s9234","s13207","s15850","s35932"]

# listOfSCkt = ["s13207","s15850","s35932","s38417","s38584"]
#listOfSCkt = ["c432","c499","c1908","c3540"]
# listOfSCkt = ["s832","s838","s1494"]
# listOfSCkt = ["s9234","s15850","s38584"]
# listOfSCkt = ["c432","c499","c880","c1355","c1908","c2670","c3540","c5315","c6288","c7552"]
# listOfSCkt = ["c432_synth","c499_synth","c1908_synth","c3540_synth","c7552_synth"]
# listOfSCkt = ["s526_synth","s832_synth","s838_synth","s1494_synth","s9234_synth","s15850_synth","s38584_synth"]
# listOfSCkt = ["b10_synth","b14_synth","b15_synth","b19_synth","b22_synth"]
# listOfSCkt = ["b10","b14","b15"]
#listOfSCkt = ["b10", "b14", "b15", "b22"]
#listOfSCkt = ["s526","s832","s838","s1494","s9234","s15850"]
# listOfSCkt = ["adder_synth","bar_synth","div_synth","hyp_synth","log2_synth","max_synth","multiplier_synth","sin_synth","sqrt_synth"]
# listOfEPFLCtrlCkt = ["arbiter_synth","cavlc_synth","ctrl_synth","dec_synth","i2c_synth","int2float_synth","mem_ctrl_synth","priority_synth","router_synth","voter_synth"]
listOfSCkt = ["c432","c499","c1908","c3540","c7552"]


seriesName = "iscas85"
#seriesName = "iscas89"
#seriesName = "itc99"
# seriesName = "epfl"+os.sep+"arithmetic"
# rootData = "/home/animesh/currentResearch/ISI/newRepos/atpg_structural/code"
rootData = "/home/animesh/currentResearch/ISI/atpg_structural/code"
tempDataPath = rootData + os.sep + "tempData"
testDataRootFolder = rootData + os.sep + "testData" + os.sep + "tessent"
dumpDataRootFolder = rootData + os.sep + "dumpData" + os.sep + "tessent" + os.sep + seriesName
csvDelimiter = ","
verilogExtension = ".v"
testFileExtension = ".spf"
atpgLogFilePathExtension = ".atpg.log"
atpgResultFilePathExtension = ".atpg.result"

doFileExtension = ".dofile"
cellLibraryPath = testDataRootFolder + os.sep + "cellLibrary.atpg"
numCircuitToBeGenerated = 40

# Choice (Random or with Dual Gates)
choice = 1

thresholdPercent = {0.1: "tenPercent",
                    0.3: "thirtyPercent",
                    0.5: "fiftyPercent",
                    0.7: "seventyPercent",
                    0.9: "ninetyPercent"}
#               1 : "hundredPercent"}


for inputCkt in listOfSCkt:
    inputCktNameSynth = inputCkt + "_synth"
    dumpDataFolder = dumpDataRootFolder + os.sep + inputCktNameSynth
    resultLogFilePath = dumpDataFolder + os.sep + "results_" + inputCktNameSynth + ".csv"

    redundancyBinInformationCombinedFilePath = dumpDataFolder + os.sep + "redundancyBinResultsCombined_" + inputCktNameSynth + ".csv"
    redundancyBinInformationIndividualFilePath = dumpDataFolder + os.sep + "redundancyBinResultsIndividual_" + inputCktNameSynth + ".csv"

    rIndividualLogFileHandler = open(redundancyBinInformationIndividualFilePath,'w+')
    rIndividualLogFileHandler.write("cktName" + csvDelimiter + "redundancyBin" + csvDelimiter + "percentChange" + csvDelimiter + "TESTABLE_FAULTS" + csvDelimiter + "OVERALL_FAULTS" + csvDelimiter + "ATPG_COV_TESTABLE" + csvDelimiter + "ATPG_COV_OVERALL" + csvDelimiter + "SIBLING_TEST_PATTERN_COV_TESTABLE"+ csvDelimiter + "SIBLING_TEST_PATTERN_COV_OVERALL")
    rIndividualLogFileHandler.write("\n")

    rCombinedLogFileHandler = open(redundancyBinInformationCombinedFilePath, 'w+')
    rCombinedLogFileHandler.write("cktName" + csvDelimiter + "redundancyBin" + csvDelimiter + "covPercentage" + csvDelimiter + "covType")
    rCombinedLogFileHandler.write("\n")



    atpgDataFrame = pd.read_csv(resultLogFilePath)
    maxRedundantFault = atpgDataFrame['UT'][atpgDataFrame['UT'].idxmax()]
    atpgDataFrame['utLabel'] = pd.cut(atpgDataFrame['UT'], 5)
    groupedData = atpgDataFrame.groupby(['utLabel'])
    # print(groupedData.groups)
    sameRedundancyBucketCkts = {}
    for gKey in groupedData.groups.keys():
        # print(gKey)
        sameRedundancyBucketCkts[gKey] = []
        listOfIndex = groupedData.groups[gKey]
        for idx in listOfIndex:
            # print(atpgDataFrame.iloc[idx])
            sameRedundancyBucketCkts[gKey].append(atpgDataFrame.iloc[idx])

    countBin = 0
    #print(sameRedundancyBucketCkts)
    for redunKey in sorted(sameRedundancyBucketCkts.keys()):
        for rowEntry in sameRedundancyBucketCkts[redunKey]:
            redundantFolderPath = dumpDataFolder + os.sep + "bin_" + str(0)
            for simulateOtherRow in sameRedundancyBucketCkts[redunKey]:
                baseName = rowEntry['cktName'] + "_" + simulateOtherRow['cktName']
                simulateResultFile = redundantFolderPath + os.sep + "simulateLogFile_" + baseName + atpgResultFilePathExtension

                if(not os.path.exists(simulateResultFile)):
                    for x in range(1,5):
                        redundantFolderPath = dumpDataFolder + os.sep + "bin_" + str(x)
                        simulateResultFile = redundantFolderPath + os.sep + "simulateLogFile_" + baseName + atpgResultFilePathExtension
                        if(os.path.exists(simulateResultFile)):
                            break

                readSimulationLogFileHandler = open(simulateResultFile,'r+')
                simulateResultFileLines = readSimulationLogFileHandler.readlines()
                numSimulatedFaultCountLine = simulateResultFileLines[7]
                percentCovUsingSiblingATPGLine = simulateResultFileLines[-1]

                numSimulateFaultLineWords = re.findall('\w+',numSimulatedFaultCountLine)
                totSimulatedFaultCount = int(numSimulateFaultLineWords[-1])
                percentCovUsingSiblingATPGLineWords = re.findall('\w+',percentCovUsingSiblingATPGLine)
                percentCovUsingSiblingATPG = float(percentCovUsingSiblingATPGLineWords[1])

                numFaultCoveredBySiblingATPG = int(percentCovUsingSiblingATPG*totSimulatedFaultCount)
                testableFaultCoverage = numFaultCoveredBySiblingATPG/rowEntry["TE"]
                overallFaultCoverage = numFaultCoveredBySiblingATPG/(rowEntry["TE"]+rowEntry["UT"])
                testableFaultCoverage = float("{0:.2f}".format(testableFaultCoverage))
                overallFaultCoverage = float("{0:.2f}".format(overallFaultCoverage))

                rIndividualLogFileHandler.write(baseName+csvDelimiter+str(redunKey).replace(",","-")+csvDelimiter+str(rowEntry["percentChange"])+csvDelimiter+str(rowEntry["TE"])+csvDelimiter+str(rowEntry["TE"]+rowEntry["UT"])+csvDelimiter+str(rowEntry["test_cov"])+csvDelimiter+str(rowEntry["fault_cov"])+csvDelimiter+str(testableFaultCoverage)+csvDelimiter+str(overallFaultCoverage))
                rIndividualLogFileHandler.write("\n")

                rCombinedLogFileHandler.write(
                    baseName + csvDelimiter + str(redunKey).replace(",", "-") + csvDelimiter + str(rowEntry["test_cov"]) + csvDelimiter + "atpgCovTest")
                rCombinedLogFileHandler.write("\n")

                rCombinedLogFileHandler.write(
                    baseName + csvDelimiter + str(redunKey).replace(",", "-") + csvDelimiter + str(rowEntry["fault_cov"]) + csvDelimiter + "atpgCovOverall")
                rCombinedLogFileHandler.write("\n")

                rCombinedLogFileHandler.write(
                    baseName + csvDelimiter + str(redunKey).replace(",", "-") + csvDelimiter + str(testableFaultCoverage) + csvDelimiter + "siblingCovTest")
                rCombinedLogFileHandler.write("\n")

                rCombinedLogFileHandler.write(
                    baseName + csvDelimiter + str(redunKey).replace(",", "-") + csvDelimiter + str(overallFaultCoverage) + csvDelimiter + "siblingCovOverall")
                rCombinedLogFileHandler.write("\n")

    rIndividualLogFileHandler.close()
    rCombinedLogFileHandler.close()


    testableCombinedDf = pd.read_csv(redundancyBinInformationCombinedFilePath)
    overallCombinedDf = pd.read_csv(redundancyBinInformationCombinedFilePath)
    individualDf = pd.read_csv(redundancyBinInformationIndividualFilePath)

    graphCombinedTestableFaultCoverage = dumpDataFolder + os.sep + "graphTestableFaultCovCombined_" + inputCktNameSynth + ".pdf"
    graphCombinedOverallFaultCoverage = dumpDataFolder + os.sep + "graphOverallFaultCovCombined_" + inputCktNameSynth + ".pdf"

    graphIndividualTestableATPGCov = dumpDataFolder + os.sep + "graphTestableFaultCovATPG_" + inputCktNameSynth + ".pdf"
    graphIndividualTestableSiblingCov = dumpDataFolder + os.sep + "graphTestableFaultCovSIBLING_" + inputCktNameSynth + ".pdf"
    graphIndividualOverallATPGCov = dumpDataFolder + os.sep + "graphOverallFaultCovATPG_" + inputCktNameSynth + ".pdf"
    graphIndividualOverallSiblingCov = dumpDataFolder + os.sep + "graphOverallFaultCovSIBLING_" + inputCktNameSynth + ".pdf"

    graphScatterPlotPercentChangeRedundancyBin = dumpDataFolder + os.sep + "graphRedundancyBinVsPercentChange_" + inputCktNameSynth + ".pdf"

    #plt.rcParams["xtick.labelsize"] = 8
    #plt.rcParams["xtick.labelsize"] = 8
    plt.xticks(rotation=30)
    plt.rc('xtick', labelsize=12)
    plt.rc('ytick', labelsize=14)

    plt.figure(figsize=(8, 7))
    graphTitle = seriesName+" - "+inputCkt

    #Individual Graph Dumps

    doBoxPlot(graphIndividualTestableATPGCov,"redundancyBin","ATPG_COV_TESTABLE","redundancyBin","ATPG_COV_TESTABLE",None,individualDf,graphTitle)
    doBoxPlot(graphIndividualTestableSiblingCov,"redundancyBin","SIBLING_TEST_PATTERN_COV_TESTABLE","redundancyBin","SIBLING_TEST_PATTERN_COV_TESTABLE",None,individualDf,graphTitle)

    doBoxPlot(graphIndividualOverallATPGCov, "redundancyBin", "ATPG_COV_OVERALL","redundancyBin", "ATPG_COV_OVERALL", None, individualDf,graphTitle)
    doBoxPlot(graphIndividualOverallSiblingCov, "redundancyBin", "SIBLING_TEST_PATTERN_COV_OVERALL","redundancyBin", "SIBLING_TEST_PATTERN_COV_OVERALL", None,individualDf,graphTitle)

    #Combined Graph Dumps

    testableCombinedDf = testableCombinedDf.drop(testableCombinedDf[(testableCombinedDf['covType'] == 'siblingCovOverall')|(testableCombinedDf['covType'] == 'atpgCovOverall')].index)

    overallCombinedDf = overallCombinedDf.drop(overallCombinedDf[(overallCombinedDf['covType'] == "atpgCovTest") | (overallCombinedDf['covType'] == "siblingCovTest")].index)

    doBoxPlot(graphCombinedTestableFaultCoverage,"redundancyBin","covPercentage","Redundancy Bins","Fault Coverage Efficiency","covType",testableCombinedDf,graphTitle)
    doBoxPlot(graphCombinedOverallFaultCoverage,"redundancyBin","covPercentage","Redundancy Bins","Overall Fault Coverage","covType",overallCombinedDf,graphTitle)


    doSwarmPlot(graphScatterPlotPercentChangeRedundancyBin,"percentChange","TESTABLE_FAULTS", "\u03B1 values","# Testable Faults",individualDf,graphTitle)
    #doBoxPlot(graphScatterPlotPercentChangeRedundancyBin, "percentChange", "TESTABLE_FAULTS", None ,individualDf)
    statisticalDataPlotFile = dumpDataFolder + os.sep + "statisticalPlot_" + inputCktNameSynth + ".txt"
    statisticalDataPlot = open(statisticalDataPlotFile,'w+')
    statisticalDataPlot.write("ATPG_COV_TESTABLE \n"+pd.Series.to_string(individualDf.groupby('redundancyBin')["ATPG_COV_TESTABLE"].median())+"\n")
    statisticalDataPlot.write("SIBLING_TEST_PATTERN_COV_TESTABLE \n"+pd.Series.to_string(individualDf.groupby('redundancyBin')["SIBLING_TEST_PATTERN_COV_TESTABLE"].median())+"\n")
    statisticalDataPlot.write("ATPG_COV_OVERALL \n"+pd.Series.to_string(individualDf.groupby('redundancyBin')["ATPG_COV_OVERALL"].median())+"\n")
    statisticalDataPlot.write("SIBLING_TEST_PATTERN_COV_OVERALL \n"+pd.Series.to_string(individualDf.groupby('redundancyBin')["SIBLING_TEST_PATTERN_COV_OVERALL"].median())+"\n")
    statisticalDataPlot.write("ATPG_TESTABLE_FAULTS \n"+pd.Series.to_string(individualDf.groupby('percentChange')["TESTABLE_FAULTS"].mean())+"\n")
    statisticalDataPlot.close()
