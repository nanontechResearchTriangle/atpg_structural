import re,os,copy
import random
import pandas as pd
import shutil

faultCountDict = {}
coverageDict = {}

def cleanAndInitializeFaultCountDict():
    faultCountDict["FU"] = "0"
    faultCountDict["UO"] = "0"
    faultCountDict["UC"] = "0"
    faultCountDict["DS"] = "0"
    faultCountDict["DI"] = "0"
    faultCountDict["PU"] = "0"
    faultCountDict["PT"] = "0"
    faultCountDict["UU"] = "0"
    faultCountDict["TI"] = "0"
    faultCountDict["BL"] = "0"
    faultCountDict["RE"] = "0"
    faultCountDict["AU"] = "0"
    coverageDict["test_coverage"] = "0"
    coverageDict["fault_coverage"] = "0"
    coverageDict["atpg_effectiveness"] = "0"

setOfFaultClass = {"FU","UO","UC","DS","DI","PU","PT","UU","TI","BL","RE","AU"}
setOfCoverageParams = {"test_coverage","fault_coverage","atpg_effectiveness"}
tessentExec = "tessent"

#listOfSCkt = ["s208", "s298", "s344" ,"s349", "s382", "s386", "s400", "s420", "s444", "s510","s526","s641","s713","s820","s832","s838","s953","s1196","s1238","s1423","s1488","s1494","s5378","s9234","s13207","s15850","s35932"]

#listOfSCkt = ["s13207","s15850","s35932","s38417","s38584"]
#listOfSCkt = ["c432","c499","c1908","c3540","c7552"]
#listOfSCkt = ["s526","s832","s838","s1494","s9234","s15850","s38584"]
#listOfSCkt = ["s9234","s15850","s38584"]
#listOfSCkt = ["c432","c499","c880","c1355","c1908","c2670","c3540","c5315","c6288","c7552"]
#listOfSCkt = ["c432_synth","c499_synth","c1908_synth","c3540_synth","c7552_synth"]
#listOfSCkt = ["s526_synth","s832_synth","s838_synth","s1494_synth","s9234_synth","s15850_synth","s38584_synth"]
#listOfSCkt = ["b10_synth","b14_synth","b15_synth","b19_synth","b22_synth"]
#listOfSCkt = ["b10","b14","b15"]
#listOfSCkt = ["b10","b14","b15","b22"]
#listOfSCkt = ["adder_synth","bar_synth","div_synth","hyp_synth","log2_synth","max_synth","multiplier_synth","sin_synth","sqrt_synth"]
listOfSCkt = ["adder","bar","max","multiplier","sin"]
#listOfEPFLCtrlCkt = ["arbiter_synth","cavlc_synth","ctrl_synth","dec_synth","i2c_synth","int2float_synth","mem_ctrl_synth","priority_synth","router_synth","voter_synth"]


#inputCktName = "c7552"
#seriesName = "iscas85"
#seriesName = "iscas89"
#seriesName = "itc99"
seriesName = "epfl"+os.sep+"arithmetic"
#rootData = "/home/animesh/currentResearch/ISI/newRepos/atpg_structural/code"
rootData = "/home/animesh/currentResearch/ISI/atpg_structural/code"
tempDataPath = rootData+os.sep+"tempData"
testDataRootFolder = rootData+os.sep+"testData"+os.sep+"tessent"
dumpDataRootFolder = rootData+os.sep+"dumpData"+os.sep+"tessent"+os.sep+seriesName
csvDelimiter = ","
verilogExtension = ".v"
testFileExtension = ".spf"
atpgLogFilePathExtension = ".atpg.log"
atpgResultFilePathExtension = ".atpg.result"

doFileExtension = ".dofile"
cellLibraryPath = testDataRootFolder+os.sep+"cellLibrary.atpg"
numCircuitToBeGenerated = 40

#Choice (Random or with Dual Gates)
choice = 1

thresholdPercent = {0.1 : "tenPercent" ,
                    0.3 : "thirtyPercent",
                    0.5 : "fiftyPercent",
                    0.7 : "seventyPercent",
                    0.9 : "ninetyPercent"}
     #               1 : "hundredPercent"}




for inputCkt in listOfSCkt:
    inputCktNameSynth = inputCkt+"_synth"
    dumpDataFolder = dumpDataRootFolder+os.sep+inputCktNameSynth
    resultLogFilePath = dumpDataFolder+os.sep+"results_"+inputCktNameSynth+".csv"
    resultLogFileHandler = open(resultLogFilePath,'w+')
    #resultLogFileHandler.write("cktName"+csvDelimiter+"percentChange"+csvDelimiter+"FU"+csvDelimiter+"TE(DS+DI+PU+PT+AU+UC+UO)"+csvDelimiter+"UT(UU+TI+BL+RE)"+csvDelimiter+"DS"+csvDelimiter+"DI"+csvDelimiter+"PT"+csvDelimiter+"PU"
    #                           +csvDelimiter+"AU"+csvDelimiter+"UC"+csvDelimiter+"UO"+csvDelimiter+"UU"+csvDelimiter+"TI"+csvDelimiter+"BL"+csvDelimiter+"RE"+csvDelimiter+"test_cov"+csvDelimiter+"fault_cov"+csvDelimiter+"atpg_eff")
    resultLogFileHandler.write("cktName" + csvDelimiter + "percentChange" + csvDelimiter + "FU" + csvDelimiter + "TE" + csvDelimiter + "UT" + csvDelimiter + "DS" + csvDelimiter + "DI" + csvDelimiter + "PT" + csvDelimiter + "PU"
                               +csvDelimiter+"AU"+csvDelimiter+"UC"+csvDelimiter+"UO"+csvDelimiter+"UU"+csvDelimiter+"TI"+csvDelimiter+"BL"+csvDelimiter+"RE"+csvDelimiter+"test_cov"+csvDelimiter+"fault_cov"+csvDelimiter+"atpg_eff")

    resultLogFileHandler.write("\n")


    for percentValue in thresholdPercent.keys():
        circuitDumpPath = dumpDataFolder + os.sep + thresholdPercent[percentValue]
        for loopVar in range(numCircuitToBeGenerated):
            inputCktName = inputCktNameSynth + "_" + thresholdPercent[percentValue] + "_" + str(loopVar)
            inputCktFilePath = circuitDumpPath+os.sep+inputCktName+verilogExtension
            inputCktFileTestPath = circuitDumpPath+os.sep+inputCktName+testFileExtension
            inputCktFileATPGLogPath = circuitDumpPath+os.sep+inputCktName+atpgLogFilePathExtension
            inputCktFileATPGResultPath = circuitDumpPath+os.sep+inputCktName+atpgResultFilePathExtension
            inputCktATPGDoFile = circuitDumpPath+os.sep+inputCktName+doFileExtension
            '''
            paraGraph = "set_context patterns -scan\n" \
                    "read_verilog " + inputCktFilePath +" \n" \
                    "read_cell_library "+cellLibraryPath + "\n" \
                    "set_logfile_handling "+inputCktFileATPGLogPath+" -rep\n" \
                    "set_current_design "+ inputCkt+"_C \n" \
                    "set_system_mode analysis\n" \
                    "add_fault -all\n" \
                    "set_fault_mode collapsed\n" \
                    "set_fault_type stuck\n" \
                    "create_patterns\n" \
                    "write_patterns "+inputCktFileTestPath+" -stil -rep\n" \
                    "report_statistics > "+inputCktFileATPGResultPath+"\n" \
                    "exit\n"

            doFileWriter = open(inputCktATPGDoFile,'w+')
            doFileWriter.write(paraGraph)
            doFileWriter.close()

            tessantRunCmd = "tessent -shell -dofile "+inputCktATPGDoFile
            os.system(tessantRunCmd)
            '''

            cleanAndInitializeFaultCountDict()
            readATPGStatsReportFile = open(inputCktFileATPGResultPath,'r')
            statsResultFileLines = readATPGStatsReportFile.readlines()
            for lineUnderTest in statsResultFileLines:
                words = re.findall('[a-zA-Z0-9._]+',lineUnderTest)

                if(len(words)>1):
                    if(setOfFaultClass.__contains__(words[0])):
                        if(words[0] == "FU"):
                            #print(words)
                            faultCountDict[words[0]] = words[2]
                        elif(len(words)==4):
                            #In order to remove entries from faultSubclasses
                            #print(words)
                            faultCountDict[words[0]] = words[2]
                    elif(setOfCoverageParams.__contains__(words[0])):
                        #print(words)
                        coverageDict[words[0]] = words[1]

                #print(words)
            totalTEFaults = int(faultCountDict["DS"])+int(faultCountDict["DI"])+int(faultCountDict["PU"])+int(faultCountDict["PT"])+int(faultCountDict["AU"])\
                            +int(faultCountDict["UC"])+int(faultCountDict["UO"])
            totalUTFaults = int(faultCountDict["UU"])+int(faultCountDict["BL"])+int(faultCountDict["TI"])+int(faultCountDict["RE"])
            #print(totalTEFaults,totalUTFaults)

            resultLogFileHandler.write(inputCktName + csvDelimiter + thresholdPercent[percentValue] + csvDelimiter + faultCountDict["FU"] + csvDelimiter
                                       + str(totalTEFaults) + csvDelimiter + str(totalUTFaults) + csvDelimiter + faultCountDict["DS"] + csvDelimiter
                                       + faultCountDict["DI"] + csvDelimiter + faultCountDict["PT"] + csvDelimiter + faultCountDict["PU"] + csvDelimiter
                                       + faultCountDict["AU"] + csvDelimiter + faultCountDict["UC"] + csvDelimiter + faultCountDict["UO"] + csvDelimiter
                                       + faultCountDict["UU"] + csvDelimiter + faultCountDict["TI"] + csvDelimiter + faultCountDict["BL"] + csvDelimiter
                                       + faultCountDict["RE"] + csvDelimiter + coverageDict["test_coverage"] + csvDelimiter + coverageDict["fault_coverage"]
                                       + csvDelimiter + coverageDict["atpg_effectiveness"])
            resultLogFileHandler.write("\n")

    resultLogFileHandler.close()


    atpgDataFrame = pd.read_csv(resultLogFilePath)
    maxRedundantFault = atpgDataFrame['UT'][atpgDataFrame['UT'].idxmax()]
    atpgDataFrame['utLabel'] = pd.cut(atpgDataFrame['UT'], 5)
    groupedData = atpgDataFrame.groupby(['utLabel'])
    #print(groupedData.groups)
    sameRedundancyBucketCkts = {}
    for gKey in groupedData.groups.keys():
        #print(gKey)
        sameRedundancyBucketCkts[gKey] = []
        listOfIndex = groupedData.groups[gKey]
        for idx in listOfIndex:
            #print(atpgDataFrame.iloc[idx])
            sameRedundancyBucketCkts[gKey].append(atpgDataFrame.iloc[idx])

    countBin = 0
    for redunKey in sameRedundancyBucketCkts.keys():
        redundantFolderPath = dumpDataFolder+os.sep+"bin_"+str(countBin)
        if(not os.path.isdir(redundantFolderPath)):
            os.mkdir(redundantFolderPath)
        countBin+=1
        print(redunKey)
        for rowEntry in sameRedundancyBucketCkts[redunKey]:
            srcFolder = dumpDataFolder+os.sep+rowEntry['percentChange']
            srcCktFile = srcFolder+os.sep+rowEntry['cktName']+verilogExtension
            srcDesignNameList = rowEntry['cktName'].split('_')
            shutil.copy(srcCktFile,redundantFolderPath)
            #shutil.copy(srcCktATPGFile,redundantFolderPath)
            for simulateOtherRow in sameRedundancyBucketCkts[redunKey]:
                simulateDoFile = redundantFolderPath+os.sep+rowEntry['cktName']+"_"+simulateOtherRow['cktName']+doFileExtension
                simulateResultFile = redundantFolderPath+os.sep+"simulateLogFile_"+rowEntry['cktName']+"_"+simulateOtherRow['cktName']+atpgResultFilePathExtension
                simulateLogFile = redundantFolderPath+os.sep+"simulateLogFile_"+rowEntry['cktName']+"_"+simulateOtherRow['cktName']+atpgLogFilePathExtension
                readVerilogFile = srcCktFile
                readTestFile = dumpDataFolder+os.sep+simulateOtherRow['percentChange'] + os.sep + simulateOtherRow['cktName'] + testFileExtension
                simulateParagraph = "set_context patterns -scan\n" \
                            "read_verilog " + readVerilogFile +" \n" \
                            "read_cell_library "+cellLibraryPath + "\n" \
                            "set_logfile_handling "+simulateLogFile+" -rep\n" \
                            "set_current_design top \n" \
                            "set_system_mode analysis\n" \
                            "add_fault -all\n" \
                            "set_fault_mode collapsed\n" \
                            "set_fault_type stuck\n" \
                            "read_patterns "+readTestFile +"\n" \
                            "simulate_patterns > "+simulateResultFile+"\n" \
                            "exit\n"

                doFileWriter = open(simulateDoFile,'w+')
                doFileWriter.write(simulateParagraph)
                doFileWriter.close()

                tessantFaultSimulationRunCmd = "tessent -shell -dofile "+simulateDoFile
                os.system(tessantFaultSimulationRunCmd)

