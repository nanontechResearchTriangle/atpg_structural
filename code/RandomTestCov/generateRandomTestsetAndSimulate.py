import os,random,re
import pandas as pd
import seaborn as sns


rootData = "/home/animesh/currentResearch/ISI/secureada/PycharmProject/RandomTestCov"
testData = rootData+os.sep+"testData"
origCkt = testData+os.sep+"mc1.bench"
partialDualCkt = testData+os.sep+"mc1_partial_dual_take1.bench"

hopeExec = "/home/animesh/currentResearch/ISI/benchmarksAndTools/hope/hope"
atalantaExec = "/home/animesh/currentResearch/ISI/benchmarksAndTools/atalanta/atalanta"
#siblingBench = testData+os.sep+"mc1_so.bench"
#siblingTestFile = rootData+os.sep+"mc1_so.test"

siblingBench = testData+os.sep+"mc1_take1.bench"
siblingTestFile = rootData+os.sep+"mc1_take1.test"

dumpData = rootData+os.sep+"dumpData"
#benchFile = testData+os.sep+"mc1_partial_dual_so.bench"
benchFile = testData+os.sep+"mc1_partial_dual.bench"



numCktInputs = 6

testFileHeader = dumpData+os.sep+"testFile_"
testFileExtension = ".test"
logFileHeader = dumpData+os.sep+"logFile_"
logFileExtension = ".log"
consolidatedLog = dumpData+os.sep+"RandomCoverage_file.log"
consolidatedLogFileHandler = open(consolidatedLog,"w+")

consolidatedLog2 = dumpData+os.sep+"SiblingCoverage_file.log"
consolidatedLogFileHandler2 = open(consolidatedLog2,"w+")
statisticalDataDump = dumpData+os.sep+"statsData.csv"
statisticalDataDumpHandler = open(statisticalDataDump,'w+')
statisticalDataDumpHandler.write("obsPoint,coverageType,percentage")
statisticalDataDumpHandler.write("\n")

for j in range(500):
    testFile = testFileHeader+str(j)+testFileExtension
    testFileHandler = open(testFile,"w+")
    logFile = logFileHeader+str(j)+logFileExtension
    siblingLogFile = dumpData+os.sep+"siblingLogFile_"+str(j)+logFileExtension
    for i in range(4):
        num = random.randint(0,2 **numCktInputs -1)
        #print("{0:06b}".format(num))
        testFileHandler.write(str(i+1)+" : "+"{0:06b}".format(num)+"\n")
    testFileHandler.close()

    atalantaRunCmd = atalantaExec+ " -N "+ siblingBench + " > tempLog"
    os.system(atalantaRunCmd)
    hopeRunCmd = hopeExec + " -t " + siblingTestFile + " " + benchFile + " > " + siblingLogFile
    os.system(hopeRunCmd)

    logFileHandler = open(siblingLogFile, 'r+')
    logFileLines = logFileHandler.readlines()
    faultCoverageLine = logFileLines[-11]
    wordsS = re.findall('[0-9.]+',faultCoverageLine)
    #print("Sibling Cover"+wordsS[0])
    consolidatedLogFileHandler2.write(str(j)+":"+faultCoverageLine + "\n")

    hopeRunCmd = hopeExec + " -t " + testFile + " " + benchFile + " > " + logFile
    os.system(hopeRunCmd)
    logFileHandler = open(logFile,'r+')
    logFileLines = logFileHandler.readlines()
    faultCoverageLine = logFileLines[-11]
    wordsR = re.findall('[0-9.]+', faultCoverageLine)
    #print("Random Cover"+wordsR[0])
    consolidatedLogFileHandler.write(str(j)+":"+faultCoverageLine+"\n")

    statisticalDataDumpHandler.write(str(j)+","+"FC(Random)"+","+str(wordsR[0]))
    statisticalDataDumpHandler.write("\n")
    statisticalDataDumpHandler.write(str(j)+","+"FC(Sibling)"+","+str(wordsS[0]))
    statisticalDataDumpHandler.write("\n")


consolidatedLogFileHandler.close()
consolidatedLogFileHandler2.close()
statisticalDataDumpHandler.close()

dataFrame = pd.read_csv(statisticalDataDump)

fileName = rootData+os.sep+"statsPlot.pdf"

ax2 = sns.lineplot(y="percentage", x="obsPoint", hue = "coverageType",markers=True,data=dataFrame)
ax2.set_xlabel("Observation Points", fontsize=14)
ax2.set_ylabel("Fault Coverage", fontsize=14)
ax2.set_title("Random Coverage v/s Sibling Coverage", fontsize=14)
ax2.tick_params(axis='x', rotation=10)
fig = ax2.get_figure()
fig.savefig(fileName, format='pdf', dpi=1500)
ax2.clear()


