ATPG_COV_TESTABLE 
redundancyBin
(106.4- 152.6]    100.0
(13.769- 60.2]    100.0
(152.6- 198.8]    100.0
(198.8- 245.0]    100.0
(60.2- 106.4]     100.0
SIBLING_TEST_PATTERN_COV_TESTABLE 
redundancyBin
(106.4- 152.6]    81.81
(13.769- 60.2]    87.97
(152.6- 198.8]    79.90
(198.8- 245.0]    81.74
(60.2- 106.4]     84.16
ATPG_COV_OVERALL 
redundancyBin
(106.4- 152.6]    75.390
(13.769- 60.2]    90.860
(152.6- 198.8]    67.800
(198.8- 245.0]    58.755
(60.2- 106.4]     83.660
SIBLING_TEST_PATTERN_COV_OVERALL 
redundancyBin
(106.4- 152.6]    61.16
(13.769- 60.2]    80.37
(152.6- 198.8]    53.93
(198.8- 245.0]    47.05
(60.2- 106.4]     70.17
ATPG_TESTABLE_FAULTS 
percentChange
fiftyPercent      374.170375
ninetyPercent     454.552012
seventyPercent    389.941958
tenPercent        452.309419
thirtyPercent     392.561086
