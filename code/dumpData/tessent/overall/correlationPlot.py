from string import ascii_letters
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import os

import matplotlib.pyplot as plt


sns.set(style="white")

# Generate a large random dataset
#rs = np.random.RandomState(33)
#d = pd.DataFrame(data=rs.normal(size=(100, 26)),
#                 columns=list(ascii_letters[26:]))

# Compute the correlation matrix

rootData = "/home/animesh/currentResearch/ISI/secureada/PycharmProject/RandomTestCov"
fileName = rootData+os.sep+"statsPlot.pdf"
csvFile = rootData+os.sep+"circuitMismatch.csv"
#csvFile = rootData+os.sep+"corrData.csv"
#corr.to_csv(csvFile)
corr = pd.read_csv(csvFile,header=[0],index_col=[0])


# Generate a mask for the upper triangle
mask = np.zeros_like(corr, dtype=np.bool)
mask[np.triu_indices_from(mask)] = True

# Set up the matplotlib figure
f, ax = plt.subplots(figsize=(11, 9))

# Generate a custom diverging colormap
#cmap = sns.diverging_palette(220, 10, as_cmap=True)

# Draw the heatmap with the mask and correct aspect ratio


#ax2 = sns.heatmap(corr, cmap=cmap, mask = mask, vmax=.3, center=0,square=True, linewidths=.5, cbar_kws={"shrink": .5})
#ax2 = sns.heatmap(corr, mask = mask, vmax=.3, center=0,square=True, linewidths=.5, cbar_kws={"shrink": .5})
#ax2 = sns.heatmap(corr, mask = mask, cmap='RdYlGn_r', linewidths=0.5, annot=True)
ax2 = sns.heatmap(corr, mask = mask, cmap='YlGnBu_r', linewidths=0.5, annot=True,cbar_kws={'label': 'Normalized Boolean Difference'})

ax2.set_xlabel("Sibling Circuits", fontsize=14)
#ax2.set_ylabel("Normalized Boolean Difference", fontsize=14)
ax2.set_title("Normalized boolean difference distribution amongst Sibling Circuits (ISCAS : c432)", fontsize=14)
ax2.tick_params(axis='x', rotation=10)
ax2.tick_params(axis='y', rotation=10)


#plt.pcolor(df)
#plt.yticks(np.arange(0.5, len(df.index), 1), df.index)
#plt.xticks(np.arange(0.5, len(df.columns), 1), df.columns)
#plt.show()
fig = ax2.get_figure()
fig.savefig(fileName, format='pdf', dpi=1500)
ax2.clear()