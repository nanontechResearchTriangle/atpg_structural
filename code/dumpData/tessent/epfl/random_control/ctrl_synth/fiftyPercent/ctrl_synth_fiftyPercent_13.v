// Benchmark "top" written by ABC on Sat May 11 19:26:20 2019

module top ( 
    \opcode[0] , \opcode[1] , \opcode[2] , \opcode[3] , \opcode[4] ,
    \op_ext[0] , \op_ext[1] ,
    \sel_reg_dst[0] , \sel_reg_dst[1] , \sel_alu_opB[0] , \sel_alu_opB[1] ,
    \alu_op[0] , \alu_op[1] , \alu_op[2] , \alu_op_ext[0] ,
    \alu_op_ext[1] , \alu_op_ext[2] , \alu_op_ext[3] , halt, reg_write,
    sel_pc_opA, sel_pc_opB, beqz, bnez, bgez, bltz, jump, Cin, invA, invB,
    sign, mem_write, sel_wb  );
  input  \opcode[0] , \opcode[1] , \opcode[2] , \opcode[3] , \opcode[4] ,
    \op_ext[0] , \op_ext[1] ;
  output \sel_reg_dst[0] , \sel_reg_dst[1] , \sel_alu_opB[0] ,
    \sel_alu_opB[1] , \alu_op[0] , \alu_op[1] , \alu_op[2] ,
    \alu_op_ext[0] , \alu_op_ext[1] , \alu_op_ext[2] , \alu_op_ext[3] ,
    halt, reg_write, sel_pc_opA, sel_pc_opB, beqz, bnez, bgez, bltz, jump,
    Cin, invA, invB, sign, mem_write, sel_wb;
  wire n34, n35, n36, n37, n38, n39, n40, n41, n42, n43, n44, n45, n46, n47,
    n48, n49, n51, n52, n53, n54, n55, n56, n57, n58, n59, n60, n62, n63,
    n64, n65, n66, n67, n68, n69, n71, n72, n73, n74, n75, n76, n77, n78,
    n79, n81, n82, n83, n84, n85, n86, n87, n88, n89, n90, n91, n92, n93,
    n94, n95, n96, n97, n98, n100, n101, n102, n103, n104, n105, n107,
    n108, n109, n110, n111, n112, n113, n114, n115, n116, n117, n118, n120,
    n121, n122, n123, n124, n126, n127, n128, n129, n130, n131, n133, n134,
    n135, n137, n138, n139, n140, n142, n143, n145, n146, n147, n148, n149,
    n150, n151, n152, n154, n157, n158, n160, n161, n163, n165, n168, n169,
    n170, n171, n172, n173, n174, n175, n177, n178, n179, n180, n181, n182,
    n184, n185, n188, n189, n190, n192;
  inv01  g000(.A(\opcode[2] ), .Y(n34));
  inv01  g001(.A(\opcode[1] ), .Y(n35));
  or02  g002(.A0(n35), .A1(\opcode[0] ), .Y(n36));
  nand02 g003(.A0(\opcode[4] ), .A1(\opcode[3] ), .Y(n37));
  inv01  g004(.A(n37), .Y(n38));
  or02  g005(.A0(n38), .A1(n36), .Y(n39));
  or02  g006(.A0(\opcode[3] ), .A1(\opcode[1] ), .Y(n40));
  and02  g007(.A0(n40), .A1(\opcode[4] ), .Y(n41));
  or02   g008(.A0(n41), .A1(n39), .Y(n42));
  and02  g009(.A0(n42), .A1(n34), .Y(n43));
  and02  g010(.A0(\opcode[3] ), .A1(n35), .Y(n44));
  or02  g011(.A0(n44), .A1(\opcode[4] ), .Y(n45));
  xor2   g012(.A0(\opcode[4] ), .A1(\opcode[3] ), .Y(n46));
  nor02  g013(.A0(n46), .A1(n35), .Y(n47));
  or02   g014(.A0(n47), .A1(n45), .Y(n48));
  or02  g015(.A0(n48), .A1(\opcode[2] ), .Y(n49));
  and02   g016(.A0(n49), .A1(n43), .Y(\sel_reg_dst[0] ));
  nand02  g017(.A0(n37), .A1(\opcode[0] ), .Y(n51));
  and02   g018(.A0(n51), .A1(\opcode[1] ), .Y(n52));
  inv01  g019(.A(\opcode[3] ), .Y(n53));
  or02  g020(.A0(\opcode[4] ), .A1(n53), .Y(n54));
  or02   g021(.A0(n54), .A1(n35), .Y(n55));
  and02  g022(.A0(n55), .A1(n52), .Y(n56));
  or02   g023(.A0(n56), .A1(\opcode[2] ), .Y(n57));
  and02   g024(.A0(\opcode[4] ), .A1(\opcode[3] ), .Y(n58));
  nand02  g025(.A0(n58), .A1(n35), .Y(n59));
  or02   g026(.A0(n59), .A1(n34), .Y(n60));
  and02  g027(.A0(n60), .A1(n57), .Y(\sel_reg_dst[1] ));
  and02   g028(.A0(n46), .A1(\opcode[0] ), .Y(n62));
  inv01  g029(.A(\opcode[0] ), .Y(n63));
  inv01  g030(.A(\opcode[4] ), .Y(n64));
  and02  g031(.A0(n64), .A1(\opcode[3] ), .Y(n65));
  and02   g032(.A0(n65), .A1(n63), .Y(n66));
  and02  g033(.A0(n66), .A1(n62), .Y(n67));
  and02   g034(.A0(n67), .A1(n35), .Y(n68));
  or02  g035(.A0(n68), .A1(n52), .Y(n69));
  and02  g036(.A0(n69), .A1(n34), .Y(\sel_alu_opB[0] ));
  and02   g037(.A0(n64), .A1(\opcode[3] ), .Y(n71));
  nor02  g038(.A0(\opcode[3] ), .A1(\opcode[0] ), .Y(n72));
  nor02 g039(.A0(n72), .A1(n71), .Y(n73));
  or02   g040(.A0(n46), .A1(n63), .Y(n74));
  nand02 g041(.A0(n74), .A1(n73), .Y(n75));
  nand02 g042(.A0(n75), .A1(n35), .Y(n76));
  or02  g043(.A0(n76), .A1(n55), .Y(n77));
  and02   g044(.A0(n77), .A1(\opcode[2] ), .Y(n78));
  or02   g045(.A0(n54), .A1(n34), .Y(n79));
  and02  g046(.A0(n79), .A1(n78), .Y(\sel_alu_opB[1] ));
  and02  g047(.A0(\opcode[3] ), .A1(n63), .Y(n81));
  or02  g048(.A0(\op_ext[0] ), .A1(\opcode[4] ), .Y(n82));
  or02  g049(.A0(n82), .A1(n81), .Y(n83));
  inv01  g050(.A(\op_ext[1] ), .Y(n84));
  or02  g051(.A0(n84), .A1(\opcode[3] ), .Y(n85));
  and02  g052(.A0(n85), .A1(n37), .Y(n86));
  nand02  g053(.A0(\op_ext[0] ), .A1(n53), .Y(n87));
  and02  g054(.A0(n87), .A1(n37), .Y(n88));
  and02  g055(.A0(\op_ext[0] ), .A1(\opcode[3] ), .Y(n89));
  and02   g056(.A0(n89), .A1(n88), .Y(n90));
  and02  g057(.A0(n90), .A1(\op_ext[1] ), .Y(n91));
  and02   g058(.A0(n91), .A1(n86), .Y(n92));
  or02  g059(.A0(n92), .A1(\opcode[0] ), .Y(n93));
  or02   g060(.A0(n93), .A1(n83), .Y(n94));
  and02  g061(.A0(n94), .A1(\opcode[1] ), .Y(n95));
  or02   g062(.A0(n95), .A1(\opcode[2] ), .Y(n96));
  or02  g063(.A0(n54), .A1(\opcode[0] ), .Y(n97));
  and02   g064(.A0(n97), .A1(n34), .Y(n98));
  or02  g065(.A0(n98), .A1(n96), .Y(\alu_op[0] ));
  or02  g066(.A0(\op_ext[1] ), .A1(\opcode[3] ), .Y(n100));
  or02   g067(.A0(n100), .A1(n86), .Y(n101));
  or02  g068(.A0(n101), .A1(\opcode[1] ), .Y(n102));
  or02   g069(.A0(n102), .A1(\opcode[2] ), .Y(n103));
  and02  g070(.A0(n54), .A1(\opcode[1] ), .Y(n104));
  or02   g071(.A0(n104), .A1(n34), .Y(n105));
  or02  g072(.A0(n105), .A1(n103), .Y(\alu_op[1] ));
  and02   g073(.A0(\opcode[4] ), .A1(\opcode[3] ), .Y(n107));
  or02  g074(.A0(n37), .A1(n35), .Y(n108));
  or02  g075(.A0(n108), .A1(n107), .Y(n109));
  or02  g076(.A0(n37), .A1(n63), .Y(n110));
  or02  g077(.A0(n110), .A1(n107), .Y(n111));
  or02  g078(.A0(n58), .A1(\opcode[0] ), .Y(n112));
  and02   g079(.A0(n112), .A1(n111), .Y(n113));
  or02  g080(.A0(n113), .A1(\opcode[1] ), .Y(n114));
  or02   g081(.A0(n114), .A1(n109), .Y(n115));
  and02  g082(.A0(n115), .A1(n34), .Y(n116));
  and02  g083(.A0(\opcode[3] ), .A1(\opcode[2] ), .Y(n117));
  and02  g084(.A0(n117), .A1(\opcode[4] ), .Y(n118));
  or02   g085(.A0(n118), .A1(n116), .Y(\alu_op[2] ));
  nand02  g086(.A0(\opcode[2] ), .A1(\opcode[1] ), .Y(n120));
  and02  g087(.A0(n120), .A1(n52), .Y(n121));
  and02  g088(.A0(n75), .A1(\opcode[1] ), .Y(n122));
  or02   g089(.A0(n122), .A1(n39), .Y(n123));
  or02  g090(.A0(n123), .A1(\opcode[2] ), .Y(n124));
  or02   g091(.A0(n124), .A1(n121), .Y(\alu_op_ext[0] ));
  or02  g092(.A0(n54), .A1(n63), .Y(n126));
  or02   g093(.A0(n126), .A1(n35), .Y(n127));
  and02  g094(.A0(n34), .A1(\opcode[1] ), .Y(n128));
  or02  g095(.A0(n128), .A1(n127), .Y(n129));
  nor02 g096(.A0(\opcode[2] ), .A1(\opcode[1] ), .Y(n130));
  nand02  g097(.A0(n130), .A1(n46), .Y(n131));
  and02   g098(.A0(n131), .A1(n129), .Y(\alu_op_ext[1] ));
  inv01  g099(.A(n108), .Y(n133));
  and02  g100(.A0(n127), .A1(n133), .Y(n134));
  and02   g101(.A0(n134), .A1(\opcode[2] ), .Y(n135));
  or02  g102(.A0(n135), .A1(n60), .Y(\alu_op_ext[2] ));
  or02   g103(.A0(n112), .A1(n81), .Y(n137));
  and02  g104(.A0(n137), .A1(\opcode[1] ), .Y(n138));
  and02   g105(.A0(n109), .A1(\opcode[2] ), .Y(n139));
  or02   g106(.A0(n139), .A1(n138), .Y(n140));
  and02  g107(.A0(n140), .A1(n79), .Y(\alu_op_ext[3] ));
  nor02  g108(.A0(n58), .A1(\opcode[0] ), .Y(n142));
  or02  g109(.A0(n142), .A1(n35), .Y(n143));
  or02  g110(.A0(n143), .A1(n34), .Y(halt));
  or02  g111(.A0(n58), .A1(\opcode[1] ), .Y(n145));
  and02  g112(.A0(n137), .A1(n35), .Y(n146));
  and02   g113(.A0(n146), .A1(n145), .Y(n147));
  or02  g114(.A0(n147), .A1(n34), .Y(n148));
  and02  g115(.A0(\opcode[4] ), .A1(n35), .Y(n149));
  nand02  g116(.A0(n65), .A1(n35), .Y(n150));
  or02   g117(.A0(n150), .A1(n149), .Y(n151));
  or02  g118(.A0(n151), .A1(\opcode[2] ), .Y(n152));
  or02   g119(.A0(n152), .A1(n148), .Y(reg_write));
  nand02  g120(.A0(n58), .A1(n63), .Y(n154));
  and02  g121(.A0(n154), .A1(\opcode[2] ), .Y(sel_pc_opA));
  or02  g122(.A0(n142), .A1(\opcode[2] ), .Y(sel_pc_opB));
  and02  g123(.A0(n65), .A1(n63), .Y(n157));
  and02  g124(.A0(n157), .A1(n35), .Y(n158));
  or02  g125(.A0(n158), .A1(\opcode[2] ), .Y(beqz));
  or02  g126(.A0(n65), .A1(\opcode[0] ), .Y(n160));
  or02  g127(.A0(n160), .A1(n35), .Y(n161));
  and02  g128(.A0(n161), .A1(\opcode[2] ), .Y(bnez));
  and02  g129(.A0(n160), .A1(\opcode[1] ), .Y(n163));
  and02  g130(.A0(n163), .A1(\opcode[2] ), .Y(bgez));
  and02  g131(.A0(n157), .A1(\opcode[1] ), .Y(n165));
  or02  g132(.A0(n165), .A1(\opcode[2] ), .Y(bltz));
  nor02  g133(.A0(n58), .A1(n34), .Y(jump));
  or02  g134(.A0(n66), .A1(n36), .Y(n168));
  and02  g135(.A0(\opcode[1] ), .A1(\opcode[0] ), .Y(n169));
  and02  g136(.A0(n169), .A1(n90), .Y(n170));
  or02   g137(.A0(n170), .A1(\opcode[2] ), .Y(n171));
  and02   g138(.A0(n171), .A1(n168), .Y(n172));
  and02   g139(.A0(n51), .A1(n35), .Y(n173));
  and02  g140(.A0(n173), .A1(n133), .Y(n174));
  and02   g141(.A0(n174), .A1(n34), .Y(n175));
  and02  g142(.A0(n175), .A1(n172), .Y(Cin));
  and02   g143(.A0(n160), .A1(\opcode[1] ), .Y(n177));
  and02  g144(.A0(n38), .A1(\op_ext[0] ), .Y(n178));
  and02  g145(.A0(n178), .A1(n84), .Y(n179));
  or02  g146(.A0(n179), .A1(\opcode[0] ), .Y(n180));
  and02   g147(.A0(n180), .A1(n35), .Y(n181));
  and02  g148(.A0(n181), .A1(n177), .Y(n182));
  and02  g149(.A0(n182), .A1(n34), .Y(invA));
  or02  g150(.A0(n169), .A1(n92), .Y(n184));
  or02   g151(.A0(n184), .A1(\opcode[2] ), .Y(n185));
  and02  g152(.A0(n185), .A1(n175), .Y(invB));
  and02   g153(.A0(n126), .A1(\opcode[1] ), .Y(n188));
  or02   g154(.A0(n97), .A1(n35), .Y(n189));
  or02  g155(.A0(n189), .A1(n188), .Y(n190));
  or02  g156(.A0(n190), .A1(n34), .Y(mem_write));
  and02  g157(.A0(n97), .A1(n35), .Y(n192));
  or02  g158(.A0(n192), .A1(n34), .Y(sel_wb));
  ONE    g159(.Y(sign));
endmodule


