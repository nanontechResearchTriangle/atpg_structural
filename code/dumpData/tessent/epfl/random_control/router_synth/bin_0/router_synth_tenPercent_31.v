// Benchmark "top" written by ABC on Mon May 20 14:32:30 2019

module top ( 
    \dest_x[0] , \dest_x[1] , \dest_x[2] , \dest_x[3] , \dest_x[4] ,
    \dest_x[5] , \dest_x[6] , \dest_x[7] , \dest_x[8] , \dest_x[9] ,
    \dest_x[10] , \dest_x[11] , \dest_x[12] , \dest_x[13] , \dest_x[14] ,
    \dest_x[15] , \dest_x[16] , \dest_x[17] , \dest_x[18] , \dest_x[19] ,
    \dest_x[20] , \dest_x[21] , \dest_x[22] , \dest_x[23] , \dest_x[24] ,
    \dest_x[25] , \dest_x[26] , \dest_x[27] , \dest_x[28] , \dest_x[29] ,
    \dest_y[0] , \dest_y[1] , \dest_y[2] , \dest_y[3] , \dest_y[4] ,
    \dest_y[5] , \dest_y[6] , \dest_y[7] , \dest_y[8] , \dest_y[9] ,
    \dest_y[10] , \dest_y[11] , \dest_y[12] , \dest_y[13] , \dest_y[14] ,
    \dest_y[15] , \dest_y[16] , \dest_y[17] , \dest_y[18] , \dest_y[19] ,
    \dest_y[20] , \dest_y[21] , \dest_y[22] , \dest_y[23] , \dest_y[24] ,
    \dest_y[25] , \dest_y[26] , \dest_y[27] , \dest_y[28] , \dest_y[29] ,
    \outport[0] , \outport[1] , \outport[2] , \outport[3] , \outport[4] ,
    \outport[5] , \outport[6] , \outport[7] , \outport[8] , \outport[9] ,
    \outport[10] , \outport[11] , \outport[12] , \outport[13] ,
    \outport[14] , \outport[15] , \outport[16] , \outport[17] ,
    \outport[18] , \outport[19] , \outport[20] , \outport[21] ,
    \outport[22] , \outport[23] , \outport[24] , \outport[25] ,
    \outport[26] , \outport[27] , \outport[28] , \outport[29]   );
  input  \dest_x[0] , \dest_x[1] , \dest_x[2] , \dest_x[3] , \dest_x[4] ,
    \dest_x[5] , \dest_x[6] , \dest_x[7] , \dest_x[8] , \dest_x[9] ,
    \dest_x[10] , \dest_x[11] , \dest_x[12] , \dest_x[13] , \dest_x[14] ,
    \dest_x[15] , \dest_x[16] , \dest_x[17] , \dest_x[18] , \dest_x[19] ,
    \dest_x[20] , \dest_x[21] , \dest_x[22] , \dest_x[23] , \dest_x[24] ,
    \dest_x[25] , \dest_x[26] , \dest_x[27] , \dest_x[28] , \dest_x[29] ,
    \dest_y[0] , \dest_y[1] , \dest_y[2] , \dest_y[3] , \dest_y[4] ,
    \dest_y[5] , \dest_y[6] , \dest_y[7] , \dest_y[8] , \dest_y[9] ,
    \dest_y[10] , \dest_y[11] , \dest_y[12] , \dest_y[13] , \dest_y[14] ,
    \dest_y[15] , \dest_y[16] , \dest_y[17] , \dest_y[18] , \dest_y[19] ,
    \dest_y[20] , \dest_y[21] , \dest_y[22] , \dest_y[23] , \dest_y[24] ,
    \dest_y[25] , \dest_y[26] , \dest_y[27] , \dest_y[28] , \dest_y[29] ;
  output \outport[0] , \outport[1] , \outport[2] , \outport[3] , \outport[4] ,
    \outport[5] , \outport[6] , \outport[7] , \outport[8] , \outport[9] ,
    \outport[10] , \outport[11] , \outport[12] , \outport[13] ,
    \outport[14] , \outport[15] , \outport[16] , \outport[17] ,
    \outport[18] , \outport[19] , \outport[20] , \outport[21] ,
    \outport[22] , \outport[23] , \outport[24] , \outport[25] ,
    \outport[26] , \outport[27] , \outport[28] , \outport[29] ;
  wire n91, n92, n93, n94, n95, n96, n97, n98, n99, n100, n101, n102, n103,
    n104, n105, n106, n107, n108, n109, n110, n111, n112, n113, n114, n115,
    n116, n117, n118, n119, n120, n121, n122, n123, n124, n125, n126, n127,
    n128, n129, n130, n131, n132, n133, n134, n135, n136, n137, n138, n139,
    n140, n141, n142, n143, n144, n145, n146, n147, n148, n149, n150, n151,
    n152, n153, n154, n155, n156, n157, n158, n159, n160, n161, n162, n163,
    n164, n165, n166, n167, n168, n169, n170, n171, n172, n173, n174, n175,
    n176, n177, n178, n179, n180, n181, n182, n183, n184, n185, n186, n187,
    n188, n189, n190, n191, n192, n193, n194, n195, n196, n197, n198, n199,
    n200, n201, n203, n204, n205, n206, n207, n208, n209, n210, n211, n212,
    n213, n214, n215, n216, n217, n218, n219, n220, n221, n222, n223, n224,
    n225, n226, n227, n228, n229, n230, n231, n232, n233, n234, n235, n236,
    n237, n238, n239, n240, n241, n242, n243, n244, n245, n246, n247, n248,
    n249, n250, n251, n252, n253, n254, n255, n256, n257, n258, n259, n260,
    n261, n262, n263, n264, n265, n266, n267, n268, n269, n270, n271, n272,
    n273, n274, n275, n276, n277, n278, n279, n280, n281, n282, n283, n284,
    n285, n286, n287, n288, n289, n290, n291, n292, n293, n294, n295, n296,
    n297, n298, n299, n300, n301, n302, n303, n304, n305, n306, n307, n308,
    n309, n310, n311, n312, n314, n315, n316;
  inv01  g000(.A(\dest_x[27] ), .Y(n91));
  inv01  g001(.A(\dest_x[23] ), .Y(n92));
  inv01  g002(.A(\dest_x[22] ), .Y(n93));
  inv01  g003(.A(\dest_x[19] ), .Y(n94));
  inv01  g004(.A(\dest_x[17] ), .Y(n95));
  inv01  g005(.A(\dest_x[14] ), .Y(n96));
  inv01  g006(.A(\dest_x[13] ), .Y(n97));
  inv01  g007(.A(\dest_x[11] ), .Y(n98));
  nor02  g008(.A0(\dest_x[10] ), .A1(\dest_x[9] ), .Y(n99));
  nor02  g009(.A0(n99), .A1(n98), .Y(n100));
  nor02  g010(.A0(n100), .A1(\dest_x[12] ), .Y(n101));
  and02  g011(.A0(n101), .A1(n97), .Y(n102));
  nor02  g012(.A0(n102), .A1(n96), .Y(n103));
  and02  g013(.A0(n103), .A1(\dest_x[15] ), .Y(n104));
  nor02  g014(.A0(n104), .A1(\dest_x[16] ), .Y(n105));
  nor02  g015(.A0(n105), .A1(n95), .Y(n106));
  nor02  g016(.A0(n106), .A1(\dest_x[18] ), .Y(n107));
  nor02  g017(.A0(n107), .A1(n94), .Y(n108));
  and02  g018(.A0(n108), .A1(\dest_x[20] ), .Y(n109));
  nor02  g019(.A0(n109), .A1(\dest_x[21] ), .Y(n110));
  and02  g020(.A0(n110), .A1(n93), .Y(n111));
  nor02  g021(.A0(n111), .A1(n92), .Y(n112));
  and02  g022(.A0(n112), .A1(\dest_x[24] ), .Y(n113));
  and02  g023(.A0(n113), .A1(\dest_x[25] ), .Y(n114));
  nor02  g024(.A0(n114), .A1(\dest_x[26] ), .Y(n115));
  nor02  g025(.A0(n115), .A1(n91), .Y(n116));
  and02  g026(.A0(n116), .A1(\dest_x[28] ), .Y(n117));
  xor2   g027(.A0(n117), .A1(\dest_x[29] ), .Y(n118));
  xnor2  g028(.A0(n116), .A1(\dest_x[28] ), .Y(n119));
  xor2   g029(.A0(n115), .A1(n91), .Y(n120));
  xor2   g030(.A0(n114), .A1(\dest_x[26] ), .Y(n121));
  xnor2  g031(.A0(n113), .A1(\dest_x[25] ), .Y(n122));
  inv01  g032(.A(n122), .Y(n123));
  xnor2  g033(.A0(n112), .A1(\dest_x[24] ), .Y(n124));
  xor2   g034(.A0(n111), .A1(n92), .Y(n125));
  xor2   g035(.A0(n110), .A1(n93), .Y(n126));
  xor2   g036(.A0(n109), .A1(\dest_x[21] ), .Y(n127));
  inv01  g037(.A(n127), .Y(n128));
  xnor2  g038(.A0(n108), .A1(\dest_x[20] ), .Y(n129));
  xor2   g039(.A0(n107), .A1(n94), .Y(n130));
  xor2   g040(.A0(n106), .A1(\dest_x[18] ), .Y(n131));
  xor2   g041(.A0(n105), .A1(n95), .Y(n132));
  xor2   g042(.A0(n104), .A1(\dest_x[16] ), .Y(n133));
  inv01  g043(.A(n133), .Y(n134));
  xnor2  g044(.A0(n103), .A1(\dest_x[15] ), .Y(n135));
  xor2   g045(.A0(n102), .A1(n96), .Y(n136));
  xor2   g046(.A0(n101), .A1(n97), .Y(n137));
  inv01  g047(.A(n137), .Y(n138));
  xor2   g048(.A0(n100), .A1(\dest_x[12] ), .Y(n139));
  xor2   g049(.A0(n99), .A1(n98), .Y(n140));
  or02  g050(.A0(\dest_x[1] ), .A1(\dest_x[0] ), .Y(n141));
  and02  g051(.A0(\dest_x[3] ), .A1(\dest_x[2] ), .Y(n142));
  and02  g052(.A0(\dest_x[5] ), .A1(\dest_x[4] ), .Y(n143));
  and02  g053(.A0(n143), .A1(n142), .Y(n144));
  and02  g054(.A0(n144), .A1(n141), .Y(n145));
  xor2   g055(.A0(\dest_x[10] ), .A1(\dest_x[9] ), .Y(n146));
  and02  g056(.A0(\dest_x[7] ), .A1(\dest_x[6] ), .Y(n147));
  inv01  g057(.A(\dest_x[9] ), .Y(n148));
  and02  g058(.A0(n148), .A1(\dest_x[8] ), .Y(n149));
  nand02 g059(.A0(n149), .A1(n147), .Y(n150));
  nor02  g060(.A0(n150), .A1(n146), .Y(n151));
  and02  g061(.A0(n151), .A1(n145), .Y(n152));
  nand02 g062(.A0(n152), .A1(n140), .Y(n153));
  nor02  g063(.A0(n153), .A1(n139), .Y(n154));
  and02  g064(.A0(n154), .A1(n138), .Y(n155));
  nand02 g065(.A0(n155), .A1(n136), .Y(n156));
  nor02  g066(.A0(n156), .A1(n135), .Y(n157));
  and02  g067(.A0(n157), .A1(n134), .Y(n158));
  nand02 g068(.A0(n158), .A1(n132), .Y(n159));
  nand02  g069(.A0(n159), .A1(n131), .Y(n160));
  nand02 g070(.A0(n160), .A1(n130), .Y(n161));
  nor02  g071(.A0(n161), .A1(n129), .Y(n162));
  nand02 g072(.A0(n162), .A1(n128), .Y(n163));
  nor02  g073(.A0(n163), .A1(n126), .Y(n164));
  nand02 g074(.A0(n164), .A1(n125), .Y(n165));
  nor02  g075(.A0(n165), .A1(n124), .Y(n166));
  nand02 g076(.A0(n166), .A1(n123), .Y(n167));
  nor02  g077(.A0(n167), .A1(n121), .Y(n168));
  nand02 g078(.A0(n168), .A1(n120), .Y(n169));
  nor02  g079(.A0(n169), .A1(n119), .Y(n170));
  nand02 g080(.A0(n170), .A1(n118), .Y(n171));
  nand02 g081(.A0(n117), .A1(\dest_x[29] ), .Y(n172));
  nand02 g082(.A0(n172), .A1(n171), .Y(n173));
  nor02  g083(.A0(\dest_x[2] ), .A1(\dest_x[1] ), .Y(n174));
  nor02  g084(.A0(\dest_x[4] ), .A1(\dest_x[3] ), .Y(n175));
  nor02  g085(.A0(\dest_x[6] ), .A1(\dest_x[5] ), .Y(n176));
  and02  g086(.A0(n176), .A1(n175), .Y(n177));
  and02  g087(.A0(n177), .A1(n174), .Y(n178));
  nand02  g088(.A0(\dest_x[8] ), .A1(\dest_x[7] ), .Y(n179));
  and02  g089(.A0(n179), .A1(\dest_x[9] ), .Y(n180));
  and02  g090(.A0(n180), .A1(n146), .Y(n181));
  nand02 g091(.A0(n181), .A1(n178), .Y(n182));
  nor02  g092(.A0(n182), .A1(n140), .Y(n183));
  and02  g093(.A0(n183), .A1(n139), .Y(n184));
  nand02 g094(.A0(n184), .A1(n137), .Y(n185));
  nand02  g095(.A0(n185), .A1(n136), .Y(n186));
  and02  g096(.A0(n186), .A1(n135), .Y(n187));
  nand02 g097(.A0(n187), .A1(n133), .Y(n188));
  nor02  g098(.A0(n188), .A1(n132), .Y(n189));
  nor02 g099(.A0(n189), .A1(n131), .Y(n190));
  nor02  g100(.A0(n190), .A1(n130), .Y(n191));
  and02  g101(.A0(n191), .A1(n129), .Y(n192));
  and02  g102(.A0(n192), .A1(n127), .Y(n193));
  nand02 g103(.A0(n193), .A1(n126), .Y(n194));
  nor02  g104(.A0(n194), .A1(n125), .Y(n195));
  or02  g105(.A0(n195), .A1(n124), .Y(n196));
  and02  g106(.A0(n196), .A1(n122), .Y(n197));
  nand02 g107(.A0(n197), .A1(n121), .Y(n198));
  nor02  g108(.A0(n198), .A1(n120), .Y(n199));
  and02  g109(.A0(n199), .A1(n119), .Y(n200));
  or02   g110(.A0(n200), .A1(n172), .Y(n201));
  nand02 g111(.A0(n201), .A1(n173), .Y(\outport[0] ));
  inv01  g112(.A(\dest_y[27] ), .Y(n203));
  inv01  g113(.A(\dest_y[23] ), .Y(n204));
  inv01  g114(.A(\dest_y[22] ), .Y(n205));
  inv01  g115(.A(\dest_y[19] ), .Y(n206));
  inv01  g116(.A(\dest_y[17] ), .Y(n207));
  inv01  g117(.A(\dest_y[13] ), .Y(n208));
  or02   g118(.A0(\dest_y[10] ), .A1(\dest_y[9] ), .Y(n209));
  or02  g119(.A0(n209), .A1(\dest_y[11] ), .Y(n210));
  nand02  g120(.A0(n210), .A1(\dest_y[12] ), .Y(n211));
  nand02 g121(.A0(n211), .A1(n208), .Y(n212));
  and02  g122(.A0(n212), .A1(\dest_y[14] ), .Y(n213));
  or02  g123(.A0(n213), .A1(\dest_y[15] ), .Y(n214));
  nor02  g124(.A0(n214), .A1(\dest_y[16] ), .Y(n215));
  nor02  g125(.A0(n215), .A1(n207), .Y(n216));
  nor02  g126(.A0(n216), .A1(\dest_y[18] ), .Y(n217));
  nor02  g127(.A0(n217), .A1(n206), .Y(n218));
  and02  g128(.A0(n218), .A1(\dest_y[20] ), .Y(n219));
  nor02  g129(.A0(n219), .A1(\dest_y[21] ), .Y(n220));
  and02  g130(.A0(n220), .A1(n205), .Y(n221));
  nor02  g131(.A0(n221), .A1(n204), .Y(n222));
  and02  g132(.A0(n222), .A1(\dest_y[24] ), .Y(n223));
  and02  g133(.A0(n223), .A1(\dest_y[25] ), .Y(n224));
  nor02  g134(.A0(n224), .A1(\dest_y[26] ), .Y(n225));
  nand02  g135(.A0(n225), .A1(n203), .Y(n226));
  and02  g136(.A0(\dest_y[29] ), .A1(\dest_y[28] ), .Y(n227));
  nand02 g137(.A0(n227), .A1(n226), .Y(n228));
  xnor2  g138(.A0(n226), .A1(\dest_y[28] ), .Y(n229));
  xor2   g139(.A0(n225), .A1(\dest_y[27] ), .Y(n230));
  xor2   g140(.A0(n224), .A1(\dest_y[26] ), .Y(n231));
  xnor2  g141(.A0(n223), .A1(\dest_y[25] ), .Y(n232));
  xnor2  g142(.A0(n222), .A1(\dest_y[24] ), .Y(n233));
  xor2   g143(.A0(n221), .A1(\dest_y[23] ), .Y(n234));
  xor2   g144(.A0(n220), .A1(n205), .Y(n235));
  xor2   g145(.A0(n219), .A1(\dest_y[21] ), .Y(n236));
  xnor2  g146(.A0(n218), .A1(\dest_y[20] ), .Y(n237));
  xor2   g147(.A0(n217), .A1(\dest_y[19] ), .Y(n238));
  xor2   g148(.A0(n216), .A1(\dest_y[18] ), .Y(n239));
  xor2   g149(.A0(n215), .A1(\dest_y[17] ), .Y(n240));
  xor2   g150(.A0(n214), .A1(\dest_y[16] ), .Y(n241));
  xnor2  g151(.A0(n213), .A1(\dest_y[15] ), .Y(n242));
  xnor2  g152(.A0(n212), .A1(\dest_y[14] ), .Y(n243));
  xor2   g153(.A0(n211), .A1(n208), .Y(n244));
  xor2   g154(.A0(n210), .A1(\dest_y[12] ), .Y(n245));
  xnor2  g155(.A0(n209), .A1(\dest_y[11] ), .Y(n246));
  nor02  g156(.A0(\dest_y[2] ), .A1(\dest_y[1] ), .Y(n247));
  nand02  g157(.A0(\dest_y[4] ), .A1(\dest_y[3] ), .Y(n248));
  nor02  g158(.A0(\dest_y[6] ), .A1(\dest_y[5] ), .Y(n249));
  and02  g159(.A0(n249), .A1(n248), .Y(n250));
  and02  g160(.A0(n250), .A1(n247), .Y(n251));
  xor2   g161(.A0(\dest_y[10] ), .A1(\dest_y[9] ), .Y(n252));
  nor02  g162(.A0(\dest_y[8] ), .A1(\dest_y[7] ), .Y(n253));
  and02  g163(.A0(n253), .A1(\dest_y[9] ), .Y(n254));
  and02  g164(.A0(n254), .A1(n252), .Y(n255));
  and02  g165(.A0(n255), .A1(n251), .Y(n256));
  and02  g166(.A0(n256), .A1(n246), .Y(n257));
  and02  g167(.A0(n257), .A1(n245), .Y(n258));
  or02  g168(.A0(n258), .A1(n244), .Y(n259));
  and02  g169(.A0(n259), .A1(n243), .Y(n260));
  and02  g170(.A0(n260), .A1(n242), .Y(n261));
  and02  g171(.A0(n261), .A1(n241), .Y(n262));
  and02  g172(.A0(n262), .A1(n240), .Y(n263));
  and02  g173(.A0(n263), .A1(n239), .Y(n264));
  and02  g174(.A0(n264), .A1(n238), .Y(n265));
  and02  g175(.A0(n265), .A1(n237), .Y(n266));
  and02  g176(.A0(n266), .A1(n236), .Y(n267));
  and02  g177(.A0(n267), .A1(n235), .Y(n268));
  and02  g178(.A0(n268), .A1(n234), .Y(n269));
  and02  g179(.A0(n269), .A1(n233), .Y(n270));
  and02  g180(.A0(n270), .A1(n232), .Y(n271));
  and02  g181(.A0(n271), .A1(n231), .Y(n272));
  and02  g182(.A0(n272), .A1(n230), .Y(n273));
  and02  g183(.A0(n273), .A1(n229), .Y(n274));
  nor02  g184(.A0(n274), .A1(n228), .Y(n275));
  nor02  g185(.A0(\dest_y[0] ), .A1(\dest_x[0] ), .Y(n276));
  or02   g186(.A0(n276), .A1(n228), .Y(n277));
  nand02 g187(.A0(\dest_y[5] ), .A1(\dest_y[4] ), .Y(n278));
  nand02 g188(.A0(\dest_y[7] ), .A1(\dest_y[6] ), .Y(n279));
  or02   g189(.A0(n279), .A1(n278), .Y(n280));
  nand02 g190(.A0(\dest_y[1] ), .A1(\dest_y[0] ), .Y(n281));
  nand02 g191(.A0(\dest_y[3] ), .A1(\dest_y[2] ), .Y(n282));
  or02   g192(.A0(n282), .A1(n281), .Y(n283));
  or02   g193(.A0(n283), .A1(n280), .Y(n284));
  inv01  g194(.A(\dest_y[8] ), .Y(n285));
  nor02  g195(.A0(\dest_y[9] ), .A1(n285), .Y(n286));
  nand02 g196(.A0(n286), .A1(\dest_y[29] ), .Y(n287));
  or02   g197(.A0(n287), .A1(n252), .Y(n288));
  or02   g198(.A0(n288), .A1(n284), .Y(n289));
  or02   g199(.A0(n289), .A1(n246), .Y(n290));
  or02   g200(.A0(n290), .A1(n245), .Y(n291));
  or02   g201(.A0(n291), .A1(n244), .Y(n292));
  and02   g202(.A0(n292), .A1(n243), .Y(n293));
  or02   g203(.A0(n293), .A1(n242), .Y(n294));
  or02   g204(.A0(n294), .A1(n241), .Y(n295));
  or02   g205(.A0(n295), .A1(n240), .Y(n296));
  and02   g206(.A0(n296), .A1(n239), .Y(n297));
  or02   g207(.A0(n297), .A1(n238), .Y(n298));
  or02   g208(.A0(n298), .A1(n237), .Y(n299));
  or02   g209(.A0(n299), .A1(n236), .Y(n300));
  or02   g210(.A0(n300), .A1(n235), .Y(n301));
  or02   g211(.A0(n301), .A1(n234), .Y(n302));
  or02   g212(.A0(n302), .A1(n233), .Y(n303));
  or02   g213(.A0(n303), .A1(n232), .Y(n304));
  and02   g214(.A0(n304), .A1(n231), .Y(n305));
  or02   g215(.A0(n305), .A1(n230), .Y(n306));
  or02   g216(.A0(n306), .A1(n229), .Y(n307));
  nand02 g217(.A0(n307), .A1(n277), .Y(n308));
  nor02  g218(.A0(n308), .A1(n275), .Y(n309));
  nand02 g219(.A0(n228), .A1(\dest_x[0] ), .Y(n310));
  nand02 g220(.A0(n310), .A1(n173), .Y(n311));
  and02   g221(.A0(n311), .A1(n309), .Y(n312));
  and02  g222(.A0(n312), .A1(n201), .Y(\outport[1] ));
  nand02 g223(.A0(\dest_y[0] ), .A1(\dest_x[0] ), .Y(n314));
  nor02  g224(.A0(n314), .A1(n228), .Y(n315));
  nor02  g225(.A0(n315), .A1(n275), .Y(n316));
  nor02  g226(.A0(n316), .A1(\outport[0] ), .Y(\outport[2] ));
  GND    g227(.Y(\outport[3] ));
  GND    g228(.Y(\outport[4] ));
  GND    g229(.Y(\outport[5] ));
  GND    g230(.Y(\outport[6] ));
  GND    g231(.Y(\outport[7] ));
  GND    g232(.Y(\outport[8] ));
  GND    g233(.Y(\outport[9] ));
  GND    g234(.Y(\outport[10] ));
  GND    g235(.Y(\outport[11] ));
  GND    g236(.Y(\outport[12] ));
  GND    g237(.Y(\outport[13] ));
  GND    g238(.Y(\outport[14] ));
  GND    g239(.Y(\outport[15] ));
  GND    g240(.Y(\outport[16] ));
  GND    g241(.Y(\outport[17] ));
  GND    g242(.Y(\outport[18] ));
  GND    g243(.Y(\outport[19] ));
  GND    g244(.Y(\outport[20] ));
  GND    g245(.Y(\outport[21] ));
  GND    g246(.Y(\outport[22] ));
  GND    g247(.Y(\outport[23] ));
  GND    g248(.Y(\outport[24] ));
  GND    g249(.Y(\outport[25] ));
  GND    g250(.Y(\outport[26] ));
  GND    g251(.Y(\outport[27] ));
  GND    g252(.Y(\outport[28] ));
  GND    g253(.Y(\outport[29] ));
endmodule


