// Benchmark "c499.blif" written by ABC on Sat May 11 15:23:14 2019

module c499  ( 
    Gid0, Gid1, Gid2, Gid3, Gid4, Gid5, Gid6, Gid7, Gid8, Gid9, Gid10,
    Gid11, Gid12, Gid13, Gid14, Gid15, Gid16, Gid17, Gid18, Gid19, Gid20,
    Gid21, Gid22, Gid23, Gid24, Gid25, Gid26, Gid27, Gid28, Gid29, Gid30,
    Gid31, Gic0, Gic1, Gic2, Gic3, Gic4, Gic5, Gic6, Gic7, Gr,
    God0, God1, God2, God3, God4, God5, God6, God7, God8, God9, God10,
    God11, God12, God13, God14, God15, God16, God17, God18, God19, God20,
    God21, God22, God23, God24, God25, God26, God27, God28, God29, God30,
    God31  );
  input  Gid0, Gid1, Gid2, Gid3, Gid4, Gid5, Gid6, Gid7, Gid8, Gid9,
    Gid10, Gid11, Gid12, Gid13, Gid14, Gid15, Gid16, Gid17, Gid18, Gid19,
    Gid20, Gid21, Gid22, Gid23, Gid24, Gid25, Gid26, Gid27, Gid28, Gid29,
    Gid30, Gid31, Gic0, Gic1, Gic2, Gic3, Gic4, Gic5, Gic6, Gic7, Gr;
  output God0, God1, God2, God3, God4, God5, God6, God7, God8, God9, God10,
    God11, God12, God13, God14, God15, God16, God17, God18, God19, God20,
    God21, God22, God23, God24, God25, God26, God27, God28, God29, God30,
    God31;
  wire n74, n75, n76, n77, n78, n79, n80, n81, n82, n83, n84, n85, n86, n87,
    n88, n89, n90, n91, n92, n93, n94, n95, n96, n97, n98, n99, n100, n101,
    n102, n103, n104, n105, n106, n107, n108, n109, n110, n111, n112, n113,
    n114, n115, n116, n117, n118, n119, n120, n121, n122, n123, n124, n125,
    n126, n127, n128, n129, n130, n131, n132, n133, n134, n135, n136, n137,
    n138, n139, n140, n141, n142, n143, n144, n145, n146, n147, n148, n149,
    n150, n151, n152, n153, n154, n155, n156, n157, n158, n159, n160, n161,
    n162, n163, n164, n165, n166, n167, n168, n169, n170, n171, n172, n173,
    n174, n175, n177, n179, n181, n183, n184, n185, n186, n187, n188, n190,
    n192, n194, n196, n197, n198, n199, n200, n202, n204, n206, n208, n209,
    n211, n213, n215, n217, n218, n219, n220, n221, n222, n223, n224, n225,
    n226, n227, n228, n230, n232, n234, n236, n237, n238, n240, n242, n244,
    n246, n247, n249, n251, n253, n255, n256, n258, n260, n262;
  xor2   g000(.A0(Gid4), .A1(Gid0), .Y(n74));
  xnor2  g001(.A0(Gid12), .A1(Gid8), .Y(n75));
  xnor2  g002(.A0(n75), .A1(n74), .Y(n76));
  or02  g003(.A0(Gr), .A1(Gic0), .Y(n77));
  xnor2  g004(.A0(Gid17), .A1(Gid16), .Y(n78));
  xnor2  g005(.A0(Gid19), .A1(Gid18), .Y(n79));
  xor2   g006(.A0(n79), .A1(n78), .Y(n80));
  xor2   g007(.A0(Gid21), .A1(Gid20), .Y(n81));
  xnor2  g008(.A0(Gid23), .A1(Gid22), .Y(n82));
  xnor2  g009(.A0(n82), .A1(n81), .Y(n83));
  xnor2  g010(.A0(n83), .A1(n80), .Y(n84));
  xor2   g011(.A0(n84), .A1(n77), .Y(n85));
  xnor2  g012(.A0(n85), .A1(n76), .Y(n86));
  xor2   g013(.A0(n85), .A1(n76), .Y(n87));
  xor2   g014(.A0(Gid5), .A1(Gid1), .Y(n88));
  xnor2  g015(.A0(Gid13), .A1(Gid9), .Y(n89));
  xnor2  g016(.A0(n89), .A1(n88), .Y(n90));
  or02  g017(.A0(Gr), .A1(Gic1), .Y(n91));
  xor2   g018(.A0(Gid29), .A1(Gid28), .Y(n92));
  xnor2  g019(.A0(Gid31), .A1(Gid30), .Y(n93));
  xor2   g020(.A0(n93), .A1(n92), .Y(n94));
  xor2   g021(.A0(Gid25), .A1(Gid24), .Y(n95));
  xnor2  g022(.A0(Gid27), .A1(Gid26), .Y(n96));
  xor2   g023(.A0(n96), .A1(n95), .Y(n97));
  xnor2  g024(.A0(n97), .A1(n94), .Y(n98));
  xor2   g025(.A0(n98), .A1(n91), .Y(n99));
  xor2   g026(.A0(n99), .A1(n90), .Y(n100));
  or02  g027(.A0(n100), .A1(n87), .Y(n101));
  xor2   g028(.A0(Gid7), .A1(Gid3), .Y(n102));
  xnor2  g029(.A0(Gid15), .A1(Gid11), .Y(n103));
  xnor2  g030(.A0(n103), .A1(n102), .Y(n104));
  or02  g031(.A0(Gr), .A1(Gic3), .Y(n105));
  xor2   g032(.A0(n94), .A1(n83), .Y(n106));
  xor2   g033(.A0(n106), .A1(n105), .Y(n107));
  xor2   g034(.A0(n107), .A1(n104), .Y(n108));
  xor2   g035(.A0(Gid6), .A1(Gid2), .Y(n109));
  xnor2  g036(.A0(Gid14), .A1(Gid10), .Y(n110));
  xnor2  g037(.A0(n110), .A1(n109), .Y(n111));
  or02  g038(.A0(Gr), .A1(Gic2), .Y(n112));
  xor2   g039(.A0(n97), .A1(n80), .Y(n113));
  xor2   g040(.A0(n113), .A1(n112), .Y(n114));
  xnor2  g041(.A0(n114), .A1(n111), .Y(n115));
  and02  g042(.A0(n115), .A1(n108), .Y(n116));
  or02  g043(.A0(n116), .A1(n101), .Y(n117));
  xnor2  g044(.A0(n107), .A1(n104), .Y(n118));
  xor2   g045(.A0(n114), .A1(n111), .Y(n119));
  or02  g046(.A0(n119), .A1(n118), .Y(n120));
  or02  g047(.A0(n120), .A1(n101), .Y(n121));
  and02   g048(.A0(n121), .A1(n117), .Y(n122));
  or02  g049(.A0(n119), .A1(n108), .Y(n123));
  xnor2  g050(.A0(n99), .A1(n90), .Y(n124));
  or02  g051(.A0(n124), .A1(n87), .Y(n125));
  or02  g052(.A0(n125), .A1(n123), .Y(n126));
  or02  g053(.A0(n100), .A1(n86), .Y(n127));
  or02  g054(.A0(n127), .A1(n123), .Y(n128));
  and02   g055(.A0(n128), .A1(n126), .Y(n129));
  and02   g056(.A0(n129), .A1(n122), .Y(n130));
  xor2   g057(.A0(Gid22), .A1(Gid18), .Y(n131));
  xnor2  g058(.A0(Gid30), .A1(Gid26), .Y(n132));
  xnor2  g059(.A0(n132), .A1(n131), .Y(n133));
  or02  g060(.A0(Gr), .A1(Gic6), .Y(n134));
  xnor2  g061(.A0(Gid9), .A1(Gid8), .Y(n135));
  xnor2  g062(.A0(Gid11), .A1(Gid10), .Y(n136));
  xor2   g063(.A0(n136), .A1(n135), .Y(n137));
  xor2   g064(.A0(Gid1), .A1(Gid0), .Y(n138));
  xnor2  g065(.A0(Gid3), .A1(Gid2), .Y(n139));
  xnor2  g066(.A0(n139), .A1(n138), .Y(n140));
  xnor2  g067(.A0(n140), .A1(n137), .Y(n141));
  xor2   g068(.A0(n141), .A1(n134), .Y(n142));
  xnor2  g069(.A0(n142), .A1(n133), .Y(n143));
  xor2   g070(.A0(Gid23), .A1(Gid19), .Y(n144));
  xnor2  g071(.A0(Gid31), .A1(Gid27), .Y(n145));
  xnor2  g072(.A0(n145), .A1(n144), .Y(n146));
  or02  g073(.A0(Gr), .A1(Gic7), .Y(n147));
  xor2   g074(.A0(Gid13), .A1(Gid12), .Y(n148));
  xnor2  g075(.A0(Gid15), .A1(Gid14), .Y(n149));
  xor2   g076(.A0(n149), .A1(n148), .Y(n150));
  xor2   g077(.A0(Gid5), .A1(Gid4), .Y(n151));
  xnor2  g078(.A0(Gid7), .A1(Gid6), .Y(n152));
  xor2   g079(.A0(n152), .A1(n151), .Y(n153));
  xnor2  g080(.A0(n153), .A1(n150), .Y(n154));
  xor2   g081(.A0(n154), .A1(n147), .Y(n155));
  xor2   g082(.A0(n155), .A1(n146), .Y(n156));
  or02  g083(.A0(n156), .A1(n143), .Y(n157));
  or02  g084(.A0(n157), .A1(n130), .Y(n158));
  xor2   g085(.A0(Gid21), .A1(Gid17), .Y(n159));
  xnor2  g086(.A0(Gid29), .A1(Gid25), .Y(n160));
  xnor2  g087(.A0(n160), .A1(n159), .Y(n161));
  or02  g088(.A0(Gr), .A1(Gic5), .Y(n162));
  xor2   g089(.A0(n150), .A1(n137), .Y(n163));
  xor2   g090(.A0(n163), .A1(n162), .Y(n164));
  xor2   g091(.A0(n164), .A1(n161), .Y(n165));
  xor2   g092(.A0(Gid20), .A1(Gid16), .Y(n166));
  xnor2  g093(.A0(Gid28), .A1(Gid24), .Y(n167));
  xnor2  g094(.A0(n167), .A1(n166), .Y(n168));
  or02  g095(.A0(Gr), .A1(Gic4), .Y(n169));
  xor2   g096(.A0(n153), .A1(n140), .Y(n170));
  xor2   g097(.A0(n170), .A1(n169), .Y(n171));
  xnor2  g098(.A0(n171), .A1(n168), .Y(n172));
  or02  g099(.A0(n172), .A1(n165), .Y(n173));
  and02  g100(.A0(n173), .A1(n158), .Y(n174));
  or02  g101(.A0(n174), .A1(n86), .Y(n175));
  xor2   g102(.A0(n175), .A1(Gid0), .Y(God0));
  or02  g103(.A0(n174), .A1(n124), .Y(n177));
  xor2   g104(.A0(n177), .A1(Gid1), .Y(God1));
  or02  g105(.A0(n174), .A1(n115), .Y(n179));
  xor2   g106(.A0(n179), .A1(Gid2), .Y(God2));
  or02  g107(.A0(n174), .A1(n118), .Y(n181));
  xor2   g108(.A0(n181), .A1(Gid3), .Y(God3));
  xor2   g109(.A0(n142), .A1(n133), .Y(n183));
  xnor2  g110(.A0(n155), .A1(n146), .Y(n184));
  and02  g111(.A0(n184), .A1(n183), .Y(n185));
  or02  g112(.A0(n185), .A1(n130), .Y(n186));
  or02  g113(.A0(n186), .A1(n173), .Y(n187));
  or02  g114(.A0(n187), .A1(n86), .Y(n188));
  xor2   g115(.A0(n188), .A1(Gid4), .Y(God4));
  or02  g116(.A0(n187), .A1(n124), .Y(n190));
  xor2   g117(.A0(n190), .A1(Gid5), .Y(God5));
  or02  g118(.A0(n187), .A1(n115), .Y(n192));
  xor2   g119(.A0(n192), .A1(Gid6), .Y(God6));
  or02  g120(.A0(n187), .A1(n118), .Y(n194));
  xor2   g121(.A0(n194), .A1(Gid7), .Y(God7));
  xnor2  g122(.A0(n164), .A1(n161), .Y(n196));
  xor2   g123(.A0(n171), .A1(n168), .Y(n197));
  or02  g124(.A0(n197), .A1(n196), .Y(n198));
  or02  g125(.A0(n198), .A1(n158), .Y(n199));
  or02  g126(.A0(n199), .A1(n86), .Y(n200));
  xor2   g127(.A0(n200), .A1(Gid8), .Y(God8));
  and02  g128(.A0(n199), .A1(n124), .Y(n202));
  xor2   g129(.A0(n202), .A1(Gid9), .Y(God9));
  or02  g130(.A0(n199), .A1(n115), .Y(n204));
  xor2   g131(.A0(n204), .A1(Gid10), .Y(God10));
  or02  g132(.A0(n199), .A1(n118), .Y(n206));
  xor2   g133(.A0(n206), .A1(Gid11), .Y(God11));
  or02  g134(.A0(n198), .A1(n186), .Y(n208));
  or02  g135(.A0(n208), .A1(n86), .Y(n209));
  xor2   g136(.A0(n209), .A1(Gid12), .Y(God12));
  or02  g137(.A0(n208), .A1(n124), .Y(n211));
  xor2   g138(.A0(n211), .A1(Gid13), .Y(God13));
  and02  g139(.A0(n208), .A1(n115), .Y(n213));
  xor2   g140(.A0(n213), .A1(Gid14), .Y(God14));
  or02  g141(.A0(n208), .A1(n118), .Y(n215));
  xor2   g142(.A0(n215), .A1(Gid15), .Y(God15));
  or02  g143(.A0(n197), .A1(n165), .Y(n217));
  and02  g144(.A0(n217), .A1(n157), .Y(n218));
  or02  g145(.A0(n217), .A1(n185), .Y(n219));
  and02   g146(.A0(n219), .A1(n218), .Y(n220));
  or02  g147(.A0(n156), .A1(n183), .Y(n221));
  or02  g148(.A0(n221), .A1(n198), .Y(n222));
  or02  g149(.A0(n221), .A1(n173), .Y(n223));
  and02   g150(.A0(n223), .A1(n222), .Y(n224));
  and02   g151(.A0(n224), .A1(n220), .Y(n225));
  or02  g152(.A0(n225), .A1(n116), .Y(n226));
  or02  g153(.A0(n226), .A1(n127), .Y(n227));
  and02  g154(.A0(n227), .A1(n172), .Y(n228));
  xor2   g155(.A0(n228), .A1(Gid16), .Y(God16));
  or02  g156(.A0(n227), .A1(n196), .Y(n230));
  xor2   g157(.A0(n230), .A1(Gid17), .Y(God17));
  or02  g158(.A0(n227), .A1(n143), .Y(n232));
  xor2   g159(.A0(n232), .A1(Gid18), .Y(God18));
  or02  g160(.A0(n227), .A1(n184), .Y(n234));
  xor2   g161(.A0(n234), .A1(Gid19), .Y(God19));
  or02  g162(.A0(n225), .A1(n120), .Y(n236));
  or02  g163(.A0(n236), .A1(n127), .Y(n237));
  or02  g164(.A0(n237), .A1(n172), .Y(n238));
  xor2   g165(.A0(n238), .A1(Gid20), .Y(God20));
  or02  g166(.A0(n237), .A1(n196), .Y(n240));
  xor2   g167(.A0(n240), .A1(Gid21), .Y(God21));
  and02  g168(.A0(n237), .A1(n143), .Y(n242));
  xor2   g169(.A0(n242), .A1(Gid22), .Y(God22));
  or02  g170(.A0(n237), .A1(n184), .Y(n244));
  xor2   g171(.A0(n244), .A1(Gid23), .Y(God23));
  or02  g172(.A0(n226), .A1(n125), .Y(n246));
  or02  g173(.A0(n246), .A1(n172), .Y(n247));
  xor2   g174(.A0(n247), .A1(Gid24), .Y(God24));
  or02  g175(.A0(n246), .A1(n196), .Y(n249));
  xor2   g176(.A0(n249), .A1(Gid25), .Y(God25));
  or02  g177(.A0(n246), .A1(n143), .Y(n251));
  xor2   g178(.A0(n251), .A1(Gid26), .Y(God26));
  or02  g179(.A0(n246), .A1(n184), .Y(n253));
  xor2   g180(.A0(n253), .A1(Gid27), .Y(God27));
  or02  g181(.A0(n236), .A1(n125), .Y(n255));
  or02  g182(.A0(n255), .A1(n172), .Y(n256));
  xor2   g183(.A0(n256), .A1(Gid28), .Y(God28));
  or02  g184(.A0(n255), .A1(n196), .Y(n258));
  xor2   g185(.A0(n258), .A1(Gid29), .Y(God29));
  or02  g186(.A0(n255), .A1(n143), .Y(n260));
  xor2   g187(.A0(n260), .A1(Gid30), .Y(God30));
  or02  g188(.A0(n255), .A1(n184), .Y(n262));
  xor2   g189(.A0(n262), .A1(Gid31), .Y(God31));
endmodule


